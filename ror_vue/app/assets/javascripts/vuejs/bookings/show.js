if ($('#addRequestComment').length > 0) {
  Vue.http.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content')

  Vue.filter('filtername', function (value) {
    return $('nav.navbar').data('user-name') === value ? 'You' : value
  })

  var showRequest = new Vue({
    el: '#addRequestComment',
    data: {
      request: {
        id: $('#addRequestComment').data('request-id'),
        comments: []
      },
      comment: ''
    },

    ready: function () {
      subscribeClient('request_' + this.request.id)
      this.fetchComments()
      $('.commentsList').removeAttr('hidden')
    },

    methods: {
      resetForm: function () {
        this.comment = ''
      },
      fetchComments: function () {
        this.$http.get('/api/bookings/' + this.request.id + '/comments', function (data) {
          this.$set('request.comments', data.comments)
        })
      },
      updateBookingStatus: function (venue_id, id, status, e) {
        e.preventDefault()
        this.$http.put('/api/bookings/' + id + '/change_status', {
          id: venue_id,
          booking: {
            status: status
          }
        }, function (data, status, request) {
          if (status === 200) {
            text = '<h4>Booking was ' + data.booking.status + '. The guest will be notified of this decision.</h4>'
            $('.respond_to_booking').html(text)
          }
        }).error(function (data, status, request) {
          if (status === 400) {
            vueErrors.$set('errors', data.users)
            $('#modalErrors').modal()
          } else {
            vueErrors.$set('errors', ['Server error 500.'])
            $('#modalErrors').modal()
          }
        })
      },
      submitComment: function (requestId, e) {
        $('form.new_comment a.addComment').attr('disabled', true)
        e.preventDefault()
        var resource = this.$resource('/api/comments')

        resource.save({
          request_id: requestId,
          comment: {
            comment: this.comment
          }
        }, function (data, status, request) {
          if (status === 200) {
            $('img.loading').show()
            // showRequest.request.comments.push(data.comment),
            this.resetForm()
          }
        })
          .error(function (data, status, request) {
            $('form.new_comment a.addComment').attr('disabled', false)
            if (status === 400) {
              vueErrors.$set('errors', data.comments)
              $('#modalErrors').modal()
            } else {
              vueErrors.$set('errors', ['Server error 500.'])
              $('#modalErrors').modal()
            }
          })
      },
    }
  })
}
