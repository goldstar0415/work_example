if ($('#bookings_list').length > 0) {
  Vue.http.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content')

  Vue.filter('filterStatus', function (value) {
    return value.replace('_', ' ')
  })

  var requestsList = new Vue({
    data: {
      filter: '',
      bookings: [],
    },

    ready: function () {
      this.fetchRequests()
      $('#requests_container').removeAttr('hidden')
    },

    methods: {
      filterRequests: function(filter, e) {
        e.preventDefault()
        this.filter = filter
      },
      fetchRequests: function () {
        this.$http.get('/api/bookings', function (data) {
          this.$set('bookings', data.bookings)
        })
      },
      deleteRequest: function (id, type, e) {
        e.preventDefault()
        $('.alert').remove()
        this.$http.put('/api/bookings/' + id + '/hide_booking', {
          booking: {
            request_type: type
          }
        }, function (data, status, request) {
          if (status === 200) {
            renderNotification('Request successfully removed.')
            $('div#requestItem_' + data.id).fadeOut('fast', function () {
              $('div#requestItem_' + data.id).remove()
            })
          }
        }).error(function (data, status, request) {
          if (status === 400) {
            vueErrors.$set('errors', data.users)
            $('#modalErrors').modal()
          } else {
            vueErrors.$set('errors', ['Server error 500.'])
            $('#modalErrors').modal()
          }
        })
      },
    }
  }).$mount('#bookings_list')
}
