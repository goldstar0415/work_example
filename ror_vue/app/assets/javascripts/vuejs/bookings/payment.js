if ($('#bookingPayment').length > 0) {
  Vue.http.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content')
  loadingAnimation('show')

  bookingPayment = new Vue({
    el: '#bookingPayment',
    data: {
      booking_id: $('#bookingPayment').data('booking-id'),
      cardType: null,
      stripeToken: null,
      states: null,
      save_card: false,
      cards: [],
      selected_card: null,
      user_signed_in: $('#bookingPayment').data('user-signed-in')
    },

    ready: function () {
      this.getCards()
      this.fetchStates()
      this.initStripe()
      $('.cs-select').chosen({
        inherit_select_classes: true,
        width: '100%',
      })
      loadingAnimation('hide')
    },

    computed: {
      cardIcon: function () {
        return this.cardClass(this.cardType)
      }
    },

    watch: {
      'stripeToken': function (val, oldVal) {
        if (val === null) {
          return
        }
        this.submitCard()
      }
    },

    methods: {
      paymentToggle: function (e, action) {
        e.preventDefault()
        if (action === "show") {
          $('#creditCard').removeClass('hidden')
          this.selected_card = null
        } else {
          $('#creditCard').addClass('hidden')
          window.scrollTo(0,0);
        }
      },
      cardClass: function (brand) {
        // "fa-cc-unionpay"
        if (brand === 'dinersclub') {
          return 'fa-cc-diners-club'
        } else if (brand === 'american express') {
          return 'fa-cc-amex'
        } else if (brand === null) {
          return 'fa-credit-card-alt'
        } else {
          return ("fa-cc-" + brand)
        }
      },
      setSelectedCard: function(e, card) {
        this.selected_card = card
      },
      initStripe: function () {
        jQuery(function($) {
          $('#creditCard').submit(function(event) {
            var $form = $(this);

            // Disable the submit button to prevent repeated clicks
            $form.find('button').prop('disabled', true)
            // Stripe.bankAccount.createToken($form, stripeResponseHandler);
            Stripe.card.createToken({
              name: $('#cc-name').val(),
              address_country: $('#cc-country').val(),
              address_state: $('#cc-state').val(),
              address_city: $('#cc-city').val(),
              address_line1: $('#cc-address-line-1').val(),
              address_zip: $('#cc-address-zip').val(),
              number: $('.cc-number').val(),
              cvc: $('.cc-cvc').val(),
              exp_month: $('.cc-exp').val().split(" / ")[0],
              exp_year: $('.cc-exp').val().split(" / ")[1]
            }, stripePaymentResponseHandler);

            // Prevent the form from submitting with the default action
            return false
          })
        })
      },
      getCards: function () {
        if (this.user_signed_in) {
          this.$http.get('/api/users/cards', function(data) {
            bookingPayment.$set('cards', data.cards)
          }).then(function() {
            if (bookingPayment.cards.length > 0) {
              $('.available_cards').removeClass('hidden')
            } else {
              $('#creditCard').removeClass('hidden')
            }
          })
        }
      },
      fetchStates: function () {
        this.$http.get('/api/locations/all_states').then(function(response) {
          states = $.parseJSON(response.responseText).states
          bookingPayment.$set('states', states)
        }).then(function() {
          $('.cs-select').trigger('chosen:updated')
        })
      },
      submitCard: function () {
        $.LoadingOverlay("show", {
          image: "/img/squares.gif",
          custom : "<h3 style='margin-top:200px'>Processing payment, please wait...</h3>"
        });
        this.$http.put('/api/bookings/' + this.booking_id + '/save_card', {
          booking: {
            token: this.stripeToken,
            save_card: this.save_card,
            card_id: this.selected_card
          }
        }, function (data, status, request) {
          if (status === 200) {
            $.LoadingOverlay("hide");
            renderNotification('Payment method has been saved successfully.')
            setTimeout(function () {
              window.location.replace(data.booking.url)
            }, 1000)
          }
        }).error(function (data, status, request) {
          if (status === 400) {
            vueErrors.$set('errors', data.errors)
            $('#modalErrors').modal()
          } else {
            vueErrors.$set('errors', ['Server error 500.'])
            $('#modalErrors').modal()
          }
        })
        $('#creditCard').find('button').prop('disabled', false)
      }
    }
  })

  Stripe.setPublishableKey($("meta[name=stripe_publish_key]").attr('content'));
  function stripePaymentResponseHandler(status, response) {
    $.LoadingOverlay("show", {
      image: "/img/squares.gif",
      custom : "<h3 style='margin-top:200px'>Processing card, please wait...</h3>"
    });

    // Grab the form:
    var $form = $('#creditCard');

    if (response.error) { // Problem!
      $.LoadingOverlay("hide");
      vueErrors.$set('errors', [response.error.message])
      $('#modalErrors').modal()
      // Show the errors on the form:
      // $form.find('.bank-errors').text(response.error.message);
      $form.find('button').prop('disabled', false); // Re-enable submission

    } else { // Token created!

      // Get the token ID:
      var token = response.id;
      bookingPayment.stripeToken = token

      // Insert the token into the form so it gets submitted to the server:
      // $form.append($('<input type="hidden" name="stripeToken" />').val(token));

      // Submit the form:
      // $form.get(0).submit();

    }
    $.LoadingOverlay("hide");
  }

  jQuery(document).ready(function($) {
    $('[data-numeric]').payment('restrictNumeric');
    $('.cc-number').payment('formatCardNumber');
    $('.cc-exp').payment('formatCardExpiry');
    $('.cc-cvc').payment('formatCardCVC');

    $.fn.toggleInputError = function(erred) {
      this.parent('.form-group').toggleClass('has-error', erred);
      return this;
    };

    $('#cc-number').keyup(function(event) {
      bookingPayment.cardType = $.payment.cardType($('.cc-number').val())
    });

    // $('form').submit(function(e) {
    //   e.preventDefault();
    //   var cardType = $.payment.cardType($('.cc-number').val());
    //   $('.cc-number').toggleInputError(!$.payment.validateCardNumber($('.cc-number').val()));
    //   $('.cc-exp').toggleInputError(!$.payment.validateCardExpiry($('.cc-exp').payment('cardExpiryVal')));
    //   $('.cc-cvc').toggleInputError(!$.payment.validateCardCVC($('.cc-cvc').val(), cardType));
    //   // $('.cc-brand').text(cardType);

    //   $('.validation').removeClass('text-danger text-success');
    //   $('.validation').addClass($('.has-error').length ? 'text-danger' : 'text-success');
    // });
  });
}
