Vue.http.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content')

if ($('#notificationContainer').length > 0) {
  var notifications = new Vue({
    el: '.navbar',
    data: {
      counter: 0,
      showNotifications: false,
      notifications: []
    },

    ready: function () {
      this.fetchNotifcations()
    },

    methods: {
      fetchNotifcations: function() {
        this.$http.get('/api/notifications', function (data) {
          this.$set('notifications', data.notifications)
          this.$set('counter', data.meta.counter)
        })
      },
      markAllAsRead: function(e) {
        e.preventDefault()
        myarr = []
        $.each(this.notifications, function(key, object) {
          myarr.push(object.id)
        })

        this.$http.put('/api/notifications/mark_all_as_read', {
          notification: {
            ids: myarr
          }
        }, function (data, status, request) {
          $('.new_notification').removeClass('new_notification')
          $('a.mark_as_read').remove()
          notifications.counter = 0
        }).error(function (data, status, request) {
          if (status === 400) {
            // vueErrors.$set('errors', data.venues)
            $('#modalErrors').modal()
          } else {
            vueErrors.$set('errors', ['Server error 500.'])
            $('#modalErrors').modal()
          }
        })
      },
      markAsRead: function(id, e) {
        e.preventDefault()
        this.$http.put('/api/notifications/' + id + '/mark_as_read', {
        }, function (data, status, request) {
          $('#NotificationLink_' + data.notification.id).removeClass('new_notification')
          $('a#mark_as_read_' + data.notification.id).remove()
          $('a#nav_mark_as_read_' + data.notification.id).remove()
          notifications.counter = --notifications.counter
        }).error(function (data, status, request) {
          if (status === 400) {
            // vueErrors.$set('errors', data.notification)
            $('#modalErrors').modal()
          } else {
            vueErrors.$set('errors', ['Server error 500.'])
            $('#modalErrors').modal()
          }
        })
      },
      deleteNotification: function(id, e) {
        e.preventDefault()
        var resource = this.$resource('/api/notifications/:id');

        resource.delete({id: id}, function (data, status, request) {
          // handle success
          $('#NotificationLink_' + data.notification.id).fadeOut('fast', function() {
            $('#NotificationLink_' + data.notification.id).remove()
          });
          if (data.notification.body.new_notice) {
            $('#NotificationLink_' + data.notification.id).removeClass('new_notification')
            notifications.counter = --notifications.counter
          }
        }).error(function (data, status, request) {
          if (status === 400) {
            // vueErrors.$set('errors', data.notification)
            $('#modalErrors').modal()
          } else {
            vueErrors.$set('errors', ['Server error 500.'])
            $('#modalErrors').modal()
          }
        })
      },
      toggleNotificationsContainer: function() {
        $('#notificationContainer').click(function(e) {
            e.stopPropagation()
        })
        $('body').click(function(event) {
          $("#notificationContainer").fadeOut(200)
        })
        $("#notificationContainer").fadeIn(200)
        event.stopPropagation()
      }
    }
  })

};

if ($('#main-search').length > 0) {
  Cookies.set('eventDate', moment().add(1, 'day').format('YYYY-MM-DD'))

  window.root_vm = new Vue({
    el: '#main-search',
    data: {
      states: [],
      venueTypes: [],
      guestsCount: null,
      opts: {
        firstLoad: true
      }
    },

    ready: function () {
      this.fetchStates()
      this.fetchVenueTypes()
      $.removeCookie('searchCity')
      $.removeCookie('searchState')
      $.removeCookie('guestsCount')
    },

    watch: {
      'guestsCount': function(val, oldVal){
        Cookies.set('guestsCount', val)
      }
    },

    methods: {
      setVenueTypeCookie: function (val) {
        Cookies.set('venueType', val)
      },

      setStateCookie: function (val) {
        if (val.length === 0) {
          Cookies.set('searchState', 'Ontario')
        } else {
          city = val.split(',')[0]
          state = val.split(',')[1]
          Cookies.set('searchCity', city)
          Cookies.set('searchState', state)
        }
      },

      fetchVenueTypes: function () {
        var resource = this.$resource('/api/search_options/venue_types')

        resource.get({}, function (data) {
          this.$set('venueTypes', data.venue_types)
          // Cookies.set('venueType', 'banquet halls')
          Cookies.set('venueType', '')
          this.initPlugins()
          $('.cs-select').trigger('chosen:updated')
        })
      },

      fetchStates: function () {
        var resource = this.$resource('/api/locations/cities_with_states')

        resource.get({}, function (data) {
          this.$set('states', data.cities_with_states)
          Cookies.set('searchState', 'Ontario')
          // Cookies.set('searchCity', 'Toronto')
          this.initPlugins()
          $('.cs-select').trigger('chosen:updated')
        })
      },

      initPlugins: function () {
        var fload = this.opts.firstLoad
        setTimeout(function () {
          initCarousel()
          if (fload) {
            $('.cs-select').trigger('chosen:updated') // this fixes the chosen select update in FireFox
            fadeInPage()
          }
          $('.loader').fadeOut()
          $('.main_search').removeClass('hidden')

          $('#dateOfEvent').datetimepicker({
            format: 'DD/MM/YYYY',
            minDate: Date.now(),
            defaultDate: Cookies.get('eventDate')
          })
          $('#dateOfEvent').on('dp.change', function (e) {
            Cookies.set('eventDate', e.date.format('YYYY-MM-DD'))
          })
        }, 1000)
        this.opts.firstLoad = false
      }
    }
  })
}
