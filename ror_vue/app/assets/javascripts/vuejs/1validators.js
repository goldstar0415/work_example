var VueValidator = require('vue-validator')
Vue.use(VueValidator)

Vue.validator('email', function (val) {
  return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(val)
})

Vue.validator('phone', function (val) {
  return /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/.test(val)
})

Vue.validator('website', function (val) {
  if (val.length === 0) {
    return true
  }
  return /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/.test(val)
})

Vue.validator('guestsNumber', function (val) {
  guestsLimit = { min: $('#bookGuestsNumber').attr('min'), max: $('#bookGuestsNumber').attr('max') }
  min = parseInt(guestsLimit['min'])
  max = parseInt(guestsLimit['max'])
  val = parseInt(val)
  return (min <= val) && (val <= max)
})
