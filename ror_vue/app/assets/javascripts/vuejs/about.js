if ($('.contact-form').length > 0) {
  Vue.http.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content')

  var feedback = new Vue({
    el: '.contact-form',
    data: {
      email: null,
      firstName: null,
      lastName: null,
      message: null
    },

    ready: function () {
      // $('.default-form').removeAttr('hidden')
    },

    methods: {
      clearForm: function () {
        this.email = null
        this.firstName = null
        this.lastName = null
        this.message = null
      },
      submitFeedback: function (e) {
        e.preventDefault()
        var resource = this.$resource('/api/feedbacks')

        resource.save({
          feedback: {
            email: this.email,
            first_name: this.firstName,
            last_name: this.lastName,
            message: this.message
          }
        }, function (data, status, request) {
          renderNotification('Feedback successfully sent.')
          this.clearForm()
        }).error(function (data, status, request) {
          if (status === 400) {
            vueErrors.$set('errors', data.feedbacks)
            $('#modalErrors').modal()
          } else {
            vueErrors.$set('errors', ['Server error 500.'])
            $('#modalErrors').modal()
          }
        })
      },
    }
  })
}
