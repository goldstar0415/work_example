if ($('#redeem_email_request').length > 0) {
  Vue.http.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content')

  var venueRedeem = new Vue({
    el: '#redeem_email_request',
    data: {
      signInForm: false,
      signUpForm: false,
      newUser: {
        email: null,
        firstName: null,
        lastName: null,
        password: null,
        passwordConfirmation: null
      },
      user: {
        email: null,
        password: null,
      }
    },

    ready: function () {
      setTimeout(function () {
        venueRedeem.loader('hide')
        $('#redeem_email_request').removeAttr('hidden')
      }, 1000)
    },

    methods: {
      loader: function (act) {
        if (act === 'show') {
          $('img.loading').show()
        } else {
          $('img.loading').hide()
        }
      },
      toggleSignIn: function (e) {
        e.preventDefault()
        this.signUpForm = false
        this.signInForm = !this.signInForm
      },
      toggleSignUp: function (e) {
        e.preventDefault()
        this.signInForm = false
        this.signUpForm = !this.signUpForm
      },
      signInUser: function (e) {
        e.preventDefault()
        token = $('#redeem_email_request').data('venue-token')
        var resource = this.$resource('/api/venues/' + token + '/redeem')

        resource.save({
          // token: $('#assets_token').val(),
          venue_token: $('#redeem_email_request').data('venue-token'),
          user: {
            email: this.user.email,
            password: this.user.password,
          }
        }, function (data, status, request) {
          window.location.replace($('#redeem_email_request').data('redirect-url'))
        })
          .error(function (data, status, request) {
            if (status === 400) {
              vueErrors.$set('errors', data.venue)
              $('#modalErrors').modal()
            } else if (status === 404) {
              vueErrors.$set('errors', data.venue)
              $('#modalErrors').modal()
            } else {
              vueErrors.$set('errors', ['Server error 500.'])
              $('#modalErrors').modal()
            }
          })
      },
      signupUser: function (e) {
        e.preventDefault()
        var resource = this.$resource('/api/users')

        resource.save({
          // token: $('#assets_token').val(),
          venue_token: $('#redeem_email_request').data('venue-token'),
          user: {
            email: this.newUser.email,
            first_name: this.newUser.firstName,
            last_name: this.newUser.lastName,
            password: this.newUser.password,
            password_confirmation: this.newUser.passwordConfirmation,
          }
        }, function (data, status, request) {
          window.location.replace($('#redeem_email_request').data('redirect-url'))
        })
          .error(function (data, status, request) {
            if (status === 400) {
              vueErrors.$set('errors', data.users)
              $('#modalErrors').modal()
            } else {
              vueErrors.$set('errors', ['Server error 500.'])
              $('#modalErrors').modal()
            }
          })
      }
    }
  })
}
