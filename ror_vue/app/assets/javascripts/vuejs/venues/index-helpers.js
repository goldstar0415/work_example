Vue.component('venue-component', {
  props: ['venue'],
  template: $('#venue-template').html(),
  methods: {
    favoriteToggle: function (slug, venueId, e) {
      e.preventDefault()
      var resource = this.$resource('/api/venues/' + slug + '/favorites_toggle')

      resource.save({
        method: $('#favLink_' + venueId).data('like')
      }, function (data) {})
        .success(function (data, status, request) {
          if (data.method === 'like') {
            $('#favLink_' + data.venue_id).addClass('addedFav')
            $('#favLink_' + data.venue_id).data('like', 'unlike')
            $('#favLink_' + data.venue_id).attr('title', 'Remove from favorites')
          } else {
            $('#favLink_' + data.venue_id).removeClass('addedFav')
            $('#favLink_' + data.venue_id).data('like', 'like')
            $('#favLink_' + data.venue_id).attr('title', 'Add to favorites')
          }
        }).error(function (data, status, request) {
        // handle error
      })
    },
  }
})

Vue.component('venue-header-message', {
  props: ['city', 'state'],
  template: 'VENUES IN <ul class="search_location addComma"><li v-if="city.length > 0" v-text="city | uppercase"></li> <li v-text="state | uppercase"></li></ul>'
})
