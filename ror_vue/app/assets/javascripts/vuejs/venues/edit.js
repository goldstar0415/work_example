if ($('#edit_venue').length > 0) {
  Vue.http.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content')
  loadingAnimation('show')

  var editVenueForm = new Vue({
    el: '#edit_venue',
    data: {
      slug: $('#edit_venue').data('slug'),
      assets_token: $('#assets_token').val(),
      venue: {
        title: '',
        address: '',
        description: '',
        email: '',
        website: '',
        venueTypeList: [],
        eventTypeList: [],
        amenityList: [],
        latitude: '',
        longitude: '',
        street_address: '',
        street_number: '',
        street_name: '',
        city: '',
        state: '',
        state_code: '',
        state_name: '',
        zip: '',
        country_code: '',
        social_links: {
          facebook: '',
          google: '',
          twitter: '',
          pinterest: '',
          linkedin: ''
        },
        assets: []
      },
      images: 0,
      venueTypes: [],
      eventTypes: [],
      amenities: [],
    },

    ready: function () {
      this.fetchVenueTypes()
      this.fetchEventTypes()
      this.fetchAmenties()
      setTimeout(function () {
        editVenueForm.loadVenueData()
        editVenueForm.initPlugins()
        $('#edit_venue').removeAttr('hidden')
      }, 1000)
    },

    methods: {
      fetchVenueTypes: function () {
        this.$http.get('/api/search_options/all_venue_types', function (data) {
          this.$set('venueTypes', data.venue_types)
        })
      },
      fetchEventTypes: function () {
        this.$http.get('/api/search_options/event_types', function (data) {
          this.$set('eventTypes', data.event_types)
        })
      },
      fetchAmenties: function () {
        this.$http.get('/api/search_options/amenities', function (data) {
          this.$set('amenities', data.amenities)
        })
      },
      loadVenueData: function () {
        this.$http.get('/api/venues/' + this.slug, function (data) {
          this.venue.title = data.venue.name
          this.venue.address = data.venue.address
          this.venue.email = data.venue.email
          this.venue.website = data.venue.website
          this.venue.description = data.venue.full_description
          this.venue.social_links = data.venue.social_links
          this.venue.latitude = data.venue.latitude
          this.venue.longitude = data.venue.longitude
          this.venue.street_address = data.venue.street_address
          this.venue.street_number = data.venue.street_number
          this.venue.street_name = data.venue.street_name
          this.venue.city = data.venue.city
          this.venue.state_code = data.venue.state_code
          this.venue.state_name = data.venue.state_name
          this.venue.zip = data.venue.zip
          this.images = data.venue.images.count
          this.venue.assets = data.venue.assets
          data.venue.venue_types.forEach(function (el) {
            $('#checkbox_vt_' + el.id).prop('checked', true)
            editVenueForm.updateVenueTypeList(el.id, el.name)
          })
          data.venue.event_types.forEach(function (el) {
            $('#checkbox_et_' + el.id).prop('checked', true)
            editVenueForm.updateEventTypeList(el.id, el.name)
          })
          data.venue.amenities.forEach(function (el) {
            $('#checkbox_amt_' + el.id).prop('checked', true)
            editVenueForm.updateAmenities(el.id, el.name)
          })
        }).then(function(response) {
          loadingAnimation('hide')
        })
      },
      updateVenueTypeList: function (id, name) {
        if ($('input#checkbox_vt_' + id).is(':checked')) {
          this.venue.venueTypeList.push(name)
        } else {
          a = this.venue.venueTypeList
          arr = jQuery.grep(a, function (value) {
            return value != name
          })
          this.venue.venueTypeList = arr
        }
      },
      updateEventTypeList: function (id, name) {
        if ($('input#checkbox_et_' + id).is(':checked')) {
          this.venue.eventTypeList.push(name)
        } else {
          a = this.venue.eventTypeList
          arr = jQuery.grep(a, function (value) {
            return value != name
          })
          this.venue.eventTypeList = arr
        }
      },
      updateAmenities: function (id, name) {
        if ($('input#checkbox_amt_' + id).is(':checked')) {
          this.venue.amenityList.push(name)
        } else {
          a = this.venue.amenityList
          arr = jQuery.grep(a, function (value) {
            return value != name
          })
          this.venue.amenityList = arr
        }
      },
      submitVenue: function () {
        this.$http.put('/api/venues/' + this.slug, {
          token: $('#assets_token').val(),
          venue: {
            name: this.venue.title,
            address: this.venue.address,
            description: this.venue.description,
            email: this.venue.email,
            website: this.venue.website,
            price: this.venue.price,
            latitude: this.venue.latitude,
            longitude: this.venue.longitude,
            street_address: this.venue.street_address,
            street_number: this.venue.street_number,
            street_name: this.venue.street_name,
            city: this.venue.city,
            state_code: this.venue.state_code,
            state_name: this.venue.state_name,
            zip: this.venue.zip,
            country_code: this.venue.country_code,
            venue_types: this.venue.venueTypeList,
            event_types: this.venue.eventTypeList,
            amenities: this.venue.amenityList,
            social_links: this.venue.social_links
          }
        }, function (data, status, request) {
          window.location.replace(data.venue.url)
        }).error(function (data, status, request) {
          if (status === 400) {
            vueErrors.$set('errors', data.venues)
            $('#modalErrors').modal()
          } else {
            vueErrors.$set('errors', ['Server error 500.'])
            $('#modalErrors').modal()
          }
        })
      },
      initPlugins: function () {
        setTimeout(function () {
          initTypeAhead()
          $('.cs-select').trigger('chosen:updated')
        }, 1000)
      },
      deleteAsset: function (id, venue_id) {
        $('.asset_' + id + ' button.delete').attr('disabled', 'disabled').html('Deleteing....')
        var resource = this.$resource('/api/assets/:id/delete_venue_asset')
        resource.delete({
          id: id,
          venue_id: editVenueForm.slug
        }, function (data, status, request) {
          if (status === 200) {
            editVenueForm.images = editVenueForm.images - 1
            $('.asset_' + id).fadeOut()
          }
        // handle success
        }).error(function (data, status, request) {
          // handle error
        })
      }
    }
  })
  // end vue object

  var previewNode = document.querySelector('#template')
  previewNode.id = ''
  var previewTemplate = previewNode.parentNode.innerHTML
  previewNode.parentNode.removeChild(previewNode)
  var my_token = $('#assets_token').val()

  var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
    url: '/api/assets', // Set the url
    acceptedFiles: 'image/jpeg,image/png',
    paramName: 'asset[image]',
    maxFiles: 6,
    maxFilesize: 1,
    thumbnailWidth: 100,
    thumbnailHeight: 100,
    parallelUploads: 20,
    previewTemplate: previewTemplate,
    autoQueue: false, // Make sure the files aren't queued until manually added
    previewsContainer: '#previews', // Define the container to display the previews
    clickable: '.fileinput-button', // Define the element that should be used as click trigger to select files.
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    }
  })

  myDropzone.on('success', function (file, response) {
    $(file.previewTemplate).find('button.delete').attr('data-dz-remove', response.asset.delete_url)
    editVenueForm.images = editVenueForm.images + 1
  })

  myDropzone.on('maxfilesexceeded', function (file) { this.removeFile(file); })
  myDropzone.on('addedfile', function (file) {
    // Hookup the start button
    file.previewElement.querySelector('.start').onclick = function () { myDropzone.enqueueFile(file); }
  })

  // Update the total progress bar
  myDropzone.on('totaluploadprogress', function (progress) {
    document.querySelector('#total-progress .progress-bar').style.width = progress + '%'
  })

  myDropzone.on('sending', function (file, xhr, data) {
    data.append('asset[token]', my_token)
    data.append('asset[assetable_type]', 'Venue')
    // Show the total progress bar when upload starts
    document.querySelector('#total-progress').style.opacity = '1'
    // And disable the start button
    file.previewElement.querySelector('.start').setAttribute('disabled', 'disabled')
  })

  // Hide the total progress bar when nothing's uploading anymore
  myDropzone.on('queuecomplete', function (progress) {
    document.querySelector('#total-progress').style.opacity = '0'
  })

  myDropzone.on('removedfile', function (file) {
    url = $(file.previewTemplate).find('button.delete').attr('data-dz-remove')
    if (url.length > 0) {
      editVenueForm.images = editVenueForm.images - 1
      $.ajax({
        type: 'DELETE',
        url: url,
        contentType: 'application/json',
        success: function (report) {
          // console.log(report)
        },
        error: function (report) {
          // console.log(report)
        }
      })
    }
  })

  // Setup the buttons for all transfers
  // The "add files" button doesn't need to be setup because the config
  // `clickable` has already been specified.
  document.querySelector('#actions .start').onclick = function () {
    myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED))
  }
  document.querySelector('#actions .cancel').onclick = function () {
    myDropzone.removeAllFiles(true)
  }
}

function initTypeAhead () {
  var mapCenter = new google.maps.LatLng(editVenueForm.venue.latitude, editVenueForm.venue.longitude)
  var addressPicker = new AddressPicker({
    map: {
      id: '#map',
      zoom: 14,
      center: mapCenter,
      displayMarker: true
    },
    marker: {
      draggable: true,
      visible: true,
      position: mapCenter
    },
    draggable: true,
    reverseGeocoding: true,
    autocompleteService: {
      types: ['geocode'],
      componentRestrictions: {
        country: 'CA'
      }
    }
  });// instantiate the typeahead UI

  $('#venueAddress').typeahead(null, {
    displayKey: 'description',
    source: addressPicker.ttAdapter()
  }).bind('typeahead:selected', addressPicker.updateMap)
  addressPicker.bindDefaultTypeaheadEvent($('#venueAddress'))
  $(addressPicker).on('addresspicker:selected', function (event, result) {
    editVenueForm.venue.latitude = result.lat()
    editVenueForm.venue.longitude = result.lng()
    editVenueForm.venue.street_address = result.nameForType('street_number') + ' ' + result.nameForType('route')
    editVenueForm.venue.street_number = result.nameForType('street_number')
    editVenueForm.venue.street_name = result.nameForType('route')
    editVenueForm.venue.city = result.nameForType('locality')
    editVenueForm.venue.state_code = result.nameForType('administrative_area_level_1', true)
    editVenueForm.venue.state_name = result.nameForType('administrative_area_level_1')
    editVenueForm.venue.zip = result.nameForType('postal_code')
    editVenueForm.venue.country_code = result.nameForType('country', true)
    editVenueForm.venue.address = result.address()
    result.addressTypes().forEach(function (type) {
      console.log('  ' + type + ': ' + result.nameForType(type, true))
    })
  })
}
