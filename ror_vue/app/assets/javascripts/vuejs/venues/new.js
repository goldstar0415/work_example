if ($('#new_venue').length > 0) {
  Vue.http.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content')

  // Vue.use(window['vue-validator'])
  var newVenueForm = new Vue({
    el: '#new_venue',
    data: {
      assets_token: $('#assets_token').val(),
      venue: {
        title: '',
        address: '',
        description: '',
        email: '',
        website: '',
        venueTypeList: [],
        eventTypeList: [],
        amenityList: [],
        latitude: '',
        longitude: '',
        street_address: '',
        street_number: '',
        street_name: '',
        city: '',
        state_code: '',
        state_name: '',
        zip: '',
        country_code: '',
        social_links: {
          facebook: '',
          google: '',
          twitter: '',
          pinterest: '',
          pinterest: '',
          linkedin: ''
        }
      },
      images: 0,
      venueTypes: [],
      eventTypes: [],
      amenities: [],
    },

    ready: function () {
      this.fetchVenueTypes()
      this.fetchEventTypes()
      this.fetchAmenties()
      this.initPlugins()
    },

    methods: {
      fetchVenueTypes: function () {
        this.$http.get('/api/search_options/all_venue_types', function (data) {
          this.$set('venueTypes', data.venue_types)
        })
      },
      fetchEventTypes: function () {
        this.$http.get('/api/search_options/event_types', function (data) {
          this.$set('eventTypes', data.event_types)
        })
      },
      fetchAmenties: function () {
        this.$http.get('/api/search_options/amenities', function (data) {
          this.$set('amenities', data.amenities)
        })
      },
      updateVenueTypeList: function (id, name) {
        if ($('input#checkbox_vt_' + id).is(':checked')) {
          this.venue.venueTypeList.push(name)
        } else {
          a = this.venue.venueTypeList
          arr = jQuery.grep(a, function (value) {
            return value != name
          })
          this.venue.venueTypeList = arr
        }
      },
      updateEventTypeList: function (id, name) {
        if ($('input#checkbox_et_' + id).is(':checked')) {
          this.venue.eventTypeList.push(name)
        } else {
          a = this.venue.eventTypeList
          arr = jQuery.grep(a, function (value) {
            return value != name
          })
          this.venue.eventTypeList = arr
        }
      },
      updateAmenities: function (id, name) {
        if ($('input#checkbox_amt_' + id).is(':checked')) {
          this.venue.amenityList.push(name)
        } else {
          a = this.venue.amenityList
          arr = jQuery.grep(a, function (value) {
            return value != name
          })
          this.venue.amenityList = arr
        }
      },
      submitVenue: function () {
        var resource = this.$resource('/api/venues')

        resource.save({
          token: $('#assets_token').val(),
          venue: {
            name: this.venue.title,
            address: this.venue.address,
            email: this.venue.email,
            description: this.venue.description,
            website: this.venue.website,
            latitude: this.venue.latitude,
            longitude: this.venue.longitude,
            street_address: this.venue.street_address,
            street_number: this.venue.street_number,
            street_name: this.venue.street_name,
            city: this.venue.city,
            state_code: this.venue.state_code,
            state_name: this.venue.state_name,
            zip: this.venue.zip,
            country_code: this.venue.country_code,
            venue_types: this.venue.venueTypeList,
            event_types: this.venue.eventTypeList,
            amenities: this.venue.amenityList,
            social_links: this.venue.social_links,
          }
        }, function (data, status, request) {
          window.location.replace(data.venue.url)
        })
          .error(function (data, status, request) {
            if (status === 400) {
              vueErrors.$set('errors', data.venues)
              $('#modalErrors').modal()
            } else {
              vueErrors.$set('errors', ['Server error 500.'])
              $('#modalErrors').modal()
            }
          })
      },
      initPlugins: function () {
        setTimeout(function () {
          $('.cs-select').trigger('chosen:updated')
        }, 1000)
      }
    }
  })
  // end vue object

  var addressPicker = new AddressPicker({
    map: {
      id: '#map',
      zoom: 14,
      center: new google.maps.LatLng(43.653226, -79.383184),
      displayMarker: true
    },
    draggable: true,
    reverseGeocoding: true,
    autocompleteService: {
      types: ['geocode'],
      componentRestrictions: {
        country: 'CA'
      }
    }
  });// instantiate the typeahead UI

  $('#venueAddress').typeahead(null, {
    displayKey: 'description',
    source: addressPicker.ttAdapter()
  }).bind('typeahead:selected', addressPicker.updateMap)
  addressPicker.bindDefaultTypeaheadEvent($('#address'))
  $(addressPicker).on('addresspicker:selected', function (event, result) {
    newVenueForm.venue.latitude = result.lat()
    newVenueForm.venue.longitude = result.lng()
    newVenueForm.venue.street_address = result.nameForType('street_number') + ' ' + result.nameForType('route')
    newVenueForm.venue.street_number = result.nameForType('street_number')
    newVenueForm.venue.street_name = result.nameForType('route')
    newVenueForm.venue.city = result.nameForType('locality')
    newVenueForm.venue.state_code = result.nameForType('administrative_area_level_1', true)
    newVenueForm.venue.state_name = result.nameForType('administrative_area_level_1')
    newVenueForm.venue.zip = result.nameForType('postal_code')
    newVenueForm.venue.country_code = result.nameForType('country', true)
    newVenueForm.venue.address = result.address()
  })

  jQuery(document).ready(function() {
    var previewNode = document.querySelector('#template')
    previewNode.id = ''
    var previewTemplate = previewNode.parentNode.innerHTML
    previewNode.parentNode.removeChild(previewNode)
    var my_token = $('#assets_token').val()

    var myDropzone = new Dropzone('div#images', { // Make the whole body a dropzone
      url: '/api/assets', // Set the url
      acceptedFiles: 'image/jpeg,image/png',
      paramName: 'asset[image]',
      maxFiles: 6,
      maxFilesize: 1,
      thumbnailWidth: 100,
      thumbnailHeight: 100,
      parallelUploads: 20,
      previewTemplate: previewTemplate,
      autoQueue: false, // Make sure the files aren't queued until manually added
      previewsContainer: '#previews', // Define the container to display the previews
      clickable: '.fileinput-button', // Define the element that should be used as click trigger to select files.
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      }
    })

    myDropzone.on('success', function (file, response) {
      $(file.previewTemplate).find('button.delete').attr('data-dz-remove', response.asset.delete_url)
      newVenueForm.images = newVenueForm.images + 1
    })

    myDropzone.on('maxfilesexceeded', function (file) { this.removeFile(file); })
    myDropzone.on('addedfile', function (file) {
      // Hookup the start button
      file.previewElement.querySelector('.start').onclick = function () { myDropzone.enqueueFile(file); }
    })

    // Update the total progress bar
    myDropzone.on('totaluploadprogress', function (progress) {
      document.querySelector('#total-progress .progress-bar').style.width = progress + '%'
    })

    myDropzone.on('sending', function (file, xhr, data) {
      data.append('asset[token]', my_token)
      data.append('asset[assetable_type]', 'Venue')
      // Show the total progress bar when upload starts
      document.querySelector('#total-progress').style.opacity = '1'
      // And disable the start button
      file.previewElement.querySelector('.start').setAttribute('disabled', 'disabled')
    })

    // Hide the total progress bar when nothing's uploading anymore
    myDropzone.on('queuecomplete', function (progress) {
      document.querySelector('#total-progress').style.opacity = '0'
    })

    myDropzone.on('removedfile', function (file) {
      url = $(file.previewTemplate).find('button.delete').attr('data-dz-remove')
      if (url.length > 0) {
        newVenueForm.images = newVenueForm.images - 1
        $.ajax({
          type: 'DELETE',
          url: url,
          contentType: 'application/json',
          success: function (report) {
            // console.log(report)
          },
          error: function (report) {
            // console.log(report)
          }
        })
      }
    })

    // Setup the buttons for all transfers
    // The "add files" button doesn't need to be setup because the config
    // `clickable` has already been specified.
    document.querySelector('#actions .start').onclick = function () {
      myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED))
    }
    document.querySelector('#actions .cancel').onclick = function () {
      myDropzone.removeAllFiles(true)
    }
  })

}
