if ($('#search-results').length > 0) {
  if (Cookies.get('venueType')) {
    var venueType = Cookies.get('venueType')
  } else {
    var venueType = ''
  }
  if (Cookies.get('searchState')) {
    var stateVal = Cookies.get('searchState')
  } else {
    var stateVal = 'Ontario'
  }
  if (Cookies.get('searchCity')) {
    var cityVal = Cookies.get('searchCity')
  } else {
    var cityVal = ''
  }
  if (Cookies.get('guestsCount')) {
    var guestsCount = Cookies.get('guestsCount')
  } else {
    var guestsCount = null
  }
  if (Cookies.get('eventDate')) {
    var eventDate = Cookies.get('eventDate')
  } else {
    var eventDate = moment().format('YYYY-MM-DD')
  }


  window.vm = new Vue({
    // el: '#search-results',
    data: {
      sorting: 'name asc',
      venues: [],
      allVenuesCount: 0,
      recentVenues: [],
      states: [],
      cities: [],
      venueTypes: [],
      eventTypes: [],
      amenities: [],
      priceMin: 0,
      priceMax: 0,
      opts: {
        firstLoad: true,
        noPriceLimit: false,
        noCapacityLimit: false
      },
      search: {
        state: stateVal,
        city: cityVal,
        venueType: venueType,
        eventType: '',
        price_range: [],
        capacity_range: [],
        amenities: [],
        guestsCount: guestsCount,
        eventDate: eventDate
      },
      reviews_count: null,
      pagination: {
        current: 1,
        totalPages: 1
      }
    },

    computed: {
      sortKey: {
        get: function () {
          return this.sorting.split(' ')[0]
        }
      },
      sortOrder: {
        get: function () {
          return this.sorting.split(' ')[1]
        }
      }
    },

    ready: function () {
      this.fetchStates()
      this.fetchCities(this.search.state)
      this.fetchVenueTypes()
      this.fetchEventTypes()
      this.fetchAmenties()
      this.fetchVenues()
      this.fetchRecents()
    },

    methods: {
      updatePriceSlider: function () {
        slider = $('#budget-slider').data('ionRangeSlider')
        if (this.opts.noPriceLimit) {
          this.search.price_range = []
        } else {
          this.search.price_range = [slider.old_from, slider.old_to]
        }
        slider.update({disable: this.opts.noPriceLimit})
        this.fetchVenues()
      },
      updateCapacitySlider: function () {
        slider = $('#capacity-slider').data('ionRangeSlider')
        if (this.opts.noCapacityLimit) {
          this.search.capacity_range = []
        } else {
          this.search.capacity_range = [slider.old_from, slider.old_to]
        }
        slider.update({disable: this.opts.noCapacityLimit})
        this.fetchVenues()
      },
      changePage: function (page, e) {
        e.preventDefault()
        this.pagination.current = page
        this.fetchVenues()
      },
      clearFilters: function (e) {
        e.preventDefault(e)
        this.pagination.current = 1
        this.search.city = ''
        this.search.venueType = ''
        this.search.eventType = ''
        this.search.amenities = []
        this.search.capacity_range = []
        this.search.guestsCount = $('#capacity-slider').data('min')
        this.search.eventDate = moment().add(1, 'day').format('YYYY-MM-DD')
        var c_slider = $('#capacity-slider').data('ionRangeSlider') // reset sliders
        // var b_slider = $('#budget-slider').data('ionRangeSlider') // reset sliders
        c_slider.reset()
        // b_slider.reset()
        $('#collapseAmenity input').attr('checked', false)
        // Cookies.set('searchCity', '')
        this.resetCookies(['venueType', 'searchCity'])
        this.fetchVenues()
      },
      resetCookies: function(cookies){
        $.each( cookies, function( index, cookie ){
          Cookies.remove(cookie)
        })
      },
      fetchStates: function () {
        this.$http.get('/api/locations/states', function (data) {
          this.$set('states', data.states)
        })
      },
      fetchCities: function (state) {
        this.$http.get('/api/locations/cities?state=' + state , function (data) {
          this.$set('cities', data.cities)
        })
      },
      fetchVenueTypes: function () {
        this.$http.get('/api/search_options/venue_types', function (data) {
          this.$set('venueTypes', data.venue_types)
        })
      },
      fetchEventTypes: function () {
        this.$http.get('/api/search_options/event_types', function (data) {
          this.$set('eventTypes', data.event_types)
        })
      },
      fetchAmenties: function () {
        this.$http.get('/api/search_options/amenities', function (data) {
          this.$set('amenities', data.amenities)
        })
      },
      fetchVenues: function () {
        loadingAnimation('show')
        this.updateNoResultsMessage(false)
        var resource = this.$resource('/api/venues')

        resource.get({
          page: this.pagination.current,
          key: this.sortKey,
          direction: this.sortOrder,
          search: {
            state: this.search.state,
            city: this.search.city,
            venue_type: this.search.venueType,
            event_type: this.search.eventType,
            amenities: this.search.amenities,
            price_range: this.search.price_range,
            capacity_range: this.search.capacity_range,
            guests_count: this.search.guestsCount,
            event_date: this.search.eventDate
          }
        }, function (data) {
          this.$set('venues', data.venues)
          this.allVenuesCount = data.meta.all_venues_count
          this.updateNoResultsMessage(data.venues.length < 1)
          this.pagination.totalPages = data.meta.total_pages
          this.pagination.current = data.meta.current_page
          this.initPlugins()
          $('.cs-select').trigger('chosen:updated')
        }).then(function(response) {
          loadingAnimation('hide')
        })
      },
      fetchRecents: function () {
        this.$http.get('/api/venues/recent', function (data) {
          this.$set('recentVenues', data.venues)
        })
      },
      updateAmenityList: function (id, name) {
        if ($('input#checkbox' + id).is(':checked')) {
          this.search.amenities.push(name)
        } else {
          a = this.search.amenities
          arr = jQuery.grep(a, function (value) {
            return value != name
          })
          this.search.amenities = arr
        }
        this.fetchVenues()
      },
      updateNoResultsMessage: function (value) {
        css = value ? 'block' : 'none'
        $('.no-results').css('display', css)
      },
      initPlugins: function () {
        var fload = this.opts.firstLoad
        setTimeout(function () {
          initCarousel()
          if (fload) {
            if (vm.$data.search.city.length > 0) {
              $('#city-select').val(vm.$data.search.city)
            }
            $('.cs-select').trigger('chosen:updated') // this fixes the chosen select update in FireFox
            // sessionStorage['searchCity'] = ''
          }
        }, 500)
        this.opts.firstLoad = false
      }
    }
  }).$mount('#search-results')

  $('#inlineCalendar').datetimepicker({
    inline: true,
    format: 'DD/MM/YYYY',
    minDate: Date.now(),
    defaultDate: Date.now()
  });
  // defaultDate: Cookies.get('eventDate')


  $('#inlineCalendar').on('dp.change', function (e) {
    vm.search.eventDate = e.date.format('YYYY-MM-DD')
    vm.fetchVenues()
  })

  vm.$watch('search.eventDate', function (val, oldVal) {
    Cookies.set('eventDate', val)
  })

  vm.$watch('search.city', function (val, oldVal) {
    Cookies.set('searchCity', val)
  })

  vm.$watch('search.guestsCount', function (val, oldVal) {
    Cookies.set('guestsCount', val)
    capacitySlider = $('#capacity-slider').data("ionRangeSlider")
    capacitySlider.update({from: val})
  })
}
