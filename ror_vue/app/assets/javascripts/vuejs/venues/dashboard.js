if ($('#views-chart').length > 0) {
  slug = $('#dashboard').data('slug')
  var venueDashboard = new Vue({
    data: {
      months: [],
      bookings: [],
      views: [],
      favorited_count: 0,
      requests_count: 0,
      views_count: 0
    },

    ready: function () {
      this.fetchMonths()
      setTimeout(function () {
        showChart(venueDashboard.months, venueDashboard.bookings, venueDashboard.views)
        $('#dashboard').removeAttr('hidden')
        venueDashboard.loader('hide')
      }, 1000)
    },

    methods: {
      loader: function (act) {
        if (act === 'show') {
          $('img.loading').show()
        } else {
          $('img.loading').hide()
        }
      },
      fetchMonths: function () {
        this.$http.get('/api/venues/' + slug +'/dashboard', function (data) {
          this.$set('months', data.venue.data.months)
          this.$set('bookings', data.venue.data.bookings)
          this.$set('views', data.venue.data.views)
          this.$set('favorited_count', data.venue.favorited_count)
          this.$set('requests_count', data.venue.requests_count)
          this.$set('views_count', data.venue.views_count)
        })
      }
    }
  }).$mount('#dashboard')

  function showChart(months, bookings, views) {
    $('#views-chart').highcharts({
        title: {
            text: 'Views and Bookings',
            x: -20 //center
        },
        subtitle: {
            text: 'Monthly Average',
            x: -20
        },
        xAxis: {
          categories: months
        },
        yAxis: {
          min: 0,
          title: {
            text: ''
          }
        },
        series: [{
            name: 'Views',
            data: views,
            color: '#e4b248'
        }, {
            name: 'Bookings',
            data: bookings
        }]
    })
  }
}
