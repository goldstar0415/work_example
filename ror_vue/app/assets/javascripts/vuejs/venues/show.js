if ($('.venue_page').length > 0) {
  var venueShow = new Vue({
    el: '#venue-details',
    data: {
      reviews: [],
      packages: [],
      newReview: null,
      slug: $('#venue-details').data('slug'),
      showForm: true,
      favorite: {
        text: $('.fav-opts').data('text'),
        disabled: false,
        method: $('.fav-opts').data('method')
      },
      activeTab: 'reviews'
    },

    ready: function () {
      // this.fetchPackages()
      this.fetchReviews()
      setTimeout(function () {
        $('section.reviews').fadeIn('fast')
        readMore()
        venueShow.loader('hide')
      }, 1000)
    },

    methods: {
      fetchReviews: function () {
        this.$http.get('/api/reviews?venue_id=' + this.slug, function (data) {
          this.$set('reviews', data.reviews)
        })
      },
      // fetchPackages: function () {
      //   this.$http.get('/api/venues/' + this.slug + '/packages', function (data) {
      //     this.$set('packages', data.packages)
      //   })
      // },
      loader: function (act) {
        if (act === 'show') {
          $('img.loading').show()
        } else {
          $('img.loading').hide()
        }
      },
      packagesReviewsToggle: function (arg, e) {
        e.preventDefault()
        this.activeTab = arg
      },
      favoritesToggle: function (e) {
        e.preventDefault()
        var resource = this.$resource('/api/venues/' + this.slug + '/favorites_toggle')

        resource.save({
          method: this.favorite.method
        }, function (data) {})
          .success(function (data, status, request) {
            if (data.method === 'like') {
              this.favorite.text = 'Added to favorites'
            } else {
              this.favorite.text = 'Removed from favorites'
            }
            this.favorite.disabled = true
          }).error(function (data, status, request) {
          // handle error
        })
      },
      resetForm: function () {
        this.newReview = null
      },
      addReview: function () {
        $('#add_new_review').attr('disabled', true)
        var resource = this.$resource('/api/reviews')

        resource.save({
          venue_id: this.slug,
          review: {
            comment: this.newReview
          }
        }, function (data, status, request) {
          if (status === 200) {
            venueShow.reviews.unshift(data.review)
            venueShow.resetForm()
            venueShow.showForm = false
          }
        })
          .error(function (data, status, request) {
            $('#add_new_review').removeAttr('disabled')
            $('form.new_comment a.addComment').attr('disabled', false)
            if (status === 400) {
              vueErrors.$set('errors', data.reviews)
              $('#modalErrors').modal()
            } else {
              vueErrors.$set('errors', ['Server error 500.'])
              $('#modalErrors').modal()
            }
          })
      }
    }
  })
}

if ($('#get-in-touch').length > 0) {
  dateToday = moment(Date.now()).format('DD/MM/YYYY')
  timeNow = moment(Date.now()).format('LT')

  var newRequest = new Vue({
    data: {
      displayForm: true,
      venueId: $('#get-in-touch').data('slug'),
      venueTitle: $('#get-in-touch').data('title'),
      senderEmail: $('#get-in-touch').data('sender-email'),
      senderPhone: null,
      ownerFirstName: $('#get-in-touch').data('owner-first-name'),
      firstName: $('#get-in-touch').data('sender-first-name'),
      lastName: $('#get-in-touch').data('sender-last-name'),
      eventOpts: {
        eventType: null,
        startDate: dateToday,
        startTime: timeNow,
        estimatedGuests: 100,
        details: ''
      }
    },

    validator: {
      validates: {
        phone: function (val) {
          return /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/.test(val)
        },
        email: function (val) {
          return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(val)
        }
      }
    },

    ready: function () {
      this.initPlugins()
    },

    methods: {
      resetForm: function () {
        this.displayForm = true
        this.eventOpts.startDate = dateToday
        this.eventOpts.startTime = timeNow
        this.eventOpts.estimatedGuests = null
        this.eventOpts.details = ''
      },
      previewToggle: function () {
        this.displayForm = !this.displayForm
        this.initPlugins()
      },
      submitRequest: function () {
        var resource = this.$resource('/api/requests')

        resource.save({
          venue_id: this.venueId,
          request: {
            first_name: this.firstName,
            last_name: this.lastName,
            email: this.senderEmail,
            phone: this.senderPhone,
            date_start: this.eventOpts.startDate,
            time_start: this.eventOpts.startTime,
            estimated_guests_count: this.eventOpts.estimatedGuests,
            details: this.eventOpts.details,
            event_type: this.eventOpts.eventType
          }
        }, function (data, status, request) {
          $('#get-in-touch').modal('hide')
          this.resetForm()
          renderNotification('Request was successfully sent.')
        })
          .error(function (data, status, request) {
            if (status === 400) {
              vueErrors.$set('errors', data.requests)
              $('#modalErrors').modal()
            } else {
              vueErrors.$set('errors', ['Server error 500.'])
              $('#modalErrors').modal()
            }
          })
      },
      initPlugins: function () {
        $('#datetimepicker6').datetimepicker({
          format: 'DD/MM/YYYY',
          minDate: Date.now()
        })
        $('#datetimepicker7').datetimepicker({
          format: 'LT',
          useCurrent: false // Important! See issue #1075
        })
        $('#datetimepicker6').on('dp.change', function (e) {
          newRequest.eventOpts.startDate = e.date.format('DD/MM/YYYY')
        })
        $('#datetimepicker7').on('dp.change', function (e) {
          newRequest.eventOpts.startTime = e.date.format('LT')
        })
      }
    }

  }).$mount('#get-in-touch')
}
