// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
// = require vue
// = require vue-resource
// = require vue-validator/dist/vue-validator
// = require jquery
// = require jquery-ui/autocomplete
// = require jquery_ujs
// = require jquery.cookie
// = require chosen-jquery
// = require bootstrap.min
// = require ie10-viewport-bug-workaround
// = require placeholders.min
// = require ion.rangeSlider.min.js
// = require owl.carousel.min.js
// = require app
// = require flip
// = require gmaps
// = require typeahead.bundle.js
// = require typeahead-addresspicker.js
// = require jquery-mmenu
// = require moment
// = require moment-timezone
// = require readmore
// = require eonasdan-bootstrap-datetimepicker
// = require select2
// = require notifications
// = require js.cookie.js
// = require jquery.sticky.js
// = require highcharts
// = require cookieconsent.min.js
// = require ./plugins/jquery.payment
// = require ./plugins/jquery.timepicker
// = require ./plugins/loadingoverlay
// = require ./plugins/pdfobject
// = require_tree ./vuejs

$(document).ready(function () {
  minGuests = $('#guests-count-slider').data('min-guests')
  maxGuests = $('#guests-count-slider').data('max-guests')
  $('#guests-count-slider').ionRangeSlider({
    type: "single",
    min: minGuests,
    max: maxGuests,
    // from: (minGuests + 10),
    from: (2),
    onStart: function (data) {
      root_vm.guestsCount = data.from
    },
    onChange: function (data) {
      root_vm.guestsCount = data.from
    }
  })

  // ===================================================
  initCarousel()

  $('.venue-images').owlCarousel({
    items: 1,
    loop: true,
    margin: 0,
    nav: true,
    responsiveClass: true,
    lazyLoad: true,
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    animateOut: 'slideOutDown',
    animateIn: 'fadeIn',
    smartSpeed: 450,
    dotData: true,
    dotsContainer: '.dots-container'
  })
  // Price sliders
  // ===================================================
  $('#budget-slider').ionRangeSlider({
    type: 'double',
    grid: false,
    min: 0,
    prefix: '$',
    hide_min_max: true,
    onFinish: function (data) {
      vm.search.price_range = [data.from, data.to]
      vm.fetchVenues()
    }
  })

  $('#capacity-slider').ionRangeSlider({
    type: 'single',
    grid: false,
    min: $(this).data('min'),
    max: $(this).data('max'),
    from: Cookies.get('guestsCount'),
    hide_min_max: true,
    onFinish: function (data) {
      vm.search.guestsCount = data.from
      Cookies.set('guestsCount', data.from)
      vm.fetchVenues()
    }
  })

  if ($('#terms_and_condition').length) {
    initTermsAndConditions()
  }
})

function initTermsAndConditions() {
  window.onload = function (){
    var success = new PDFObject({ url: "/fastvenues_terms_and_privacy.pdf" }).embed('pdf');
  }
  $('#terms_and_condition').fadeIn('fast')
}

function spinner(action) {
  if (action === 'show') {
    $('img.loading').show()
  } else {
    $('img.loading').hide()
  }
}

function changeButtonState(button, attr, disabled) {
  button.prop('disabled', disabled)
  button.data(attr)
}

function initCarousel () {
  $('.owl').owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    lazyLoad: true,
    responsiveClass: true,
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    dots: true,
    animateOut: 'flipOutY',
    animateIn: 'flipInY',
    responsive: {
      0: {
        items: 1
      }
    }
  })
}

var vueErrors = new Vue({
  el: '#modalErrors',
  data: {
    errors: []
  }
})
