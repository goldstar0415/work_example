class VenuesController < ApplicationController
  before_filter :authenticate_user!, except: [:show, :index, :favorites, :redeem]
  before_filter :load_venue, only: [:show]
  before_filter :load_owned_venue, only: [:edit, :dashboard]
  before_filter :similar_venues, only: :show
  before_filter :verify_ownership, only: [:edit, :destroy]
  before_filter :verify_if_can_add_venues, only: [:new, :create]

  def index
    @meta_title = Settings.page_title.search_page
    @price = Venue.any?  ? Venue.order('price desc').first.price : 100
    @capacity_min = Venue.pluck(:capacity_min).compact.min
    @capacity_max = Venue.pluck(:capacity_max).compact.max
    Tracker.track(current_user.id, 'Visited search page') if current_user.present?
  end

  def show
    @meta_title = @venue.name
    unless @venue.listed
      raise ActionController::RoutingError.new('Not Found') unless current_user && current_user.owner_of?(@venue)
    end
    recents = session[:recent]
    recents << @venue.id unless recents.include? @venue.id
    recents.shift if recents.length > 4
    @recent = recents
    @social_networks = []
    @social_networks = @venue.social_links.map{ |k, v| [k, v] } if @venue.social_links.present?
    @packages = @venue.packages.order('discount desc NULLS LAST') #.order('discount desc')
    Tracker.track(current_user.id, 'Visited venue', {venue_id: @venue.id, slug: @venue.slug}) if current_user.present?
    impressionist(@venue, nil, unique: [:session_hash])
  end

  def new
    @token = SecureRandom.uuid
    Tracker.track(current_user.id, 'Visited new venue page')
  end

  def create
  end

  def edit
    @token = SecureRandom.uuid
  end

  def dashboard
    Tracker.track(current_user.id, 'Visited dashboard page', {
      venue_id: @venue.id, slug: @venue.slug
    })
  end

  def redeem
    @venue = Venue.where(token: params[:id], redeemed: false).first
    redirect_to root_path unless @venue.present?
  end

  def destroy
  end

  private

  def load_owned_venue
    @venue = current_user.venues.friendly.find(params[:id])
  end

  def load_venue
    @venue = Venue.friendly.find(params[:id])
  end

  def verify_ownership
    return if current_user.owner_of?(@venue)
    redirect_to root_path
    flash[:danger] = 'Wrong move mate! Only venue owner has access to this page.'
  end

  def verify_if_can_add_venues
    return if current_user.can_add_venues?
    redirect_to root_path
    limit = ActionController::Base.helpers.pluralize(current_user.venues_limit, 'venue')
    flash[:error] = "You can't add more then #{limit} at the moment."
  end

  def similar_venues
    @similar_venues = Venue.where(listed: true).tagged_with(
      @venue.venue_type_list, on: :venue_types, any: true
    ).where('venues.id <> ?', @venue.id).sample(4)
  end
end
