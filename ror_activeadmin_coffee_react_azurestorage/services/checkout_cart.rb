class CheckoutCart

  CENT = 100

  attr_reader :coupon, :user, :cart, :score, :composition, :errors

  alias_method :current_user, :user

  def self.checkout(user, params, score, composition, cart, **options)
    new(user, params, score, composition, cart, **options).charge!
  end

  def initialize(user, params, score, composition, cart, **options)
    @errors      = []
    @user        = user
    @cart        = cart
    @score       = score
    @discount    = 0
    @params      = params
    @options     = options
    @composition = composition
    @amount      = cart.grand_total
    @address     = @params[:billing_address]
    @invoice     = @user.invoices.build(amount: Money.new(@amount * CENT))
  end

  def charge!
    raise NoCartItemAvailable, 'Please add items to cart' if @cart.nil?
    @cart.retouch_price_quantity

    if @params[:stripeToken].present?
      charge_with_stripe!
    else
      charge_with_invoice_only!(paid: false, paid_with: 'Invoice')
    end

    payment_attrs = {amount: @amount, user_id: @user.id, composition_id: @composition.id, cart_id: @cart.id}
    payment_attrs.merge!({transaction_id: @charge.try(:id)}) if @charge
    Payment.create(payment_attrs)

    @score.promoted! if @options.fetch(:promotions, nil)

    self
  end

  def charge_with_stripe!
    @amount = @cart.reload.grand_total
    @charge = Stripe::Charge.create(
      :amount      => (@amount * CENT).to_i,
      :description => "Cart##{@cart.id}- Composition##{@composition.id} - Price: $#{@amount} (After discount if applicable)",
      :currency    => @cart.currency || 'EUR',
      :source      => @params[:stripeToken]
    )

    if @charge.present? && @charge.paid?
      @cart.recalculate_total_price
      charge_with_invoice_only!(paid: true, paid_with: 'Credit Card')
    end

    self

  rescue Stripe::CardError => e
    @errors << e.message
  end

  def charge_with_invoice_only!(paid: false, paid_with: )
    @invoice.assign_attributes(
      {
        title:          "#{@composition.title}",
        invoicing_date: Date.current,
        user_id:        current_user.id,
        cart_id:        @cart.id,
        paid_with:      'Credit Card',
        vat_number:     current_user.vat_id,
        due_date:       Date.current,
        description:    "Your subscription invoice for the composition: #{@composition.title}"
      }
    )

    @invoice = build_invoice_item(@invoice)

    if paid
      @invoice.paid_at = Date.current

      @invoice.state = Invoice::STATUS_PAID
    else
      @invoice.state = Invoice::STATUS_DUE
    end

    @invoice.save!

    deliver_emails

    self
  end

  def build_invoice_item(invoice)
    @cart.cart_items.each do |item|
      invoice.general_invoice_items.build(
        {
          description:        "#{item.score.product.title}",
          quantity:           item.quantity,
          unit_cost_cents:    (@amount * CENT).to_i,
          unit_cost_currency: @cart.currency
        }
      )
    end

    @cart.save!

    invoice
  end

  def deliver_emails
    PromoterMailer.promote(current_user, @score, @invoice).deliver_now
  end

  def find_or_create_customer
    if @user.stripe_customer_id.present?
      @customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
    else
      @customer = Stripe::Customer.create(:email => @user.email, :source => @params[:stripeToken])
    end

    self
  end

  NoCartItemAvailable = Class.new(StandardError)
end
