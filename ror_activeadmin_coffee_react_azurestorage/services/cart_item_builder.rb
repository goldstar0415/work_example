class CartItemBuilder

  def self.build(cart, score)
    new(cart, score).build
  end

  def initialize(cart, score)
    @cart  = cart
    @score = score
  end

  def build
    price             = @score.user_price
    transcriber_price = @score.transcriber_price

    cart_item         = @cart.cart_items.where(score_id: @score.id).first_or_create do |cart_item|
      cart_item.transcriber_price = transcriber_price
      cart_item.price             = price
      cart_item.discounted_price  = price
      cart_item.quantity          = 1
      cart_item.currency          = @cart.currency || 'EUR'
    end

    cart_item
  end
end
