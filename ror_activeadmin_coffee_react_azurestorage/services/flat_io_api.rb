require 'rest-client'
require 'open-uri'

class FlatIoApi

  def self.list_scores
    begin
      FlatApi::ScoreApi.new.get_user_scores('me')
    rescue FlatApi::ApiError => e
      puts "Exception when calling ScoreApi->get_user_scores: #{e}"
    end
  end

  def self.get_score(slug)
    begin
      FlatApi::ScoreApi.new.get_score(slug)
    rescue FlatApi::ApiError => e
      puts "Exception when calling ScoreApi->get_score: #{e}"
    end
  end

  def self.edit_score(slug, attributes = {})
    opts = {body: FlatApi::ScoreModification.new(attributes)}

    begin
      FlatApi::ScoreApi.new.edit_score(slug, opts)
    rescue FlatApi::ApiError => e
      puts "Exception when calling ScoreApi->edit_score: #{e}"
    end
  end

  def self.delete_score(slug)
    begin
      FlatApi::ScoreApi.new.delete_score(slug)
    rescue FlatApi::ApiError => e
      puts "Exception when calling ScoreApi->delete_score: #{e}"
    end
  end

  def self.create_score(title, file_path)
    score_data = open(file_path, "r:UTF-8") { |f| f.read }
    body = FlatApi::ScoreCreation.new({title: title, privacy: "public", data: score_data})

    begin
      FlatApi::ScoreApi.new.create_score(body)
    rescue FlatApi::ApiError => e
      puts "Exception when calling ScoreApi->create_score: #{e}"
    end
  end

end
