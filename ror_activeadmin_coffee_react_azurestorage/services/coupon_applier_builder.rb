class CouponApplierBuilder

  attr_reader :composition, :coupon, :cart

  def self.build(coupon, cart)
    new(coupon, cart).build
  end

  def initialize(code, cart)
    @composition = cart.composition
    @coupon      = Coupon.find_by(code: code)
    @cart        = cart
    @available   = @coupon.present? ? @coupon.available?(@cart.user_id) : false

    validate_coupon
  end

  def validate_coupon
    if @coupon.nil? || ((@coupon.present? && @cart.coupon.present?) && @cart.coupon.id == @coupon.id) || !@available
      @cart.errors.add :coupon, :coupon_is_invalid
    end

    self
  end

  def build
    return self if @cart.errors.present?

    if @available
      @cart.cart_items.each do |item|
        amount = DiscountService.discount!(@coupon, @available, item.price)
        item.update_columns(price: amount, discounted_price: item.price - amount)
      end

      @cart.apply_coupon(coupon)
      @cart.reload
      Rails.logger.info "============ Final Price ============="
      Rails.logger.info @cart.price.to_s
      Rails.logger.info "============ Discounted Price ============="
      Rails.logger.info @cart.discount.to_s
    else
      @cart.errors.add :coupon, :coupon_is_invalid
    end

    self
  end


  def saved?
    @saved === true
  end

  def save!
    @saved = false

    Composition.transaction do

      yield # builder

      if @cart.errors.empty?
        @saved = true
      end
    end
  end
end
