class CreatorWizard

  attr_reader :next_step, :current_step, :previous_step, :composition, :saved, :cart, :errors

  # Steps to perform wizard action
  #   on server side
  AUDIO_UPLOADING_STEP = 1
  ADD_DETAILS_STEP     = 2
  ADD_VERSION_STEP     = 3
  CHECKOUT_STEP        = 4

  # Builder method to build composition
  #   topic step by step
  #
  # @param [User] current user of the session
  # @param [ActionController::Parameters] params of current request
  # @param [Integer] step from allowed 4 steps
  # @param [Integer | Nil] composition_id if available
  def self.build(current_user, params, cart = nil, step = 1, composition_id = nil)
    new(
      current_user,
      step,
      params,
      composition_id,
      cart
    ).build
  end

  # Constructor method to build composition
  #   topic step by step through object for
  #   thread safe operation
  #
  # @param [User] current user of the session
  # @param [Integer] step from allowed 4 steps
  # @param [ActionController::Parameters] params of current request
  # @param [Integer | Nil] composition_id if available
  def initialize(current_user, step, params, composition_id, cart = nil)
    @current_step   = step.to_i
    @next_step      = @current_step
    @previous_step  = @current_step - 1
    @params         = params
    @current_user   = current_user
    @composition_id = composition_id.presence || cart.try(:composition_id).presence
    @composition    = find_or_build_new_composer
    @saved          = false
    @cart           = cart || find_or_create_new_cart

    # checkout if the currency is updated or not present
    update_cart_currency

    self
  end

  def update_cart_currency
    if !@cart.currency.nil? && !(@cart.currency == (@cart.user.try(:currency).code || 'EUR'))
      @cart.currency = @cart.user.currency.try(:code)
      @cart.save!
    end
  end

  def saved?
    @saved === true
  end

  # Ensure [Cart] contains [Composition] id
  def find_or_create_new_cart
    Cart.open.where(user_id: @composition.user_id).first_or_create do |cart|
      cart.composition_id = @composition_id
    end
  end

  # Either update current composition
  # Or build a new one (first step)
  #
  # @return [Composition] instance by finding
  #   or creating
  def find_or_build_new_composer
    @composition ||= @current_user.compositions.where(id: @composition_id).first_or_initialize
  end

  def build
    case @current_step
      when CompositionsController::AUDIO_UPLOADING_STEP
        build_with builder: CompositionAudioBuilder
      when CompositionsController::ADD_DETAILS_STEP
        build_with builder: CompositionInfoBuilder
      when CompositionsController::ADD_VERSION_STEP
        build_with builder: ScoreBuilder
      when CompositionsController::CHECKOUT_STEP
        @checkout = CheckoutProcessor.checkout(@current_user, @params, @composition, cart)

        # Verify if payment is made
        # Perform after verification job
        #
        # @return [Composition] with updated status
        @composition = CheckoutVerifier.verify?(@checkout)
      else
        RaiseInvalidStep
    end

    self
  end

  def build_with(builder:)
    @params.merge!({cart_id: @cart.id}) if builder == ScoreBuilder
    @built_info  = builder.build(@composition, @params)

    @composition = @built_info.composition.reload if @built_info.composition.try(:persisted?)
    @cart        = @built_info.cart if @built_info.cart.present?

    if @built_info.saved? && @built_info.errors.blank?
      @current_step  = @current_step += 1
      @next_step     = @current_step + 1
      @previous_step = @current_step - 1
      @saved         = true
    else
      @errors = @built_info.errors
      @saved  = false
    end

    self
  end

  class RaiseInvalidStep < ArgumentError;
  end
end
