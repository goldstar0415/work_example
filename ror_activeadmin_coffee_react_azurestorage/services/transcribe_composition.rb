require 'fileutils'
require 'rest-client'

class TranscribeComposition
  attr_reader :composition, :input_audio, :output_xml, :output_mxml, :input_xml, :lyric_file, :locale

  def self.transcribe(composition_id)
    new(composition_id).call_transcribe
  end

  def self.transcribe_with_xml(composition_id, input_xml)
    new(composition_id, input_xml).call_transcribe_with_xml
  end

  def initialize(composition_id, input_xml = nil)
    output_dir = "public/audio_player_xml/composition-#{composition_id}/"
    output_basename = "#{output_dir}/original"
    FileUtils::mkdir_p File.absolute_path(output_dir)

    @composition = Composition.find composition_id

    @input_audio = File.absolute_path(composition.audio_file.path)
    @output_xml = File.absolute_path("#{output_basename}.xml")
    @output_mxml = File.absolute_path("#{output_basename}.mxml")
    @lyric_file = File.absolute_path(composition.lyric_file_path)
    @locale = composition.product_language.lang_locale.downcase
    @input_xml = input_xml
  end

  def call_transcribe
    if RemoteTranscriber.transcribe(params)
      mark_scores_as_default
    end
  end

  def call_transcribe_with_xml
    if RemoteTranscriber.transcribe_with_xml(params.merge(input_xml: input_xml))
      mark_scores_as_default
    end
  end

  private

  def mark_scores_as_default
    composition.scores.each(&:default!)
  end

  def params
    {
      input_audio: input_audio, output_xml: output_xml, output_mxml: output_mxml,
      lyric_file: (File.file?(lyric_file) ? lyric_file : nil), locale: locale
    }
  end
end
