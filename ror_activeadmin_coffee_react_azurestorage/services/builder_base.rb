class BuilderBase

  NotImplementedError = Class.new(StandardError)

  attr_reader :cart, :coupon, :errors

  def self.build(composition, params)
    new(params, composition).build
  end

  def initialize(params, composition)
    @cart   = Cart.none
    @coupon = Coupon.none
    @errors = []
  end

  def build
    raise NotImplementedError
  end

  def saved?
    raise NotImplementedError
  end

  def save!
    raise NotImplementedError
  end
end
