class BackupDatabase
  def self.dump
    new.dump
  end

  def dump
    system(command)
  end

  def command
    # "pg_dump --dbname=postgresql://username:password@127.0.0.1:5432/mydatabase"
    "pg_dump --dbname=#{url} > #{filename}.sql"
  end

  def filename
    @filename ||= [:backup, db[:database], Time.now.to_s(:usec)].join('-')
  end

  def db
    @db = ActiveRecord::Base.connection_config
  end

  def url
    creds = db[:username].to_s
    creds.concat ":#{db[:password]}" if db[:password]
    host = db[:host].to_s
    host.concat ":#{db[:port]}" if db[:port]

    url = 'postgresql://'
    url.concat "#{creds}" if creds.present?
    url.concat "#{host}" if host.present?
    url.concat "/#{db[:database]}" if db[:database].present?
    url
  end
end
