class ScoreBuilder < BuilderBase
  attr_reader :composition, :cart

  # Builder method
  def self.build(composition, params)
    new(
      composition,
      params,
    ).build
  end

  def initialize(composition, params, cart_id = nil)
    @params                 = params
    cart_id               ||= params[:cart_id]
    @composition            = composition
    @composition_attributes = {lyrics: params[:lyrics], product_language_id: params[:product_language_id]} if params[:lyrics].present?

    @cart                   = Cart.includes(:cart_items).open.where(id: cart_id, user_id: @composition.user_id).first_or_create do |cart|
        cart.composition_id = @composition.id
    end

    if @cart.cart_items.present?
      # Every time we will reassign rather then append to avoid duplicate item.
      @cart.cart_items.delete_all
      @cart.composition.scores.collect(&:destroy) if @cart.composition.present?
      @cart.update_columns(price: 0, quantity: 0, discount: 0, coupon_id: nil) # reset cart
    end
  end

  def build
    save! do
      @scores_attributes = @params[:scores_attributes].values.map do |score_attribute|
        score             = {}
        score[:scoring]   = score_attribute[:scoring]
        score[:product_id]= score_attribute[:product_id]
        score[:difficulty]= score_attribute[:difficulty]
        score[:has_lyrics]= score_attribute[:has_lyrics]
        score[:has_chords]= score_attribute[:has_chords]
        score[:temp]      = true
        score[:genre]     = @composition.genre
        score[:status]    = @composition.is_audio? ? :preprocessing : :default
        score
      end

      @composition.assign_attributes(scores_attributes: @scores_attributes)
      @composition.assign_attributes(@composition_attributes) unless @composition_attributes.blank?
    end

    self
  end

  def saved?
    @saved === true
  end

  def build_cart_items(score)
    CartItemBuilder.build(@cart, score)
  end

  def save!
    @saved = false

    Composition.transaction do

      yield # builder

      if @composition.save
        @composition.scores.each do |score|
          build_cart_items(score)
        end
        @cart.retouch_price_quantity if @cart.persisted? && @cart.reload.cart_items.present?

        @saved = true
      else
        @errors = @composition.errors.messages
        @saved = false
      end
    end
  end
end
