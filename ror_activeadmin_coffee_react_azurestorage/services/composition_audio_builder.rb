# This CompositionAudioBuilder class is a
# standalone service, that save audio files to composition
# and extra meta information from the audio file
require 'taglib'

class CompositionAudioBuilder < BuilderBase

  attr_reader :composition, :audio_params

  # Builder method
  def self.build(composition, params)
    new(
      composition,
      params,
    ).build
  end

  def initialize(composition, params)
    super

    @saved = false
    @composition = composition
    @audio_params = params
    @language_id = @audio_params.require(:language_id)
    @audio_file = @audio_params.require(:audio_file)
    @original_filename = @audio_file.original_filename
    @file_name = @original_filename.split('.')[0]
  end

  def saved?
    @saved === true
  end

  def build
    save! do
      assign_audio_file
      update_meta_data
    end

    self
  end

  def assign_audio_file
    @composition.title = @file_name
    @composition.audio_file = @audio_file
    @composition.product_language_id = @language_id

    self
  end

  def save!
    @saved = false

    Composition.transaction do

      yield

      if @composition.save
        @saved = true
      else
        @errors = @composition.errors.messages
        @saved = false
      end
    end

    self
  end

  def assign_meta_data(tags)
    genre_str = tags.delete(:genre)
    second_relations = build_artist_attributes(
      tags.delete(:artist)
    )

    tags.merge!(second_relations)

    @composition.assign_attributes(tags)
  end

  def find_genre(genre_str)
    Score::GENRE.find {|genre| genre.to_s.match(genre_str.underscore)}
  end

  def build_artist_attributes(author_str)
    authors_attributes = author_str.nil? ? [] : find_artists(author_str)
    {authors_attributes: authors_attributes}
  end

  def find_artists(artist_str)
    artist_lists = []
    artist_str.split(',').each do |artist|
      names = artist.strip.split(/ (?=\S+$)/)

      artist = {}
      artist[:author_type] = Author::COMPOSER

      artist[:first_name] = names.size > 1 ? names.first : nil
      artist[:last_name] = names.last

      artist_lists << artist
    end

    artist_lists
  end

  def update_meta_data
    @tags = {}
    @tags = find_tags if @composition.is_audio?
    @tags.merge!({title: @file_name}) if @tags[:title].blank?
    @tags.merge!({artist: I18n.t('my_soundnotation.options.creator.metadata.please_insert_composer')}) if @tags[:artist].blank?
    assign_meta_data(@tags) unless no_tag?

    self
  end

  def no_tag?
    @tags.values.select {|value| value.present?}.empty?
  end

  def find_tags
    AudioMetaDataExtractor.extract!(
      @composition.audio_file.path,
      {
        title: :title,
        artist: :artist,
        genre: :genre,
      }
    ).tags
  end

end
