module YearToWords
  module Wrappers
    class Integer
      attr_accessor :number

      def initialize(number)
        @number = number
      end

      def n_to_words(locale)
        res = ''
        hundred = I18n.t('numbers.hundreds', locale: locale)
        ab = @number / 100
        cd = @number % 100
        ab_s = I18n.with_locale(locale) { ab.to_words }
        cd_s = I18n.with_locale(locale) { cd.to_words }
        number_to_words = I18n.with_locale(locale) { @number.to_words }
        case locale
        when :en
          cd_s.prepend('oh ') if cd < 10
          cd_s = hundred if cd == 0
          ab == 10 ? res << number_to_words : res << [ab_s, cd_s].join(' ')
        when :de
          thousand = I18n.t('numbers.thousands.one', locale: :de)
          thousand = number_to_words if @number == 1000
          thousand = ab == 10 ? thousand : ab_s + hundred
          cd_s = '' if cd == 0
          res << [thousand, cd_s].join('')
        end
        res.empty? ? number_to_words : res
      end
    end
  end
end