# This standalone class is responsible for
# extracting meta information from
# audio file

# Just pass the audio path so it can read!
#
# Additionally, can pass specific attribute set.
# In the attribute argument key will remain same
# and the value from attribute list
# will be working as a original tag attribute
#
# {title: :title} will be

# converted into: {
#   title: 'One day'
# }
#
# with different key name, { name: :title }:
# {
#     name: 'One day'
# }
#
# Attribute value must be listed value of PERMITTED_ATTRIBUTE only.

class AudioMetaDataExtractor

  # List of Permissible attributes only
  PERMITTED_ATTRIBUTE = [:name, :genre, :artist, :album]

  # A Default attribute list If no attribute
  #   set is provided
  DEFAULT_ATTRIBUTES  = {
    title: :title,
    genre: :genre
  }

  attr_reader :tags

  # static Extractor method of this class.
  def self.extract!(file, attributes = {})
    new(file, attributes).extract!
  end

  # Constructor, require a file.
  def initialize(file, attributes = {})
    @file       = file
    @attributes = attributes.present? ? attributes : DEFAULT_ATTRIBUTES
    @tags       = {}
  end

  # To perform extraction
  def extract!
    extract_tag!

    self
  end

  # Building hash based on given attribute
  def build_tags(tag)
    @tags = Hash[
      @attributes.map do |key, value|
        [key, tag.send(value)]
      end
    ]
  end

  # Find tag information from the file
  #
  def extract_tag!
    tag = TagLib::FileRef.open(@file) do |file_ref|
      build_tags(file_ref.tag) unless file_ref.null?
    end
    tag[:title] = remove_brackets(tag[:title])
    tag[:title] = remove_words(tag[:title])
    tag

    self
  end

  def remove_words(s)
    s = "" if s.nil?
    ['mp3', 'MP3'].each do |word|
      s = [s[0..s.index(word)-1].strip, s[s.index(word)+word.size..-1].strip].reject(&:empty?).join(' ') if s.include?(word)
    end
    s
  end

  def remove_brackets(s)
    s = "" if s.nil?
    [['(', ')'], ['[', ']'], ['{', '}']].each do |ob, cb|
      s = [s[0..s.index(ob)-1].strip, s[s.index(cb)+1..-1].strip].reject(&:empty?).join(' ') if s.include?(ob) && s.include?(cb)
    end
    s
  end

  def validate_valid_tags!
    @attributes.keys.each do |attribute|
      verify_valid_tag!(attribute)
    end

    true
  end

  def verify_valid_tag!(attribute)
    raise InvalidTagAttribute if PERMITTED_ATTRIBUTE.exclude?(attribute.to_sym)
  end

  class InvalidTagAttribute < ArgumentError;
  end
end