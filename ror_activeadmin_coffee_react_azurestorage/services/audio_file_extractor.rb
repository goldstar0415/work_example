class AudioFileExtractor
  def self.extract!(file)
    new(file).extract
  end

  def initialize(file)
    @file = file
  end

  def extract
    @info = {
      :title  => get_name,
      :artist => get_artist,
      :genre  => get_genre
    }
  end

  def get_name
    file.original_filename
  end

  def get_artist

  end

  def get_genre

  end
end