require 'rest-client'

class SoundSliceApi

  attr_reader :api_end, :app_id, :password, :settings

  def initialize
    @app_id = 'EEsuKuRQZQRLXTpuGQDeJDtKdXSkcXqM'
    @password = 'EWKyCJt:g2IHZ,Gx[?M.W?VsC0X957?d'
    @endpoint = 'https://www.soundslice.com/api/v1'
  end

  def create_score(name, artist)
    params = {
      name: name,
      artist: artist,
      status: 3,
      embed_status: 2,
      print_status: 3
    }

    call_api(:post, 'scores', params)
  end

  def list_scores
    call_api(:get, 'scores')
  end

  def get_score(slug)
    call_api(:get, "scores/#{slug}")
  end

  def delete_score(slug)
    call_api(:delete, "scores/#{slug}")
  end

  def get_score_notation(slug)
    call_api(:get, "scores/#{slug}/notation")
  end

  def upload_score_notation(slug, file_path)
    api_response = call_api(:post, "scores/#{slug}/notation")
    aws_path = api_response['url']

    response = RestClient::Request.new(
      :method => :put,
      :url => aws_path,
      :payload => open(file_path),
      :headers => {content_type: ''}
    ).execute

    success = response.code.to_i == 200
    OpenStruct.new({response: response, success: success, code: response.code.to_i})
  end

  private

  def call_api(method, api, params={})
    response = RestClient::Request.new(
      :method => method,
      :url => "#{@endpoint}/#{api}/",
      :user => @app_id,
      :password => @password,
      :payload => params,
      timeout: 30000
    ).execute

    JSON.parse(response)
  rescue => e
    Rails.logger.error(e.message)
  end

end
