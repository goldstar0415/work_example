class CompositionInfoBuilder < BuilderBase

  attr_reader :composition

  def self.build(composition, params)
    new(composition, params).build
  end

  def initialize(composition, params)
    super

    @params = params
    @composition = composition
  end

  def build
    @authors_attributes = []
    @copyrights_attributes = []
    @main_composer = {}

    lyrics = @params[:lyrics]
    lyrics_exist = lyrics.present? ? true : false

    @composition.contributions.destroy_all

    save! do
      # previous_main_contribution = @composition.contributions.find_by(main_composer: true) || @composition.contributions.order(created_at: :asc).first
      # @previous_main_composer = previous_main_contribution.author
      @params[:authors_attributes].each do |number, artist|
        names = artist[:full_name_str].strip.split(/ (?=\S+$)/)

        author = {}
        author[:id] = artist[:id]
        author[:author_type] = artist[:author_type]
        author[:first_name] = names.size > 1 ? names.first : nil
        author[:last_name] = names.last

        author_not_to_modify = Author.find_by_id(author[:id])
        author[:id] = "" unless author_not_to_modify.present? && author_not_to_modify.first_name == author[:first_name] && author_not_to_modify.last_name == author[:last_name] && author_not_to_modify.author_type == author[:author_type] # to prevent overwriting the author name with the same id if we go back from step 3 and write a new author in the field of a preexisting one

        @main_composer = author if number.to_i == 1 # first author inserted is the main composer

        @composition.add_author(author[:id])
        @authors_attributes << author
      end

      @composition.copyrights.collect(&:destroy)

      @copyrights_attributes = @params[:copyrights_attributes].values.map do |copyright_attribute|
        copyright = {}
        copyright[:id] = copyright_attribute[:id]
        copyright[:name] = copyright_attribute[:name]
        copyright
      end

      @authors_attributes.compact!
      @composition.assign_attributes(authors_attributes: @authors_attributes) if @authors_attributes.present?
      @copyrights_attributes.compact!
      @composition.assign_attributes(copyrights_attributes: @copyrights_attributes) if @copyrights_attributes.present?
    end

    if @main_composer.present?
      if @main_composer[:id].present?
        @composition.contributions.find_by(author_id: @main_composer[:id]).update(main_composer: true)
      else
        main_composer = @composition.authors.find_by(author_type: @main_composer[:author_type], first_name: @main_composer[:first_name], last_name: @main_composer[:last_name])
        if main_composer.present?
          main_contribution = main_composer.contributions.find_by(composition_id: @composition.id)
          main_contribution.update(main_composer: true) if main_contribution.present?
        end
      end
    end
    @composition.assign_attributes(product_language_id: @params[:product_language_id])
    @composition.update_columns(title: @params[:title], genre: @params[:genre], lyrics: lyrics, lyrics_exist: lyrics_exist)
    @composition.export_lyrics_text_file # Invoke lyrics exporter

    self
  end


  def saved?
    @saved === true
  end

  def save!
    @saved = false

    Composition.transaction do

      yield # builder

      if @composition.save
        @saved = true
      else
        @saved = false
        @errors = @composition.errors.messages
      end
    end
  end
end
