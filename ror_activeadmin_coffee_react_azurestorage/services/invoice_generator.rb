require 'money'
#
class InvoiceGenerator
#FIXME need refactoring to making capable to deal with both types of
#FIXME invoice: 1. creator invoice, 1. general invoice
  attr_reader :invoice, :composition, :cart, :options, :paid, :errors

  DUE_DATE_DURATION = 7

  def self.generate(composition_id, paid: false, payment_method: 'Invoice Only', **options)
    new(composition_id, paid: paid, payment_method: payment_method, **options).build_invoice!
  end

  def initialize(composition_id, paid: false, payment_method:, **options)
    @composition  = Composition.find(composition_id)
    @user         = @composition.user
    @cart         = @composition.cart
    @options      = options
    @template     = options.fetch(:template, nil) # future reference
    @paid         = paid
    @invoice      = @composition.build_invoice
    @payment_with = payment_method
    @errors       = {}
  end

  def build_invoice!
    update_basic_attributes

    # Maintain invoice price and discount
    update_invoice_price

    # Add score wise invoice items and their prices
    build_invoice_item

    update_paid_status

    # Persist invoice
    save!


    self
  end

  private

  def paid?
    @paid === true
  end

  def description
    @composition.subtitle || "This invoice is for #{@composition.title}"
  end

  def update_basic_attributes
    @invoice.assign_attributes(
      {
        title:          @composition.title,
        description:    description,
        invoicing_date: @composition.created_at,
        user_id:        @composition.user_id,
        cart_id:        @cart.id,
        paid_with:      @payment_with,
        vat_number:     @composition.user.vat_id
      }
    )
  end

  def update_invoice_price
    @invoice.amount_cents        = @cart.grand_total * 100
    @invoice.amount_currency     = @cart.currency
    @invoice.discount_cents      = @cart.discount.to_f * 100
    @invoice.discount_currency   = @cart.currency
    @invoice.tax_cents           = @cart.calculate_tax.to_f * 100
    @invoice.shipping_cost_cents = @cart.shipping_cost.to_f * 100
    @invoice.shipping_cost_currency = @cart.currency
  end

  def update_paid_status
    # Default due date
    @invoice.due_date = @composition.created_at + DUE_DATE_DURATION.days
    @invoice.paid_at  = @composition.created_at if paid?

    paid? ? mark_as_paid : mark_as_due

    self
  end

  def mark_as_paid
    @invoice.state = Invoice::STATUS_PAID
  end

  def mark_as_due
    @invoice.state = Invoice::STATUS_DUE
  end

  def build_invoice_item
    @cart.cart_items.each do |item|
      @invoice.invoice_items.build(
        {
          score_id:        item.score_id,
          composition_id:  @composition.id,
          quantity:        item.quantity,
          difficulty:      item.score.try(:difficulty),
          amount_cents:    item.price * 100,
          amount_currency: @cart.currency
        }
      )
    end
  end

  def save!
    unless @invoice.save
      @errors = @invoice.errors.messages
    end

    self
  end
end
