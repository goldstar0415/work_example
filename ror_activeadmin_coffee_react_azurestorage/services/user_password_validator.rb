class UserPasswordValidator

  def self.validate!(user, params)
    new(user, params).validate!
  end

  def initialize(user, params)
    @user   = user
    @params = params
  end

  def validate!
    if updating_password?
      unless current_password.present?
        @user.errors.add :current_password, 'must be provided in order to update password'

        return @user
      end

      unless valid_current_password?
        @user.errors.add :current_password, 'is incorrect'
      end
    end

    @user
  end

  def updating_password?
    (@params[:password] || @params[:password_confirmation]).present?
  end

  def current_password
    @params[:current_password]
  end

  def valid_current_password?
    @user.valid_password?(current_password)
  end
end