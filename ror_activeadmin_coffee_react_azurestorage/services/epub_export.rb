class EpubExport
  def initialize(score = nil, only_one_pdf_page = nil)
    @score = score
    @only_one_pdf_page = only_one_pdf_page
    @composition = @score.composition
    @user = @composition.user
    @workdir = Rails.public_path.join('epub_workdir')
    @epub_path = @workdir.join('epub.epub')
    @cover_blob = @score.cover_thumb
    @pdf_score_file = @score.score_files.only_pdf.first
    @pdf_score_file_blob = @pdf_score_file.path
    @pdf_score_file_filename = @pdf_score_file_blob.file.filename
    @score_pages = []
    @svgs = []
    @font = 'Source Sans Pro'
    @fonts = {
      semibold:     'SourceSansPro-Semibold.ttf',
      regular:      'SourceSansPro-Regular.ttf',
      italic:       'SourceSansPro-LightItalic.ttf'
    }
    @fonts_epub = {
      semibold:    "OEBPS/fonts/#{@fonts[:semibold]}",
      regular:     "OEBPS/fonts/#{@fonts[:regular]}",
      italic:      "OEBPS/fonts/#{@fonts[:italic]}"
    }
    @fonts_orig = {
      semibold:    Rails.root.join('app', 'assets', 'fonts', @fonts[:semibold]),
      regular:     Rails.root.join('app', 'assets', 'fonts', @fonts[:regular]),
      italic:      Rails.root.join('app', 'assets', 'fonts', @fonts[:italic])
    }
    @files = {
      mimetype:   'mimetype',
      container:  'container.xml',
      nav:        'nav.xhtml',
      package:    'package.opf',
      toc:        'toc.ncx',
      style:      'style.css',
      cover:      "cover.#{@cover_blob.file.extension}",
      cover_page: 'coverPage.xhtml',
      title_page: 'sec1-titlePage.xhtml',
      copyright:  'sec2-copyrightPage.xhtml'
    }
    @files_epub = {
      mimetype:   @files[:mimetype],
      container:  "META-INF/#{@files[:container]}",
      nav:        "OEBPS/#{@files[:nav]}",
      package:    "OEBPS/#{@files[:package]}",
      toc:        "OEBPS/#{@files[:toc]}",
      style:      "OEBPS/css/#{@files[:style]}",
      cover:      "OEBPS/images/#{@files[:cover]}",
      cover_page: "OEBPS/xhtml/#{@files[:cover_page]}",
      title_page: "OEBPS/xhtml/#{@files[:title_page]}",
      copyright:  "OEBPS/xhtml/#{@files[:copyright]}"
    }
    @colors = {
      black:       [0, 0, 0, 100]  .to_hex,
      brown:       [33, 71, 77, 32].to_hex,
      light_brown: [0, 42, 57, 40] .to_hex,
      gray_45:     "8C8C8C",
      gray_75:     "404040",
      gray_93:     "111111",
      gray_95:     "0d0d0d"
    }
    @four_three_width = FOUR_THREE[:width]
    @four_three_height = FOUR_THREE[:height]
    @add_svg = AddSvg.new(score: @score, pdf_score_file: @pdf_score_file, colors: @colors, font: @font)
  end

  def design
    FileUtils.mkdir_p(@workdir) unless File.directory?(@workdir)
    # copy cover in workdir
    cover_azure_client = AzureStorageClient.new(@cover_blob, @workdir).tmp_blob(@files[:cover])
    cover_path = cover_azure_client.tmp_file_path
    # copy score file in workdir
    score_file_azure_client = AzureStorageClient.new(@pdf_score_file_blob, @workdir).tmp_blob(@pdf_score_file_filename)
    @pdf_score_file_path = score_file_azure_client.tmp_file_path.to_s
    # set cover size to 595 x 842
    cover_resized = Magick::ImageList.new(cover_path).scale(@four_three_width, @four_three_height)
    cover_resized.write(cover_path)
    @title = @composition.title
    @subtitle = @composition.subtitle || ''
    @main_composer = @composition.main_composer
    @other_composers = @composition.other_authors.composers.order(:last_name, :first_name).to_a
    @composers = [@main_composer] + @other_composers
    @artists = @composition.authors.artists.order(:last_name, :first_name).to_a
    @lyricists = @composition.authors.lyricists.order(:last_name, :first_name).to_a
    @arrangers = @composition.authors.arrangers.order(:last_name, :first_name).to_a
    @creators = @composers + @lyricists
    @contributors = @artists + @arrangers
    @uid = @score.gtin.present? ? @score.gtin.to_s : 'noID'
    @language = @composition.product_language.lang_locale.downcase
    @company = @user.company.present? ? @user.company : @user.full_name
    @copyright = @composition.copyright.present? ? @composition.copyright : @company
    save_mimetype
    save_container
    save_cover_page
    save_title_page
    save_copyright_page
    save_score_file
    save_css
    save_nav     # this must take place after save_score_file!
    save_package # this must take place after save_score_file!
    save_toc     # this must take place after save_score_file!


    FileUtils.rm @epub_path if File.exists? @epub_path
    Zip::File.open(@epub_path, ::Zip::File::CREATE) do |zipfile|
      @files_epub.each do |k,v|
        zipfile.add(v, @workdir.join(File.basename(v)))
      end
      @fonts_epub.each do |k, v|
        zipfile.add(v, @fonts_orig[k])
      end
      @score_pages.each do |v|
        zipfile.add("OEBPS/xhtml/#{v}", @workdir.join(v))
      end
      @svgs.each do |v|
        zipfile.add("OEBPS/images/#{v}", @workdir.join(v))
      end
    end
    {path: @epub_path, dirname: File.dirname(@epub_path), basename: File.basename(@epub_path)}
  end

  def save_mimetype
    File.open(@workdir.join(@files[:mimetype]), 'w'){|f| f.write "application/epub+zip"}
  end

  def save_container
    render_and_save(@files[:container], {
        package: @files[:package]
      })
  end

  def save_cover_page
    render_and_save(@files[:cover_page], {
        language: @language,
        title: @title,
        cover: @files[:cover],
        style: @files[:style],
        author: @composition.author_name_for_sales
      })
  end

  def save_title_page
    render_and_save(@files[:title_page], {
        language: @language,
        title: @title,
        composer: @composition.author_name_for_sales,
        style: @files[:style],
        authors: {
          composer: @composers,
          artist: @artists,
          lyricist: @lyricists,
          arranger: @arrangers
        },
        genre: @score.genre,
        instrumentation: @score.product.title
      })
  end

  def save_copyright_page
    render_and_save(@files[:copyright], {
        language: @language,
        title: @title,
        style: @files[:style],
        company: @company,
        copyright: @copyright,
        address: {
          street: @user.street_house_number,
          zip: @user.zip_code,
          city: @user.city,
          country: @user.country.translations.find_by(locale: "en").name
        },
        website: @user.website #can be nil!!!
      })
  end

  def save_css
    render_and_save(@files[:style], {
        fonts: @fonts,
        gray_45_hex: "8C8C8C"
      })
  end

  def save_score_file
    number_of_pages = @only_one_pdf_page.present? ? 1 : @pdf_score_file.pdf_reader.page_count
    number_of_pages.times do |index|
      score_page_local_path = "sec#{index + 3}-scorePage#{index + 1}.xhtml"
      svg_local_path = "score#{index + 1}.svg"
      svg_path = @workdir.join(svg_local_path)
      @score_pages << score_page_local_path
      @svgs << svg_local_path

      # add svg
      @add_svg.add_svg(score_file_path: @pdf_score_file_path, svg_path: svg_path, index: index, requested: :epub) if @pdf_score_file.present?

      # # fit dimensions
      # fit_dimensions(svg_path, 595, 842)
      # render template
      template = "secX-scorePageY.xhtml"
      render_and_save(template, {
          language: @language,
          title: @title,
          style: @files[:style],
          svg: svg_local_path
        })
      # rename template with right numbers
      FileUtils.mv(@workdir.join(template), @workdir.join(score_page_local_path))
    end
  end

  def save_nav
    render_and_save(@files[:nav], {
        language: @language,
        title: @title,
        style: @files[:style],
        cover_page: @files[:cover_page],
        title_page: @files[:title_page],
        copyright: @files[:copyright],
        score_page: @score_pages[0]
      })
  end

  def save_package
    render_and_save(@files[:package], {
        language: @language,
        uid: @uid,
        title: @title,
        subtitle: @subtitle, #can be nil!!!
        date: Time.now.utc.iso8601,
        preview: @only_one_pdf_page,
        creators: @creators,
        contributors: @contributors,
        copyright: @copyright,
        descriptions: @score.main_descriptions,
        score_pages: @score_pages,
        svgs: @svgs,
        fonts: @fonts,
        files: @files,
        cover_ext: @cover_blob.file.extension.downcase
      })
  end

  def save_toc
    render_and_save(@files[:toc], {
        language: @language,
        uid: @uid,
        title: @title,
        cover_page: @files[:cover_page],
        title_page: @files[:title_page],
        copyright: @files[:copyright],
        score_page: @score_pages[0]
      })
  end

  def fit_dimensions(svg_path, width, height)
    doc = File.open(svg_path) { |f| Nokogiri::XML(f) }
    svg_replace = doc.at_css "svg"
    svg_replace['width'] = width.to_s
    svg_replace['height'] = height.to_s
    svg_replace['viewBox'] = "0 0 #{width} #{height}"
    File.write(svg_path, doc.to_xml)
  end

  def render_and_save(filename, locals)
    s = ActionController::Base.new.render_to_string(file: "epub/#{filename}", locals: locals)
    File.open(@workdir.join(filename), 'w'){|f| f.write s}
  end
end