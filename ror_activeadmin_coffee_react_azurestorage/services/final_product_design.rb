require 'prawn'

class FinalProductDesign
  
  attr_reader :score, :only_cover, :only_impressum, :tmp_file_path, :prawn_cover_pdf, :prawn_impressum_pdf, :final_pdf_path, :final_pdf, :pdf_score_file, :cover_logo, :upload_to_storage, :fonts_hash

  def initialize(score = nil, only_cover = nil, only_impressum = nil, only_one_pdf_page = nil)
    @score = score
    @only_cover = only_cover
    @only_impressum = only_impressum
    @only_one_pdf_page = only_one_pdf_page
    @workdir = Rails.public_path.join('score_export')
    @final_pdf_path = @workdir.join("final.pdf")
    @prawn_cover_pdf = @workdir.join('prawn_cover.pdf')
    @prawn_impressum_pdf = @workdir.join('prawn_impressum.pdf')
    @footer_local_path = "footer.svg"
    @footer_path = @workdir.join(@footer_local_path)
    @final_pdf = CombinePDF.new
    @pdf_score_file = @score.score_files.only_pdf.first
    @score_cover = @score.cover
    @background_image = @score_cover.file.filename
    @background_image_ext = @score_cover.file.extension
    @background_image_name = File.basename(@background_image, ".#{@background_image_ext}")
    @default_backgrounds = ["Cover_Image_1.jpg", "Cover_Image_2.jpg", "Cover_Image_3.jpg"]
    @template_orig = Rails.root.join('app', 'assets', 'pdfs', 'cover_default_pdfs', "#{@background_image_name}.pdf")
    @default_logo = Rails.root.join('app', 'assets', 'images', 'logo-cropped.png')
    @default_logo = Rails.root.join('app', 'assets', 'images', 'logo-cropped-brown.png') if @background_image == @default_backgrounds.second
    @default_logo = Rails.root.join('app', 'assets', 'images', 'logo-cropped-gray.png') if @background_image == @default_backgrounds.third
    @cover_logo = @score.cover_logo.url || @default_logo
    @cover_logo = @score.cover_logo.path if @score.cover_logo_uploading?
    @score_file_svgs_local_paths = []
    @score_file_pdfs_local_paths = []
    @upload_to_storage = false
    @font = 'Source Sans Pro'
    @fonts = {
      semibold:     'SourceSansPro-Semibold.ttf',
      regular:      'SourceSansPro-Regular.ttf',
      italic:       'SourceSansPro-LightItalic.ttf'
    }
    @fonts_hash = {
      bold:   Rails.root.join('app', 'assets', 'fonts', @fonts[:semibold]),
      normal: Rails.root.join('app', 'assets', 'fonts', @fonts[:regular]),
      italic: Rails.root.join('app', 'assets', 'fonts', @fonts[:italic])
               }
    @colors = {
      black:       [0, 0, 0, 100]  .to_hex,
      brown:       [33, 71, 77, 32].to_hex,
      light_brown: [0, 42, 57, 40] .to_hex,
      gray_45:     "8C8C8C",
      gray_75:     "404040",
      gray_93:     "111111",
      gray_95:     "0d0d0d"
    }
    @logo_height = 30.7
    @abs_left_margin = 68
    @add_svg = AddSvg.new(score: @score, pdf_score_file: @pdf_score_file, colors: @colors, font: @font, fonts_hash: @fonts_hash, abs_left_margin: @abs_left_margin, final_pdf: @final_pdf, footer_path: @footer_path) if @pdf_score_file.present?
  end

  def design
    FileUtils.mkdir_p(@workdir) unless File.directory?(@workdir)
    cover_design unless @only_impressum
    impressum_design unless @only_cover
    add_score_file unless @only_cover || @only_impressum
    @final_pdf.save(@final_pdf_path)
    {filename: "#{@score.combined_title}.pdf", final_pdf: @final_pdf, final_pdf_path: @final_pdf_path, tmp_file_path: @workdir, prawn_cover_pdf: @prawn_cover_pdf, prawn_impressum_pdf: @prawn_impressum_pdf, upload_to_storage: @upload_to_storage}
  end

  def cover_design
    if @score.cover_from_user?
      add_user_cover
    else
      case @background_image
      when @default_backgrounds.first
        cover_design_1
      when @default_backgrounds.second
        cover_design_2
      when @default_backgrounds.third
        cover_design_3
      end
    end
  end

  def cover_design_1
    prawn_pdf = Prawn::Document.new(template: @template_orig)
    top_right = prawn_pdf.bounds.top_right
    right_bound = top_right[0]
    absolute_bottom_left = prawn_pdf.bounds.absolute_bottom_left
    left_margin = right_margin = absolute_bottom_left[0]
    top_margin = bottom_margin = absolute_bottom_left[1]
    abs_left_margin = @abs_left_margin - left_margin
    prawn_pdf.font_families.update(@font => @fonts_hash)
    prawn_pdf.font @font
    prawn_pdf.image open(@cover_logo), at: [abs_left_margin, 85 - bottom_margin + @logo_height], height: @logo_height
    prawn_pdf.move_down 180
    prawn_pdf.bounding_box([abs_left_margin, prawn_pdf.cursor], width: 480) do
      prawn_pdf.text @score.author_name_for_sales, size: 26, leading: -2, style: :italic
    end
    prawn_pdf.move_down 7
    prawn_pdf.bounding_box([abs_left_margin, prawn_pdf.cursor], width: 425) do
      prawn_pdf.text @score.composition.title, size: 35, leading: 0, style: :bold
    end
    prawn_pdf.move_down 46
    prawn_pdf.bounding_box([abs_left_margin, prawn_pdf.cursor], width: 425) do
      prawn_pdf.text @score.product.try(:title), size: 26, leading: 5, color: @colors[:gray_75], style: :bold
    end
    prawn_pdf.move_up 1
    prawn_pdf.bounding_box([abs_left_margin, prawn_pdf.cursor], width: 350) do
      prawn_pdf.text @score.composition.copyright, size: 21, leading: 8, color: @colors[:gray_45], style: :bold
    end

    prawn_pdf.render_file @prawn_cover_pdf
    @final_pdf << CombinePDF.load(@prawn_cover_pdf)
  end

  def cover_design_2
    prawn_pdf = Prawn::Document.new(template: @template_orig)
    top_right = prawn_pdf.bounds.top_right
    right_bound = top_right[0]
    absolute_bottom_left = prawn_pdf.bounds.absolute_bottom_left
    left_margin = right_margin = absolute_bottom_left[0]
    top_margin = bottom_margin = absolute_bottom_left[1]
    abs_left_margin = @abs_left_margin - left_margin
    prawn_pdf.font_families.update(@font => @fonts_hash)
    prawn_pdf.font @font
    prawn_pdf.image open(@cover_logo), at: [abs_left_margin, 85 - bottom_margin + @logo_height], height: @logo_height
    prawn_pdf.move_down 350
    prawn_pdf.bounding_box([abs_left_margin, prawn_pdf.cursor], width: 480) do
      prawn_pdf.text @score.author_name_for_sales, size: 26, leading: -2, color: @colors[:brown], style: :italic
    end
    prawn_pdf.move_down 7
    prawn_pdf.bounding_box([abs_left_margin, prawn_pdf.cursor], width: 425) do
      prawn_pdf.text @score.composition.title, size: 35, leading: 0, color: @colors[:brown], style: :bold
    end
    prawn_pdf.move_down 46
    prawn_pdf.bounding_box([abs_left_margin, prawn_pdf.cursor], width: 425) do
      prawn_pdf.text @score.product.try(:title), size: 26, leading: 5, color: @colors[:brown], style: :bold
    end
    prawn_pdf.move_up 1
    prawn_pdf.bounding_box([abs_left_margin, prawn_pdf.cursor], width: 350) do
      prawn_pdf.text @score.composition.copyright, size: 21, leading: 8, color: @colors[:brown], style: :bold
    end

    prawn_pdf.render_file @prawn_cover_pdf
    @final_pdf << CombinePDF.load(@prawn_cover_pdf)
  end

  def cover_design_3
    prawn_pdf = Prawn::Document.new(template: @template_orig)
    top_right = prawn_pdf.bounds.top_right
    right_bound = top_right[0]
    absolute_bottom_left = prawn_pdf.bounds.absolute_bottom_left
    left_margin = right_margin = absolute_bottom_left[0]
    top_margin = bottom_margin = absolute_bottom_left[1]
    abs_left_margin = @abs_left_margin - left_margin
    prawn_pdf.font_families.update(@font => @fonts_hash)
    prawn_pdf.font @font
    prawn_pdf.image open(@cover_logo), position: :center, vposition: top_right[1] - (85 - bottom_margin + @logo_height), height: @logo_height
    prawn_pdf.move_down 200
    prawn_pdf.bounding_box([abs_left_margin, prawn_pdf.cursor], width: right_bound - 2 * abs_left_margin) do
      prawn_pdf.text @score.author_name_for_sales, size: 26, leading: -2, style: :italic, align: :center
    end
    prawn_pdf.move_down 7
    prawn_pdf.bounding_box([abs_left_margin, prawn_pdf.cursor], width: right_bound - 2 * abs_left_margin) do
      prawn_pdf.text @score.composition.title, size: 35, leading: 0, style: :bold, align: :center
    end
    prawn_pdf.move_down prawn_pdf.cursor - 190
    prawn_pdf.bounding_box([abs_left_margin, prawn_pdf.cursor], width: right_bound - 2 * abs_left_margin) do
      prawn_pdf.text @score.product.try(:title), size: 26, leading: 5, color: @colors[:gray_75], style: :bold, align: :center
    end
    prawn_pdf.bounding_box([abs_left_margin, prawn_pdf.cursor], width: right_bound - 2 * abs_left_margin) do
      prawn_pdf.text @score.composition.copyright, size: 21, leading: 8, color: @colors[:gray_45], style: :bold, align: :center
    end

    prawn_pdf.render_file @prawn_cover_pdf
    @final_pdf << CombinePDF.load(@prawn_cover_pdf)
  end

  # to use if user uploaded own cover
  def add_user_cover
    # # Retrieve page size
    # page_size = [595, 842]
    # page_size = @pdf_score_file.page_size unless @pdf_score_file.nil?

    # Cover Image file
    if @score_cover.present?
      if @score.cover_uploading?
        # if cover is uploading: we're getting the user cover image from the cache: the uploader path points temporarily to server tmp
        cover_image = Magick::Image.read(@score_cover.path).first
        cover_image.write(@prawn_cover_pdf)
        pdf = CombinePDF.load(@prawn_cover_pdf)
      else
        # otherwise, the cover is downloaded from storage
        azure_client = AzureStorageClient.new(@score_cover, @workdir).convert_image_blob(@prawn_cover_pdf)
        pdf = CombinePDF.load(azure_client.cover_file_path)
      end
      a4_size = [0, 0, A4[:width_pdf_fit], A4[:height]]
      pdf.pages.each {|p| p.resize a4_size}
      @final_pdf << pdf
    end
  end

  def impressum_design
    prawn_pdf = Prawn::Document.new(page_size: "A4")
    top_right = prawn_pdf.bounds.top_right
    right_bound = top_right[0]
    absolute_bottom_left = prawn_pdf.bounds.absolute_bottom_left
    left_margin = right_margin = absolute_bottom_left[0]
    top_margin = bottom_margin = absolute_bottom_left[1]
    abs_left_margin = @abs_left_margin - left_margin
    footer_text_size = 10
    gray_95_hex = @colors[:gray_95]
    gray_75_hex = @colors[:gray_75]
    prawn_pdf.font_families.update(@font => @fonts_hash)
    prawn_pdf.font @font
    prawn_pdf.move_down 300 # reference with original: 445
    prawn_pdf.bounding_box([abs_left_margin, prawn_pdf.cursor], width: 480) do
      prawn_pdf.text @score.author_name_for_sales, size: 26, leading: -2, style: :italic
    end
    prawn_pdf.move_down 7
    prawn_pdf.bounding_box([abs_left_margin, prawn_pdf.cursor], width: 425) do
      prawn_pdf.text @score.composition.title, size: 35, leading: 0, style: :bold
    end
    table_data = []
    @score.composition.author_lines.each do |k, v|
      table_data << [ v[:column].upcase, v[:lines].join("\n") ] unless v[:lines].empty?
    end
    table_data << [ "Genre".upcase, @score.genre ]
    table_data << [ "Instrumentation".upcase, @score.product.title ]
    prawn_pdf.move_down 38
    prawn_pdf.bounding_box([abs_left_margin, prawn_pdf.cursor], width: 500) do
      prawn_pdf.table(table_data) do
        cells.size = footer_text_size
          cells.padding = [3, 0, 3, 0]
          cells.borders = []
        cells.font_style = :bold
        columns(0).width = 108
        columns(0).text_color = gray_95_hex
        columns(1).text_color = gray_75_hex
      end
    end

    # footer
    prawn_pdf.stroke_color @colors[:gray_75]
    prawn_pdf.stroke_horizontal_line abs_left_margin, right_bound + 100, at: 144
    prawn_pdf.fill_color @colors[:gray_45]
    first_row_y = 123
    second_col_x = right_bound / 2 + 57
    address_col_x = right_bound / 2 + 135
    prawn_pdf.text_box "Unauthorized copying of music is forbidden by law\nand may result in criminal or civil action.", at: [abs_left_margin, first_row_y], size: footer_text_size, leading: 2, style: :bold
    prawn_pdf.text_box "Published by", at: [second_col_x, first_row_y], size: footer_text_size, style: :bold
    prawn_pdf.bounding_box([address_col_x, first_row_y], width: right_bound + 10 - address_col_x) do
      prawn_pdf.text @score.composition.user.company.blank? ? @score.composition.user.full_name : @score.composition.user.company, size: footer_text_size, leading: 2, color: @colors[:gray_75], style: :bold
      prawn_pdf.text @score.composition.user.street_house_number, size: footer_text_size, leading: 2, color: @colors[:gray_45], style: :bold
      prawn_pdf.text "#{@score.composition.user.zip_code} #{@score.composition.user.city}", size: footer_text_size, leading: 2, color: @colors[:gray_45], style: :bold
      prawn_pdf.text @score.composition.user.country.translations.find_by(locale: "en").name, size: footer_text_size, leading: 2, color: @colors[:gray_45], style: :bold
      prawn_pdf.text @score.composition.user.website, size: footer_text_size, leading: 2, color: @colors[:gray_45], style: :bold unless @score.composition.user.website.blank?
    end
    prawn_pdf.fill_color @colors[:gray_75]
    prawn_pdf.text_box "All rights reserved", at: [abs_left_margin, 81], size: footer_text_size, style: :bold
    prawn_pdf.text_box "© #{Time.now.year} by #{@score.composition.copyright}", at: [abs_left_margin, 67], size: footer_text_size + 1, style: :bold
    prawn_pdf.bounding_box([abs_left_margin, 35], width: right_bound - 50) do
      prawn_pdf.text "Transcription by Soundnotation", size: footer_text_size, leading: 2, color: @colors[:gray_45], style: :bold, align: :center
      prawn_pdf.text "<link href='https://soundnotation.com'>www.soundnotation.com</link>", size: footer_text_size, leading: 2, color: @colors[:gray_45], style: :bold, align: :center, inline_format: true
    end

    prawn_pdf.render_file @prawn_impressum_pdf
    @final_pdf << CombinePDF.load(@prawn_impressum_pdf)
  end

  def add_score_file
    if @pdf_score_file.present?
      @upload_to_storage = true
      azure_client = AzureStorageClient.new(@pdf_score_file.path, @workdir).tmp_blob
      @score_file_path = azure_client.tmp_file_path.to_s
      save_score_file
    end
  end

  def save_score_file
    number_of_pages = @only_one_pdf_page.present? ? 1 : @pdf_score_file.pdf_reader.page_count
    number_of_pages.times do |index|
      svg_local_path = "score_file_svg_#{index}.svg"
      svg_path = @workdir.join(svg_local_path)
      @score_file_svgs_local_paths << svg_local_path
      tmp_pdf_local_path = "score_file_pdf_#{index}.pdf"
      tmp_pdf_path = @workdir.join(tmp_pdf_local_path)
      @score_file_pdfs_local_paths << tmp_pdf_local_path

      # add svg
      @add_svg.add_svg(score_file_path: @score_file_path, svg_path: svg_path, tmp_pdf_path: tmp_pdf_path, index: index, requested: :pdf) if @pdf_score_file.present?
    end
  end
end