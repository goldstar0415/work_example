class CheckoutProcessor

  CENT = 100

  UnknownPaymentMethodSelected = Class.new(StandardError)

  attr_reader :coupon, :user, :cart, :composition, :errors

  def self.checkout(user, params, composition, cart)
    new(user, params, composition, cart).charge!
  end

  def initialize(user, params, composition, cart)
    @params      = params
    @composition = composition
    @user        = user
    @charged     = false
    @cart        = cart
    @amount      = cart.grand_total
    @discount    = 0
    @errors      = []
    @address     = @params[:billing_address]
  end

  def refresh_cart!
    @cart.calculate_transcriber_price
    @cart.retouch_price_quantity
    @cart.reload
  end

  def charge!
    raise NoCartItemAvailable, 'Please add scores to cart' if @cart.nil?
    # Cart & Composition finishing job!
    refresh_cart!

    @composition.update_column(:freelancer_price, @cart.total_transcriber_price)
    @composition.update_column(:creator_price, @amount)

    if @params[:stripeToken].present?
      charge_with_stripe!
    else
      charge_with_invoice_only!
    end

    payment_attrs = {
      amount:         @amount,
      user_id:        @user.id,
      composition_id: @composition.id,
      cart_id:        @cart.id
    }

    if @charge
      payment_attrs[:transaction_id] = @charge.try(:id)
    end

    Payment.create(payment_attrs)

    deliver_emails

    @composition.update_column(:checkout, true)

    @cart.completed! if @composition.invoice.present? # if invoice is present then it means we have completed the transaction

    self
  end

  def charge_with_invoice_only!
    build_invoice!(paid: false)

    self
  end

  def build_invoice!(paid: false, paid_with: 'Invoice Only')
    @invoice_generator = InvoiceGenerator.generate(@composition.id, paid: paid, payment_method: paid_with, **{})
    @invoice           = @invoice_generator.invoice

    self
  end

  def charge_with_stripe!
    @amount = @cart.reload.grand_total
    @charge  = Stripe::Charge.create(
      :amount      => (@amount* CENT).to_i,
      :description => "Cart##{@cart.id}- Composition##{@composition.id} - Price: $#{@amount} (After discount if applicable)",
      :currency    => @user.currency.code,
      :source      => @params[:stripeToken]
    )

    @charged = true if @charge.present?

    if @charged
      build_invoice!(paid: true, paid_with: 'Credit Card')
    end

    self

  rescue Stripe::CardError => e
    @errors << e.message

    @charged = false
  end

  def deliver_emails
    if Rails.env.development?
      CreatorMailer.confirm(composition_id: @composition.id, **{billing_address: @address}).deliver
    else
      CreatorMailer.confirm(composition_id: @composition.id, **{billing_address: @address}).deliver_now
    end

    unless @user.admin? || @user.admin_light?
      @composition.scores.each do |score|
        FreelancerMailerJob.perform_async(@composition.id, score.id)
      end
    end
  end

  def find_or_create_customer
    if (@user.stripe_customer_id).present?
      @customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
    else
      @customer = Stripe::Customer.create(
        :email  => @user.email,
        :source => @params[:stripeToken]
      )
    end

    self
  end

  NoCartItemAvailable = Class.new(StandardError)
end
