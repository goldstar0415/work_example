require 'prawn'

class AddSvg
  def initialize(score:, pdf_score_file:, colors:, font:, fonts_hash:nil, abs_left_margin:nil, final_pdf:nil, footer_path:nil)
    @score = score
    @pdf_score_file = pdf_score_file # can be NIL!!!
    @new_layout = @pdf_score_file.updated_at.to_date >= Date.new(2018,7,17)
    @font = font
    @fonts_hash = fonts_hash
    @abs_left_margin = abs_left_margin
    @colors = colors
    @final_pdf = final_pdf
    @footer_path = footer_path
    @four_three_width = FOUR_THREE[:width]
    @four_three_height = FOUR_THREE[:height]
  end

  def add_svg(score_file_path:, svg_path:, tmp_pdf_path:nil, index:, requested:)
    # generate svg
    cmd = ["pdf2svg"]
    cmd << score_file_path
    cmd << svg_path
    cmd << index + 1
    log_and_run(cmd.join(' '))
    # add metadata
    add_metadata(svg_path, index, tmp_pdf_path, requested)
  end

  def add_metadata(svg_path, index, tmp_pdf_path, requested)
    a4_height = A4[:height]
    a4_width = A4[:width]
    a4_four_three_diff = a4_height - @four_three_height

    if requested == :pdf
      prawn_pdf = Prawn::Document.new(page_size: "A4")
      top_right = prawn_pdf.bounds.top_right
      right_bound = top_right[0]
      absolute_bottom_left = prawn_pdf.bounds.absolute_bottom_left
      left_margin = right_margin = absolute_bottom_left[0]
      top_margin = bottom_margin = absolute_bottom_left[1]
      abs_left_margin = @abs_left_margin - left_margin
      prawn_page_top = a4_height - top_margin
      notes_y = (index == 0 || !@new_layout) ? prawn_page_top : prawn_page_top - a4_four_three_diff
      footer_y = index == 0 ? a4_four_three_diff - bottom_margin : prawn_page_top
      notes_position = [-left_margin, notes_y]
      footer_position = [-left_margin, footer_y]

      prawn_pdf.font_families.update(@font => @fonts_hash)
      prawn_pdf.font @font
    end

    font_sizes = {
      title:        23,
      subtitle:     14.5,
      composers:    12.5,
      lyricists:    11,
      copyright:    11,
      page_number:  11
    }
    font_weights = {
      title:        'bold',
      subtitle:     'normal',
      composers:    'normal',
      lyricists:    'normal',
      copyright:    'normal',
      page_number:  'normal'
    }
    colors = {
      title:        @colors[:gray_93],
      subtitle:     @colors[:gray_75],
      composers:    @colors[:gray_93],
      lyricists:    @colors[:gray_75],
      copyright:    @colors[:gray_75],
      page_number:  @colors[:gray_75],
      notes:        @colors[:gray_93]
    }

    x_pos_center = @four_three_width / 2
    x_page_number = 30
    y_diff = {
      title: 46,
      subtitle: 24,
      composers: 35,
      lyricists: 15
    }
    y = {
      copyright:   ((a4_four_three_diff - font_sizes[:copyright] * 0.75) / 2).round(3),
      page_number: ((a4_four_three_diff - font_sizes[:page_number] * 0.75) / 2).round(3)
    }
    [:title, :subtitle, :composers, :lyricists].each do |k|
      y[k] = 0
      y_diff.each do |kk, vv|
        y[k] += vv
        break if kk == k
      end
    end

    if requested == :pdf
      page_number = index + 1
      page_number_x = page_number % 2 == 1 ? x_page_number : @four_three_width - x_page_number
      page_number_text_anchor = page_number % 2 == 1 ? "start" : "end"

      svg_opening_tag =
        '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" ' +
        "width=\"#{@four_three_width}pt\" height=\"#{a4_four_three_diff}pt\" " +
        "viewBox=\"0 0 #{@four_three_width} #{a4_four_three_diff}\" version=\"1.1\">"

      # copyright
      copyright_node = svg_text({
          "html_tag" => "text",
          "y" => y[:copyright] + font_sizes[:copyright] * 0.75,
          "x" => x_pos_center,
          "style" => {
            "font-weight" => font_weights[:copyright],
            "font-size" => font_sizes[:copyright],
            "font-family" => @font,
            "text-anchor" => 'middle',
            "fill" => "##{colors[:copyright]}"
          },
          "inner_html" => "© #{Time.now.year.to_s} #{@score.composition.copyright}"
        })

      # page number
      page_number_node = svg_text({
          "html_tag" => "text",
          "y" => y[:page_number] + font_sizes[:page_number] * 0.75,
          "x" => page_number_x,
          "style" => {
            "font-weight" => font_weights[:page_number],
            "font-size" => font_sizes[:page_number],
            "font-family" => @font,
            "text-anchor" => page_number_text_anchor,
            "fill" => "##{colors[:page_number]}"
          },
          "inner_html" => page_number
        })

      svg_closing_tag = '</svg>'
      footer_doc_str = svg_opening_tag
      footer_doc_str << copyright_node if index == 0
      footer_doc_str << page_number_node if index > 0
      footer_doc_str << svg_closing_tag

      File.open(@footer_path, 'w') { |f| f.write(footer_doc_str) }
      prawn_pdf.svg IO.read(@footer_path), at: footer_position if @new_layout
    end

    #change notes color
    svg = File.read(svg_path)
    File.open(svg_path, "w") { |f| f.write svg.gsub("rgb(0%,0%,0%)", "##{colors[:notes]}") } if @new_layout

    render_methods = [:svg] # values: :prawn (renders using prawn), :svg (edits the svg document and inserts metadata accordingly). keep both values for comparison (e.g. text position etc.)
    if index == 0
      base_values = {
        title:     @score.composition.title,
        subtitle:  @score.composition.subtitle,
        composers: @score.composition.authors.composers,
        lyricists: @score.composition.authors.lyricists
      }
      inner_text = {
        title: @score.composition.title,
        subtitle:  @score.composition.subtitle
      }
      [:composers, :lyricists].each do |v|
        if base_values[v].present?
          inner_text[v] = v.to_s[0..-2].capitalize
          inner_text[v] << 's' if base_values[v].size > 1
          inner_text[v] << ': '
          inner_text[v] << @score.composition.composers_name_sort_last_name if v == :composers
          inner_text[v] << @score.composition.lyricists_name_sort_last_name if v == :lyricists
        end
      end

      # first approach: edit svg
      if render_methods.include?(:svg) && @new_layout
        doc = File.open(svg_path) { |f| Nokogiri::XML(f) }
        svg_main_node = doc.at "svg"

        base_values.each do |k, v|
          if v.present?
            svg_main_node << svg_text({
                "html_tag" => "text",
                "y" => y[k] + font_sizes[k] * 0.75,
                "x" => x_pos_center,
                "style" => {
                  "font-weight" => font_weights[k],
                  "font-size" => font_sizes[k],
                  "font-family" => @font,
                  "text-anchor" => 'middle',
                  "fill" => "##{colors[k]}"
                },
                "inner_html" => inner_text[k]
              })
          end
        end
        File.write(svg_path, doc.to_xml)
      end

      #second approach: use prawn
      if render_methods.include?(:prawn) && requested == :pdf && @new_layout
        base_values.each do |k, v|
          if v.present?
            prawn_pdf.bounding_box([abs_left_margin, prawn_page_top - y[k]], width: right_bound - 2 * abs_left_margin) do
              prawn_pdf.text inner_text[k], size: font_sizes[k], leading: 0, style: font_weights[k].to_sym, align: :center, color: colors[k]
            end
          end
        end
      end
    end
    # # test spacing with border
    # doc = File.open(svg_path) { |f| Nokogiri::XML(f) }
    # svg_main_node = doc.at "svg"
    # border = '<rect
    #    y="3.75"
    #    x="3.75"
    #    height="786.5"
    #    width="588.5"
    #    style="opacity:1;fill:#000000;fill-opacity:0;stroke:#000000;stroke-width:7.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />'
    # svg_main_node << border
    # File.write(svg_path, doc.to_xml)
    if requested == :pdf
      prawn_pdf.svg IO.read(svg_path), at: notes_position
      prawn_pdf.render_file tmp_pdf_path
      @final_pdf << CombinePDF.load(tmp_pdf_path)
    end
  end

  def log_and_run(cmd)
    puts cmd
    Rails.logger.info cmd
    output = Open3.capture3(cmd)
    Rails.logger.info $?
    output.map{|o| Rails.logger.info o }
    output.last.success? || raise("FAILED: #{cmd}")
  end

  def svg_text(params)
    tag = params["html_tag"]
    text = params["inner_html"]
    params.reject!{|k, v| k == "html_tag" || k == "inner_html" || k.blank?}
    res = "<#{tag}"
    params.each do |k, v|
      res << " #{k}"
      if v.class == Hash
        vs = v.reject{|k, v| v.blank?}
        res << "=\"" if vs.present?
        vs.each do |kk, vv|
          res << "#{kk}:"
          vvv = "#{vv}"
          vvv = "#{vvv}px" if kk.to_s == "font-size"
          vvv = "'#{vvv}'" if kk.to_s == "font-family"
          res << "#{vvv};"
        end
        res << "\"" if vs.present?
      else
        res << "=\"#{v}\"" if v.present?
      end
    end
    res << ">#{text}"
    params.present? ? res << "</#{tag}>" : res << '/>'
  end
end