class SubscriptionProcessor

  attr_reader :user, :plan, :cart, :errors

  alias_method :current_user, :user

  def self.subscribe(current_user, score, params, plan, invoice)
    new(current_user, score, params, plan, invoice).proceed!
  end

  def initialize(user, score, params, plan, invoice)
    @errors      = []
    @user        = user
    @plan        = plan
    @score       = score
    @params      = params
    @amount      = Cart::SUBSCRIPTION_PRICE[plan.to_sym]
    @invoice     = @user.invoices.build(amount: Money.new(@amount))
    @composition = @score.composition

    @cart = Cart.includes(:cart_items).open.create do |cart|
      cart.user_id        = current_user.id
      cart.composition_id = @composition.id
      cart.price          = Money.new(@amount).dollars.to_f
    end

    cart_item = @cart.cart_items.create do |cart_item|
      cart_item.price            = @amount
      cart_item.discounted_price = @amount
      cart_item.quantity         = 1
      cart_item.score_id         = score.id
    end

    find_or_create_customer
  end


  def proceed!
    # calculate total price!
    @cart.retouch_price_quantity_sub

    @charge = Stripe::Subscription.create(:customer => @customer.id, :plan => @plan)

    charge_with_invoice_only!(paid: true) if @charge.status == 'active'

    @score.update_columns(
      subscription_created_at: Time.zone.now,
      delivery_status:         :delivery_processing
    )

    self

  rescue Stripe::StripeError => e
    @errors << e.message

    self
  end

  def find_or_create_customer
    if @user.stripe_customer_id.present?
      @customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
      @customer.sources.create({:source => @params[:stripeToken]}) if @customer.sources.data.blank?
      @customer.save
    else
      @customer = Stripe::Customer.create(:email => @user.email, :source => @params[:stripeToken])
      @user.update_column(:stripe_customer_id, @customer.id)
    end
  end

  def charge_with_invoice_only!(paid: false)
    @invoice.assign_attributes(
      {
        title:          "#{@composition.title}",
        invoicing_date: Date.current,
        user_id:        current_user.id,
        cart_id:        @cart.id,
        paid_with:      'Credit Card',
        vat_number:     current_user.vat_id,
        due_date:       Date.current,
        description:    "Your subscription invoice for the composition: #{@composition.title}"
      }
    )

    @invoice = build_invoice_item(@invoice)

    if paid
      @invoice.paid_at = Date.current

      @invoice.state = Invoice::STATUS_PAID
    else
      @invoice.state = Invoice::STATUS_DUE
    end

    @invoice.save!

    deliver_emails

    self
  end

  def build_invoice_item(invoice)
    @cart.cart_items.each do |item|
      invoice.general_invoice_items.build(
        {
          description:        "#{item.score.composition.title} #{item.score.product.title} - #{I18n.t('invoices.show.distribution.fee_year')}",
          quantity:           item.quantity,
          unit_cost_cents:    @amount,
          unit_cost_currency: @cart.currency
        }
      )
    end
    invoice.update(amount: Money.new((invoice.amount.to_f + @cart.tax.to_f) * 100, invoice.amount.currency))
    @cart.save!

    invoice
  end

  def deliver_emails
    DistributorMailer.subscription(current_user, @score, @invoice).deliver_now
  end

end
