require 'open3'
require 'rest-client'

class ScorePreselect
  attr_reader :score, :xml_text, :file_paths, :dir
  def self.process(score, xml_text)
    new(score, xml_text).process
  end

  def initialize(score, xml_text)
    @score = score
    @xml_text = xml_text
    @file_paths = compose_file_paths
  end

  def process
    save_config_xml
    transcriber_api_call
    log_and_run(metadata_cmd)
    update_score_bpm
  end

  private

  def update_score_bpm
    bpm = Nokogiri::XML(xml_text).at_css('BeatGrid').attributes['bpm'].value.to_f.round
    score.update!(status: :in_creation_preselect, beats_per_minute: bpm)
  end

  def save_config_xml
    File.open(file_paths[:config], 'w') {|f| f.puts xml_text}
  end

  def transcriber_api_call
    RemoteTranscriber.transcribe_with_xml(
      input_audio: file_paths[:audio], output_xml: file_paths[:output],
      output_mxml: file_paths[:output_mxml], input_xml: file_paths[:config],
      lyric_file: file_paths[:lyric], locale: score.composition.product_language.lang_locale.downcase,
    )
  end

  def metadata_cmd
    composition = score.composition
    cmd_args = file_paths.values_at(:output_mxml, :output_msa)
    cmd_args << score.product.id
    cmd_args << score.product.title
    cmd_args << (score.product.maximum_difficulties > 0)
    cmd_args << score.difficulty_before_type_cast + 1
    cmd_args.concat composition.slice(:title, :composer_name).values
    cmd_args << (composition.copyright.presence || composition.user.full_name)
    cmd_args << score.has_lyrics.to_s
    file_paths[:lyric].nil? ? cmd_args << "no_lyrics" : cmd_args << file_paths[:lyric].to_s
    cmd_args << score.has_chords.to_s
    cmd_args << score.status.to_s
    cmd_args << composition.composers_name
    cmd_args << composition.artists_name
    cmd_args << composition.lyricists_name
    cmd_args << composition.arrangers_name

    "java -jar ~/MSA.jar #{cmd_args.compact.map(&:inspect).join(' ')}".strip
  end

  def compose_file_paths
    setup_directory
    {
      config: file_path('preselected'),
      output: file_path('output'),
      output_mxml: file_path('output', ext: 'mxml'),
      output_msa: file_path('output-msa'),
      extra: file_path('original', prefix: false), # This is coming from TranscribeComposition
      audio: score.composition.audio_file.path.to_s,
      lyric: File.file?(lyric_file) ? lyric_file : nil,
    }
  end

  def lyric_file
    File.absolute_path(score.composition.lyric_file_path)
  end

  def file_path(name, prefix: true, ext: 'xml')
    file_name = prefix ? "score-#{score.id}-#{name}" : name
    dir.join("#{file_name}.#{ext}").to_s
  end

  def setup_directory
    @dir = Rails.public_path.join('audio_player_xml', "composition-#{score.composition_id}")
    FileUtils.mkdir_p(dir) unless File.directory?(dir)
  end

  def log_and_run(cmd)
    puts cmd
    Rails.logger.info cmd
    output = Open3.capture3(cmd)
    Rails.logger.info $?
    output.map{|o| Rails.logger.info o }
    output.last.success? || raise("FAILED: #{cmd}")
  end
end
