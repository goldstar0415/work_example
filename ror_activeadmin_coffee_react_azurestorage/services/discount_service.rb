class DiscountService
  def self.discount!(coupon, available, price)
    new(coupon, available, price).apply!
  end

  def initialize(coupon, available, price)
    @coupon           = coupon
    @available        = available
    @price            = price
    @discounted_price = price
  end

  def applied?
    !@available
  end

  def apply!
    return @discounted_price if applied?

    case @coupon.discount_type
      when 'fixed_price'
        @discounted_price = @price - @coupon.discount_amount.to_f

        @coupon.update_column(:applied_at, Time.zone.now) unless @coupon.reusable?

        return @discounted_price
      when 'percentage'
        @discounted_price = @price - (@price * @coupon.discount_amount.to_f/100)
        @coupon.update_column(:applied_at, Time.zone.now) unless @coupon.reusable?

        return @discounted_price
      else
        return @discounted_price
    end
  end
end
