class OrderValidation
  def self.validate!(composition, score)
    new(composition, score).validate
  end

  def initialize(composition, score)
    @composition = composition
    @score = score
  end

  def validate
    if @score.leadsheet? || @score.chordsheet? || @score.piano_accompaniment?
      @composition.lyrics.present?
    end
  end
end
