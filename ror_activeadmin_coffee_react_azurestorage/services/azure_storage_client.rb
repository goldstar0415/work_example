class AzureStorageClient

  attr_reader :client, :blob, :content, :file_path, :extension, :result, :tmp_file_path, :cover_file_path

  def initialize(blob = nil, tmp_file_path = nil)
    @blob = blob
    @tmp_file_path = tmp_file_path
    @extension = @blob.file.extension
    @storage_account_name = ENV['AZURE_STORAGE_ACCOUNT_NAME']
    @storage_access_key = ENV['AZURE_STORAGE_ACCESS_KEY']
  end

  def get_blob
    @result, @content = client.blob_client.get_blob(@blob.azure_container, @blob.file.path)

    self
  end

  def tmp_blob(path = nil)
    get_blob
    @tmp_file_path = path.present? ? @tmp_file_path.join(path) : @tmp_file_path.join("score_file.pdf")
    File.open(@tmp_file_path, 'wb') { |f| f.write(@content) }

    self
  end

  def scale_image_blob(page_size, path = nil)
    get_blob
    @cover_file_path = path || @tmp_file_path.join("cover.#{@extension}")
    @tmp_file_path = @tmp_file_path.join("tmp_image.#{@extension}")
    File.open(@tmp_file_path, 'wb') { |f| f.write(@content) }
    @cover_image = Magick::ImageList.new(@tmp_file_path).scale(page_size[0], page_size[1])
    @cover_image.write(@cover_file_path)

    self
  end

  def convert_image_blob(path = nil)
    get_blob
    @cover_file_path = path || @tmp_file_path.join("cover.pdf")
    @tmp_file_path = @tmp_file_path.join("tmp_image.#{@extension}")
    File.open(@tmp_file_path, 'wb') { |f| f.write(@content) }
    @cover_image = Magick::Image.read(@tmp_file_path).first
    @cover_image.write(@cover_file_path)

    self
  end

  def scale_and_convert_image_blob(page_size, path = nil)
    get_blob
    @cover_file_path = path || @tmp_file_path.join("cover.pdf")
    @tmp_file_path = @tmp_file_path.join("tmp_image.#{@extension}")
    File.open(@tmp_file_path, 'wb') { |f| f.write(@content) }
    @cover_image = Magick::ImageList.new(@tmp_file_path).scale(page_size[0], page_size[1])
    # convert only if formats are different
    if File.extname(@tmp_file_path) != File.extname(@cover_file_path)
      @cover_image.write(@tmp_file_path)
      @cover_image = Magick::Image.read(@tmp_file_path).first
    end
    @cover_image.write(@cover_file_path)

    self
  end

  private

  def client
    @client = Azure::Storage::Client.create(storage_account_name: @storage_account_name, storage_access_key: @storage_access_key)
  end

end
