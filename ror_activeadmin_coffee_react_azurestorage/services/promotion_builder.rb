class PromotionBuilder
  attr_reader :score, :composition, :cart, :promotions, :errors

  def self.build(user, score, products)
    new(user, score, products).build
  end

  def initialize(user, score, products)
    @user = user
    @score = score
    @products = products
    @composition = @score.composition
    @promotions = []

    build_cart
    reset_cart_items
  end

  def build_cart
    @cart = Cart.includes(:cart_items).open.where(user_id: @user.id).first_or_create do |cart|
      cart.composition_id = @composition.id
    end
  end

  def reset_cart_items
    @cart.cart_items.delete_all if @cart.cart_items.present?
    @score.promotions.delete_all if @score.promotions.present?
    @cart.update_columns(price: 0, quantity: 0, discount: 0, coupon_id: nil)
  end

  def build
    @products.each do |product|
      promotion = Promotion.create(score_id: @score.id, promoter_product_id: product.id, date: product.date)
      @cart.cart_items.where(score_id: @score.id, promotion_id: promotion.id).first_or_create do |cart_item|
        cart_item.price = product.price
        cart_item.discounted_price = product.price
        cart_item.quantity = 1
      end
      @promotions << promotion
    end

    @cart.retouch_price_quantity if @cart.persisted? && @cart.reload.cart_items.present?

    self
  end

end
