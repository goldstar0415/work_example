require 'rest-client'

class RemoteTranscriber
  attr_reader :success_status, :req_params, :input_xml, :output_paths

  def self.transcribe(input_audio:, output_xml:, output_mxml:, lyric_file: nil, locale: nil)
    new(
      input_audio: input_audio, output_xml: output_xml,
      output_mxml: output_mxml, lyric_file: lyric_file, locale: locale
    ).transcribe
  end

  def self.transcribe_with_xml(input_audio:, output_xml:, output_mxml:, input_xml:, lyric_file: nil, locale: nil)
    new(
      input_audio: input_audio, output_xml: output_xml, input_xml: input_xml,
      output_mxml: output_mxml, lyric_file: lyric_file, locale: locale
    ).transcribe_with_xml
  end

  def initialize(input_audio:, output_xml:, output_mxml:, lyric_file: nil, locale: nil, input_xml: nil, identifier: SecureRandom.hex)
    @req_params = {
      input_audio: read_file(input_audio), lyric_file: read_file(lyric_file), locale: locale, id: identifier
    }
    @output_paths = { output_xml: output_xml, output_mxml: output_mxml }
    @input_xml = read_file(input_xml)
  end

  def transcribe
    # req_endpoint = "http://#{ENV['TRANSCRIPTION_API_HOST_PORT']}/transcribe"
    req_endpoint = transcriber('transcribe')
    call_api(req_endpoint)
    success_status
  end

  def transcribe_with_xml
    # req_endpoint = "http://#{ENV['TRANSCRIPTION_API_HOST_PORT']}/transcribe_with_xml"
    req_endpoint = transcriber('transcribe_with_xml')
    @req_params = req_params.merge(input_xml: @input_xml)
    call_api(req_endpoint)
    success_status
  end

  private

  def make_request(req_endpoint)
    RestClient::Request.execute(method: :post, url: req_endpoint,
                                timeout: 90_000_000, payload: req_params)
  rescue RestClient::ExceptionWithResponse => e
    Rails.logger.fatal('Transcriber service down') if e.is_a?(RestClient::Exceptions::OpenTimeout)
    OpenStruct.new(code: 400, body: e.response.to_json)
  end

  def call_api(req_endpoint)
    start_time = Time.now
    response = make_request(req_endpoint)
    response_time = Time.now - start_time
    @success_status = (response.code.to_i == 200)
    save_record(req_endpoint, response, response_time)
    save_outputs(JSON.parse(response.body)) if success_status
  end

  def save_record(req_endpoint, response, response_time)
    TranscriptionHistory.create(
      api_endpoint: req_endpoint, request_parameters: req_params,
      response_code: response.code, response_body: response.body,
      response_time: response_time.round(4), success: success_status
    )
  rescue => e
    Rails.logger.info "*** Error saving Transcription History: #{e.message} ***"
    Rails.logger.info e.backtrace.join("\n")
  end

  def read_file(path)
    return unless path
    File.new(path, 'rb')
  end

  def transcriber(path)
    host = 'http://23.101.66.39:4040'
    [host, path].join('/')
  end

  def save_outputs(response)
    output_paths.each do |type, path|
      open(path, 'wb') do |file|
        file << open(transcriber(response[type.to_s])).read
      end
    end
  end
end
