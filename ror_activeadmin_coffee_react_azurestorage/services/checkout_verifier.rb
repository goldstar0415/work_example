class CheckoutVerifier
  PaymentProcessingError = Class.new(StandardError)

  def self.verify?(checkout)
    @checkout = checkout
    @composition = checkout.composition

    if @checkout.errors.present?
      @checkout.errors.each do |error|
        @composition.errors.add :base, error
      end

      @composition.declined_payment!

      raise PaymentProcessingError, 'we are unable to charge your card! Please try another time'
    else
      UpdateMailchimpInterestsJob.perform_later(@composition.user.id, {a547e7c3c5: true}) if @composition.user.compositions.payment_paid.blank? # updates "Score ordered?" MailChimp interest to Yes if user has no paid compositions
      @composition.payment_paid!
      if !PurchaseOrder.exists?(user_id: @composition.user.id, composition_id: @composition.id)
        PurchaseOrder.create!(user_id: @composition.user.id, composition_id: @composition.id, email: @composition.user.email)
      end
      TranscriptionJob.perform_async(@composition.id) if @composition.is_audio?
    end

    @composition
  end
end
