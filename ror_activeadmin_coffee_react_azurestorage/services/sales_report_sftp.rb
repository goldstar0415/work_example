require 'net/sftp'
class SalesReportSftp

  attr_reader :tmp_csv_path

  def initialize(date)
    @host = "ftp.libreka.de"
    @username = "5279927"
    @password = "e4mztXCdVW"
    @date = date
    @filename = filename(date)
    @remote_csv_path = "/sales/#{@filename}"
    @tmp_csv_path = Rails.public_path.join(@filename).to_s
  end

  def download!
    begin
      sftp.download!(@remote_csv_path, @tmp_csv_path) do |event, downloader, *args|
        case event
          when :open then
            # args[0] : file metadata
            puts "starting download: #{args[0].remote} -> #{args[0].local} (#{args[0].size} bytes}"
          when :get then
            # args[0] : file metadata
            # args[1] : byte offset in remote file
            # args[2] : data that was received
            puts "writing #{args[2].length} bytes to #{args[0].local} starting at #{args[1]}"
          when :close then
            # args[0] : file metadata
            puts "finished with #{args[0].remote}"
          when :mkdir then
            # args[0] : local path name
            puts "creating directory #{args[0]}"
          when :finish then
            puts "all done! #{@filename}"
            SalesReport.import(@tmp_csv_path)
            SalesReportDownloadTracker.track(@date, @filename)
        end
      end
    rescue => ex
      Rails.logger.error ex.message
    end
  end

  def self.download
    tracker = SalesReportDownloadTracker.last

    end_date = Date.today
    start_date = tracker.present? ? (tracker.date + 1.days) : (end_date - 30.days)

    (start_date..end_date).each do |date|
      SalesReportSftp.new(date).download!
    end
  end

  private

  def sftp
    @sftp = Net::SFTP.start(@host, @username, :password => @password)
  end

  def filename(date)
    "transactions_5279927_#{date.strftime("%Y-%m-%d")}.csv"
  end

end
