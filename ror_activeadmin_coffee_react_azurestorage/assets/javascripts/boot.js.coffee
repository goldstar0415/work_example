jQuery ->
  if $('#new_composition').length > 0
    window.CreatorFormService.init()

  if $(".form-button").length > 0
    window.CreatorForm.init({})

  if $('#payment-form').length > 0
    window.PaymentForm.init({})

  if $('#coupon-gift-form').length > 0
    window.CouponeManager.init({})

  if $('.pricing-content').length > 0
    window.ProductPricing.init({})
