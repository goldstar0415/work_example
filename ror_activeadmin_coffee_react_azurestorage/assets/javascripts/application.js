// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery.turbolinks
//= require jquery.slick
//= require bootstrap-sprockets
//= require bootstrap/modal
//= require bootstrap-table
//= require bootstrap-typeahead-rails
//= require ckeditor/init
// require ckeditor/plugins/html5video/plugin

// require ckeditor/plugins/widget/plugin
// require ckeditor/plugins/widgetselection/plugin

// require ckeditor/plugins/clipboard/plugin

// require ckeditor/plugins/lineutils/plugin
//= require bootstrap-datepicker
//= require turbolinks
//= require i18n
//= require i18n/translations
//= require underscore
//= require select2
//= require highcharts
//= require chartkick
//= require moment
//= require daterangepicker

//= require_tree ./helpers
//= require_tree ./components
//= require_tree ./navigation
//= require boot


$.fn.datepicker.defaults.autoclose = true;

bootbox.setDefaults({
    size: "small",
    title: "Confirm?"
});

$(window).unload(function () {
    sessionStorage.removeItem('is_seen');
});

$(".nav-item").click(function () {
    if ($(".nav-item").hasClass("active")) {
        $(this).removeClass("active");
    } else {
        $(this).addClass("active");
    }
});

_.templateSettings = {
    interpolate: /\{\{=(.+?)\}\}/g,
    evaluate: /\{\{(.+?)\}\}/g,
};

function upperFirst(str) {
    var firstLetter = str.substr(0, 1);
    return firstLetter.toUpperCase() + str.substr(1);
}

$(document).on('click', 'form .add_fields', function (event) {
    var regexp, time;
    time = new Date().getTime();
    regexp = new RegExp($(this).data('id'), 'g');
    $(this).before($(this).data('fields').replace(regexp, time));
    $('textarea').each(function () {
        var textid = $(this).attr('id');
        if (typeof CKEDITOR.instances[textid] === 'undefined') {
            CKEDITOR.replace(textid);
        }
    });
    return event.preventDefault();
});

function formatDate(date){
  var separator = I18n.t('date.formats.separator');
  return new Date(date).toLocaleDateString().split('/').join(separator);
}

function addToDate(date, sumDays){
  var newDate = new Date(date);
  newDate.setDate(newDate.getDate() + sumDays);
  return newDate;
}

function humanize(str) {
    return str.trim().split(/\s+/).map(function (str) {
        return str.replace(/_/g, ' ').replace(/\s+/, ' ').trim();
    }).join(' ').toLowerCase().replace(/^./, function (m) {
        return m.toUpperCase();
    });
}

function acceptTermsCondition() {
    if ($('.terms-condition').is(":checked")) {
        return true;
    } else {
        toastr.warning(I18n.t('sign_up.accept_terms_text'));
        return false;
    }
}

function validateFormRequiredField($wrapper) {
    var input = $($wrapper + ' .required');
    var value = input.filter(function () {
        return this.value != '';
    });

    if ((value.length >= 0 && (value.length !== input.length))) {
        toastr.warning(I18n.t('my_soundnotation.options.creator.metadata.please_fill_in'));
        return false;
    } else {
        return true;
    }
}

function validAddress() {
    var address = $('.user-address').val().trim().split(" ").pop();
    if (address.match(".*\\d.*")) {
        return true;
    } else {
        toastr.warning(I18n.t('sign_up.house_number_needed'));
        return false;
    }
}

function validateEmail() {
    var validEmail = true;
    var email = $('.user-email').val();
    $.ajax({
        type: 'GET',
        url: 'users/with_email',
        async: false,
        data: {email: email},
        success: function (data) {
            if (data.user) {
                toastr.warning(I18n.t('sign_up.email_already_in_use'));
                validEmail = false;
            } else {
                validEmail = true;
            }
        }
    });
    return validEmail;
}

function matchPassword() {
    var password = $('.user-password').val();
    var passwordConfirmation = $('.user-password-confirmation').val();

    if (password.length < 6) {
        toastr.warning(I18n.t('sign_up.password_too_short'));
        return false;
    }

    if (password != passwordConfirmation) {
        toastr.warning(I18n.t('sign_up.passwords_didnt_match'));
        return false;
    }
    return true;
}

//in order to have user sign up modal appear when given its link - e.g. to send user invitations on media
$(document).ready(function () {
    if (window.location.href.indexOf('#user_sign_up') != -1) {
        $('#user_sign_up').modal('show');
    }
});
