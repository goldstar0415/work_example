# It makes api requests!

class window.ServiceManager
  constructor: ()->

  updateCompositionTitle: (composition, formId)->
    $("##{formId}").prepend(
      _.template($('#composition-id-field').html())({composition: composition})
    )

  updateTotalPrice: (cart)->
    $("#total-price").html(
      _.template($('#total-price-tpl').html())({grand_total: cart.grand_total})
    )


  findPrice: (prices, product_id, difficulty = 0)->
    priceObj = _.find(prices, (price)->
      price.product_id == product_id && price.difficulty == difficulty
    )
    if priceObj? then priceObj.value else 0.0

  scorePrices: (product, prices)->
    priceHash = {}
    difficulties = ['beginner', 'easy', 'intermediate', 'early_advanced', 'advanced']
    productPrices = _.filter(prices, (price)->
      price.product_id == product.id
    )

    for difficulty in difficulties
      price = _.find(productPrices, (price)->
        price.difficulty == difficulty
      )
      priceHash[difficulty] = if price? then price.value else 0.0

    return priceHash

  updateCartDetails: (data)->
    $("#order_details").html(
      _.template($('#composition-cart-details').html())({
        composition: data.composition,
        cart: data.cart,
        cart_items: data.cart_items,
        gift_code: data.gift_code
      })

      $("#total-price").html(
        "<p>Total: #{price_with_unit(data.cart.grand_total)}</p>"
      )

      $("#invoice-total-price").html(
        "<p>Total: #{price_with_unit(data.cart.grand_total)}</p>"
      )
    )

  updateCompositionInfo: ->
    self = @
    $('#composition-info-form').ajaxForm
      dataType: 'json'

      uploadProgress: (event, position, total, percentComplete)->

      complete: (data, textStatus, jqXHR, $form)->
        if data.responseJSON.step
          window.composition = data.responseJSON.composition
          window.current_step = data.responseJSON.step

          self.updateCompositionTitle(data.responseJSON.composition, 'score-details-form')
          self.updateCartDetails(data.responseJSON)
          self.updateTotalPrice(data.responseJSON.cart) if data.responseJSON.cart
          self.manageUberchord(data.responseJSON.composition)

          $('#steps a[href="#options"]').tab 'show'
          $('.add-version').show()
          $('.add-details').hide()
          $('.upload-text').hide()
          $('.checkout-section').hide()
        else
          toastr.error(I18n.t('my_soundnotation.creator_form.unable_to_proceed'))

  updateScoring: ->
    self = @

    $('#score-details-form').ajaxForm
      dataType: 'json'

      beforeSubmit: ->
        toastr.info(I18n.t('my_soundnotation.creator_form.updating_information'))
        $(".page-loader").show();

      complete: (data, textStatus, jqXHR, $form)->
        if data.responseJSON.step
          self.updateCartDetails(data.responseJSON)
          self.updateCompositionTitle(data.responseJSON.composition, 'payment-form')
          manipulatePaymentOptions(data.responseJSON.cart.grand_total)

          $(".page-loader").hide();
          $("#steps a[href='#chordsheet']").tab 'show'
          $('.invoice-total-price').hide()
          $('.invoice-sentence').hide()
          $('.checkout-section').show()
          $('.add-version').hide()
          $('.add-details').hide()
          $('.upload-text').hide()
          $('#back_to_options').show()
          $('#back_to_cart').hide()
        else
          toastr.error(I18n.t('my_soundnotation.creator_form.unable_to_proceed'))

  manageUberchord: (composition)->
    self = @
    if composition.lyrics
      $('.selected-card.section-uberchord .btn-uberchord-lyrics').hide()
    else
      $('.selected-card.section-uberchord .btn-uberchord-lyrics').show()

window.ServiceManager.init = => new window.ServiceManager
