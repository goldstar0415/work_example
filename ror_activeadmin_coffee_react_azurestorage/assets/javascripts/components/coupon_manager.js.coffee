# It applys and handle coupon related form and service

class window.CouponeManager
  constructor: ()->
    @applyCoupon()

  applyCoupon: ->
    self = @
    $('#coupon-gift-form').ajaxForm
      dataType: 'json'

      complete: (data, textStatus, jqXHR, $form)->
        if(data.responseJSON.errors && data.responseJSON.errors.length > 0)
          _.each data.responseJSON.errors, (error)->
            toastr.error(error)
        else
          toastr.success(I18n.t('my_soundnotation.options.creator.shop_checkout.coupon_applied_successfully'))
          window.ServiceManager.init().updateTotalPrice(data.responseJSON.cart)
          window.ServiceManager.init().updateCartDetails(data.responseJSON)

window.CouponeManager.init = => new window.CouponeManager