class window.CreatorFormService
  constructor: ()->
    window.authorInputCount ||= 0
    window.lyricsInputCount ||= 1

    @initiateFormAutoSubmit()
    @intializeFormTabs()
    @initiateAudioUploadingService()

  intializeFormTabs: ->
    $('#to_song_info, #wrapper-nav-song-info').click (e) ->
      e.stopImmediatePropagation()
      if validateFormRequiredField('.upload-options-buttons')
        $('#steps a[href="#song-info"]').tab 'show'
        $('.add-details').show()
        $('.upload-text').hide()
        $('.add-version').hide()
        $('.checkout-section').hide()

    $('#back_to_uploads').click (e) ->
      e.preventDefault()
      $('#steps a[href="#upload-file"]').tab 'show'
      $('.upload-text').show()
      $('.add-version').hide()
      $('.add-details').hide()
      $('.checkout-section').hide()

    $('#back_to_song_info').click (e) ->
      e.preventDefault()
      $('#steps a[href="#song-info"]').tab 'show'
      $('.add-details').show()
      $('.add-version').hide()
      $('.checkout-section').hide()
      $('.upload-text').hide()

    $('#back_to_options').click (e) ->
      e.preventDefault()
      $('#steps a[href="#options"]').tab 'show'
      $('.add-version').show()
      $('.checkout-section').hide()
      $('.add-details').hide()
      $('.upload-text').hide()

    $('#back_to_cart, #wrapper-nav-chordsheet').click (e) ->
      e.preventDefault()
      $('.checkout-options').show()
      $('.checkout-credit-card').hide()
      $('.invoice-total-price').show()
      $('.gift-code-label').show()
      $('.gift-code-input').show()
      $('.gift-line').show()
      $('#back_to_options').show()
      $('#back_to_cart').hide()
      $('.payment-select').prop('selectedIndex', 0);
      $('#buy_button').prop("disabled", true)


    $('.lyrics-modal-action-button').click (e) ->
      if ($(this).data('action') == "cancel" && !window.lyricsInputPersisted)
        $('.lyrics-form-input').val(" ");
        $('.preview-lyrics').html("")
      else
        validateLyrics()
        window.lyricsInputPersisted = true
        lyricsText = $('.lyrics-form-input').val()
        lyricsText = lyricsText.split(/\s+/).slice(0, 12).join(" ").concat('...') if lyricsText
        $('.preview-lyrics').html(lyricsText)

  resetProgress: (progress = 0)->
    progressBar = $('#audio_uploading_progress')
    $("#percent-complete").html("#{progress}%")
    progressBar.css(
      {
        'width': "#{progress}%"
      }
    )

    $("#audio_uploading_progress").html("#{progress}%")
    $('#progress_percentage').html(progress)

  initiateFormAutoSubmit: ->
    $('#composition_audio_file').on 'change', ->
      allowedFileExtension = ['mp3', 'wav', 'pdf', 'mscz', 'mscx', 'sib', 'musx', 'xml' , 'mxl']
      fileExtension = $(this).val().split('.').pop().toLowerCase()
      if $.inArray(fileExtension, allowedFileExtension) == -1
        $(this).val('');
        toastr.warning(I18n.t('my_soundnotation.creator_form.upload.allowed_formats') + ': ' + allowedFileExtension.join(', '))
      else
        $('#new_composition').submit()

  initiateAudioUploadingService: ->
    self = @
    progressBar = $('#audio_uploading_progress')
    $("#percent-complete").html("0%")

    $('#new_composition').ajaxForm
      dataType: 'json'

      uploadProgress: (event, position, total, percentComplete)->
        self.resetProgress(percentComplete)

      beforeSubmit: ->
        toastr.info(I18n.t('my_soundnotation.creator_form.updating_information'))

      complete: (data, textStatus, jqXHR, $form)->
        if data.responseJSON.step
          step = data.responseJSON.step
          authors = data.responseJSON.composition.authors
          window.composition = data.responseJSON.composition
          window.current_step = data.responseJSON.step
          default_author = _.first(authors)
          self.loadFirstAuthor(default_author)
          self.appendNewAuthorForm(authors.slice(1))
          self.updateCompositionTitle(data.responseJSON.composition)
          self.manageProductsDisplay(data.responseJSON.composition)
          createTypeahead($('.first-composer'), $('.first-composer-id'), 1, 'composition')

          if step
            switch step
              when 2
                $("#steps a[href='#song-info']").tab 'show'
              when 3
                $("#steps a[href='#options']").tab 'show'
              when 4
                $("#steps a[href='#chordsheet']").tab 'show'

        if(textStatus == 'error' || data.responseJSON.errors && data.responseJSON.errors.length > 0)
          if data.responseJSON.errors
            _.each data.responseJSON.errors, (error) ->
              toastr.error(error)
          else
            toastr.error(I18n.t('my_soundnotation.creator_form.unable_to_proceed'))
          self.resetProgress()


  updateCompositionTitle: (composition)->
    $("#composition-title-field").html(
      _.template($('#composition-title').html())({composition: composition})
    )

  loadFirstAuthor: (composer)->
    self = @

    $("#default-composer-name").html(
      _.template($('#first-composer').html())({composer: composer, composition_form_number: window.authorInputCount})
    )
    window.authorInputCount += 1

  authorTypeList: ->
    {
      'Composer': I18n.t('meta_data.editor_modal.composer'),
      'Lyricist': I18n.t('meta_data.editor_modal.lyricist'),
      'Artist': I18n.t('meta_data.editor_modal.artist'),
      'Arranger': I18n.t('meta_data.editor_modal.arranger')
    }

  appendNewAuthorForm: (authors)->
    self = @

    composer_wrapper = $(".composer-container");
    add_option_button = $(".form-button");

    _.each(authors, (author, index)->
      $(composer_wrapper).append(
        _.template($('#composer-form').html())({
          author_types: self.authorTypeList(),
          author: author,
          composition_form_number: (window.authorInputCount)
        })
      );

      window.authorInputCount += 1
    )

  manageProductsDisplay: (composition)->
    self = @
    if composition.pdf_file
      $(".product-pdf").removeClass("hidden")
      $(".main-card-block").not(".product-pdf").addClass("hidden")
    else if composition.muse_score_file
      $(".product-muse-score").removeClass("hidden")
      $(".main-card-block").not(".product-muse-score").addClass("hidden")
    else if composition.sibelius_file
      $(".product-sibelius").removeClass("hidden")
      $(".main-card-block").not(".product-sibelius").addClass("hidden")
    else if composition.finale_file
      $(".product-finale").removeClass("hidden")
      $(".main-card-block").not(".product-finale").addClass("hidden")
    else if composition.music_xml_file
      $(".product-music-xml").removeClass("hidden")
      $(".main-card-block").not(".product-music-xml").addClass("hidden")
    else
      $(".product-regular").removeClass("hidden")
      $(".main-card-block").not(".product-regular").addClass("hidden")

window.CreatorFormService.init = => new window.CreatorFormService()
