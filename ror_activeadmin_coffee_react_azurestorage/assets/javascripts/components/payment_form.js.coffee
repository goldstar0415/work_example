class window.PaymentForm
  constructor: ()->
    self = @


    if($('#buy_button').prop('disabled') )
      $('.tooltip_handler').tooltip();
    else
      $('.tooltip_handler').hide();



    $('#payment-form').ajaxForm
      dataType: 'json'

      beforeSubmit: ->
        toastr.info(I18n.t('my_soundnotation.creator_form.updating_information'))
        $(".page-loader").show()

      complete: (data, textStatus, jqXHR, $form)->
        $(".page-loader").hide();
        if !(textStatus == 'error')
          $('.checkout-section').hide()
          $('.form-steps').hide()
          $('.checkout-section').hide();
          $('.checkout-information').hide();
          $('.order-confirmation').toggle();
          $('.order-confirmation').append('<input id="paid" type="hidden" value="true">');
          #toastr.success(I18n.t('my_soundnotation.creator_form.shop_checkout.payment_successful'))
        else
          toastr.error(I18n.t('my_soundnotation.creator_form.shop_checkout.payment_error'))

    @setBuyButton()
    @observeFormFields()
    $('.save-address').hide()

    $('.edit-address').click ->
      $('.edit-address').hide()
      $('.billing-address').each ->
        content = $(this).html()
        $(this).html '<input  value="' + content + '">'
        return
      $('.save-address').show()
      return

    $('.save-address').click ->
      billingAddress = ''
      $('.save-address').hide()
      $('.billing-address input').each ->
        content = $(this).val()
        $(this).html content
        $(this).contents().unwrap()
        return
      $('.billing-address').each ->
        billingAddress += $(this).text() + '<br>'
        return
      $('.edit-address').show()
      $('.payment-billing-address').val billingAddress
      return

    if $('.payment-select').val() == 'paypal'
      $('#buy_button').prop("disabled", true)
      $('.checkout-options').show()
      $('.checkout-credit-card').hide()
      $('.gift-code-label').show()

    if $('.payment-select').val() == 'invoice'
      $('#buy_button').prop("disabled", true)
      $('.checkout-options').show()
      $('.checkout-credit-card').hide()
      $('.invoice-total-price').show()
      $('.invoice-sentence').show()
      $('.gift-code-label').show()

    $('.payment-select').change ->
      $('#back_to_options').hide()
      $('#back_to_cart').show()
      if $('.payment-select').val() == 'paypal'
        $('#buy_button').prop("disabled", true)
        $('.checkout-options').show()
        $('.checkout-credit-card').hide()
        $('.gift-code-label').show()
        $('.gift-code-input').show()
        $('.gift-line').show()

      if $('.payment-select').val() == 'credit-card'
        $('.checkout-options').hide()
        $('.checkout-credit-card').show()
        $('.invoice-total-price').hide()
        $('.invoice-sentence').hide()
        $('.gift-code-label').hide()
        $('.gift-code-input').hide()
        $('.gift-line').hide()
        return

      if $('.payment-select').val() == 'invoice'
        $('.checkout-options').hide()
        $('.checkout-credit-card').hide()
        $('.invoice-total-price').show()
        $('.invoice-sentence').show()
        $('.gift-code-label').hide()
        $('.gift-code-input').hide()
        $('.gift-line').hide()
        $('#buy_button').prop 'disabled', false
        $('.tooltip_handler').tooltip('destroy');
        $('.tooltip_handler').hide();
        return

  setBuyButton: ->
    self = @
    $('#buy_button').click (e)->
      self.setStripeToken()
      e.preventDefault()

  observeFormFields: ->
    $('#payment-form input').on 'input', () ->
      $('#buy_button').prop 'disabled', false
      $('.tooltip_handler').tooltip('destroy');
      $('.tooltip_handler').hide();

  setStripeToken: ->
    if($('.payment-select').val() == 'credit-card')
      $form = $('#payment-form')
      Stripe.card.createToken $form, (status, response) ->
        if response.error
          $('#buy_button').prop 'disabled', true
          $form.find('#' + response.error.param).addClass('has-error');
          $('#error-message').html "<strong>"+response.error.message+"</strong>"
          $('#payment-error').removeClass('hide')
        else
          token = response.id
          $form.append $('<input type="hidden" name="stripeToken" />').val(token)
          $form.append $('<input type="hidden" name="coupon" />').val($('#gift-code-input').attr('value'))
          $('#payment-form').submit()
          true
    else
      $('#payment-form').submit()

window.PaymentForm.init = => new window.PaymentForm()
