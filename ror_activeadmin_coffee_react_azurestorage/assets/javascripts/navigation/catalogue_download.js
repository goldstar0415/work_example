$(document).ready(function () {
  var hideDownloadOptions = function () {
    $('.download-options').hide();
    $('.download-image').removeClass('current')
  };

  $("html").click(hideDownloadOptions);

  $('#catalogue').on('click', '.download-image', function (e) {
    e.stopPropagation();
    hideDownloadOptions();
    $(this).next().toggle();
    $(this).toggleClass("current")
  });
});
