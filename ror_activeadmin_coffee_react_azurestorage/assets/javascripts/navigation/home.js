$(document).ready(function() {
  if($(window).width() >= 993){
    $('.creator-card').click(function() {
      $(this).toggleClass('active');
      $('.distributor-card, .promoter-card, .catalogue-card').removeClass('active');
      $('.creator-description-container, .creator-divider').toggle();

      $('.distributor-description-container, .distributor-divider').hide();
      $('.promoter-description-container, .promoter-divider').hide();
      $('.catalogue-description-container, .catalogue-divider').hide();
    });
    $('.creator-description-container').click(function() {
      $('.creator-description-container, .creator-divider').hide();
    })
    $('.distributor-card').click(function() {
      $(this).toggleClass('active');
      $('.creator-card, .promoter-card, .catalogue-card').removeClass('active');
      $('.distributor-description-container, .distributor-divider').toggle();

      $('.promoter-description-container, .promoter-divider').hide();
      $('.catalogue-description-container, .catalogue-divider').hide();
      $('.creator-description-container, .creator-divider').hide();
    });
    $('.distributor-description-container').click(function() {
      $('.distributor-description-container, .distributor-divider').hide();
    })
    $('.promoter-card').click(function() {
      $(this).toggleClass('active');
      $('.creator-card, .distributor-card, .catalogue-card').removeClass('active');
      $('.promoter-description-container, .promoter-divider').toggle();

      $('.catalogue-description-container, .catalogue-divider').hide();
      $('.creator-description-container, .creator-divider').hide();
      $('.distributor-description-container, .distributor-divider').hide();
    });
    $('.promoter-description-container').click(function() {
      $('.promoter-description-container, .promoter-divider').hide();
    });
    $('.catalogue-card').click(function() {
      $(this).toggleClass('active');
      $('.creator-card, .promoter-card, .distributor-card').removeClass('active');
      $('.catalogue-description-container, .catalogue-divider').toggle();

      $('.creator-description-container, .creator-divider').hide();
      $('.distributor-description-container, .distributor-divider').hide();
      $('.promoter-description-container, .promoter-divider').hide();
    });
    $('.catalogue-description-container').click(function() {
      $('.catalogue-description-container, .catalogue-divider').hide();
    });
  }

  $('.references').slick({
    dots: false,
    centerMode: true,
    slidesToShow: 3,
    infinite: true,
    prevArrow: '<i class="prev_arrow"></i>',
    nextArrow: '<i class="next_arrow"></i>',
    responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 2,
      infinite: true,
    }
  },
  {
    breakpoint: 600,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 2
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  }
]
  })
});
