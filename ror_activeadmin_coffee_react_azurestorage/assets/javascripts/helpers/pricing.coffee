class window.ProductPricing
  constructor: ()->
    @scoreNavigation()

  calculateSummary: ->
    _.each($(".difficulty-select").find(':selected'), (option)->
      o = $(option).data()
      scorePrice = o.scorePrice
      parentBlock = $(option).closest('.price-card');
      priceTag = parentBlock.find(".score-price");
      withLyrics = parentBlock.find(".with-lyrics:checkbox").prop('checked');
      withChords = parentBlock.find(".with-chords:checkbox").prop('checked');

      if withLyrics
        scorePrice += 10;
      if withChords
        scorePrice += 10;

      priceTag.text(price_with_unit(scorePrice))
    )

    _.each($(".selected-card-normal"), (option)->
      o = $(option).data()
      scorePrice = o.scorePrice
      priceTag = $(option).find(".score-price");
      withLyrics = $(option).find(".with-lyrics:checkbox").prop('checked');
      withChords = $(option).find(".with-chords:checkbox").prop('checked');

      if withLyrics
        scorePrice += 10;
      if withChords
        scorePrice += 10;

      priceTag.text(price_with_unit(scorePrice))
    )


  scoreNavigation: ->
    self = @

    _.map(window.products, (product, index) ->
      $(".#{product.code} div").on 'click', ->
        $('.with-lyrics').on 'change', ->
          self.calculateSummary()
        $('.difficulty-select, .with-chords').on 'change', ->
          self.calculateSummary()

        scorePrices = window.ServiceManager.init().scorePrices(product, prices)
        $(".section-#{product.code}").removeClass("price-#{product.code}-section").addClass('selected-card')
        if product.code in self.pianoProductList()
          $(".#{product.code}-wrapper").replaceWith '' +
            "<div class=\"new-#{product.code}-wrapper\">" +
            '  <div class="col-sm-12">' +
            '       <div class="checkbox score-checkbox pull-left lyrics-pricing">' +
            "             <label class='#{if product.can_have_lyrics then "" else "hidden"}'><input type='checkbox' class='with-lyrics' name='composition[scores_attributes][#{index}][has_lyrics]' value='1'>" + I18n.t('my_soundnotation.options.creator.add_version.with_lyrics') + "</label>" +
            '       </div>' +
            '       <div class="checkbox chords-checkbox pull-right chords-pricing">' +
            "             <label class='#{if product.can_have_chords then "" else "hidden"}'><input type='checkbox' class='with-chords' name='composition[scores_attributes][#{index}][has_chords]' value='1'>" + I18n.t('my_soundnotation.options.creator.add_version.with_chords') + "</label>" +
            '       </div>' +
            '  </div>' +
            ' <div class="form-group difficulty-pricing">' +
            '   <div class="col-sm-6 difficulty-label-pricing">' +
            '     <label for="difficulty" class="control-label difficulty-label">' + I18n.t('my_soundnotation.options.creator.add_version.difficulty') + '*</label>' +
            '   </div>' +
            "  <div class='col-sm-6'>" +
            "   <select name=\"composition[scores_attributes][#{index}][difficulty]\" class=\"form-control creator-form-input select-background difficulty-select\" >" +
            "     <option data-score-title='#{product.title}' data-score-price='#{scorePrices['beginner']}' data-score-difficulty=\"Beginner\" value=\"beginner\">" + I18n.t('activerecord.attributes.user.beginner') +
            "     </option>" +
            "     <option data-score-title='#{product.title}' data-score-price='#{scorePrices['easy']}' data-score-difficulty=\"Easy\" value=\"easy\">" + I18n.t('activerecord.attributes.user.easy') +
            "     </option>" +
            "     <option data-score-title='#{product.title}' data-score-price='#{scorePrices['intermediate']}' data-score-difficulty=\"Intermediate\" value=\"intermediate\">" + I18n.t('activerecord.attributes.user.intermediate') +
            "     </option>" +
            "     <option data-score-title='#{product.title}' data-score-difficulty=\"Early Advanced\" value=\"early_advanced\" disabled>" + I18n.t('activerecord.attributes.user.early_advanced') + " (coming soon)</option>" +
            "     <option data-score-title='#{product.title}' data-score-difficulty=\"Advanced\" value=\"advanced\" disabled>" + I18n.t('activerecord.attributes.user.advanced') + " (coming soon)</option>" +
            '   </select>' +
            ' </div>' +
            '</div>'
        else
          $(".section-#{product.code}").addClass('selected-card-normal')
          $(".section-#{product.code}").data('score-title', product.code)
          $(".section-#{product.code}").data('score-price', scorePrices['beginner'])
          $(".section-#{product.code}").data('score-difficulty', 'Beginner')
          $(".#{product.code}-wrapper").replaceWith '' +
            "<div class=\"new-#{product.code}-wrapper\">" +
            '  <div class="col-sm-12">' +
            '       <div class="checkbox score-checkbox pull-left lyrics-pricing">' +
            "             <label class='#{if product.can_have_lyrics then "" else "hidden"}'><input type='checkbox' class='with-lyrics' name='composition[scores_attributes][#{index}][has_lyrics]' value='1'>" + I18n.t('my_soundnotation.options.creator.add_version.with_lyrics') + "</label>" +
            '       </div>' +
            '       <div class="checkbox chords-checkbox pull-right chords-pricing">' +
            "             <label class='#{if product.can_have_chords then "" else "hidden"}'><input type='checkbox' class='with-chords' name='composition[scores_attributes][#{index}][has_chords]' value='1'>" + I18n.t('my_soundnotation.options.creator.add_version.with_chords') + "</label>" +
            '       </div>' +
            '  </div>' +
            '</div>'

        $(".#{product.code} img").after "<span class=\'cancel-option cancel-#{product.code}\'>X</span>"
        $(".score-details-#{product.id}").hide()
        $('.price-line').css("border-bottom", "2px solid white")
        $(".#{product.code}-image").attr 'src', product.highlighted_icon.url

        $(".cancel-#{product.code}").on 'click', (e)->
          $(".score-price-#{product.id}").text(price_with_unit(product.default_user_price))
          $(".section-#{product.code}").removeClass('selected-card').addClass("price-#{product.code}-section")
          $(".section-#{product.code}").removeClass('selected-card-normal')
          $(".new-#{product.code}-wrapper").replaceWith "<div class=\"price-wrapper #{product.code}-wrapper\"></div>"
          $(".cancel-#{product.code}").remove()
          $(".score-details-#{product.id}").show()
          $('.price-line').css("border-bottom", "2px solid lightgrey")
          $(".#{product.code}-image").attr 'src', product.icon.url
          return false
        return
    )

  pianoProductList: ->
    ['piano_solo', 'piano_lyrics', 'piano_accompaniment']

window.ProductPricing.init = (e)=> new window.ProductPricing(e)
