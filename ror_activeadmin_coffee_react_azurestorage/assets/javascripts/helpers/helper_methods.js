function validateLyrics() {
  var restrictedWords = ["werbung", "advertisement", "©"];
  var inputText = $('.input-composition-lyrics').val() || $('.lyrics-form-input').val();
  var invalidWords = restrictedWords.filter(function (word) {
    return inputText.toUpperCase().indexOf(word.toUpperCase()) > -1;
  });

  if (invalidWords.length > 0) {
    toastr.warning("You should not use those words: " + invalidWords.join(", "))
  }
}

function manipulatePaymentOptions(totalPrice) {
  if (totalPrice > 0.50)
    $(".payment-select option[value='credit-card']").removeAttr("disabled");
  else
    $(".payment-select option[value='credit-card']").prop("disabled", "disabled");
}
