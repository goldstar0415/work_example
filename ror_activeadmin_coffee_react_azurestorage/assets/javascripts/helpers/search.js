$(document).ready(function () {

  //// Client side search

  // $(document).on('keyup', '.search-input', function () {
  //   var index, tdTitle, tdComposer;
  //   var target = $(this).data('tab-name');
  //   var query = $(this).val().toUpperCase();
  //   var table = document.getElementById("table-" + target + "");
  //   var tr = table.getElementsByTagName("tr");
  //
  //   for (index = 0; index < tr.length; index++) {
  //     tdTitle = tr[index].querySelectorAll("td.composition-title")[0];
  //     tdComposer = tr[index].querySelectorAll("td.composition-author")[0];
  //     if (tdTitle || tdComposer) {
  //       if ((tdTitle.innerHTML.toUpperCase().indexOf(query) > -1) || (tdComposer.innerHTML.toUpperCase().indexOf(query) > -1)) {
  //         tr[index].style.display = "";
  //       } else {
  //         tr[index].style.display = "none";
  //       }
  //     }
  //   }
  //   return false;
  // });

  //// Server side search

  function startSearching($input) {
    var query = $input.val();
    var searchPath = $input.data('search-path');

    $(".page-loader").show();
    $.ajax({
      type: 'GET',
      url: searchPath,
      data: {query: query}
    }).done(function () {
      $(".page-loader").hide();
    });
  }

  $(document).on('keydown', '.search-input', function (event) {
    if (event.which == 13)
      startSearching($(this));
  });

  $(document).on('click', '.search-addon', function () {
    $input = $(this).parent().find('.search-input');
    startSearching($input);
  });

});


function createTypeahead($element, $bind, $sequence, $wrapper) {
  $element.typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: 'author-search',
      source: function (query, process) {

        $authorType = $("[name='" + $wrapper + "[authors_attributes][" + $sequence + "][author_type]']").val() || 'composer';

        $.get('/api/search_authors.json', {query: query, author_type: $authorType}, function (data) {
          process(data);
        });
      },
      displayKey: 'name',
      templates: {
        suggestion: function (data) {
          return '<div>' + data.name + '</div>'
        }
      }
    }).bind('typeahead:selected', function (obj, data) {
    $this = $(this);
    $bind.val(data.id);
  });
}

// Reset authors auto-complete if user change author type after search

function clearAutoCompleteAuthor($this, $wrapper) {
  var sequence = $this.data('sequence');
  var inputId = $("[name='" + $wrapper + "[authors_attributes][" + sequence + "][id]']");
  var inputName = $("[name='" + $wrapper + "[authors_attributes][" + sequence + "][full_name_str]']");

  if (inputId.val()) {
    inputId.val("");
    inputName.val("");
  }
}

$(document).on('change', '.creator-composer-dynamic-select', function () {
  clearAutoCompleteAuthor($(this), 'composition');
});

$(document).on('change', '.composer-autocomplete', function () {
  clearAutoCompleteAuthor($(this), 'meta_data');
});
