$(document).ready(function() {
        I18n.locale = $('body').data('locale')
        var tour = new Tour({
          template: "<div class='popover tour'> \
            <div class='arrow'></div> \
            <h3 class='popover-title'></h3> \
            <div class='popover-content'></div> \
            <div class='popover-navigation'> \
              <div class='checkbox'> \
                <label> \
                  <input type='checkbox' class='tour-checkbox'> "
                  + I18n.t('dont_show_me_this_box_anymore', {scope: 'my_soundnotation.tour'})
                + " </label> \
              </div> \
              <div class='row'> \
                <button class='btn btn-default tour-button tour-nav-button' data-role='prev'>« "
                + I18n.t('prev', {scope: 'my_soundnotation.tour'})
                + "</button> \
                <span data-role='separator'>|</span> \
                <button class='btn btn-default tour-button tour-nav-button' data-role='next'>"
                + I18n.t('next', {scope: 'my_soundnotation.tour'})
                + " »</button> \
                <button class='btn btn-default tour-button end-tour-button' data-role='end'>"
                + I18n.t('end_tour', {scope: 'my_soundnotation.tour'})
                + "</button> \
              </div> \
            </div> \
          </div>",
          steps: [
          {
            element: "#creator-step-container",
            title: I18n.t('popup_heading', {scope: 'my_soundnotation.tour'}),
            placement: "bottom",
            content: I18n.t('creator_content', {scope: 'my_soundnotation.tour'})
          },
          {
            element: "#catalogue-step-container",
            title: I18n.t('popup_heading', {scope: 'my_soundnotation.tour'}),
            placement: "bottom",
            content: I18n.t('catalogue_content', {scope: 'my_soundnotation.tour'})
          },
          {
            element: "#promoter-step-container",
            title: I18n.t('popup_heading', {scope: 'my_soundnotation.tour'}),
            placement: "bottom",
            content: I18n.t('promoter_content', {scope: 'my_soundnotation.tour'})
          },
          {
            element: "#distributor-step-container",
            title: I18n.t('popup_heading', {scope: 'my_soundnotation.tour'}),
            placement: "bottom",
            content: I18n.t('distributor_content', {scope: 'my_soundnotation.tour'})
          },
          {
            element: "#demo-tour",
            title: I18n.t('popup_heading', {scope: 'my_soundnotation.tour'}),
            placement: "bottom",
            content: I18n.t('demo_content', {scope: 'my_soundnotation.tour'})
          }
        ]});

        if (!tour.ended()){
            // Initialize the tour
            tour.init(true);
            tour.setCurrentStep(0);
            // Start the tour
            tour.start();
        }

        $(document).on('click', '.tour-checkbox', function() {
          tour.end();
        });
});
