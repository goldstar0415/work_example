function price_with_unit(amount) {
    locale = $('body').data('currency-locale');
    return number_to_currency(amount, {
            unit: I18n.t('number.currency.format.unit', {locale: locale}),
            separator: I18n.t('number.currency.format.separator', {locale: locale}),
            format: I18n.t('number.currency.format.format', {locale: locale})
        }
    )
}

function number_to_currency(number, options) {
    try {
        var options = options || {};
        var precision = options["precision"] || 2;
        var unit = options["unit"] || "$";
        var separator = precision > 0 ? options["separator"] || "." : "";
        var delimiter = options["delimiter"] || ",";
        var format = options["format"];

        var parts = parseFloat(number).toFixed(precision).split('.');
        var amount = number_with_delimiter(parts[0], delimiter) + separator + parts[1].toString();
        var formatted_price = format.replace('%n', amount).replace('%u', unit);
        return formatted_price;
    } catch (e) {
        return number;
    }
}

function number_with_delimiter(number, delimiter, separator) {
    try {
        var delimiter = delimiter || ",";
        var separator = separator || ".";

        var parts = number.toString().split('.');
        parts[0] = parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + delimiter);
        return parts.join(separator);
    } catch (e) {
        return number
    }
}
