$(document).ready(function() {
    var field = 1;
    var x = 0;
    $(".order-table-row").hover(function(e) {
        e.preventDefault();
        if (x < field) {
            x++;
            // $(".pdf_field").append("<select class='form-control pdf-select'> \
            //                         <option>MORE</option> \
            //                         <option>Other option</option> \
            //                     </select>")
        }
    }, function() {
        x--;
        $(".pdf-select").hide();
    })

    $("#company-logo-uploader").click(function (e) {
        $('input[name="user[logo]"]').focus().trigger('click');
    })

    $('input[name="user[logo]"]').change(function(){
        readURL(this)
    });
});

$(document).ready(function() {
    $('.vat-id-field').toggle($('.vat-select').val() === 'true');
    $('.vat-select').change(function(e) {
        $('.vat-id-field').toggle(e.target.value === 'true');
    });

    var bankFormToggler = function(bankLocation) {
        $('.bank-account-options').toggle(bankLocation === 'europe');
        $('.other-bank-account-options').toggle(bankLocation === 'other');
    };

    var paymentFormToggler = function(method) {
        $('.paypal-account-options').toggle(method === 'paypal');
        $('.bank-account-select').toggle(method === 'bank');
        bankFormToggler(method === 'bank' && $('.country-payment-select').val()); 
    };

    paymentFormToggler($('.profile-payments-select').val());
    $('.profile-payments-select').change(function(e) { paymentFormToggler(e.target.value) });
    $('.country-payment-select').change(function(e) { bankFormToggler(e.target.value) });

});

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#logo-preview').css('background-image', "url("+e.target.result+")");
            $('#logo-placeholder').html('&nbsp;')
        }

        reader.readAsDataURL(input.files[0]);
    }
}
