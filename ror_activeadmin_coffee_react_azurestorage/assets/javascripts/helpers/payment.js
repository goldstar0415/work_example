$(document).ready(function() {

  $('.checkout-credit-card').hide();
  
	if($('.payment-select').val() == 'paypal') {
      $('.checkout-options').show(); 
      $('.checkout-credit-card').hide();
  }

  $('.payment-select').change(function(){
      if($('.payment-select').val() == 'paypal') {
          $('.checkout-options').show(); 
          $('.checkout-credit-card').hide();
          $('.gift-code-label').show();
          $('.gift-code-input').show();
          $('.gift-line').show();
          $('.buy_button').text(I18n.t('my_soundnotation.creator_form.shop_checkout.buy'));
      }
      if($('.payment-select').val() == 'credit-card') {
          $('.checkout-options').hide(); 
          $('.checkout-credit-card').show();
          $('.gift-code-label').hide();
          $('.gift-code-input').hide();
          $('.gift-line').hide();
          $('.buy_button').text(I18n.t('my_soundnotation.creator_form.shop_checkout.pay_now'));
      }
  });
});
