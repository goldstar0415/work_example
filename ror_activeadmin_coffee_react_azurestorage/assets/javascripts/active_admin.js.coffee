#= require active_admin/base
#= require activeadmin_addons/all
#= require bootstrap-typeahead-rails
#= require ./helpers/search

$(document).on 'change', '.has_many_fields .required input:file', ->
  fileName = $(this).val().replace(/C:\\fakepath\\/i, '')
  name = fileName.split('.')[0]
  ext = fileName.split('.')[1]
  if ext == 'wav'
    ext = 'flac'
  else
    ext = fileName.split('.')[1]
  $(this).parents('.has_many_fields').find('.stringish input:text').val name
  $(this).parents('.has_many_fields').find('.required input:text').val ext
  return
