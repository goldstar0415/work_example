xml.instruct!

xml.declare! :DOCTYPE, :ONIXMessage, :SYSTEM, "http://www.editeur.org/onix/2.1/reference/onix-international.dtd"

xml.ONIXMessage do
  datestamp = Date.today.to_s.split('-').join
  xml.Header do
    xml.FromCompany('Soundnotation GmbH')
    xml.FromPerson('Soundnotation Distribution')
    xml.FromEmail('info@soundnotation.com')
    xml.SentDate(datestamp)
  end
  @scores.each do |score|
    composition = score.composition
    gtin = score.gtin.blank? ? "" : score.gtin
    descriptions = score.main_descriptions.where.not(text: "")
    xml.Product(datestamp: datestamp) do
      xml.RecordReference(gtin)
      if score.delivery_processing?
        xml.NotificationType("03")
      elsif score.update_processing?
        xml.NotificationType("04")
      elsif score.take_down_processing?
        xml.NotificationType("05")
      else
        xml.NotificationType
      end
      xml.ProductIdentifier do
        xml.ProductIDType("03")
        xml.IDValue(gtin)
      end
      xml.ProductForm("PI")
      xml.EpubType("029")
      xml.EpubTypeNote("00")
      xml.Title do
        xml.TitleType("01")
        xml.TitleText(composition.title)
        xml.Subtitle(composition.subtitle) unless composition.subtitle.blank?
      end
      composition.authors.each_with_index do |author, index|
        xml.Contributor do
          xml.SequenceNumber(index + 1)
          if author.author_type.downcase == "composer"
            xml.ContributorRole("A06")
          elsif author.author_type.downcase == "artist"
            xml.ContributorRole("E08")
          elsif author.author_type.downcase == "lyricist"
            xml.ContributorRole("A05")
          elsif author.author_type.downcase == "arranger"
            xml.ContributorRole("B25")
          else
            xml.ContributorRole
          end
          xml.SequenceNumberWithinRole(1)
          xml.PersonName(author.full_name)
          xml.PersonNameInverted([author.last_name, author.first_name].compact.join(", "))
          xml.NamesBeforeKey(author.first_name)
          xml.KeyNames(author.last_name)
        end
      end
      xml.Language do
        xml.LanguageRole("01")
        xml.LanguageCode(composition.product_language.code.downcase)
      end
      pdf_score_files = []
      score.score_files.each { |sf| pdf_score_files << sf if sf.format_is_in(['pdf']) }
      score_pages = pdf_score_files.empty? ? "" : pdf_score_files.first.pdf_reader.page_count.to_s
      xml.NumberOfPages(score_pages)
      xml.BASICMainSubject("MUS037000")
      xml.MainSubject do
        xml.MainSubjectSchemeIdentifier(26)
        xml.SubjectSchemeVersion("2.0")
        xml.SubjectCode(9595)
        # xml.SubjectHeadingText("Nonbooks, PBS / Sachbücher/Kunst, Literatur/Architektur")
      end
      if score.keywords.present?
        score.keywords.split(',').map(&:strip).each do |keyword|
          xml.Subject do
            xml.SubjectSchemeIdentifier(20)
            xml.SubjectHeadingText(keyword)
          end
        end
      end
      unless descriptions.empty?
        if descriptions.count == 1
          descriptions.sort_by_language.each do |description|
            xml.OtherText do
              xml.TextTypeCode("01")
              xml.TextFormat("06")
              xml.Text(description.text)
            end
          end
        else
          descriptions.one_by_language.sort_by_language.each do |description|
            xml.OtherText(language: description.language.code.downcase) do
              xml.TextTypeCode("01")
              xml.TextFormat("06")
              xml.Text(description.text)
            end
          end
        end
      end
      xml.Publisher do
        xml.PublishingRole("01")
        xml.NameCodeType("05")
        xml.PublisherName(composition.user.default_copyright)
      end
      if score.delivery_processing? || score.update_processing?
        xml.PublishingStatus("04")
      elsif score.take_down_processing?
        xml.PublishingStatus("17")
      else
        xml.PublishingStatus
      end
      unless score.publishing_date.blank?
        xml.PublicationDate(score.publishing_date.to_s.split('-').join)
      end
      if score.world_release?
        xml.SalesRights do
          xml.SalesRightsType("02")
          xml.RightsTerritory("WORLD")
        end
      else
        xml.SalesRights do
          xml.SalesRightsType("02")
          xml.RightsCountry(score.countries.map(&:code).join(" "))
        end
        xml.SalesRights do
          xml.SalesRightsType("03")
          xml.RightsTerritory("ROW")
        end
      end
      xml.RelatedProduct do
        xml.RelationCode(13)
        xml.ProductIdentifier do
          xml.ProductIDType("03")
          xml.IDValue(gtin)
        end
        xml.ProductForm("PI")
      end
      xml.SupplyDetail do
        xml.SupplierName("Soundnotation")
        xml.Website do
          xml.WebsiteRole("00")
          xml.WebsiteLink("https://www.soundnotation.com")
        end
        xml.SupplierRole("01")
        xml.AvailabilityCode("IP")
        xml.ProductAvailability(20)
        unless score.publishing_date.blank?
          xml.OnSaleDate(score.publishing_date.to_s.split('-').join)
        end
        xml.Price do
          xml.PriceTypeCode("04")
          xml.PriceStatus("02")
          xml.PriceAmount(score.price)
          xml.CurrencyCode("EUR")
          xml.CountryCode("DE")
          xml.TaxRateCode1("S")
        end
      end
    end
  end
end
