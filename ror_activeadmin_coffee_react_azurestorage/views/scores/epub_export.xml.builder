xml.instruct!

# xml.declare! :DOCTYPE, :ONIXMessage, :SYSTEM, "http://www.editeur.org/onix/2.1/reference/onix-international.dtd"

xml.html(xmlns: "http://www.w3.org/1999/xhtml") do
  xml.head do
    xml.meta(charset: "utf-8")
    xml.meta(name: "viewport", content: "width=595, height=842")
    xml.title("Page 1")
  end
  xml.body do
    xml.img(alt: "Bild", src: "02.png")
  end
end
