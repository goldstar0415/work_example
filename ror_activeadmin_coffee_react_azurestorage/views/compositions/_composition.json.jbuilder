json.extract! composition, :id, :title, :subtitle, :key, :copyright, :created_at, :updated_at
json.url composition_url(composition, format: :json)