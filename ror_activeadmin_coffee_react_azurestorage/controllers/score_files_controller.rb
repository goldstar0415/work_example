class ScoreFilesController < ApplicationController
  before_action :set_score_file, only: [:download, :download_flac_as_mp3]

  def download
    data = open(@score_file.path.url).read
    send_data data, type: @score_file.path.content_type, disposition: :attachment, filename: @score_file.name_for_download
  end

  def download_flac_as_mp3
    data = open(@score_file.path.url).read

    # create the flac temp file
    flac_file = Tempfile.new('flac')
    flac_file.binmode
    flac_file.write(data)
    flac_file.close

    flac_path = flac_file.path
    mp3_path = flac_path + '.mp3'

    system("ffmpeg -i #{flac_path.shellescape} -v 0 -b:a 256k #{mp3_path.shellescape}")

    mp3_data = open(mp3_path).read
    send_data mp3_data, type: @score_file.path.content_type, disposition: :attachment, filename: @score_file.name_for_download('mp3')

    # remove temp file
    system("rm #{mp3_path.shellescape}")
    flac_file.unlink
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_score_file
    @score_file = ScoreFile.find(params[:id])
  end
end
