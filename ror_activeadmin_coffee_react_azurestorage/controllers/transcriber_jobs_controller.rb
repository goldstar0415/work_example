class TranscriberJobsController < ApplicationController
  def accept
    job = TranscriberJob.open.find(params[:id])
    job.update! deadline: job.timer_duration.from_now, user: current_user
    job.score.composition.is_audio? ? job.score.in_creation! : job.score.in_creation_preselect!
    render json: {job: job.reload.as_json}, status: 200
  end

  def preselect
    job = TranscriberJob.find(params[:id])
    ScorePreselectJob.perform_later(job.score_id, params[:xml_text])

    render json: {job: job.reload.as_json}, status: 200
  end

  def show
    job = TranscriberJob.find(params[:id])
    render json: job.as_json, status: 200
  end

  def mark_downloaded
    job = TranscriberJob.find(params[:id])
    job.score.in_creation_download!

    render json: { job: job.reload.as_json }, status: 200
  end

  def upload
    job = TranscriberJob.find(params[:id])
    job.score.score_files.create!(name: 'Job Music XML', path: params[:file])
    job.score.in_creation_upload!

    render json: { job: job.reload.as_json }, status: 200
  end
end
