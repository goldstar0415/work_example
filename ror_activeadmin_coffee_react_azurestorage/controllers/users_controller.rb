class UsersController < ApplicationController
  skip_before_action :authenticate_user!, only: [:with_email, :newsletter, :subscribe_newsletter]
  protect_from_forgery except: :newsletter

  def with_email
    @user = User.find_by_email(params[:email])
    render json: {user: @user}, status: 200
  end

  def newsletter
    @lan = params[:lng]
  end

  def subscribe_newsletter
    interest_id = user_params[:interest_id]
    SubscribeUserToMailingListJob.perform_later(user_params[:email], user_params[:first_name], user_params[:last_name], I18n.locale.to_s, {interest_id => true})
    render json: {}, status: 200
  end

  private

  def user_params
    params.require(:user).permit(
      :email, :first_name, :last_name, :interest_id
    )
  end
end
