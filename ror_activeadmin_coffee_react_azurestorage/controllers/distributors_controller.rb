class DistributorsController < ApplicationController

  before_action :set_score, only: [:settings, :update_settings]

  def settings
    @plan = Plan.find_by_identity('basic')
    @option_update = params[:selected_option] == 'update'
    @option_take_down = params[:selected_option] == 'take_down'
    @available_gtins = GTin.available
  end

  def update_settings
    @score.update_attributes!(distributors_params[:score_attributes])
    render json: {score: {shops_name: @score.shops_name}}, status: 200
  end


  def subscribe
    @score = Score.find_by_id(params[:score_id])
    response = SubscriptionProcessor.subscribe(current_user, @score, params, 'basic', false)
    if response.errors.blank?
      respond_to do |format|
        format.js
        format.json { render json: {notice: 'Payment successful!'}, status: 200 }
      end
    else
      render json: {error: response.errors.join('<br>')}, status: 402
    end
  end

  def sales_report
    @sales_reports = sales_reports
    @data_json = @sales_reports.order(:transaction_date).group(:transaction_date).count
  end

  def search_sales_report
    @sales_reports = sales_reports(params[:start_date], params[:end_date])
    @data_json = @sales_reports.order(:transaction_date).group(:transaction_date).count
  end

  private

  def set_score
    @score = Score.find(params[:score_id])
    @composition = @score.composition
    @shops = Shop.as_priority
    @recording_shops = RecordingShop.all
  end

  def sales_reports(start_date = 4.weeks.ago.to_date, end_date = Date.today)
    sales_report = current_user.admin? ? SalesReport.all : SalesReport.by_scores(current_user.id)
    sales_report.between(start_date, end_date)
  end

  def distributors_params
    params.require(:distributors).permit!
  end

end
