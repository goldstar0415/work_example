class PromotersController < ApplicationController

  before_action :set_score, only: [:settings, :add_cart_items, :checkout]

  def settings

  end

  def add_cart_items
    @products          = params[:promoters].collect { |key, value| OpenStruct.new(value) }
    @promotion_builder = PromotionBuilder.build(current_user, @score, @products)


    render json: {promotion_data: promotion_data, status: 200}
  end

  def checkout
    @cart     = Cart.open.where(user_id: current_user.id).first_or_create
    @checkout = CheckoutCart.checkout(current_user, params, @score, @composition, @cart, **{promotions: true})

    if @checkout.errors.blank?
      respond_to do |format|
        format.js
        format.json { render json: {notice: 'Payment successful!'}, status: 200 }
      end
    else
      render json: {error: response.errors.join('<br>')}, status: 402
    end
  end

  private

  def promotion_data
    @promotion_data = {
      composition: ComposerDecorator.new(@composition),
      cart:        CartDecorator.new(@promotion_builder.cart),
      cart_items:  @promotion_builder.cart.try(:cart_items),
      promotions:  @promotion_builder.promotions,
      gift_code:   @promotion_builder.cart.try(:coupon) || @promotion_builder.try(:coupon) || Coupon.none.new,
    }

    @promotion_data
  end

  def set_score
    @score             = Score.find(params[:score_id])
    @composition       = @score.composition
    @promoter_products = PromoterProduct.as_sort_index
  end

  def promoters_params
    params.require(:promoters).permit!
  end

end
