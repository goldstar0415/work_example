class MySoundnotationController < ApplicationController
  skip_before_action :authenticate_user!, :except => [:index]
  before_action :check_no_transcriber, only: [:index]
  before_action :clear_previous_open_cart, only: [:index]


  def index
    @scores         = Score.by_user_status(current_user).with_paginate(params[:page], 10)
    @created_scores = Score.by_user_status(current_user, {status: 'created'}).with_paginate(params[:page], 10)
    @composition    = current_user.compositions.new
    @score          = @composition.scores.build
    @author         = @composition.authors.build
    @copyright      = @composition.copyrights.build

    load_initialized_scores
  end

  def catalogue
    @scores = Score.by_user_status(current_user, {query: params[:query]}).with_paginate(params[:page], params[:cat_per_page])
  end

  def promoter
    @created_scores = Score.by_user_status(current_user, {status: 'created', query: params[:query]}).with_paginate(params[:page], params[:pro_per_page])
    load_initialized_scores
  end

  def distributor
    @created_scores = Score.by_user_status(current_user, {status: 'created', query: params[:query]}).with_paginate(params[:page], params[:dis_per_page])
    load_initialized_scores
  end

  private

  def clear_previous_open_cart
    if @current_cart.open?
      composition = @current_cart.composition
      if composition && !composition.checkout?
        composition.purchase_orders.destroy_all
        composition.scores.find_each {|score| score.invoice_item.delete if score.invoice_item}
        composition.scores.find_each {|score| score.transcriber_job.delete if score.transcriber_job}
        composition.invoice.destroy if composition.invoice
        composition.scores.collect(&:destroy)
        @current_cart.destroy
      elsif composition.blank?
        @current_cart.destroy
      end
    end
    if current_user.present?
      ## Clean orphan compositions
      Composition.without_score(current_user.id).collect(&:destroy)
      ## Clean orphan authors
      Author.no_contribution.collect(&:destroy)
      # Author.no_composition.collect(&:destroy) #REMOVES THE AUTHOR!!!
    end
  end

  def load_initialized_scores
    @products                  = Product.active.as_sort_index.sort_by{|p|p.product_type}
    @promoter_products         = PromoterProduct.as_sort_index
    @prices                    = Price.user_price

    @chains_and_lone_shops     = []
    @chains_and_lone_shops     << DistributionChain.all
    @chains_and_lone_shops     << Shop.not_distribution_chain
    @chains_and_lone_shops.flatten!
  end

  private

  def check_no_transcriber
    redirect_to root_path, alert: t('my_soundnotation.index.no_access_to_page') unless current_user && (current_user.admin? || current_user.admin_light? || current_user.default?)
  end

end
