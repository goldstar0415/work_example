class ProfilesController < ApplicationController
  before_action :set_user, :preload_countries_languages

  def show
    @jobs = own_completed_jobs if user.transcriber?
    @scores = Score.by_user_status(user).joins(composition: :invoice).with_paginate(params[:page], 10)
  end

  def update
    @user = UserPasswordValidator.validate!(user, user_params)

    if user.errors.messages.empty? && user.update_attributes(user_params)
      keep_session if updating_password?

      redirect_to profile_path(locale: user.locale), notice: I18n.t('profiles.show.profile_update_notice')
    else
      flash.now[:error] = I18n.t('profiles.show.profile_update_notice_fail')
      render :show
    end
  end

  def order_history
    @scores = Score.by_user_status(user).joins(composition: :invoice).with_paginate(params[:page], 10)
  end

  private

  attr_reader :user

  def updating_password?
    (params[:user][:password] || params[:user][:password_confirmation]).present?
  end

  def set_user
    @user = current_user
  end

  def preload_countries_languages
    @countries = Country.order_by_name
    @languages = Language.visible.order_by_name
    @currencies = Currency.all
  end

  def user_params
    params.require(:user).permit(
      *expected_fields
    )
  end

  def expected_fields
    user_fields = [
      :email, :first_name, :last_name, :company, :language_id, :city,
      :country_id, :currency_id, :street, :house_number, :zip_code, :logo, :keywords,
      :profession, :musical_education, :education_place,
      :paypal_email, :bank_location, :beneficial_owner, :bic, :iban, :bank_name,
      :routing_number, :account_number, :payment_method, :vat_id
    ]

    user_fields.concat [:password, :password_confirmation, :current_password] if updating_password?

    user_fields
  end

  def keep_session
    sign_in(user.reload, :bypass => true)
  end

  def own_completed_jobs
    TranscriberJob.includes(:score).where(scores: {status: Score.statuses[:created]}).where(user_id: user.id).order('deadline DESC')
  end
end
