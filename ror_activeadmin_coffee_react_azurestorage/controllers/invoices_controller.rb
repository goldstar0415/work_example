class InvoicesController < ApplicationController
  layout 'invoice'

  before_action :set_invoice, only: [:edit, :update, :destroy]

  skip_before_action :authenticate_user!, :only => [:show]

  # GET /invoices
  # GET /invoices.json
  def index
    @invoices = current_user.invoices
  end

  # GET /invoices/1
  # GET /invoices/1.json
  def show
    @invoice = Invoice.find(params.require(:id))
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Invoice##{@invoice.invoice_id}-#{@invoice.invoicing_date.strftime('%m-%d-%Y')}",
               print_media_type: true
      end
    end
  end

  # GET /invoices/1/edit
  def edit
  end

  # PATCH /invoices/1/mark_as_paid
  def mark_as_paid

  end

  # PATCH /invoices/1/mark_as_cancelled
  def mark_as_cancelled

  end

  # PATCH /invoices/1/mark_as_due
  def mark_as_due

  end

  # DELETE /invoices/1
  # DELETE /invoices/1.json
  def destroy
    @invoice.destroy
    respond_to do |format|
      format.html { redirect_to invoices_url, notice: 'Invoice was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invoice
      @invoice = current_user.invoices.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def invoice_params
      params.fetch(:invoice, {})
    end
end
