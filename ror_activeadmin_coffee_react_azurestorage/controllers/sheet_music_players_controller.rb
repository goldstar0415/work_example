class SheetMusicPlayersController < ApplicationController
  before_action :set_sheet_music_player, only: [:show]

  def show
  end

  private
  
  def set_sheet_music_player
    @sheet_music_player = SheetMusicPlayer.find(params[:id])
  end
end
