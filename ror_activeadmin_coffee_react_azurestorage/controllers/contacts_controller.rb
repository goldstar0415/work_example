class ContactsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:new, :create]

  def new
	  @contact = Contact.new
  end

	def create
	  begin
	    @contact = Contact.new(params[:contact])
	    @contact.request = request
	    if @contact.deliver
	      flash.now[:notice] = 'Thank you for your message!'
				@contact = Contact.new
				render :new
	    else
	      render :new
	    end
	  rescue ScriptError
	    flash[:error] = 'Sorry, this message appears to be spam and was not delivered.'
	  end
	end
end