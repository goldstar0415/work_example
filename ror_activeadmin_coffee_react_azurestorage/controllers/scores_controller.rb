class ScoresController < ApplicationController
  include ScoresHelper
  before_action :set_score, only: [:update, :cover_design, :square_cover_design, :delete_catalog, :cover_download, :square_cover_download, :impressum_download, :final_pdf_download, :final_epub_download, :preview_pdf_download]

  def update
    respond_to do |format|
      if @score.update(score_params)
        format.json { render json: @score, status: :ok }
      else
        format.json { render json: {errors: @score.errors.full_messages}, status: :unprocessable_entity }
      end
    end
  end

  def cover_design
    if score_params[:cover].present?
      @score.cover = score_params[:cover]
    elsif score_params[:cover_logo].present?
      @score.cover_logo = score_params[:cover_logo]
      @score.cover_background_image = @score.cover.file.filename
      @score.cover = @score.cover
    elsif score_params[:cover_background_image].present?
      @score.cover_background_image = score_params[:cover_background_image]
      @score.cover = File.open(Rails.root.join("app", "assets", "images", 'cover_background', @score.cover_background_image))
    end

    respond_to do |format|
      if @score.save
        format.json { render json: @score.reload, status: :ok }
      else
        format.json { render json: {errors: @score.errors.full_messages}, status: :unprocessable_entity }
      end
    end
  end

  def square_cover_design
    if score_params[:square_cover].present?
      @score.square_cover = score_params[:square_cover]
    elsif score_params[:cover_logo].present?
      @score.cover_logo = score_params[:cover_logo]
      @score.square_cover_background_image = @score.square_cover.file.filename
      @score.square_cover = @score.square_cover
    elsif score_params[:square_cover_background_image].present?
      @score.square_cover_background_image = score_params[:square_cover_background_image]
      @score.square_cover = File.open(Rails.root.join("app", "assets", "images", 'cover_background', @score.square_cover_background_image))
    end

    respond_to do |format|
      if @score.save
        format.json { render json: @score.reload, status: :ok }
      else
        format.json { render json: {errors: @score.errors.full_messages}, status: :unprocessable_entity }
      end
    end
  end

  def delete_catalog
    if @score.demo_product?
      current_user.hide_demo_product!
      render json: {notice: I18n.t('my_soundnotation.options.catalogue.score_destroyed')}, status: 200
    elsif @score.created? && @score.not_delivered?
      @score.destroy
      render json: {notice: I18n.t('my_soundnotation.options.catalogue.score_destroyed')}, status: 200
    else
      render json: {error: I18n.t('my_soundnotation.options.catalogue.score_cannot_be_deleted')}, status: 403
    end
  end

  def export
    @shop_name = params[:shop_name]
    @scores = Score.by_ids(params[:score_ids].split(','))

    export_to_zip_data(params[:shop_name], params[:score_ids])

  end

  def cover_download
    cover = @score.cover
    # if cover different than default, download the image uploaded from user as image format
    if @score.cover_from_user?
      data = open(cover.distributor_download.url).read
      send_data data, disposition: :attachment, filename: [@score.composition.title, cover.file.extension].join('.')
    #otherwise, download cover as pdf using the design tool
    else
      response = FinalProductDesign.new(@score, only_cover = true).design
      data = open(response[:prawn_cover_pdf]).read
      send_data data, disposition: :attachment, filename: "#{@score.composition.title}.pdf", type: "application/pdf"
      FileUtils.rm_rf(response[:tmp_file_path])
    end
  end

  def square_cover_download
    data = open(@score.square_cover.distributor_download.url).read
    send_data data, disposition: :attachment, filename: [@score.composition.title, @score.cover.file.extension].join('.')
  end

  def impressum_download
    response = FinalProductDesign.new(@score, only_cover = false, only_impressum = true).design
    pdf = open(response[:prawn_impressum_pdf]).read
    # pdf = WickedPdf.new.pdf_from_string(render_to_string(template: 'scores/impressum_download.pdf.erb', layout: false, :footer => {:html => {:template => 'scores/impressum_download_footer.pdf.erb', :layout => false}}))
    send_data pdf, :filename => "#{@score.composition.title} - edition notice.pdf", type: "application/pdf"
    FileUtils.rm_rf(response[:tmp_file_path])
  end

  def final_pdf_download
    # Combine pdf files
    if @score.combined_cover_pdf_score.blank?
      response = FinalProductDesign.new(@score).design
      pdf = open(response[:final_pdf_path]).read
      @score.combined_cover_pdf_score = File.open(response[:final_pdf_path]) if response[:upload_to_storage]
      tmp_img_path = response[:tmp_file_path].join('cover_thumb.jpg')
      im = Magick::Image.read(response[:prawn_cover_pdf])
      im[0].write(tmp_img_path)
      @score.cover_thumb = File.open(tmp_img_path)
      @score.save
      send_data pdf, filename: "#{@score.combined_title}.pdf" , type: "application/pdf"
      # Remove tmp files
      FileUtils.rm_rf(response[:tmp_file_path])
    else
      pdf = open(@score.combined_cover_pdf_score.url).read
      send_data pdf, filename: "#{@score.combined_title}.pdf" , type: "application/pdf"
    end
    # send_data response[:final_pdf].to_pdf, :filename => response[:filename], type: "application/pdf"
  end

  def final_epub_download
    if @score.combined_epub.blank?
      response = EpubExport.new(@score).design
      epub = open(response[:path]).read
      @score.combined_epub = File.open(response[:path]) if @score.score_files.only_pdf.present?
      @score.save
      send_data epub, filename: "#{@score.combined_title}.epub" , type: "application/epub"
      # Remove tmp files
      FileUtils.rm_rf(response[:dirname])
    else
      epub = open(@score.combined_epub.url).read
      send_data epub, filename: "#{@score.combined_title}.epub" , type: "application/epub"
    end
  end

  def preview_pdf_download
    response = FinalProductDesign.new(@score, nil, nil, only_one_pdf_page = true).design
    pdf = open(response[:final_pdf_path]).read
    send_data pdf, filename: "#{@score.combined_title} - Preview.pdf" , type: "application/pdf"
    FileUtils.rm_rf(response[:tmp_file_path])
  end

  private

  def set_score
    @score = Score.find(params[:id])
  end

  def score_params
    params.require(:score).permit(:gtin, :publishing_date, :pages, :notafina_shop, :scoring, :difficulty, :arrangement, :genre, :cover, :square_cover, :delivery_status, :cover_background_image, :square_cover_background_image, :cover_logo)
  end
end
