class CompositionsController < ApplicationController
  before_action :set_composition, only: [:show, :edit, :update, :destroy, :invoice_download]

  # Predefined steps!
  AUDIO_UPLOADING_STEP = 1
  ADD_DETAILS_STEP     = 2
  ADD_VERSION_STEP     = 3
  CHECKOUT_STEP        = 4

  def index
    @compositions = Composition.all
  end

  def show
  end

  def creator_wizard
    @wizard      = CreatorWizard.build(
      current_user,
      creator_wizard_params.merge(language_id: current_user.language_id),
      @current_cart
    )
    @composition = @wizard.composition
    @author      = @composition.authors.first || @composition.authors.build
    @copyright   = @composition.copyrights.build

    render_wizard_response
  end

  def step
    @wizard_params = get_permitted_params(params[:step_number].to_i)

    @wizard = CreatorWizard.build(
      current_user,
      @wizard_params.merge(language_id: current_user.language_id),
      @current_cart,
      params[:step_number],
      params[:composition][:id] || @current_cart.try(:composition_id)
    )

    @composition = @wizard.composition

    set_cart_to_session(@wizard.cart.id) if @wizard.cart.present?

    render_wizard_response
  end

  def apply_coupon
    @wizard = CouponApplierBuilder.build(params[:code], @current_cart)

    respond_to do |format|
      if @wizard.cart.errors.messages.empty?
        format.json { render json: load_coupon_response, status: :ok }
        format.js
        format.html { redirect_to root_path, notice: 'Successfully applied coupon' }
      else
        format.html { redirect_to root_path, error: 'failed to apply coupon' }
        format.json { render json: load_coupon_response, status: :unprocessable_entity }
        format.js
      end
    end
  end

  def invoice_download
    @invoice = @composition.invoice
    invoice_pdf = WickedPdf.new.pdf_from_string(
      render_to_string(template: 'invoices/show.pdf.erb', layout: false)
    )
    send_data invoice_pdf, :filename => "#{@composition.title} - invoice.pdf", :type => "application/pdf"
    end

  private

  def load_coupon_response
    @cart        = @wizard.cart
    @composition = @cart.composition

    @cart_data = {
      composition: ComposerDecorator.new(@composition),
      cart:        CartDecorator.new(@cart),
      cart_items:  @cart.try(:cart_items),
      gift_code:   @cart.try(:coupon) || @wizard.coupon || Coupon.none.new
    }

    @cart_data.merge!(
      errors: @cart.errors.full_messages
    ) if @cart.errors.present?

    @cart_data
  end

  def render_wizard_response
    respond_to do |format|
      if @composition.persisted? && !(@composition.declined_payment? || @composition.errors.messages.present?)
        format.json { render json: composition_data, status: :created }
        format.js
      else
        format.json { render json: composition_data(with_error_messages: true), status: :unprocessable_entity }
        format.js
      end
    end
  end

  def composition_data(with_error_messages: false)
    @composition_data = {
      composition:   ComposerDecorator.new(@composition),
      cart:          CartDecorator.new(@wizard.cart),
      cart_items:    @wizard.cart.try(:cart_items),
      gift_code:     @wizard.cart.try(:coupon) || @wizard.try(:coupon) || Coupon.none.new,
      previous_step: @wizard.previous_step,
      step:          @wizard.current_step,
      next_step:     @wizard.next_step
    }

    @composition_data.merge!(
      errors: @composition.errors.full_messages
    ) if with_error_messages

    @composition_data
  end

  # Permitted params list for creator wizard
  def creator_wizard_params
    params.require(:composition).permit(
      :audio_file
    )
  end

  # Permitted parameters for 2ns step
  #   of the creator!
  def composition_info_params
    params.require(:composition).permit(
      :id,
      :title,
      :subtitle,
      :product_language_id,
      :genre,
      :lyrics,
      copyrights_attributes:  [:id, :name],
      scores_attributes:      [:id, :product_id, :scoring, :difficulty, :arrangement, :genre, :has_lyrics, :has_chords],
      authors_attributes:     [:id, :full_name_str, :author_type],
    )
  end

  def scoring_info_params
    params.require(:composition).permit(
      :id,
      :lyrics,
      :product_language_id,
      scores_attributes: [:id, :product_id, :scoring, :difficulty, :arrangement, :genre, :has_lyrics, :has_chords]
    )
  end

  def payment_params
    params.permit(:stripeToken, :invoice_only, :coupon, :utf8, :authenticity_token, :step_number, :billing_address, composition: [:id])
  end

  def get_permitted_params(step = 1)
    if session[:cart].present?
      params[:composition] ||= {}
      params[:composition][:id] = session[:cart].composition.id
    end

    case step
      when AUDIO_UPLOADING_STEP
        creator_wizard_params
      when ADD_DETAILS_STEP
        composition_info_params
      when ADD_VERSION_STEP
        scoring_info_params
      when CHECKOUT_STEP
        payment_params
      else
        {}
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_composition
    @composition = Composition.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def composition_params
    params.require(:composition).permit(:title, :subtitle, :key)
  end
end
