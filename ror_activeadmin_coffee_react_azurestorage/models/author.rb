# == Schema Information
#
# Table name: authors
#
#  id          :integer          not null, primary key
#  first_name  :string
#  last_name   :string           not null
#  author_type :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Author < ApplicationRecord
  has_many :author_roles, dependent: :delete_all
  has_many :contributions
  has_many :compositions, through: :contributions


  before_validation :split_full_name
  validates_presence_of :last_name, :author_type

  after_create :create_author_roles

  attr_accessor :full_name_str
  COMPOSER = 'composer'.freeze

  scope :artists, -> { by_type('artist') }
  scope :composers, -> { by_type('composer') }
  scope :lyricists, -> { by_type('lyricist') }
  scope :arrangers, -> { by_type('arranger') }
  scope :contributors, -> { by_type('contributor') }
  scope :collaborators, -> { by_type('collaborator') }
  scope :by_type, ->(author_type) { where('author_type ~* ?', author_type) }
  scope :order_by_contribution, -> { order('contributions.created_at ASC') }
  scope :by_query, ->(query) { where('authors.first_name ILIKE :term OR authors.last_name ILIKE :term', {term: "%#{query}%"}) }
  scope :no_composition, -> { left_outer_joins(:compositions).where(compositions: {id: nil}) }
  scope :no_contribution, -> { left_outer_joins(:contributions).where(contributions: {id: nil}) }

  def full_name
    [first_name, last_name].compact.join(' ').strip
  end

  def reversed_name
    first_name.present? ? [last_name, first_name].join(', ') : last_name
  end

  def split_full_name
    if full_name_str.present?
      names = full_name_str.strip.split(/ (?=\S+$)/)
      self.first_name = names.size > 1 ? names.first.strip : nil
      self.last_name = names.last
    end

    unless self.id.present?
      exist_artist = Author.where(author_type: author_type, first_name: first_name, last_name: last_name).first
      id = exist_artist.id if exist_artist.present?
    end
  end

  private

  def create_author_roles
    AuthorRole.create!(author_id: self.id, role: self.author_type.downcase)
  end

  RailsAdmin.config do |config|
    config.model Author do
      edit do
        fields do
          help false
        end
        exclude_fields :author_roles
      end
    end
  end
end
