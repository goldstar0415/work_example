# == Schema Information
#
# Table name: plans
#
#  id             :integer          not null, primary key
#  identity       :integer
#  name           :string
#  amount         :float            default(0.0)
#  currency_id    :integer
#  interval       :integer
#  interval_count :integer          default(1)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Plan < ApplicationRecord

  TAX_DE = 19.0 # percentage

  belongs_to :currency

  enum identity: ['basic', 'gold']
  enum interval: ['month', 'year']

  validates :identity, :name, uniqueness: true
  validates :identity, :name, :amount, :currency_id, :interval, presence: true

  after_create :create_stripe_plan
  after_update :update_stripe_plan
  after_destroy :delete_stripe_plan

  def create_stripe_plan
    return if stripe_plan.present?

    Stripe::Plan.create(
      :amount => self.amount_in_cents,
      :interval => self.interval,
      :interval_count => self.interval_count,
      :name => self.name,
      :currency => self.currency.code,
      :id => self.identity
    )
  end

  def update_stripe_plan
    plan = self.stripe_plan
    plan.name = self.name
    plan.save
  end

  def delete_stripe_plan
    plan = self.stripe_plan
    plan.delete
  end

  def stripe_plan
    begin
      Stripe::Plan.retrieve(self.identity)
    rescue
      nil
    end
  end

  def amount_in_cents
    (self.amount*100).to_i
  end

  def calculate_tax(user_id)
    user = User.find(user_id)
    country_code = user.country.try(:code)
    return 0.0 unless Country::EUROPE.include?(country_code)
    return 0.0 if user.vat_id.present? && country_code != 'DE'
    amount * (TAX_DE/100)
  end

  def amount_with_tax(user_id)
    self.amount + self.calculate_tax(user_id)
  end

  RailsAdmin.config do |config|
    config.model Plan do
      edit do
        fields do
          help false
        end
      end
      update do
        exclude_fields :identity, :amount, :currency, :interval, :interval_count
      end
    end
  end
end
