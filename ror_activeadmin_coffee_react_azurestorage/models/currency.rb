# == Schema Information
#
# Table name: currencies
#
#  id         :integer          not null, primary key
#  code       :string           not null
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Currency < ApplicationRecord
  has_many :prices

  validates_presence_of :name, :code
  validates_length_of :code, :is => 3
  validates :code, :uniqueness => {:case_sensitive => false}

  before_validation :uppercase_code

  scope :with_code, ->(code) { where(code: code) }

  def uppercase_code
    code.upcase!
  end

  def locale
    self.code == 'EUR' ? 'de' : 'en'
  end

  def self.default(country)
    code = Country::EUROPE.include?(country) ? "EUR" : "USD"
    return Currency.with_code(code).first
  end

  def self.find_with_code(code = 'eur')
    Currency.where('code ~* ?', code).first
  end

  RailsAdmin.config do |config|
    config.model Currency do
      edit do
        fields do
          help false
        end
      end
    end
  end
end
