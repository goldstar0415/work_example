# == Schema Information
#
# Table name: products
#
#  id                        :integer          not null, primary key
#  default_user_price        :decimal(, )      default(0.0)
#  default_transcriber_price :decimal(, )      default(0.0)
#  maximum_difficulties      :integer          default(0)
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  title                     :string           not null
#  code                      :string           not null
#  icon                      :json
#  highlighted_icon          :json
#  can_have_lyrics           :boolean
#  can_have_chords           :boolean
#  sort_index                :integer
#

class Product < ApplicationRecord
  translates :description, touch: true

  has_many :scores
  has_many :prices
  has_many :user_prices, -> { user_price }, class_name: 'Price', foreign_key: :product_id
  has_many :transcriber_prices, -> { transcriber_price }, class_name: 'Price', foreign_key: :product_id
  has_many :product_skills
  has_many :skills, through: :product_skills

  accepts_nested_attributes_for :prices, allow_destroy: true
  accepts_nested_attributes_for :translations, allow_destroy: true
  accepts_nested_attributes_for :product_skills, allow_destroy: true

  mount_uploader :icon, LogoUploader
  mount_uploader :highlighted_icon, LogoUploader

  # Score based scope to find product quickly for specific score
  scope :by_code, ->(code) { where(code: code) }
  scope :active, -> { where(active: true) }
  scope :as_sort_index, -> { order(:sort_index) }

  validates :code, uniqueness: true, presence: true
  validates :sort_index, uniqueness: true, if: :should_validate?
  before_create :set_sort_index
  after_save :update_product_design

  def update_product_design
    if self.title_changed?
      self.scores.created.each do |score|
        if score.score_files.only_pdf.present?
          UpdateScoreProductJob.perform_async(score.id)
          UpdateEpubJob.perform_async(score.id)
        end
      end
    end
  end

  def should_validate?
    new_record? || sort_index.present?
  end

  def self.next_sort_index
    Product.count.zero? ? 1 : Product.as_sort_index.last.sort_index.to_i + 1
  end

  def set_sort_index
    self.sort_index ||= Product.next_sort_index
  end

  def uberchord?
    self.code == 'uberchord'
  end

  def html_class
    case
      when self.pdf?
        'product-pdf'
      when self.muse_score?
        'product-muse-score'
      when self.sibelius?
        'product-sibelius'
      when self.finale?
        'product-finale'
      when self.music_xml?
        'product-music-xml'
      else
        "product-regular"
    end
  end

  def product_type
    str = html_class[/product-(.*)/, 1]
    return '' if str.nil?
    str == "regular" ? "Audio" : str.titleize
  end

  RailsAdmin.config do |config|
    config.model Product do
      configure :translations, :globalize_tabs


      show do
        field :default_user_price do
          formatted_value do
            '%.2f' % value
          end
        end

        field :default_transcriber_price do
          formatted_value do
            '%.2f' % value
          end
        end

        field :maximum_difficulties
        field :title
        field :code
        field :icon
        field :highlighted_icon
        field :translations
        field :user_prices
        field :scores
      end
      update do
        exclude_fields :code
      end

    end

    config.model 'Product::Translation' do
      visible false
      configure :locale, :hidden do
        help ''
      end
      include_fields :locale, :description
    end
  end
end
