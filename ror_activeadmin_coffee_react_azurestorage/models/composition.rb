# == Schema Information
#
# Table name: compositions
#
#  id                  :integer          not null, primary key
#  title               :string           not null
#  subtitle            :string
#  key                 :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  product_language_id :integer          not null
#  audio_file          :string
#  user_id             :integer          default(1), not null
#  genre               :string
#  creator_price       :decimal(, )
#  freelancer_price    :decimal(, )
#  payment_confirmed   :boolean
#  transaction_number  :string
#  payment_status      :integer          default("not_paid")
#  lyrics              :text
#  checkout            :boolean          default(FALSE)
#

class Composition < ApplicationRecord
  has_many :scores
  belongs_to :user
  has_one :cart
  has_many :cart_items, through: :cart
  has_one :payment
  has_one :invoice, dependent: :destroy
  has_many :purchase_orders, dependent: :delete_all
  has_many :copyrights, dependent: :destroy
  belongs_to :product_language, class_name: 'Language'
  has_many :contributions
  has_many :authors, through: :contributions
  has_and_belongs_to_many :countries, join_table: :rights_countries

  enum key: [
    :a_major,
    :a_minor,
    :a_sharp_major,
    :a_sharp_minor,
    :a_flat_major,
    :a_flat_minor,
    :b_major,
    :b_minor,
    :b_sharp_major,
    :b_sharp_minor,
    :b_flat_major,
    :b_flat_minor,
    :c_major,
    :c_minor,
    :c_sharp_major,
    :c_sharp_minor,
    :c_flat_major,
    :c_flat_minor,
    :d_major,
    :d_minor,
    :d_sharp_major,
    :d_sharp_minor,
    :d_flat_major,
    :d_flat_minor,
    :e_major,
    :e_minor,
    :e_sharp_major,
    :e_sharp_minor,
    :e_flat_major,
    :e_flat_minor,
    :f_major,
    :f_minor,
    :f_sharp_major,
    :f_sharp_minor,
    :f_flat_major,
    :f_flat_minor,
    :g_major,
    :g_minor,
    :g_sharp_major,
    :g_sharp_minor,
    :g_flat_major,
    :g_flat_minor
  ]

  keys_map = {}
  keys_map[[0, :major]] = :c_major
  keys_map[[0, :minor]] = :a_minor
  keys_map[[1, :major]] = :d_flat_major
  keys_map[[1, :minor]] = :b_flat_minor
  keys_map[[2, :major]] = :d_major
  keys_map[[2, :minor]] = :b_minor
  keys_map[[3, :major]] = :e_flat_major
  keys_map[[3, :minor]] = :c_minor
  keys_map[[4, :major]] = :e_major
  keys_map[[4, :minor]] = :c_sharp_minor
  keys_map[[5, :major]] = :f_major
  keys_map[[5, :minor]] = :d_minor
  keys_map[[6, :major]] = :f_sharp_major
  keys_map[[6, :minor]] = :d_sharp_minor
  keys_map[[7, :major]] = :g_major
  keys_map[[7, :minor]] = :e_minor
  keys_map[[8, :major]] = :a_flat_major
  keys_map[[8, :minor]] = :f_minor
  keys_map[[9, :major]] = :a_major
  keys_map[[9, :minor]] = :f_sharp_minor
  keys_map[[10, :major]] = :b_flat_major
  keys_map[[10, :minor]] = :g_minor
  keys_map[[11, :major]] = :b_major
  keys_map[[11, :minor]] = :g_sharp_minor

  validate :validate_content, on: :create
  validates :audio_file, presence: true, on: :update
  validates_presence_of :title, :product_language, :user


  attr_accessor :first_author

  accepts_nested_attributes_for :authors,
                                reject_if: :all_blank,
                                allow_destroy: true

  accepts_nested_attributes_for :copyrights,
                                allow_destroy: true

  accepts_nested_attributes_for :scores,
                                reject_if: :all_blank,
                                allow_destroy: true

  after_save :export_lyrics_text_file # Ensure export, if updated in RailsAdmin

  mount_uploader :audio_file, AudioFileUploader

  scope :without_score, ->(user_id) { where(user_id: user_id).left_outer_joins(:scores).group("compositions.id").having("count(scores.composition_id) = ?", 0) }


  enum payment_status: [
    :not_paid,
    :payment_paid,
    :declined_payment
  ]

  def main_composer
    main_contribution = self.contributions.find_by(main_composer: true)
    main_contribution.present? ? main_contribution.author : self.authors.order(:id).first
  end

  def other_authors
    self.authors.where.not(id: self.main_composer.id)
  end

  def composer_name
    self.main_composer.try(:full_name) || 'Unnamed'
  end

  def composer_name_nd
    self.main_composer.try(:reversed_name) || 'Unnamed'
  end

  def artists_name
    self.authors.artists.collect(&:full_name).reverse.join(", ")
  end

  def artists_name_sort_last_name
    self.authors.artists.order(:last_name, :first_name).collect(&:full_name).join(", ")
  end

  def artists_name_nd
    self.authors.artists.order(:last_name, :first_name).collect(&:reversed_name).join("/")
  end

  def lyricists_name
    self.authors.lyricists.collect(&:full_name).reverse.join(", ")
  end

  def lyricists_name_sort_last_name
    self.authors.lyricists.order(:last_name, :first_name).collect(&:full_name).join(", ")
  end

  def lyricists_name_nd
    self.authors.lyricists.order(:last_name, :first_name).collect(&:reversed_name).join("/")
  end

  def composers_name
    self.authors.composers.collect(&:full_name).reverse.join(", ")
  end

  def composers_name_sort_last_name
    self.authors.composers.order(:last_name, :first_name).collect(&:full_name).join(", ")
  end

  def composers_name_nd
    self.authors.composers.order(:last_name, :first_name).collect(&:reversed_name).join("/")
  end

  def arrangers_name
    self.authors.arrangers.collect(&:full_name).reverse.join(", ")
  end

  def arrangers_name_sort_last_name
    self.authors.arrangers.order(:last_name, :first_name).collect(&:full_name).join(", ")
  end

  def arrangers_name_nd
    self.authors.arrangers.order(:last_name, :first_name).collect(&:reversed_name).join("/")
  end

  def authors_name_nd
    [self.composers_name_nd, self.arrangers_name_nd].reject(&:blank?).join('/')
  end

  def author_name_for_sales
    artists = self.authors.artists
    artists.present? ? self.artists_name_sort_last_name : self.composer_name
  end

  def transcriber_emails
    transcribers_to_exclude = ["sascha.grollmisch@idmt.fraunhofer.de", "neildwood@hotmail.com"]
    transcribers = User.transcriber.where.not(email: transcribers_to_exclude)
    transcribers_to_exclude_for_users = ["gabie.alphieri@gmail.com", "sebastian.genzink@mh-luebeck.de"]
    users_to_exclude = ['simo_erli@hotmail.it', 'simone@soundnotation.com']
    transcribers = transcribers.where.not(email: transcribers_to_exclude_for_users) if Rails.env.production? && users_to_exclude.include?(user.email)
    transcribers.order(:ranking).map(&:email)
  end

  def key_terms
    return [] if self.key.blank?
    terms = self.key.split('_')
    [terms.shift, terms.join(' ')]
  end

  def key_terms_notendownload
    return [] if self.key.blank?
    terms = self.key.split('_')
    last = terms.pop
    [terms.join(' '), last]
  end

  def lyric_file_path
    lyric_file_name = "composition-#{self.id}.txt"
    File.join('data', 'lyrics_files', lyric_file_name)
  end

  def export_lyrics_text_file
    return if self.lyrics.blank?
    lyrics = lyrics_formatting
    parent_dir = File.dirname lyric_file_path
    FileUtils.mkdir_p(parent_dir) unless File.directory? parent_dir
    File.open(lyric_file_path, 'w') { |f| f << lyrics }
  rescue => e
    logger.error "Error occurred while exporting lyrics for composition id: #{self.id}\n\tError Details: #{e.message}"
  end

  def lyrics_formatting
    lyrics = self.lyrics
    locale = self.product_language.lang_locale.downcase.to_sym
    # remove all numbers followed by a closed parenthesis, e.g. "3)"
    lyrics.gsub!(/\d+\)/, '')
    # transliterate groups of numbers regardless of punctuation in between
    lyrics.gsub!(/\b(\d+([,.]\d+)*)\b/) {|v| v.gsub(/[,.]/, '').to_i.n_to_words(locale)}
    # transliterate remaining numbers
    lyrics.gsub!(/\d+/) {|v| " #{v.to_i.n_to_words(locale)} "}
    # remove "Ref."
    lyrics.gsub!("Ref.", "")
    # remove "\t"
    lyrics.gsub!("\t", "")
    # replace "strange" apostrophies like ´, `, ‘ with '
    lyrics.gsub!("´", "'")
    lyrics.gsub!("`", "'")
    lyrics.gsub!("‘", "'")
    # remove header
    # lyrics.gsub!("\A(.*?)\\r\\n\\r\\n", "") # doesn't work somehow, although tested online at http://rubular.com/
    valid_lyrics = lyrics.split("\r\n\r\n")[1..-1]
    lyrics = valid_lyrics.join("\r\n\r\n") if valid_lyrics.size > 1
    valid_lyrics = lyrics.split("\n\n")[1..-1]
    lyrics = valid_lyrics.join("\n\n") if valid_lyrics.size > 1
    lyrics
  end

  def author_lines
    authors_hash = {
      artist: artists_name_sort_last_name,
      composer: composers_name_sort_last_name,
      lyricist: lyricists_name_sort_last_name,
      arranger: arrangers_name_sort_last_name
    }
    final_hash = {}
    split_seq = ", "
    seq_end = ","
    max_authors_per_line = 3
    authors_hash.each do |k, v|
      before_names = k.to_s.capitalize
      composers = v.split(split_seq)
      composers_number = composers.length
      before_names += "s" if composers_number > 1
      composers_lines = []
      if composers_number > 0
        composers_line_count = composers_number / max_authors_per_line + 1;
        composers_line_count = composers_line_count - 1 if composers_number % max_authors_per_line == 0
        (0..composers_line_count - 2).each do |i|
          res = ""
          (0..max_authors_per_line - 1).each do |j|
            res += composers[max_authors_per_line * i + j]
            res += split_seq if j != max_authors_per_line - 1
          end
          composers_lines << res + seq_end unless res.empty?
        end
        res = ""
        ((composers_line_count - 1) * max_authors_per_line..composers_line_count * max_authors_per_line - 1).each do |i|
          if (i < composers_number)
            res += composers[i]
            res += split_seq if i != composers_number - 1
          end
        end
        composers_lines << res unless res.empty?
      end
      final_hash[k] = {}
      final_hash[k][:column] = before_names
      final_hash[k][:lines] = composers_lines
    end
    final_hash
  end

  def is_audio?
    file_type = File.extname(self.audio_file.path)
    file_type.is_audio?
  end

  def product_type
    file_type = File.extname(self.audio_file.path)

    case
      when file_type.is_audio?
        'Audio'
      when file_type.is_muse_score?
        'MuseScore'
      when file_type.is_sibelius?
        'Sibelius'
      when file_type.is_finale?
        'Finale'
      when file_type.is_music_xml?
        'MusicXml'
      when file_type.is_pdf?
        'Pdf'
      else
        nil
    end
  end

  def copyright
    self.copyrights.collect(&:name).join(", ")
  end

  def add_author(author_id)
    return if author_id.blank? || self.authors.exists?(author_id)
    self.authors << Author.find(author_id)
  end

  private

  def validate_content
    file_type = File.extname(self.audio_file.path)

    case
      when file_type.is_audio?
        return true
      when file_type.is_muse_score?
        self.muse_score_file = true
      when file_type.is_sibelius?
        self.sibelius_file = true
      when file_type.is_finale?
        self.finale_file = true
      when file_type.is_music_xml?
        self.music_xml_file = true
      when file_type.is_pdf?
        pdf = PDF::Reader.new(self.audio_file.path)
        self.pdf_file = true
        self.pdf_page_count = pdf.page_count
      else
        return false
    end

  rescue => e
    self.errors.add(:base, e.message)
  end

  RailsAdmin.config do |config|
    config.model Composition do
      list do
        configure :countries do
          label "Rights countries"
        end
        items_per_page 100
      end
      edit do
        fields do
          help false
        end
        field :countries do
          label "Rights countries"
        end
        field :id do
          def value
            bindings[:object].id
          end
        end
        configure :id do
          read_only true
        end
      end
    end
  end
end
