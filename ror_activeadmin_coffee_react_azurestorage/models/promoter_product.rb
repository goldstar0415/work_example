# == Schema Information
#
# Table name: promoter_products
#
#  id                 :integer          not null, primary key
#  title              :string
#  sort_index         :integer
#  icon               :json
#  highlighted_icon   :json
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  code               :string
#  has_promotion_date :boolean          default(TRUE)
#  price_id           :integer
#

class PromoterProduct < ApplicationRecord
  translates :description, touch: true
  accepts_nested_attributes_for :translations, allow_destroy: true

  belongs_to :price
  has_many :promotions


  validates :sort_index, uniqueness: true, on: :create
  before_create :set_sort_index

  mount_uploader :icon, LogoUploader
  mount_uploader :highlighted_icon, LogoUploader

  scope :as_sort_index, -> { order(:sort_index) }


  def self.next_sort_index
    PromoterProduct.as_sort_index.last.sort_index.to_i + 1
  end

  def set_sort_index
    self.sort_index ||= PromoterProduct.next_sort_index
  end


  RailsAdmin.config do |config|
    config.model PromoterProduct do
      configure :translations, :globalize_tabs

      edit do
        configure :promotions do
          hide
        end
      end
    end

    config.model 'PromoterProduct::Translation' do
      visible false
      configure :locale, :hidden do
        help ''
      end
      include_fields :locale, :description
    end
  end
end
