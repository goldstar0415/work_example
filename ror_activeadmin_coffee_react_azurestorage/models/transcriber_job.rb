# == Schema Information
#
# Table name: transcriber_jobs
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  score_id   :integer
#  title      :string
#  deadline   :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TranscriberJob < ApplicationRecord
  belongs_to :user
  belongs_to :score

  delegate :original_xml_path, :output_xml_path, :audio_file_url, :composer_name, :composition_id, to: :score

  validates :score, presence: true, uniqueness: true

  scope :with_details, -> { includes(score: [:composition, :product, :translations]).where(user: nil) }
  scope :open, -> { where(user: nil) }
  scope :incomplete, -> { joins(:score).where.not('scores.status': Score.statuses[:created]) }
  scope :no_pdf, -> { where(score: Score.joins(:product).where.not('products.pdf': true)) }

  scope :composition_includes, ->(search) {
    current_scope = self
    search.split.uniq.each do |word|
      current_scope = current_scope.joins(score: :composition).where('compositions.title ILIKE ?', "%#{word}%")
    end
    current_scope
  }

  def self.ransackable_scopes(auth_object = nil)
    [:composition_includes]
  end

  def timer_duration
    48.hours
  end

  def acceptance_date
    return unless deadline
    deadline - timer_duration
  end

  def duration_to_end
    return unless deadline
    (deadline - Time.now).to_i
  end

  def as_json(options = {})
    super(options.merge(
      include: {score: {
        include: [:product, composition: {methods: :product_type}],
        methods: :transcriber_price
      }},
      methods: [:duration_to_end, :original_xml_path, :output_xml_path, :audio_file_url, :composer_name],
      ))
  end

  RailsAdmin.config do |config|
    config.model TranscriberJob do
      edit do
        fields do
          help false
        end
      end
      list do
        field :composition_title do
          def value
            bindings[:object].score.composition.title
          end
        end
        field :copyright do
          def value
            bindings[:object].score.composition.copyright
          end
        end
        include_all_fields
      end
    end
  end
end
