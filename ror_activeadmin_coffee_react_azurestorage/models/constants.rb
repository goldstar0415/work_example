class Constants
  COVER_BACKGROUND_IMAGE = ['Cover_Image_1', 'Cover_Image_2', 'Cover_Image_3']
  SQUARE_COVER_BACKGROUND_IMAGE = ['Square_Cover_Image_1', 'Square_Cover_Image_2', 'Square_Cover_Image_3']

  ICONS = {
    leadsheet: {
      icon: File.new(Rails.root.join('app', 'assets', 'images', 'purchase', 'notes-normal.png')),
      highlighted_icon: File.new(Rails.root.join('app', 'assets', 'images', 'purchase', 'notes-red.png'))
    },

    chordsheet: {
      icon: File.new(Rails.root.join('app', 'assets', 'images', 'purchase', 'hammas-normal.png')),
      highlighted_icon: File.new(Rails.root.join('app', 'assets', 'images', 'purchase', 'hammas-red.png'))
    },

    piano_solo: {
      icon: File.new(Rails.root.join('app', 'assets', 'images', 'purchase', 'keys-normal.png')),
      highlighted_icon: File.new(Rails.root.join('app', 'assets', 'images', 'purchase', 'keys-red.png'))
    },

    piano_accompaniment: {
      icon: File.new(File.join(Rails.root, 'app', 'assets', 'images', 'purchase', 'human-keys-normal.png')),
      highlighted_icon: File.new(File.join(Rails.root, 'app', 'assets', 'images', 'purchase', 'human-keys-red.png'))
    },

    piano_lyrics: {
      icon: File.new(File.join(Rails.root, 'app', 'assets', 'images', 'purchase', 'human-guitar-normal.png')),
      highlighted_icon: File.new(File.join(Rails.root, 'app', 'assets', 'images', 'purchase', 'human-guitar-red.png'))
    },

    guitar_tabs: {
      icon: File.new(File.join(Rails.root, 'app', 'assets', 'images', 'purchase', 'bass-guitar-normal.png')),
      highlighted_icon: File.new(File.join(Rails.root, 'app', 'assets', 'images', 'purchase', 'bass-guitar-red.png'))
    },

    original_bassline: {
      icon: File.new(File.join(Rails.root, 'app', 'assets', 'images', 'purchase', 'guitar-normal.png')),
      highlighted_icon: File.new(File.join(Rails.root, 'app', 'assets', 'images', 'purchase', 'guitar-red.png'))
    }
  }
end
