# == Schema Information
#
# Table name: scores
#
#  id                      :integer          not null, primary key
#  gtin                    :integer
#  ismn13                  :integer
#  publishing_date         :date
#  pages                   :integer
#  notafina_shop           :string
#  scoring                 :integer          not null
#  difficulty              :integer
#  arrangement             :integer
#  genre                   :integer          default("Blues")
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  composition_id          :integer          not null
#  cover                   :string
#  temp                    :boolean          default(FALSE)
#  product_id              :integer
#  status                  :integer          default("default")
#  keywords                :string
#  beats_per_minute        :integer
#  delivery_status         :integer          default("not_delivered")
#  price                   :integer          default("2.99")
#  world_release           :boolean          default(TRUE)
#  custom_world_release    :boolean          default(FALSE)
#  subscription_created_at :datetime
#  has_lyrics              :boolean
#  has_chords              :boolean
#  demo_product            :boolean          default(FALSE)
#  promoted                :boolean          default(FALSE)
#  cover_logo              :string
#

class Score < ApplicationRecord

  belongs_to :composition
  belongs_to :product
  has_one :transcriber_job
  has_one :invoice_item #FIXME every score should have one invoice item.
  has_one :cart_item #FIXME every score should have one cart item.
  has_many :score_files
  has_one :sheet_music_player
  has_one :user, through: :composition
  has_many :promotions, dependent: :delete_all
  has_and_belongs_to_many :shops, join_table: :rights_shops
  has_and_belongs_to_many :recording_shops, join_table: :rights_recording_shops
  has_and_belongs_to_many :countries, join_table: :released_countries
  has_and_belongs_to_many :restricted_countries, class_name: 'Country', join_table: :restricted_countries

  has_many :main_descriptions, dependent: :delete_all do
    def sort_by_language(preferences = ["GER", "ENG"])
      sort_ids = []
      preferences.each do |p|
        each do |d|
          (sort_ids << d.id && break) if d.language.code == p
        end
      end
      each do |d|
        (sort_ids << d.id && break) if !preferences.include?(d.language.code)
      end
      sort_by { |d| sort_ids.index(d.id) }
    end

    def one_by_language
      ids = []
      language_ids = pluck(:language_id).uniq
      language_ids.each do |l|
        each do |d|
          (ids << d.id && break) if d.language_id == l
        end
      end
      where(id: ids)
    end
  end

  accepts_nested_attributes_for :score_files, allow_destroy: true

  validates_presence_of :product_id, :scoring, :genre, :composition
  validates :gtin, allow_blank: true, uniqueness: true, if: :original?
  validates :gtin, :numericality => {:greater_than => 0}, allow_blank: true, if: :original?
  validate :validate_cover_ratio, :validate_square_cover_ratio

  delegate :composer_name, :author_name_for_sales, to: :composition
  attr_accessor :cover_background_image, :square_cover_background_image, :skip_combine_pdf_files

  serialize :descriptions, Hash

  mount_uploader :cover, CoverUploader
  mount_uploader :cover_logo, CoverLogoUploader
  mount_uploader :square_cover, SquareCoverUploader
  mount_uploader :combined_cover_pdf_score, CombinedCoverPdfScoreUploader
  mount_uploader :combined_epub, CombinedEpubUploader
  mount_uploader :cover_thumb, CoverThumbUploader

  accepts_nested_attributes_for :main_descriptions,
                                reject_if: :all_blank,
                                allow_destroy: true

  after_create :set_default_cover, :set_default_square_cover
  after_create :create_transcriber_job!
  after_save :upload_notation
  before_destroy :destroy_chain
  after_update :score_created_email, :score_creation_upload_email
  after_update :combine_pdf_files, unless: :skip_combine_pdf_files

  before_save :remove_blank_emails, :check_gtin

  default_scope { order(created_at: :desc) }
  scope :by_ids, ->(ids) { where(id: ids) }
  scope :in_created, -> { by_status('created') }
  scope :by_status, ->(status) { where(status: status) }
  scope :with_demo, ->(status) { where(demo_product: status) }
  scope :with_paginate, ->(page, per_page) { paginate(:page => page, :per_page => per_page) }
  scope :with_user, ->(user_id) { joins(:composition).where('compositions.user_id = ?', user_id) }
  scope :with_user_demo, ->(user_id) { joins(:composition).where('scores.demo_product = true OR compositions.user_id = ?', user_id) }

  scope :composition_includes, ->(search) {
    current_scope = self
    search.split.uniq.each do |word|
      current_scope = current_scope.joins(:composition).where('compositions.title ILIKE ?', "%#{word}%")
    end
    current_scope
  }

  scope :user_includes, ->(search) {
    current_scope = self
    search.split.uniq.each do |word|
      current_scope = current_scope.joins(:user).where('users.first_name ILIKE :term OR users.last_name ILIKE :term', {term: "%#{word}%"})
    end
    current_scope
  }

  scope :search_with_composition, ->(search) {
    current_scope = self
    search.split.uniq.each do |word|
      current_scope = current_scope.joins(:composition => :authors).where('compositions.title ILIKE :term OR authors.first_name ILIKE :term OR authors.last_name ILIKE :term', {term: "%#{word}%"}).distinct(&:id)
    end
    current_scope
  }


  def self.ransackable_scopes(auth_object = nil)
    [:composition_includes, :user_includes]
  end

  GENRE_DISPLAY = [
    :"Blues",
    :"Children's Music",
    :"Country",
    :"Electronic",
    :"Singer/Songwriter",
    :"Folk",
    :"Pop",
    :"R&B/Soul",
    :"Dance",
    :"Hip-Hop/Rap",
    :"Rock",
    :"Alternative Rock",
    :"Vocal"
  ]

  GENRE_ADMIN = [
    :"Jazz",
    :"Classical",
    :"Afro",
    :"Bossa Nova",
    :"Funk",
    :"Gaita de Furro",
    :"Reggae",
    :"Rumba",
    :"Salsa",
    :"Samba",
    :"Ska",
    :"Swing",
    :"Tango",
    :"Flamenco"
  ]

  GENRE = GENRE_DISPLAY + GENRE_ADMIN

  enum genre: GENRE

  enum difficulty: [
    :beginner,
    :easy,
    :intermediate,
    :early_advanced,
    :advanced
  ]

  enum arrangement: [
    :original,
    :jazz
  ]

  enum status: {
    default: 0,
    in_creation: 1,
    in_creation_preselect: 2,
    in_creation_download: 3,
    in_creation_upload: 4,
    created: 5,
    preprocessing: 6,
    in_creation_layout: 7,
  }

  enum delivery_status: {
    not_delivered: 0,
    delivery_processing: 1,
    delivery_processed: 2,
    update_processing: 3,
    update_processed: 4,
    take_down_processing: 5,
    take_down_processed: 6,
  }

  enum price: [
    '1.99',
    '2.49',
    '2.99',
    '3.99',
    '4.49',
    '4.99',
    '5.99',
    '6.99',
    '7.99',
    '8.99',
    '9.99',
    '19.99'
  ]

  def original?
    !temp?
  end

  def first_description(language_code)
    self.main_descriptions.where(language_id: Language.find_by(code: language_code).id).first
  end

  def difficulty_key
    Score.difficulties[self.difficulty]
  end

  def user_price
    price = self.product.user_prices.find_by(difficulty: difficulty_key).try(:value)
    price ||= product.default_user_price.to_f
    price = price * self.composition.pdf_page_count if self.product.pdf?
    price += 10 if self.has_lyrics?
    price += 10 if self.has_chords?
    price
  end

  def transcriber_price
    price = self.product.transcriber_prices.find_by(difficulty: difficulty_key).try(:value)
    price ||= product.default_transcriber_price.to_f
    price = price * self.composition.pdf_page_count if self.product.pdf?
    price += 5 if self.has_lyrics?
    price += 5 if self.has_chords?
    price
  end

  def original_xml_path
    "/audio_player_xml/composition-#{composition_id}/original.xml"
  end

  def preselected_xml_path
    xml_file_path('preselected')
  end

  def output_xml_path
    msa = xml_file_path('output-msa')
    Rails.public_path.join(msa.sub('/', '')).file? ? msa : xml_file_path('output')
  end

  def xml_file_path(name)
    "/audio_player_xml/composition-#{composition_id}/score-#{id}-#{name}.xml"
  end

  def audio_file_url
    composition.audio_file.url
  end

  def musical_file_url
    musical_xml = self.score_files.select { |score_file| score_file.music_xml? }.first
    musical_xml.present? ? musical_xml.path.url : nil
  end

  def first_file(format)
    self.score_files.format(format).first
  end

  def flac_file
    self.first_file('flac')
  end

  def shops_name
    self.shops.collect(&:name).join(", ")
  end

  def recording_shops_name
    self.recording_shops.collect(&:name).join(", ")
  end

  def combined_title
    self.gtin || [self.composition.title, self.product.title].join(" - ")
  end

  def take_down?
    ['take_down_processing', 'take_down_processed'].include?(self.delivery_status)
  end

  def delivered?
    ['delivery_processing', 'delivery_processed', 'update_processing', 'update_processed'].include?(self.delivery_status)
  end

  def delivered_no_update?
    ['delivery_processing', 'delivery_processed'].include?(self.delivery_status)
  end

  def updated?
    ['update_processing', 'update_processed'].include?(self.delivery_status)
  end

  def zero_difficulties?
    self.try(:product).try(:maximum_difficulties) == 0
  end

  def leadsheet?
    self.scoring = 'leadsheet'
  end

  def chordsheet?
    self.scoring = 'chordsheet'
  end

  def piano_solo?
    self.scoring = 'piano_solo'
  end

  def piano_accompaniment?
    self.scoring = 'piano_accompaniment'
  end

  def promoted!
    self.update_column(:promoted, true)
  end

  def new_cover?
    self.cover.path.blank? || self.cover_background_image.present?
  end

  def new_square_cover?
    self.square_cover.path.blank? || self.square_cover_background_image.present?
  end

  def cover_changed?
    self.cover.path.present? && self.cover_was.path != self.cover.path
  end

  def square_cover_changed?
    self.square_cover.path.present? && self.square_cover_was.path != self.square_cover.path
  end

  def self.by_user_status(user, options = {})
    query = options.fetch(:query, nil)
    status = options.fetch(:status, nil)
    query.blank? ? Score.search_default(user, status) : Score.search_with_query(user, query, status)
  end

  def self.search_default(user, status, options = {})
    return Score.with_demo(false) if user.admin? && status.blank?
    return Score.by_status(status).with_demo(false) if user.admin? && status.present?

    if user.show_demo_product?
      return Score.with_user_demo(user.id) if status.blank?
      return Score.with_user_demo(user.id).by_status(status) if status.present?
    else
      return Score.with_user(user.id) if status.blank?
      return Score.with_user(user.id).by_status(status) if status.present?
    end
  end

  def self.search_with_query(user, query, status, options = {})
    return Score.with_demo(false).search_with_composition(query) if user.admin? && status.blank?
    return Score.by_status(status).with_demo(false).search_with_composition(query) if user.admin? && status.present?

    if user.show_demo_product?
      return Score.with_user_demo(user.id).search_with_composition(query) if status.blank?
      return Score.with_user_demo(user.id).by_status(status).search_with_composition(query) if status.present?
    else
      return Score.with_user(user.id).search_with_composition(query) if status.blank?
      return Score.with_user(user.id).by_status(status).search_with_composition(query) if status.present?
    end
  end

  def self.next_isrc_code
    isrc_codes = Score.where.not(isrc_code: [nil, ""]).collect(&:isrc_code)
    isrc_codes = isrc_codes.collect { |code| code.scan(/\d+/).first.to_i }
    isrc_codes.empty? ? "DETL61500001" : "DETL#{isrc_codes.max + 1}"
  end

  def cover_from_user?
    cover = self.cover
    file = cover.file
    cover.distributor_download.url.present? && (!Constants::COVER_BACKGROUND_IMAGE.include? File.basename(file.filename, ".#{file.extension}"))
  end

  def cover_uploading?
    cover = self.cover
    return false if cover.blank?
    cover.path.include? Rails.root.to_s
  end

  def cover_logo_uploading?
    cover_logo = self.cover_logo
    return false if cover_logo.blank?
    cover_logo.path.include? Rails.root.to_s
  end

  private

  def check_gtin
    return true unless gtin_changed?
    unless gtin.blank?
      return false unless Score.where(:gtin => gtin).empty?
      gt = GTin.where(:gtin => gtin.to_s).first
      unless gt.nil?
        return false if gt.used == true
        gt.update_attributes!({:used => true, :score_id => id})
      end
    end
    unless gtin_was.blank?
      gt = GTin.where(:gtin => gtin_was.to_s).first
      gt.update_attributes!({:used => false, :score_id => nil}) unless gt.nil?
    end
    true
  end

  def score_created_email
    if status_changed? && self.created? && !already_created?
      ScoreCreatedMailerJob.perform_async(mailer_class = 'ScoreCreatedMailer', mailer_action = 'score_created', composition.id, id, composition.user.id)
      update_column :already_created, true
    end
  end

  def score_creation_upload_email
    if status_changed? && self.in_creation_upload?
      ScoreCreationUploadJob.perform_async(mailer_class = 'ScoreCreationUploadMailer', mailer_action = 'score_creation_upload', composition.id, id)
    end
  end

  def destroy_chain
    self.transcriber_job.destroy if self.transcriber_job.present?
    self.score_files.collect(&:destroy) if self.score_files.present?
    InvoiceItem.where(score_id: self.id).collect(&:destroy)
  end

  def remove_blank_emails
    artists_names.reject!(&:blank?) unless artists_names.nil?
  end

  def combine_pdf_files
    if self.created? && (status_changed? || self.cover_changed?)
      self.skip_combine_pdf_files = true #skip callback trigger multiple time
      is_saved = false
      if self.cover_from_user? && self.cover_uploading?
        self.cover_thumb = File.open(self.cover.path)
        is_saved = self.save!
        ActiveRecord::Base.after_transaction do
          UpdateScoreProductJob.perform_async(self.id) if self.created? && self.score_files.only_pdf.present? && is_saved
        end
      else

        response = FinalProductDesign.new(self).design

        self.combined_cover_pdf_score = File.open(response[:final_pdf_path]) if response[:upload_to_storage]

        tmp_img_path = response[:tmp_file_path].join('cover_thumb.jpg')
        im = Magick::Image.read(response[:prawn_cover_pdf])
        im[0].write(tmp_img_path)
        self.cover_thumb = File.open(tmp_img_path)
        is_saved = self.save!

        FileUtils.rm_rf(response[:tmp_file_path])
      end
      ActiveRecord::Base.after_transaction do
        UpdateEpubJob.perform_async(self.id) if self.created? && self.score_files.only_pdf.present? && is_saved
      end
    elsif self.cover_changed?
      # case where score is not created yet: no need to save combined cover pdf score, but cover thumb
      self.skip_combine_pdf_files = true #skip callback trigger multiple time
      cover = self.cover
      # if cover different than default, set as cover
      if self.cover_from_user?
        self.cover_thumb = open(cover.path)
      #otherwise, set it from design
      else
        response = FinalProductDesign.new(self, only_cover = true).design
        tmp_img_path = response[:tmp_file_path].join('cover_thumb.jpg')
        im = Magick::Image.read(response[:prawn_cover_pdf])
        im[0].write(tmp_img_path)
        self.cover_thumb = File.open(tmp_img_path)
        FileUtils.rm_rf(response[:tmp_file_path])
      end
      self.save
    end
  end

  def validate_cover_ratio
    if self.cover_changed? && !self.new_cover?
      begin
        image = MiniMagick::Image.open(cover.path)
        ratio = image[:height].to_f / image[:width].to_f
        unless ratio >= 1.40 && ratio <= 1.43
          errors.add :cover, "ratio should observe the A4 format (1.40 - 1.42): current ratio is #{'%.2f' % ratio} (height: #{image[:height]} px, width: #{image[:width]} px)"
        end
      rescue => e
        logger.error "Error validating cover photo :: #{e.message}"
      end
    end
  end

  def validate_square_cover_ratio
    if self.square_cover_changed? && !self.new_square_cover?
      begin
        image = MiniMagick::Image.open(square_cover.path)
        ratio = image[:height].to_f / image[:width].to_f
        unless ratio >= 0.98 && ratio <= 1.02
          errors.add :cover, "ratio should observe the square format (0.98 - 1.02): current ratio is #{'%.2f' % ratio} (height: #{image[:height]} px, width: #{image[:width]} px)"
        end
      rescue => e
        logger.error "Error validating square cover photo :: #{e.message}"
      end
    end
  end

  def set_default_cover
    if self.new_cover?
      self.cover_background_image ||= 'Cover_Image_1.jpg'
      self.cover = File.open(Rails.root.join('app', 'assets', 'images', 'cover_background', cover_background_image))
      self.save
    end
  end

  def set_default_square_cover
    if self.new_square_cover?
      self.square_cover_background_image ||= 'Square_Cover_Image_1.jpg'
      self.square_cover = File.open(Rails.root.join('app', 'assets', 'images', 'cover_background', square_cover_background_image))
      self.save
    end
  end

  def upload_notation
    SheetMusicPlayerJob.perform_async(self.id, self.musical_file_url) if self.status_changed? && self.created? && self.musical_file_url.present?
  end


  RailsAdmin.config do |config|
    config.model Score do
      edit do
        fields do
          help false
        end
        exclude_fields :pages
      end
      list do
        field :composition_title do
          def value
            bindings[:object].composition.title
          end
        end
        field :user_name do
          def value
            bindings[:object].composition.user.full_name
          end
        end
        items_per_page 100
        include_all_fields
      end
    end
  end
end
