# == Schema Information
#
# Table name: shops
#
#  id                    :integer          not null, primary key
#  name                  :string
#  logo                  :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  priority              :integer
#  ftp_access            :string
#  distribution_chain_id :integer
#

class Shop < ApplicationRecord
  mount_uploader :logo, LogoUploader

  has_and_belongs_to_many :scores, join_table: :rights_shops
  belongs_to :distribution_chain

  scope :order_by_name, -> { order(:name) }
  scope :as_priority, -> { order(:priority) }
  scope :not_distribution_chain, -> { where(distribution_chain_id: nil) }

  validates :priority, uniqueness: true
  validates :name, presence: true
  before_create :set_priority

  def self.next_priority
    Shop.as_priority.last.priority.to_i + 1
  end

  def set_priority
    self.priority ||= Shop.next_priority
  end

  RailsAdmin.config do |config|
    config.model Shop do
      edit do
        fields do
          help false
        end
      end
    end
  end
end
