# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  role                   :integer          default("default"), not null
#  first_name             :string           default(""), not null
#  last_name              :string           default(""), not null
#  street                 :string           default(""), not null
#  house_number           :string           default(""), not null
#  city                   :string           default(""), not null
#  zip_code               :string           default(""), not null
#  company                :string           default("")
#  viewability            :float            default(0.0)
#  logo                   :string           default("")
#  customer_role          :integer
#  website                :string           default("")
#  parent_id              :integer
#  country_id             :integer          default(1), not null
#  language_id            :integer          default(1), not null
#  ranking                :integer
#  stripe_customer_id     :string
#  keywords               :string
#  currency_id            :integer
#  vat_id                 :string
#  payment_method         :integer
#  payment_method_details :jsonb
#  show_demo_product      :boolean          default(TRUE)
#  profession             :string
#  musical_education      :string
#  education_place        :string
#
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  store :payment_method_details,
        accessors: %i(paypal_email bank_location beneficial_owner bic iban bank_name routing_number account_number),
        coder: JSON

  after_create :welcome_email
  after_create :subscribe_user_to_mailing_list

  belongs_to :country
  belongs_to :currency
  belongs_to :language
  has_many :compositions
  has_many :transcriber_jobs
  has_many :scores, :through => :compositions, source: :scores
  has_many :subusers, class_name: 'User', foreign_key: :parent_id
  has_many :invoices
  belongs_to :parent, class_name: 'User'
  has_many :user_skills
  has_many :skills, through: :user_skills

  enum payment_method: %w(paypal bank), _suffix: true

  validates :last_name, presence: true
  validates :street, presence: true
  validates :house_number, presence: true
  validates :city, presence: true
  validates :zip_code, presence: true
  validates :ranking, presence: {if: :transcriber?}, uniqueness: {if: :transcriber?}
  validates_presence_of :country, :language
  validates_presence_of :paypal_email, if: :paypal_payment_method?
  with_options if: :bank_payment_method? do |user|
    user.validates_inclusion_of :bank_location, in: %w(other europe)
    user.validates_presence_of :beneficial_owner, :bank_location
    user.validates_presence_of :bic, :iban, if: :european_bank?
    user.validates_presence_of :bank_name, :routing_number, :account_number, unless: :european_bank?
  end

  validates :terms, acceptance: true, allow_nil: false, on: :create, if: :default?

  accepts_nested_attributes_for :user_skills, allow_destroy: true

  mount_uploader :logo, LogoUploader

  before_save :set_currency, if: :currency_not_set?
  after_save :update_product_design
  before_validation :set_ranking, if: :transcriber?
  before_validation :split_full_name, :split_street_house_number, :set_language
  before_destroy :destroy_chain

  scope :admin_or_transcriber, -> { where("role = 1 OR role = 2 OR role = 5") }

  scope :user_full_name, ->(search) {
    current_scope = self
    search.split.uniq.each do |word|
      current_scope = current_scope.where('first_name ILIKE :term OR last_name ILIKE :term', {term: "%#{word}%"})
    end
    current_scope
  }

  attr_accessor :current_password, :full_name_str, :street_house_number_str

  ROLES = [:default, :transcriber, :admin, :editor, :inactive, :admin_light]
  enum role: ROLES

  def update_product_design
    if self.company_changed? ||
      (self.company.blank? && (self.first_name_changed? || self.last_name_changed?)) ||
      self.street_changed? ||
      self.house_number_changed? ||
      self.zip_code_changed? ||
      self.city_changed? ||
      self.country_id_changed?
      
      self.scores.created.each do |score|
        if score.score_files.only_pdf.present?
          UpdateScoreProductJob.perform_async(score.id)
          UpdateEpubJob.perform_async(score.id)
        end
      end
    end
  end

  def self.ransackable_scopes(auth_object = nil)
    [:user_full_name]
  end

  def active_for_authentication?
    # Uncomment the below debug statement to view the properties of the returned self model values.
    # logger.debug self.to_yaml
      
    super && !self.inactive?
  end

  def street_house_number
    self.country.code == 'GB' || self.country.code == 'US' ?
      [house_number, street].join(' ') :
      [street, house_number].join(' ')
  end

  def zip_code_city
    [zip_code, city].join(' ')
  end

  def full_address
    [street_house_number, zip_code_city].join(' ')
  end

  def full_name
    [first_name, last_name].join(' ')
  end

  def leon?
    self.email == "leon@soundnotation.com"
  end

  def show_switch_user?
    self.admin? || self.leon?
  end

  def locale
    self.language.lang_locale.try(:downcase) || 'en'
  end

  def currency_locale
    self.currency.try(:code) == 'EUR' ? 'de' : 'en'
  end

  def currency_not_set?
    self.currency_id.nil?
  end

  def set_currency
    country = self.country.try(:code)
    self.currency_id = Currency.default(country).id
  end

  def set_language
    self.language_id ||= Language.find_with_locale.id
  end

  def split_full_name
    return if self.full_name_str.blank?
    names = full_name_str.strip.split(/ (?=\S+$)/)
    self.first_name = names.size > 1 ? names.first : nil
    self.last_name = names.last
  end

  def split_street_house_number
    return if self.street_house_number_str.blank?
    address = self.street_house_number_str.strip.split(/ (?=\S+$)/)
    self.street = address.first
    self.house_number = address.last if address.size > 1
  end

  def vat_registered?
    vat_id.present?
  end

  def european_bank?
    bank_location.to_s == 'europe'
  end

  def hide_demo_product!
    self.update_column(:show_demo_product, false)
  end

  def default_copyright
    self.company || self.full_name
  end

  def destroy_chain
    self.compositions.each do |composition|
      composition.purchase_orders.destroy_all
      composition.scores.find_each { |score| score.invoice_item.delete if score.invoice_item }
      composition.scores.find_each { |score| score.transcriber_job.delete if score.transcriber_job }
      composition.invoice.destroy if composition.invoice
      composition.scores.collect(&:destroy)
      composition.destroy
    end

    self.invoices.each do |invoice|
      invoice.destroy
    end

  end

  def self.next_ranking
    max_ranking = User.where('ranking is not null').collect(&:ranking).max
    max_ranking.to_i + 1
  end

  def set_ranking
    self.ranking ||= User.next_ranking
  end

  RailsAdmin.config do |config|
    config.model User do
      edit do
        fields do
          help false
        end
      end

      list do
        field :first_name
        field :email
        field :role
        field :company
        field :viewability
      end
    end
  end

  private

  def subscribe_user_to_mailing_list
    SubscribeUserToMailingListJob.perform_later(self.email, self.first_name, self.last_name, self.locale, {'9ce2615f4d' => true, '1b9e7d633b' => true})
    # 9ce2615f4d is the id for Account = Yes on MailChimp groups. It means that the user also registered on our website
    # 1b9e7d633b is the id for Score ordered? = No on MailChimp groups. It means that the user registered on our website but did not ordered a score
  end

  def welcome_email
    NewUserMailer.welcome_email(self).deliver
  end
end
