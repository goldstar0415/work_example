# == Schema Information
#
# Table name: score_files
#
#  id         :integer          not null, primary key
#  path       :string           not null
#  name       :string           not null
#  format     :string           not null
#  visibility :integer          default("user")
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  score_id   :integer          not null
#

class ScoreFile < ApplicationRecord
  belongs_to :score
  enum visibility: [:user, :transcriber, :admin, :unrestricted]

  validates_presence_of :path, :score, :format
  mount_uploader :path, ScoreFileUploader

  before_save :convert_wav_to_flac
  after_save :upload_notation, :update_score_pages
  before_destroy :destroy_from_azure

  scope :only_mp3, -> {where(format: ['mp3', 'MP3'])}
  scope :only_pdf, -> {where(format: ['pdf', 'PDF'])}
  scope :no_pdf, -> {where.not(format: ['pdf', 'PDF'])}
  scope :format, -> (format) {where(format: [format, format.upcase])}
  scope :format_except, -> (format) {where.not(format: [format, format.upcase])}

  def name_with_format
    [self.name, self.format].join('.')
  end

  def name_for_download(extension=nil)
    extension = self.format.downcase if extension.blank?
    filename = [self.score.composition.title, self.score.product.title].join(" - ")
    filename = [filename, I18n.t("activerecord.attributes.user.#{self.score.difficulty}")].join(" - ") unless self.score.zero_difficulties?
    [filename, extension].join(".")
  end

  def format_is_in(formats)
    formats.each do |format|
      return true if ['.' + format, format].include?(self.format.downcase)
    end
    false
  end

  def destroy_from_azure
    self.remove_path!
  end

  def music_xml?
    self.path.file.extension.is_xml?
  end

  def upload_notation
    SheetMusicPlayerJob.perform_async(self.score_id) if self.score.created? && self.music_xml?
  end

  def update_score_pages
    Score.find(self.score_id).update(pages: self.pdf_reader.page_count) if format_is_in(['pdf'])
  end

  def pdf_reader
    PDF::Reader.new(open(self.path.url))
  end

  def page_size
    page_sizes = []
    self.pdf_reader.pages.each do |page|
      bbox   = page.attributes[:MediaBox]
      width  = bbox[2] - bbox[0]
      height = bbox[3] - bbox[1]
      page_sizes << [width, height]
    end
    freq = page_sizes.inject(Hash.new(0)) { |h,v| h[v] += 1; h }
    page_sizes.max_by { |v| freq[v] }
  end

  def convert_wav_to_flac
    file_type = File.extname(self.path.path)
    if file_type == '.wav'
      wav_path = self.path.path
      flac_path = wav_path.gsub(".wav", ".flac")
      system("ffmpeg -i #{wav_path.shellescape} #{flac_path.shellescape}")
      self.path = File.open(flac_path)
      self.format = 'flac'
      self.save
      system("rm #{wav_path.shellescape}")
      system("rm #{flac_path.shellescape}")
    end
  end

  RailsAdmin.config do |config|
    config.model ScoreFile do
      edit do
        fields do
          help false
        end
      end
      list do
        configure :name do
          column_width 400
        end
      end
    end
  end

end
