# == Schema Information
#
# Table name: invoices
#
#  id                     :integer          not null, primary key
#  composition_id         :integer
#  state                  :string           default("due")
#  amount_cents           :integer          default(0), not null
#  amount_currency        :string           default("USD"), not null
#  discount_cents         :integer          default(0), not null
#  discount_currency      :string           default("USD"), not null
#  user_id                :integer
#  title                  :string
#  description            :text
#  purchase_order_id      :integer
#  due_date               :datetime         not null
#  invoicing_date         :datetime         not null
#  paid_at                :datetime
#  cancelled_at           :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  sent                   :boolean          default(FALSE)
#  invoice_id             :integer          default(10000)
#  paid_with              :string           default("Invoice Only"), not null
#  tax_cents              :integer          default(0), not null
#  tax_currency           :string           default("USD"), not null
#  shipping_cost_cents    :integer          default(0), not null
#  shipping_cost_currency :string           default("USD"), not null
#  cart_id                :integer
#  vat_number             :string
#

class Invoice < ApplicationRecord

  STATUS_DUE = 'due'
  STATUS_PAID = 'paid'
  STATUS_CANCELLED = 'cancelled'

  validates :title, :description, :invoicing_date, :due_date, :state, presence: true

  has_many :invoice_items
  belongs_to :composition
  belongs_to :purchase_order, foreign_key: :purchase_order_id

  belongs_to :user
  belongs_to :cart

  has_many :general_invoice_items

  monetize :amount_cents

  scope :paid, -> {where state: STATUS_PAID}
  scope :due, -> {where state: STATUS_DUE}
  scope :cancelled, -> {where state: STATUS_CANCELLED}

  before_destroy :destroy_chain

  before_create do
    self.invoice_id = Invoice.last.try(:invoice_id).try(:next) || 10001
  end

  def paid?
    state == STATUS_PAID
  end

  def cancelled?
    state == STATUS_CANCELLED
  end

  def due?
    state == STATUS_DUE
  end

  def update_state_change_date
    if state_changed? && state == STATUS_PAID
      #FIXME User may have option to set a paid date manually
      self.paid_at = Time.zone.now unless self.paid_at.present?
    end

    if state_changed? && state == STATUS_CANCELLED
      #FIXME User may have option to set a cancel date manually
      self.cancelled_at = Time.zone.now unless self.cancelled_at.present?
    end
  end

  def destroy_chain
    self.invoice_items.collect(&:destroy)
  end

end
