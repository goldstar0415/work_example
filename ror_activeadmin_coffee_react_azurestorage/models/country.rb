# == Schema Information
#
# Table name: countries
#
#  id         :integer          not null, primary key
#  code       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Country < ApplicationRecord
  translates :name
  accepts_nested_attributes_for :translations, allow_destroy: true

  has_and_belongs_to_many :compositions, join_table: :rights_countries
  has_many :users

  validates_presence_of :name, :code
  validates_length_of :code, :is => 2
  validates :code, :uniqueness => {:case_sensitive => false}

  before_validation :uppercase_code

  scope :order_by_name, -> { includes(:translations).order(:name) }

  EUROPE = [
    "DE",
    "AT",
    # "CH", Switzerland is considered as rest of world regarding invoice tax
    "AD",
    "BE",
    "CY",
    "EE",
    "FI",
    "FR",
    "GR",
    "IE",
    "IT",
    "XK",
    "LV",
    "LI",
    "LT",
    "LU",
    "MT",
    "MC",
    "ME",
    "NL",
    "PT",
    "SM",
    "SK",
    "SI",
    "ES"
  ]

  def uppercase_code
    code.upcase!
  end

  RailsAdmin.config do |config|
    config.model Country do
      configure :translations, :globalize_tabs
      edit do
        fields do
          help false
        end
      end
    end

    config.model 'Country::Translation' do
      visible false
      configure :locale, :hidden do
        help ''
      end
      include_fields :locale, :name
    end
  end
end
