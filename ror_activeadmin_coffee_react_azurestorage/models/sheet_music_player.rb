class SheetMusicPlayer < ApplicationRecord
  belongs_to :score

  def self.upload_notation(score_id, musical_file_url)
    score = Score.find(score_id)
    begin
      file_path = score.musical_file_url
      logger.info "FilePath in Params: #{musical_file_url}"
      logger.info "FilePath in Score: #{file_path}"
      file_path ||= musical_file_url
      unless file_path.blank?
        response = FlatIoApi.create_score(score.composition.title, file_path)
        sheet_music_player = SheetMusicPlayer.find_or_create_by(score_id: score_id)
        sheet_music_player.update_attributes(has_notation: true, slug: response.id)
      end
    rescue => e
      logger.error 'Error :: Unable to upload musical xml!'
      logger.error e.message
    end
  end
end
