# == Schema Information
#
# Table name: coupons
#
#  id              :integer          not null, primary key
#  name            :string
#  code            :string
#  starting_time   :datetime
#  ending_time     :datetime
#  active          :boolean          default(TRUE)
#  discount_type   :string           default("fixed_price")
#  discount_amount :decimal(, )      default(0.0)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  reusable        :boolean          default(FALSE)
#  applied_at      :datetime
#  once_per_user   :boolean          default(FALSE)
#

# SCHEMA INFO:
#
# create_table :coupons do |t|
#   t.string :name
#   t.string :code
#   t.datetime :starting_time
#   t.datetime :ending_time
#   t.boolean :active, default: true
#   t.integer :discount_type, default: 0
#   t.decimal :discount_amount, default: 0
#
#   t.timestamps
# end
# add_index :coupons, :code

class Coupon < ApplicationRecord

  has_many :carts
  has_many :users, through: :carts

  DISCOUNT_TYPES = {
    'fixed_price' => 'fixed_price',
    'percentage' => 'percentage'
  }

  validates :name, :code, :discount_type,
            :discount_amount, presence: true

  validates :discount_amount, numericality: {greater_than_or_equal_to: 0}

  validates :code, uniqueness: true

  enum discount_type: DISCOUNT_TYPES

  scope :available, -> {where(applied_at: nil)}

  def display_name(currency_unit = 'USD')
    percentage? ? "#{discount_amount}%" : "#{discount_amount} #{currency_unit}"
  end

  def available?(user_id=nil)
    return true if within_time? && reusable?
    return true if within_time? && once_per_user? && !already_used?(user_id)
    return true if within_time? && one_time? && self.applied_at.nil?
    false
  end

  def within_time?
    if starting_time.present? && ending_time.present?
      (starting_time <= Time.zone.now.beginning_of_day) && (ending_time >= Time.zone.now.beginning_of_day)
    else
      true
    end
  end

  def expired?
    !within_time?
  end

  def one_time?
    !reusable?
  end

  def discount_type_list
    [['Fixed Price', 'fixed_price'], ['Percentage', 'percentage']]
  end

  def already_used?(user_id)
    return false if user_id.blank?
    users = self.users.where(id: user_id)
    users.present?
  end

  RailsAdmin.config do |config|
    config.model Coupon do
      field :name
      field :code
      field :reusable
      field :once_per_user do
        label 'Multi use (once per user)'
      end
      field :starting_time
      field :ending_time
      field :discount_amount
      field :discount_type
      field :discount_type do
        help 'Type fixed_price or percentage'
      end
    end
  end
end
