class AudioFileUploader < CarrierWave::Uploader::Base

  storage :file

  def extension_whitelist
    %w(mp3 wav pdf mscz mscx sib musx xml mxl)
  end

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "system/uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end
end
