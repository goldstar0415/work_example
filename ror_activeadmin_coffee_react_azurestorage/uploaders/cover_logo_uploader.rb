class CoverLogoUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  storage :azure_rm

  def azure_container
    ENV['SERVER']
  end

  def store_dir
    "user-#{model.composition.user_id}/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :thumb do
    process resize_to_fill: [300, -1, 'Center']
  end

end
