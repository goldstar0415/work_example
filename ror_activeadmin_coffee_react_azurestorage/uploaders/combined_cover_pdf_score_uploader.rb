class CombinedCoverPdfScoreUploader < CarrierWave::Uploader::Base
  storage :azure_rm

  def azure_container
    ENV['SERVER']
  end

  def store_dir
    "user-#{model.composition.user_id}/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def filename
    ActiveSupport::Inflector.transliterate(original_filename).to_s if original_filename.present?
  end
end
