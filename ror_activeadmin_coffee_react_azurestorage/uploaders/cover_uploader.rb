class CoverUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  storage :azure_rm

  def azure_container
    ENV['SERVER']
  end

  def store_dir
    "user-#{model.composition.user_id}/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :distributor_download do
    process resize_to_fill: [2480, 3508, 'Center']
    process :cover_design_for_download
  end

  def overlay_logo
    model.cover_logo.url || Rails.root.join('app', 'assets', 'images', 'cover_background', 'cover_logo.png')
  end

  def formatted_composition_title
    begin
      title = Composition.find(model.composition_id).title
      res = []
      length = 20
      while title.length >= length do
        sub_title = title.match(/^.{0,#{length}}\b/)[0]
        break if sub_title.empty?
        title.slice!(sub_title)
        res << sub_title
      end
      res << title
      res.join("\n")
    rescue
      'Not a good title'
    end
  end

  def word_wrap(text, columns = 80)
    text.split("\n").collect do |line|
      line.length > columns ? line.gsub(/(.{1,#{columns}})(\s+|$)/, "\\1\n").strip : line
    end * "\n"
  end

  def write_text(text, cover, gravity, pointsize, color, x_pos, y_pos, next_margin, wrap_data, margin = 80)
    cover.gravity gravity
    cover.pointsize pointsize.to_s
    position = y_pos + next_margin
    if wrap_data[:wrap]
      word_wrap(text, wrap_data[:columns]).split("\n").each_with_index do |row, index|
        position += margin unless index == 0
        next_margin += margin unless index == 0
        cover.draw "text #{x_pos},#{position} '#{row.gsub("'"){"\\'"}}'"
      end
    else
      cover.draw "text #{x_pos},#{position} '#{text.gsub("'"){"\\'"}}'"
    end
    cover.fill color
    next_margin
  end

  def cover_design_for_download
    if model.cover_background_image.present?
      manipulate! do |image|
        image.combine_options do |c|
          if model.cover_background_image.first_background?
            next_margin = write_text(model.composer_name, c, 'NorthWest', 150, 'black', 300, 900, 0, {wrap: true, columns: 25}, 160)
            next_margin = write_text(formatted_composition_title, c, 'NorthWest', 200, 'black', 300, 1120, next_margin, {wrap: false})
            next_margin = write_text(model.product.try(:title).try(:humanize), c, 'NorthWest', 120, 'black', 300, 1460, next_margin, {wrap: false})
            next_margin = write_text(model.composition.copyright, c, 'NorthWest', 75, 'black', 300, 1750, next_margin, {wrap: true, columns: 35}, 120) unless model.composition.copyright.blank?
            # c.draw "image Over 1500,300 900,306 '#{open(overlay_logo)}'"
          elsif model.cover_background_image.second_background?
            next_margin = write_text(model.composer_name, c, 'NorthWest', 150, 'black', 300, 1800, 0, {wrap: true, columns: 25}, 160)
            next_margin = write_text(formatted_composition_title, c, 'NorthWest', 200, 'black', 300, 2020, next_margin, {wrap: false})
            next_margin = write_text(model.product.try(:title).try(:humanize), c, 'NorthWest', 120, 'black', 300, 2400, next_margin, {wrap: false})
            next_margin = write_text(model.composition.copyright, c, 'NorthWest', 75, 'black', 300, 3000, 0, {wrap: true, columns: 35}, 120) unless model.composition.copyright.blank?
            # c.draw "image Over 1000,3100 700,238 '#{open(overlay_logo)}'"
          elsif model.cover_background_image.third_background?
            next_margin = write_text(model.composer_name, c, 'North', 150, 'black', 0, 1250, 0, {wrap: true, columns: 25}, 160)
            next_margin = write_text(formatted_composition_title, c, 'North', 200, 'black', 0, 1470, next_margin, {wrap: false})
            next_margin = write_text(model.product.try(:title).try(:humanize), c, 'North', 120, 'black', 0, 1900, next_margin, {wrap: false})
            next_margin = write_text(model.composition.copyright, c, 'NorthEast', 75, 'black', 50, 2950, 0, {wrap: true, columns: 35}, 120) unless model.composition.copyright.blank?
            c.gravity 'North'
            # c.draw "image Over 0,3100 700,238 '#{open(overlay_logo)}'"
          end
        end
        image
      end
    end
  end

  def filename
    ActiveSupport::Inflector.transliterate(original_filename).to_s if original_filename.present?
  end

end
