class BlogImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :azure_rm

  def azure_container
    ENV['SERVER']
  end

  def extension_whitelist
    %w(jpg jpeg png tiff bmp)
  end

  process resize_to_fit: [900, 500]

  version :thumb do
    process resize_to_fill: [315, 100, 'Center']
  end
  version :category_thumb do
    process resize_and_pad: [160, 160, :transparent, 'Center']
  end

  def store_dir
    "blog/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def filename
    ActiveSupport::Inflector.transliterate(original_filename).to_s if original_filename.present?
  end
end
