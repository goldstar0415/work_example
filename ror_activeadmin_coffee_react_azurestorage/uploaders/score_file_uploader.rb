class ScoreFileUploader < CarrierWave::Uploader::Base
  storage :azure_rm

  def azure_container
    ENV['SERVER']
  end

  def store_dir
    "user-#{model.score.composition.user_id}/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  process :default_format

  def default_format
    model.format ||= file.content_type if file.content_type
  end

  def filename
    ActiveSupport::Inflector.transliterate(original_filename).to_s if original_filename.present?
  end
end
