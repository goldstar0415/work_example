ActiveAdmin.register Score, as: 'Delivery' do
  batch_action :destroy, false

  permit_params :gtin,
                :ismn13,
                :publishing_date,
                :isrc_code,
                :pages,
                :notafina_shop,
                :scoring,
                :arrangement,
                :genre,
                :composition_id,
                :cover,
                :square_cover,
                :temp,
                :product_id,
                :keywords,
                :beats_per_minute,
                :delivery_status,
                :price,
                :world_release,
                :custom_world_release,
                :subscription_created_at,
                :has_lyrics,
                :has_chords,
                :demo_product,
                :promoted,
                :cover_logo,
                :already_created,
                promotion_ids: [],
                shop_ids: [],
                recording_shop_ids: [],
                country_ids: [],
                restricted_country_ids: [],
                score_files_attributes: [:id, :name, :format, :visibility, :path, :_destroy]

  controller do
    include ScoresHelper
    def scoped_collection
      end_of_association_chain.where.not(delivery_status: :not_delivered)
    end

    actions :all, :except => [:new, :destroy]

    def update
      super do |success, failure|
        success.html do
          if @delivery.created? && @delivery.score_files.only_pdf.present?
            UpdateScoreProductJob.perform_async(params[:id])
            UpdateEpubJob.perform_async(params[:id])
          end
          redirect_to admin_delivery_path(@delivery.id)
        end
        failure.html do
          render :edit
        end
      end
    end
  end

  batch_action :export, form: {shops: (DistributionChain.all + Shop.not_distribution_chain).collect(&:name)} do |ids, inputs|
    export_to_zip_data(inputs[:shops], ids.join(","))
    # redirect_to(collection_path, notice: "Deliveries have been updated successfully.")
  end
  batch_action :update_delivery_status, form: {delivery_status: Score.delivery_statuses.keys[1..-1]} do |ids, inputs|
    Score.where(id: ids).update_all(delivery_status: inputs[:delivery_status].to_sym)
    redirect_to collection_path, notice: "Deliveries have been updated successfully."
  end

  filter :composition_includes, as: :string, label: "Composition Title"
  filter :user_includes, as: :string, label: "User Name"
  filter :product
  filter :gtin
  filter :publishing_date
  filter :shops
  filter :created_at
  filter :updated_at

  index do
    selectable_column
    column :composition do |delivery|
      link_to delivery.composition.title, admin_composition_path(delivery.composition)
    end
    column :product do |delivery|
      delivery.product.title
    end
    column :user_name do |delivery|
      link_to delivery.user.full_name, admin_user_path(delivery.user)
    end
    column :gtin
    column :publishing_date
    column :delivery_status

    actions defaults: false do |delivery|
      item image_tag("activeadmin/show.png", size: 16), admin_delivery_path(delivery.id), class: 'member_link'
      item image_tag("activeadmin/edit.png", size: 16), edit_admin_delivery_path(delivery.id), class: 'member_link'
    end
  end

  show do |model|
    attributes_table do
      row :gtin
      row :delivery_status
      row :price
      row :publishing_date
      row :isrc_code

      panel "Shops" do
        table_for model.shops do
          column :name do |shop|
            shop.name
          end
          column :logo do |shop|
            image_tag shop.logo.shop_thumb.url rescue nil
          end
          column :ftp_access
          column :distribution_chain
        end
      end

      panel "Recording Shops" do
        table_for model.recording_shops do
          column :name do |recording_shop|
            recording_shop.name
          end
          column :logo do |recording_shop|
            image_tag recording_shop.logo.shop_thumb.url rescue nil
          end
          column :ftp_access
          column :distribution_chain
        end
      end

      panel "Countries" do
        table_for model.countries do
          column :name
          column :code
        end
      end

      row :world_release

      panel "Restricted Countries" do
        table_for model.restricted_countries do
          column :name
          column :code
        end
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :gtin, input_html: {type: :text, value: "#{f.object.gtin.blank? ? (GTin.first_available) : f.object.gtin}"}
      f.input :delivery_status
      f.input :price
      f.input :publishing_date, :as => :string, :input_html => {:class => 'datepicker hasDatePicker'}
      f.input :isrc_code, input_html: {value: score_isrc_code(f.object.isrc_code)}

      f.input :shops, as: :check_boxes
      f.input :recording_shops, as: :check_boxes
      f.input :countries, as: :check_boxes
      f.input :world_release
      f.input :restricted_countries, as: :check_boxes

    end
    f.actions
  end

end
