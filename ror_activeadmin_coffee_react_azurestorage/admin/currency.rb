ActiveAdmin.register Currency do
  permit_params :code,
                :name
                
  index do
    selectable_column
    column :code
    column :name

    actions defaults: false do |currency|
      item image_tag("activeadmin/show.png", size: 16), admin_currency_path(currency.id), class: 'member_link'
      item image_tag("activeadmin/edit.png", size: 16), edit_admin_currency_path(currency.id), class: 'member_link'
      item image_tag("activeadmin/delete.png", size: 16), admin_currency_path(currency.id), class: 'member_link', method: :delete, data: { confirm: I18n.t('active_admin.delete_confirmation') }
    end
  end

  show do |model|
    attributes_table do
      row :code
      row :name
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :code
      f.input :name
    end
    f.actions
  end
end

