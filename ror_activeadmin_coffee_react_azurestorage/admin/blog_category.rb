ActiveAdmin.register BlogCategory do
  menu parent: "Blog"
  permit_params :image,
                :description,
                translations_attributes: [:id, :name, :image, :locale],
                blog_articles_attributes: [:id, :image, :_destroy,
                                           translations_attributes: [:id, :title, :text, :alt_tag_content, :meta_description, :locale]]

  action_item :add, only: :show do
    link_to I18n.t('active_admin.new_model', model: "Blog Category"), new_admin_blog_category_path
  end

  index do
    selectable_column
    id_column
    column :image do |category|
      image_tag category.image.category_thumb.url if category.image.present?
    end
    column :description
    column :blog_articles do |category|
      table_for category.blog_articles do
        column do |article|
          link_to article.title, [:admin, article]
        end
      end
    end
    actions defaults: false do |category|
      item image_tag("activeadmin/show.png", size: 16), admin_blog_category_path(category.id), class: 'member_link'
      item image_tag("activeadmin/edit.png", size: 16), edit_admin_blog_category_path(category.id), class: 'member_link'
      item image_tag("activeadmin/delete.png", size: 16), admin_blog_category_path(category.id), class: 'member_link', method: :delete, data: { confirm: I18n.t('active_admin.delete_confirmation') }
    end
  end

  show do |model|
    attributes_table do
      row :description
      translate_attributes_table_for model do
        row :name
        row :image do |category|
          link_to(image_tag(category.image.category_thumb.url), category.image.url) rescue nil
        end
      end

      panel "Blog Articles" do
        table_for model.blog_articles do
          column :title do |article|
            article.title
          end
          column :text do |article|
            article.text.html_safe
          end
          column :alt_tag_content do |article|
            article.alt_tag_content
          end
          column :meta_description do |article|
            article.meta_description
          end
        end
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :description

      f.translate_inputs do |t|
        t.input :name
        t.input :image, hint: t.object.image.present? ? image_tag(t.object.image.url(:category_thumb)) : content_tag(:span, "no image yet")
      end

      panel "Blog Articles" do
        f.has_many :blog_articles, allow_destroy: true, new_record: true do |i|
          i.input :image, hint: i.object.image.present? ? image_tag(i.object.image.url(:thumb)) : content_tag(:span, "no image yet")
          i.translate_inputs do |t|
            t.input :title
            t.input :text, as: :ckeditor
            t.input :alt_tag_content
            t.input :meta_description
          end
        end
      end

    end
    f.actions
  end

end
