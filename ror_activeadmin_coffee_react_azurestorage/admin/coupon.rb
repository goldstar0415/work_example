ActiveAdmin.register Coupon do
  permit_params :name,
                :code,
                :starting_time,
                :ending_time,
                :active,
                :discount_type,
                :discount_amount,
                :reusable,
                :applied_at,
                :once_per_user

  action_item :add, only: :show do
    link_to I18n.t('active_admin.new_model', model: "Coupon"), new_admin_coupon_path
  end

  index do
    column :name
    column :code
    column :reusable
    column 'Multi use (once per user)' do |coupon|
      coupon.once_per_user
    end
    column :starting_time
    column :ending_time
    column :discount_amount
    column :discount_type
    actions defaults: false do |model|
      item image_tag("activeadmin/show.png", size: 16), admin_coupon_path(model.id), class: 'member_link'
      item image_tag("activeadmin/edit.png", size: 16), edit_admin_coupon_path(model.id), class: 'member_link'
      item image_tag("activeadmin/delete.png", size: 16), admin_coupon_path(model.id), class: 'member_link', method: :delete, data: { confirm: I18n.t('active_admin.delete_confirmation') }
    end
  end

  show do |model|    
    attributes_table do
      row :name
      row :code
      row :reusable
      row 'Multi use (once per user)' do |coupon|
        coupon.once_per_user
      end
      row :starting_time
      row :ending_time
      row :discount_amount
      row :discount_type
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :name
      f.input :code
      f.input :reusable
      f.input :once_per_user, label: 'Multi use (once per user)'
      f.input :starting_time, as: :datetime_picker
      f.input :ending_time, as: :datetime_picker
      f.input :discount_amount
      f.input :discount_type, as: :select, collection: ["fixed_price", "percentage"]
    end
    f.actions
  end

end
