ActiveAdmin.register Country do
  permit_params :code,
                translations_attributes: [:id, :locale, :name]

  action_item :add, only: :show do
    link_to I18n.t('active_admin.new_model', model: "Country"), new_admin_country_path
  end

  index do
    selectable_column
    id_column
    column :code
    column :name do |model|
      model.name
    end
    actions defaults: false do |model|
      item image_tag("activeadmin/show.png", size: 16), admin_country_path(model.id), class: 'member_link'
      item image_tag("activeadmin/edit.png", size: 16), edit_admin_country_path(model.id), class: 'member_link'
      item image_tag("activeadmin/delete.png", size: 16), admin_country_path(model.id), class: 'member_link', method: :delete, data: { confirm: I18n.t('active_admin.delete_confirmation') }
    end
  end  

  show do |model|
    attributes_table do
      row :code
      translate_attributes_table_for model do
        row :name
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :code
      f.translate_inputs do |t|
        t.input :name
      end
    end
    f.actions
  end

end
