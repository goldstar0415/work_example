ActiveAdmin.register BlogArticle do
  menu parent: "Blog"

  permit_params :image, :blog_category_id,
                translations_attributes: [:id, :language_id, :title, :text, :alt_tag_content, :meta_description, :locale]

  action_item :add, only: :show do
    link_to I18n.t('active_admin.new_model', model: "Blog Article"), new_admin_blog_article_path
  end

  index do
    selectable_column
    id_column
    column :title
    column :image do |article|
      image_tag article.image.thumb.url if article.image.present?
    end
    column :blog_category_id do |article|
      article.blog_category
    end
    actions defaults: false do |article|
      item image_tag("activeadmin/show.png", size: 16), admin_blog_article_path(article.id), class: 'member_link'
      item image_tag("activeadmin/edit.png", size: 16), edit_admin_blog_article_path(article.id), class: 'member_link'
      item image_tag("activeadmin/delete.png", size: 16), admin_blog_article_path(article.id), class: 'member_link', method: :delete, data: { confirm: I18n.t('active_admin.delete_confirmation') }
    end
  end

  show do |model|
    attributes_table do
      row :image do |article|
        image_tag article.image.thumb.url if article.image.present?
      end
      translate_attributes_table_for model do
        row :title
        row :text do |article|
          article.text.html_safe
        end
        row :alt_tag_content
        row :meta_description
      end
      panel "Category" do
        translate_attributes_table_for model.blog_category do
          row :name do |category|
            category.name.humanize
          end
          row :image do |category|
            image_tag category.image.category_thumb.url if category.image.present?
          end
        end
        attributes_table_for model.blog_category do
          row :description
        end
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :image, hint: f.object.image.present? ? image_tag(f.object.image.url(:thumb)) : content_tag(:span, "no image yet")

      f.translate_inputs do |t|
        t.input :title
        t.input :text, as: :ckeditor
        t.input :alt_tag_content
        t.input :meta_description
      end
      f.input :blog_category
    end

    f.actions
  end


end
