ActiveAdmin.register Composition do
  permit_params :title,
                :subtitle,
                :key,
                :product_language_id,
                :audio_file,
                :user_id,
                :genre,
                :creator_price,
                :freelancer_price,
                :payment_confirmed,
                :transaction_number,
                :payment_status,
                :lyrics,
                :checkout,
                :pdf_file,
                :pdf_page_count,
                :lyrics_exist,
                authors_attributes: [:id, :first_name, :full_name_str, :author_type, :_update, :_destroy],
                scores_attributes: [:id, :gtin, :ismn13, :difficulty, :arrangement, :genre, :product_id, :keywords, :_update, :_destroy],
                country_ids: [],
                copyrights_attributes: [:id, :name, :_destroy]

  controller do
    def update
      author = []
      contribution_delete = []
      params[:composition][:authors_attributes].each do |number, artist|
        names = artist[:full_name_str].strip.split(/ (?=\S+$)/)
        if artist[:id].present?
          if artist[:_destroy] == "1"
            cont_id = Contribution.where(author_id:artist[:id],composition_id: params[:id]).first.id
            contribution_delete << cont_id
          end  
        else
          author_id = Author.where(first_name: names.first,last_name: names.last,author_type: artist[:author_type]).first.id
          author << author_id
        end
      end
      params[:composition].delete :authors_attributes
      update! do |success, failure|
        success.html do
          if author.present?
            author.each do |id|
              Contribution.create(author_id: id, composition_id: params[:id])
            end
          end  

          if contribution_delete.present?
            contribution_delete.each do |id|
              Contribution.find(id).destroy
            end
          end

          @composition.scores.created.each do |score|
            if score.score_files.only_pdf.present?
              UpdateScoreProductJob.perform_async(score.id)
              UpdateEpubJob.perform_async(score.id)
            end
          end
          redirect_to admin_composition_path(@composition.id)
        end
        failure.html do
          render :edit
        end
      end
    end

    def create
      author = []
      params[:composition][:authors_attributes].each do |number, artist|
        names = artist[:full_name_str].strip.split(/ (?=\S+$)/)
        unless artist[:id].present? 
          author_id = Author.where(first_name: names.first,last_name: names.last,author_type: artist[:author_type]).first.id
          author << author_id
        end
      end
      params[:composition].delete :authors_attributes  
      create! do |success, failure|
        success.html do
          if author.present?
            author.each do |id|
              composition_id = Composition.last.id
              Contribution.create(author_id: id, composition_id: composition_id)
            end
          end 

          redirect_to admin_composition_path(@composition.id)
        end
        failure.html do
          render :new
        end
      end
    end

  end

  filter :user, collection: proc { User.all.sort_by{ |u| u.full_name.downcase } }
  filter :copyrights
  filter :product_language
  filter :authors, collection: proc { Author.all.sort_by{ |a| a.full_name.downcase } }
  filter :countries
  filter :title
  filter :subtitle
  filter :key
  filter :created_at
  filter :updated_at
  filter :genre
  filter :creator_price
  filter :freelancer_price

  action_item :add, only: :show do
    link_to I18n.t('active_admin.new_model', model: "Composition"), new_admin_composition_path
  end

  index do
    selectable_column
    id_column
    column :title
    column :user_name do |composition|
      link_to composition.user.full_name, admin_user_path(composition.user)
    end
    column :genre

    actions defaults: false do |composition|
      item image_tag("activeadmin/show.png", size: 16), admin_composition_path(composition.id), class: 'member_link'
      item image_tag("activeadmin/edit.png", size: 16), edit_admin_composition_path(composition.id), class: 'member_link'
      item image_tag("activeadmin/delete.png", size: 16), admin_composition_path(composition.id), class: 'member_link', method: :delete, data: { confirm: I18n.t('active_admin.delete_confirmation') }
    end
  end

  show do |model|
    attributes_table do
      row :title
      row :subtitle
      row :key
      row :product_language
      row :audio_file do |model|
        link_to model.audio_file.file.filename, model.audio_file.url if model.audio_file.present?
      end
      row :user_name do |model|
        link_to(model.user.full_name, admin_user_path(model.user)) rescue nil
      end
      row :genre
      row :creator_price
      row :freelancer_price
      row :payment_confirmed
      row :transaction_number
      row :payment_status
      row :lyrics
      row :checkout
      row :pdf_file
      row :pdf_page_count
      row :lyrics_exist

      panel "Scores" do
        table_for model.scores do
          column do |score|
            link_to score.product.title, admin_score_path(score.id)
          end
        end
      end

      panel "Authors" do
        table_for model.authors do
          column :name do |model|
            link_to model.full_name, admin_author_path(model.id)
          end
        end
      end

      panel "Countries" do
        table_for model.countries do
          column :name do |model|
            link_to model.name, admin_country_path(model.id)
          end
          column :code
        end
      end

      panel "Copyrights" do
        table_for model.copyrights do
          column :name do |model|
            model.name
          end
        end
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :title
      f.input :subtitle
      f.input :key
      f.input :product_language_id, as: :select, collection: Language.all.collect { |c| [c.name, c.id] }, include_blank: false, hint: f.object.new_record? || f.object.product_language.nil? ? '' : link_to("Current language: " + f.object.product_language.name, admin_language_path(f.object.product_language))
      f.input :audio_file, hint: f.object.new_record? || f.object.audio_file.nil? ? '' : link_to(f.object.audio_file.file.try(:filename), f.object.audio_file.url)
      f.input :user_id, as: :select, collection: User.all.collect { |c| [c.full_name, c.id] }, include_blank: false, hint: f.object.new_record? || f.object.user.nil? ? '' : link_to("Current user: " + f.object.user.full_name, admin_user_path(f.object.user))
      f.input :genre, as: :select, collection: Score.genres.keys
      f.input :creator_price
      f.input :freelancer_price
      f.input :payment_confirmed
      f.input :transaction_number
      f.input :payment_status
      f.input :lyrics
      f.input :checkout
      f.input :pdf_file
      f.input :pdf_page_count
      f.input :lyrics_exist
      f.input :countries, as: :check_boxes
      f.inputs do
        f.has_many :authors, allow_destroy: true, new_record: true do |i|
          i.input :author_type, as: :select, collection: AuthorRole::AUTHOR_TYPE.map(&:to_s), input_html: { onchange: remote_request(:post, :change_author_name_list, {author_type: "$(this).val()", source_id: "$(this).attr('id')"}, "") }
          i.input :full_name_str, as: :select, collection: Author.where(author_type: i.object.author_type).map(&:full_name), label: "Name", selected: i.object.full_name
          i.input :first_name, input_html: { value: "" }, as: :hidden
        end
        unless f.object.new_record?
          render partial: ('admin/new_author')
        end
      end
      
      f.has_many :scores, allow_destroy: true, new_record: true do |i|
        i.input :gtin
        i.input :ismn13
        i.input :difficulty
        i.input :arrangement
        i.input :genre
        i.input :product_id, as: :select, collection: Product.all.collect { |c| [c.title, c.id] }, include_blank: false
        i.input :keywords
      end
      f.has_many :copyrights, allow_destroy: true, new_record: true do |i|
        i.input :name
      end
    end
    f.actions
  end


  collection_action :change_author_name_list, method: :post do
    @author_name_lists = Author.where(author_type: params[:author_type])
    render json: { options: view_context.options_from_collection_for_select(@author_name_lists, :full_name, :full_name), source_id: params[:source_id], status: 200 }
  end

  member_action :change_author_name_list, method: :post do
    @author_name_lists = Author.where(author_type: params[:author_type])
    render json: { options: view_context.options_from_collection_for_select(@author_name_lists, :full_name, :full_name), source_id: params[:source_id], status: 200 }
  end
end
