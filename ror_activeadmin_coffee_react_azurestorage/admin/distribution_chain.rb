ActiveAdmin.register DistributionChain do
  menu parent: "Distributor"

  permit_params :name,
                :website,
                :ftp_access

  index do
    id_column
    column :name
    column :website do |model|
      model.website.blank? ? '' : link_to(model.website, model.website)
    end
    column :ftp_access

    actions defaults: false do |distribution_chain|
      item image_tag("activeadmin/show.png", size: 16), admin_distribution_chain_path(distribution_chain.id), class: 'member_link'
      item image_tag("activeadmin/edit.png", size: 16), edit_admin_distribution_chain_path(distribution_chain.id), class: 'member_link'
      item image_tag("activeadmin/delete.png", size: 16), admin_distribution_chain_path(distribution_chain.id), class: 'member_link', method: :delete, data: { confirm: I18n.t('active_admin.delete_confirmation') }
    end
  end

  show do |model|
    attributes_table do
      row :name
      row :website do |model|
        model.website.blank? ? '' : link_to(model.website, model.website)
      end
      row :ftp_access
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :name
      f.input :website
      f.input :ftp_access
    end
    f.actions
  end
end
