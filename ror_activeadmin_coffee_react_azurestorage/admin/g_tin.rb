require 'roo'

ActiveAdmin.register GTin do
  permit_params :gtin,
                :used,
                :score_id

  config.sort_order = "gtin_asc"

  collection_action :upload_xlsx, :title => 'Import XLSX' do
    render 'admin/g_tin/upload_xlsx'
  end

  action_item :only => :index do
    link_to 'Import XLSX', :action => 'upload_xlsx'
  end

  collection_action :import_xlsx, method: :post do
    begin
      # Uploaded xlsx file
      xlsx_file = params.require(:g_tin_xlsx).permit(:file)
      # Open xlsx
      xlsx = Roo::Spreadsheet.open(xlsx_file[:file], extension: :xlsx)
      # Import results
      total_rows = 0
      imported_rows = 0
      # For each row
      xlsx.each_row_streaming do |row|
        imported_rows += 1 unless ::GTin.create(:gtin => row[0].cell_value).errors.messages.any?
        total_rows += 1
      end
      # Finally, redirect to index page
      redirect_to collection_path, notice: "#{imported_rows} out of #{total_rows} GTINs imported!"
    rescue ActionController::ParameterMissing => e
      redirect_to upload_xlsx_admin_g_tins_path, alert: 'Please specify the xlsx file.'
    rescue => e
      redirect_to upload_xlsx_admin_g_tins_path, alert: e.message
    end
  end

  index do
    selectable_column
    column :gtin
    column :used
    column :score do |gtin|
      gtin.score_id.blank? ? '' : link_to(gtin.score_id, admin_score_path(gtin.score_id))
    end

    actions defaults: false do |g_tin|
      item image_tag("activeadmin/show.png", size: 16), admin_g_tin_path(g_tin.id), class: 'member_link'
      item image_tag("activeadmin/edit.png", size: 16), edit_admin_g_tin_path(g_tin.id), class: 'member_link'
      item image_tag("activeadmin/delete.png", size: 16), admin_g_tin_path(g_tin.id), class: 'member_link', method: :delete, data: { confirm: I18n.t('active_admin.delete_confirmation') }
    end
  end

  show do |model|
    attributes_table do
      row :gtin
      row :used
      row :score do |gtin|
      gtin.score_id.blank? ? '' : link_to(gtin.score_id, admin_score_path(gtin.score_id))
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :gtin
      f.input :used
    end
    f.actions
  end

end
