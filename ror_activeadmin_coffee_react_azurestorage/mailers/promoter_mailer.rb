class PromoterMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.promoter_mailer.promote.subject
  #
  def promote(user, promotion, invoice, plan: :basic)
    @promotion = promotion
    @invoice = invoice

    attachments["invoice_#{@invoice.id}.pdf"] = WickedPdf.new.pdf_from_string(
      render_to_string(pdf: 'todo', template: 'invoices/show.pdf.erb', layout: 'invoice')
    )

    mail to: user.email, subject: 'Your promotion order was successfully received by us.', bcc: ['customer@soundnotation.com']
  end
end
