class NewUserMailer < ApplicationMailer
 
  def welcome_email(user)
    @user = user
    mail(to: 'customer@soundnotation.com', subject: 'A new customer has registered')
  end
end
