class DistributorMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.distributor_mailer.subscription.subject
  #
  def subscription(user, score, invoice, plan: :basic)
    @score = score
    @invoice = invoice
    composition = Composition.find(@score.composition_id)
    @composition = ComposerDecorator.new(composition)

    attachments["invoice_#{@invoice.id}.pdf"] = WickedPdf.new.pdf_from_string(
      render_to_string(pdf: 'todo', template: 'invoices/show.pdf.erb', layout: 'invoice')
    )

    mail to: user.email, subject: I18n.t('distributor_mailer.subject'), bcc: ['customer@soundnotation.com']
  end
end
