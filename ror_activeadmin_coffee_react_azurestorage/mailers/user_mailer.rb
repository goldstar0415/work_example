class UserMailer < Devise::Mailer
  default from: 'Soundnotation <info@soundnotation.com>'
end
