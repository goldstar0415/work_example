class CreatorMailer < ApplicationMailer
  def confirm(composition_id:, **options)
    composition = Composition.find(composition_id)
    @composition = ComposerDecorator.new(composition)
    @invoice = @composition.invoice
    @billing_address = options.fetch(:billing_address, nil).presence

    attachments["invoice_#{@invoice.id}.pdf"] = WickedPdf.new.pdf_from_string(
      render_to_string(pdf: 'todo', template: 'invoices/show.pdf.erb', layout: 'invoice')
    )
    @amount = @composition.source.invoice.try(:amount)
    @currency = @amount.try(:currency)
    
    mail(to: @composition.source.user.email, subject: I18n.t('creator_mailer.subject'), bcc: ['customer@soundnotation.com'])
  end
end

# ssh ubuntu@40.68.187.176
