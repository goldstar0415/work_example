class ApplicationMailer < ActionMailer::Base
  default from: 'Soundnotation <info@soundnotation.com>'
  layout 'mailer'
end
