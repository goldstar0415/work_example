class ScoreMailer < ApplicationMailer
  def new_announcement(composition_id:, score_id:, email:, mins:)
    composition = Composition.find(composition_id)
    @composition = ComposerDecorator.new(composition)
    @score = Score.find(score_id)
    @mins = mins

    mail(to: email, subject: 'We have a job for you', bcc: 'info@soundnotation.com')
  end
end
