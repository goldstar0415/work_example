class ScoreCreationUploadMailer < ApplicationMailer
  def score_creation_upload(composition_id, score_id)
    composition = Composition.find(composition_id)
    @composition = ComposerDecorator.new(composition)
    @score = Score.find(score_id)
    transcriber_job = @score.transcriber_job
    unless (transcriber_job.nil?)
      @user = transcriber_job.user
      unless @user.nil?
      	mail(to: ['customer@soundnotation.com', 'info@soundnotation.com'], subject: 'New sheet music uploaded by ' + @user.full_name)
      end
    end
  end
end
