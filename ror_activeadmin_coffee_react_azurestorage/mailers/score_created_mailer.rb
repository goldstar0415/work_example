class ScoreCreatedMailer < ApplicationMailer
  def score_created(composition_id, score_id, user_id)
    composition = Composition.find(composition_id)
    @composition = ComposerDecorator.new(composition)
    @score = Score.find(score_id)
    @user = User.find(user_id)
    language = @user.language
    @locale = language.nil? ? :en : language.lang_locale.downcase.to_sym

    mail(to: @user.email, subject: t('.subject', locale: @locale), bcc: 'info@soundnotation.com')
  end
end
