import * as React from 'react';
import thunk from 'redux-thunk';
import * as jwtDecode from 'jwt-decode';
import { render } from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { eventReducer } from './redux/Events/reducer';
import { loginReducer } from './redux/Login/reducer';
import { userReducer } from './redux/Users/reducer';
import { AuthRoutes } from './AuthRoutes';
import { AppRoutes } from './AppRoutes';
import { metricReducer } from './redux/Metrics/reducers';


export interface FSA<P> {
    type: string;
    payload?: Array<P>;
    err?: Error;
}

export interface IState {
    err?: Error;
}

// Remove compose - that's just for redux dev tools
// const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(combineReducers({
        login: loginReducer,
        events: eventReducer,
        users: userReducer,
        metrics: metricReducer,
        form: formReducer
    }),
    // composeEnhancers(applyMiddleware(thunk)));
    applyMiddleware(thunk));

const auth = (nextState, replace) => {
    const state = store.getState();
    if (!state.token) {
        replace('/');
    }

    else {
        const token = jwtDecode(state.token);
    }
};

render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route {...AuthRoutes}/>
            <Route {...AppRoutes}/>
        </Router>
    </Provider>
    , document.getElementById('root'));