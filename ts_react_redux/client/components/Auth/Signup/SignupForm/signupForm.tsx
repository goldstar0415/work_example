import * as React from 'react';
import { reduxForm, Field } from 'redux-form';
import { renderInput } from '../../../../../utils/utils';
import {
    required,
    noSpecialCharacters,
    minLength,
    maxLength,
    securePassword,
    validEmail
} from '../../../../../utils/validators';

export class SignupForm extends React.Component<any, any> {
    constructor(props) {
        super(props);
    }

    render() {
        // TODO
        // Add password verification
        return (
            <form onSubmit={(this.props as any).handleSubmit}>
                <Field name='firstName' className='' component={renderInput}
                       props={{
                            type: 'text',
                            label: 'First Name'}}
                       validate={[ required ]} tabIndex={0}/>
                <Field name='lastName' className='' component={renderInput}
                       props={{
                            type: 'text',
                            label: 'Last Name'}}
                       validate={[ required ]} tabIndex={1}/>
                <Field name='userName' className='' component={renderInput}
                       props={{
                            type: 'text',
                            label: 'Username'}}
                       validate={[ required, noSpecialCharacters ]} tabIndex={2}/>
                <Field name='email' className='' component={renderInput}
                       props={{
                            type: 'email',
                            label: 'Email'}}
                       validate={[ required, validEmail ]} tabIndex={3}/>
                <Field name='_password' className='' component={renderInput}
                       props={{
                            type: 'password',
                            label: 'Password'}}
                       validate={[ required, minLength, maxLength, securePassword ]} tabIndex={4}/>
                <button className='waves-effect waves-light btn fill' type='submit'>Sign Up</button>
            </form>
        );
    };
}

export const SignupFormCTN = reduxForm({
    form: 'signup'
})(SignupForm);

export default SignupFormCTN;

