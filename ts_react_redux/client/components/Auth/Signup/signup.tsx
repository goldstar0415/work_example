import * as React from 'react';
import { SignupFormCTN } from './SignupForm/signupForm';
import { connect } from 'react-redux';
import { handleSignup } from '../../../redux/Login/actions';

require('./signup.scss');

export const Signup = (props) => {
    return (
        <div className='signup__container'>
            <div className='signup card'>
                <h1 className='center-text'>Sign Up</h1>
                <SignupFormCTN onSubmit={props.handleSignup}/>
            </div>
        </div>
    );
};


const mapStateToProps = (state) => {
    const _state = state.login;
    return {
        err: _state.err
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        handleSignup(data) {
            dispatch(handleSignup(data));
        }
    };
};

export const SignupCTN = (connect as any)(mapStateToProps, mapDispatchToProps)(Signup);