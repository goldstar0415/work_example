import * as React from 'react';
import { reduxForm, Field } from 'redux-form';
import { Link } from 'react-router';
import { required } from '../../../../../utils/validators';
import { renderInput } from '../../../../../utils/utils';


export class LoginForm extends React.Component<any, any> {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <form className='redux-form' onSubmit={(this.props as any).handleSubmit}>
                <Field className='redux-form-field'
                       name='username' component={renderInput}
                       props={{
                           type: 'text',
                           label: 'Username'}}
                       validate={[ required ]}/>
                <Field className='redux-form-field'
                       name='password' component={renderInput}
                       props={{
                           type: 'password',
                           label: 'Password'}}
                       validate={[ required ]}/>


                <div className='redux-form-field'>
                    <button className='waves-effect waves-light btn'
                            tabIndex={0} role='button' type='submit'>
                        Login
                    </button>
                    <Link to='/signup' role='button' className='waves-effect waves-light btn'>
                        Sign up
                    </Link>
                </div>
            </form>
        );
    };
}

export const LoginFormCTN = reduxForm({
    form: 'login'
})(LoginForm);

export default LoginFormCTN;

