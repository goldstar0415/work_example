import * as React from 'react';
import { handleLogin } from '../../../redux/Login/actions';
import { LoginFormCTN } from './LoginForm/loginForm';
import { connect } from 'react-redux';
import { ServerError } from '../../../../utils/utils';

require('./login.scss');

export const Login = (props) => {
    return (
        <div className='login__container'>
            <div className='login card'>
                {props.err ? <ServerError err={props.err}/> : undefined}
                <h1 className='center-text'>
                    Login
                </h1>
                <LoginFormCTN onSubmit={props.handleLogin}/>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    const _state = state.login;
    return {
        err: _state.err
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        handleLogin(data) {
            dispatch(handleLogin(data));
        }
    };
};

export const LoginCTN = (connect as any)(mapStateToProps, mapDispatchToProps)(Login);