import * as React from 'react';
import { Link } from 'react-router';

require('./header.scss');

export const Header = () => {
    return (
        <header className='header'>
            <nav className='header__nav nav'>
                <div className='nav-left'>
                    <div className='nav-item'>AtmoScan</div>
                </div>
                <div className='header__menu nav-right'>
                    <Link to='/events'><button className='btn-flat'>Events</button></Link>
                    <Link to='/users'><button className='btn-flat'>Users</button></Link>
                    <Link to='/events/globe'><button className='btn-flat'>Globe</button></Link>
                    <Link to='/events/anevent/live'><button className='btn-flat'>Live</button></Link>
                    <Link to='/help'><button className='btn-flat'>Help</button></Link>
                </div>
            </nav>
        </header>
    );
};