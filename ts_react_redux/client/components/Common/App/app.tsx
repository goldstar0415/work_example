import * as React from 'react';
import { Header } from '../Header/header';

require('./app.scss');

export const App = (props) => {
    return (
        <div className='app'>
            {sessionStorage.getItem('token') ? <Header/> : undefined}
            {props.children}
        </div>
    );
};