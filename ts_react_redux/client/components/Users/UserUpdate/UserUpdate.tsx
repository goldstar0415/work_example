import * as React from 'react';
import { IUserClient } from '../../../../models/users';
import { UserFormCTN } from '../UserForm/UserForm';
import { putUsersFunc } from '../Users';

interface IUserUpdateProps {
    user: IUserClient;
    err?: Error;
    putUsers: putUsersFunc;
}

export const UserUpdate = (props: IUserUpdateProps) => {
    const user = props.user;
    const handleSubmit = (data: IUserClient) => {
        props.putUsers(data, user.userName.toLowerCase());
    };
    const userFormProps = {
        onSubmit: handleSubmit,
        err: props.err,
        initialValues: {
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
        }
    };

    return (
        <div>
            <h3 className='center-align'>
                USER UPDATE
            </h3>
            <UserFormCTN {...userFormProps}/>
        </div>
    );
};