import * as React from 'react';
import { Link } from 'react-router';
import { ServerError, checkPermissionsClient } from '../../../../utils/utils';
import { IUser } from '../../../../models/users';
import jwtDecode = require('jwt-decode');
import { deleteUsersFunc } from '../Users';

interface IUserDetailsProp {
    user: IUser;
    err?: Error;
    deleteUsers: deleteUsersFunc;
}

export const UserDetail = (props: IUserDetailsProp) => {
    // Determine if the user has permission to edit/delete this user
    // By default, users can only modify/delete themselves
    // Admins can modify/delete anyone
    // ${method}-(?:${this.routeParam}
    const user = props.user;
    const userName = user && user.userName ? user.userName : '';
    const token = sessionStorage.getItem('token') || 'BAD_TOKEN';
    const handleDelete = () => {
        props.deleteUsers(userName);
    };
    const canEdit = checkPermissionsClient(token, {
        method: 'PUT_USER',
        resource: userName
    });
    const canDelete = checkPermissionsClient(token, {
        method: 'DELETE_USER',
        resource: userName
    });

    // Only display the edit button if the user passed the
    // check in the constructor
    const editBtn = canEdit
        ? <Link to={`/users/${userName}/update`}>
            <button>Edit</button>
        </Link>
        : undefined;

    const deleteBtn = canDelete
        ? <button onClick={handleDelete}>Delete</button>
        : undefined;

    return (
        <div className='content'>
            {props.err ? <ServerError err={props.err.message}/> : undefined}
            <table>
                <tbody>
                <tr>
                    <td><strong>First Name</strong></td>
                    <td><span>{user.firstName}</span></td>
                </tr>
                <tr>
                    <td><strong>Last Name</strong></td>
                    <td><span>{user.lastName}</span></td>
                </tr>
                <tr>
                    <td><strong>Username</strong></td>
                    <td><span>{user.userName}</span></td>
                </tr>
                <tr>
                    <td><strong>Email</strong></td>
                    <td><span>{user.email}</span></td>
                </tr>
                </tbody>
            </table>
            {editBtn}
            {deleteBtn}
        </div>
    );
};
