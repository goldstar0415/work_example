import * as React from 'react';
import { connect } from 'react-redux';
import { fetchUsers, deleteUsers, postUsers, putUsers } from '../../redux/Users/actions';
import { IUserClient } from '../../../models/users';
import { IUsersState } from '../../redux/Users/reducer';
import { UserList } from './UserList/UserList';
import { UserUpdate } from './UserUpdate/UserUpdate';
import { UserDetail } from './UserDetail/UserDetail';

export type fetchUsersFunc = (resource: string) => void;
export type postUsersFunc = (payload: IUserClient) => void;
export type putUsersFunc = (payload: IUserClient, resource: string) => void;
export type deleteUsersFunc = (resource: string) => void;

export interface IUsersProps extends IUsersState {
    fetchUsers: fetchUsersFunc;
    postUsers: postUsersFunc;
    putUsers: putUsersFunc;
    deleteUsers: deleteUsersFunc;
}

export class Users extends React.Component<IUsersProps, any> {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.fetchUsers) {
            this.props.fetchUsers('');
        }
    }

    render() {
        const users = this.props.users;
        const {route, routeParams} = (this.props as any);
        const userProps: any = {
            err: this.props.err
        };

        users.map((user) => {
            if (user.userName === routeParams.userName) {
                userProps.user = user;
            }
        });

        switch (route.path) {
            case ':userName':
                userProps.fetchUsers = this.props.fetchUsers;
                userProps.deleteUsers = this.props.deleteUsers;
                return <UserDetail {...userProps} />;
            case ':userName/update':
                userProps.mode = 'update';
                userProps.putUsers = this.props.putUsers;
                userProps.deleteUsers = this.props.deleteUsers;
                return <UserUpdate {...userProps}/>;
            default:
                userProps.users = users;
                userProps.fetchUsers = this.props.fetchUsers;
                return <UserList {...userProps}/>;
        }
    }
}

// React-Redux binding
const mapStateToProps = (state) => {
    const _state: IUsersState = state.users;
    return {
        users: _state.users,
        err: _state.err
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchUsers(resource) {
            dispatch(fetchUsers(resource));
        },
        postUsers(payload) {
            dispatch(postUsers(payload));
        },
        putUsers(payload, resource) {
            dispatch(putUsers(payload, resource));
        },
        deleteUsers(resource) {
            dispatch(deleteUsers(resource));
        }
    };
};

export const UsersCTN = (connect as any)(mapStateToProps, mapDispatchToProps)(Users);