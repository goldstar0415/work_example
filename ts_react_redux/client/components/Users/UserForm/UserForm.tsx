import * as React from 'react';
import { reduxForm, Field } from 'redux-form';
import { ServerError } from '../../../../utils/utils';

interface IUserFormProps {
    err: boolean;
}
export const UserForm = (props: IUserFormProps) => {
    // TODO
    // Add password verification
    return (
        <form onSubmit={(props as any).handleSubmit}>
            {
                props.err
                    ? <ServerError err='Error communicating with the server. Please try again later'/>
                    : undefined
            }
            <div>
                <label htmlFor='firstName'>First Name</label>
                <Field name='firstName' component='input' type='text' placeholder='First Name'/>
            </div>
            <div>
                <label htmlFor='lastName'>Last Name</label>
                <Field name='lastName' component='input' type='text' placeholder='Last Name'/>
            </div>
            <div>
                <label htmlFor='email'>Email</label>
                <Field name='email' component='input' type='email' placeholder='Email'/>
            </div>
            <button type='submit'>Update</button>
        </form>
    );
};

export const UserFormCTN = reduxForm({
    form: 'user'
})(UserForm as any);


