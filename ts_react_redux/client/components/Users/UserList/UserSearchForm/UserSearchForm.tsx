import * as React from 'react';
import { reduxForm, Field } from 'redux-form';
import { noSpecialCharacters, validEmail } from '../../../../../utils/validators';
import { renderInput } from '../../../../../utils/utils';

export const UserSearchForm = (props) => {
    /* tslint:disable */
    return (
        <form onSubmit={(props as any).handleSubmit}>
            <Field className='redux-form-field'
                   name='userName' component={renderInput}
                   props={{
                           type: 'text',
                           label: 'Username'}}
                   validate={noSpecialCharacters}/>
            <Field className='redux-form-field'
                   name='firstName' component={renderInput}
                   props={{
                           type: 'text',
                           label: 'First Name'}}
                   validate={noSpecialCharacters}/>
            <Field className='redux-form-field'
                   name='lastName' component={renderInput}
                   props={{
                           type: 'text',
                           label: 'Last Name'}}
                   validate={noSpecialCharacters}/>
            <Field className='redux-form-field'
                   name='email' component={renderInput}
                   props={{
                           type: 'text',
                           label: 'Email'}}
                   validate={validEmail}/>
            <div className='redux-form-field btn-group'>
                <button className='waves-effect waves-light btn'>Back</button>
                <button className='waves-effect waves-light btn' type='submit'>Search</button>
                <button className='waves-effect waves-light btn'>Forward</button>
            </div>
        </form>
    );
    /* tslint:enable */
};

export const UserSearchFormCTN = reduxForm({
    form: 'userSearch'
})(UserSearchForm as any);