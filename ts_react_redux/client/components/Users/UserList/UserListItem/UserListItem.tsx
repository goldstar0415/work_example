import * as React from 'react';
import { Link } from 'react-router';

export interface IUserListItemProps {
    username: string;
    firstname: string;
    lastname: string;
    email: string;
}

export const UserListItem = (props: IUserListItemProps) => (
    <li className='user__listItem'>
        {/*<Link to={`/users/${props.username.toLowerCase()}`}>*/}
            <div>
                <h3 className='user__listItemHeader'>{props.username}</h3>
                <div className='user__listItemDetails'>
                    <p>{`${props.firstname} ${props.lastname}`}</p>
                    <p>{props.email}</p>
                </div>
            </div>
        {/*</Link>*/}
    </li>
);