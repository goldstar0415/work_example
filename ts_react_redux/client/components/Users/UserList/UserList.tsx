import * as React from 'react';
import { IUsersState } from '../../../redux/Users/reducer';
import { IUser, IUserClient } from '../../../../models/users';
import { ServerError } from '../../../../utils/utils';
import { IUserListItemProps, UserListItem } from './UserListItem/UserListItem';
import { UserSearchFormCTN } from './UserSearchForm/UserSearchForm';
import { fetchUsersFunc } from '../Users';

require('./UserList.scss');

export interface IUserListProps extends IUsersState {
    users: Array<IUser>;
    err?: Error;
    fetchUsers: fetchUsersFunc;
}

export const UserList = (props: IUserListProps) => {
    const handleSumbit = (data: IUserClient) => {
        let queryString = '?';
        if (data.userName) {
            queryString = `${queryString}userName=${data.userName}&`;
        }
        if (data.firstName) {
            queryString = `${queryString}firstName=${data.firstName}&`;
        }
        if (data.lastName) {
            queryString = `${queryString}lastName=${data.lastName}&`;
        }
        if (data.email) {
            queryString = `${queryString}email=${data.email}&`;
        }

        queryString = queryString.replace(/[&]$/, '');
        props.fetchUsers(queryString);
    };

    const users = props.users;
    const items = users
        ? (users as Array<IUser>).map((val) => {
            const userListItemProps: IUserListItemProps = {
                username: val.userName,
                firstname: val.firstName,
                lastname: val.lastName,
                email: val.email
            };
            return <UserListItem {...userListItemProps} key={val.userName}/>;
        })
        : [];

    return (
        /* tslint:disable */
        <div className='content'>
            <div className='row'>
                <div className='col s3'>
                    <div><h3 className='center-align'>USERS</h3></div>
                    <UserSearchFormCTN onSubmit={handleSumbit}/>
                </div>
                <div className='col s9'>
                    {props.err ? <ServerError err={props.err.message}/> : undefined}
                    <ul className='user__list'>
                        {items}
                    </ul>
                </div>
            </div>
        </div>
        /* tslint:enable */
    );
};

