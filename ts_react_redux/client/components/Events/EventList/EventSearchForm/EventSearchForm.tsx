import * as React from 'react';
import { Link } from 'react-router';
import { reduxForm, Field } from 'redux-form';
import { noSpecialCharacters } from '../../../../../utils/validators';
import { renderInput } from '../../../../../utils/utils';

export class EventSearchForm extends React.Component<any, any> {
    constructor(props) {
        super(props);
    }

    render() {
        /* tslint:disable */
        return (
            <form className='col s3 event__searchForm' onSubmit={(this.props as any).handleSubmit}>
                <div><h3 className='center-align'>EVENTS</h3></div>
                <Field name='category' component={renderInput} baseComponent='select' label='Category'/>
                <Field className='redux-form-field'
                       name='eventName' component={renderInput}
                       props={{
                           type: 'text',
                           label: 'Event Name'}}
                       validate={noSpecialCharacters}/>
                <Field className='redux-form-field'
                       name='location' component={renderInput} baseComponent='geo'
                       props={{
                           label: 'Location'}}
                />
                <Field name='start'
                       component={renderInput}
                       baseComponent='date'
                       props={{label: 'Start Date'}}/>
                <Field name='end'
                       component={renderInput}
                       baseComponent='date'
                       props={{label: 'End Date'}}/>
                <div className='btn-group fill'>
                    <button className='waves-effect waves-light btn' type='submit'>Search</button>
                    <button className='waves-effect waves-light btn'>Back</button>
                    <button className='waves-effect waves-light btn'>Forward</button>
                    <Link to='/events/create'><button className='waves-effect waves-light btn'>New</button></Link>
                </div>
            </form>
        );
        /* tslint:enable */
    }
}

export const EventSearchFormCTN = reduxForm({
    form: 'eventSearch'
})(EventSearchForm);