import * as React from 'react';
import { ServerError } from '../../../../utils/utils';
import { IEventsProps } from '../Events';
import { EventListItemWithRouter } from './EventListItem/EventListItem';
import { EventSearchFormCTN } from './EventSearchForm/EventSearchForm';
require('./EventList.scss');

export const EventList = (props: IEventsProps) => {
    const events = props.events;
    const eventRows = events.map((event) => {
        return (
            <EventListItemWithRouter key={event._id} event={event}/>
        );
    });

    const handleSubmit = (data) => {
        let queryString = '?';
        if (data) {
            if (data.category && data.category > -1) {
                queryString = `${queryString}category=${data.category}&`;
            }
            if (data.eventName) {
                queryString = `${queryString}eventName=${data.eventName}&`;
            }
            if (data.location) {
                queryString = `${queryString}lng=${data.location.lng}&lat=${data.location.lat}&`;
            }
            if (data.start) {
                queryString = `${queryString}start=${data.start.toISOString()}&`;
            }
            if (data.end) {
                queryString = `${queryString}end=${data.end.toISOString()}&`;
            }
        }
        queryString = queryString.replace(/[&]$/, '');
        props.fetchEvents(queryString);
    };

    return (
        <div className='content'>
            <div className='row '>
                <EventSearchFormCTN onSubmit={handleSubmit}/>
                <div className='col s9'>
                    {props.err ? <ServerError err={props.err.message}/> : undefined}
                    <table className='events__table highlight'>
                        <thead className='events__header'>
                        <tr>
                            <td className='events__item--header'>Category</td>
                            <td className='events__item--header'>Name</td>
                            <td className='events__item--header'>Location</td>
                            <td className='events__item--header'>Start</td>
                            <td className='events__item--header'>End</td>
                        </tr>
                        </thead>
                        <tbody>
                        {eventRows}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};