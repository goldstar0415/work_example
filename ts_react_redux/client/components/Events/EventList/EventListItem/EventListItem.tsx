import * as React from 'react';
import * as moment from 'moment';
import { EVENT_TYPES } from '../../../../../models/event_types';
import { withRouter } from 'react-router';
import { IEvent } from '../../../../../models/events';

interface IEventListItemProps {
    event: IEvent;
}

export const EventListItem = (props: IEventListItemProps) => {
    const event = props.event;
    const href = (`/events/${event.slug}`);
    const handleClick = () => {
        (props as any).router.push(href);
    };

    return (
        <tr onClick={handleClick}>
            <td className='events__item--body'>
                {EVENT_TYPES[event.category]}
            </td>
            <td className='events__item--body'>
                {event.eventName}
            </td>
            <td className='events__item--body'>
                {event.locString}
            </td>
            <td className='events__item--body'>
                <div>{moment(event.startDate).format('LT')}</div>
                <div>{moment(event.startDate).format('ll')}</div>
            </td>
            <td className='events__item--body'>
                <div>{moment(event.endDate).format('LT')}</div>
                <div>{moment(event.endDate).format('ll')}</div>
            </td>
        </tr>
    );
};

export const EventListItemWithRouter = withRouter(EventListItem);