import * as React from 'react';
import { Promise } from 'es6-promise';
import { connect } from 'react-redux';
import { IEvent } from '../../../../models/events';
import { IMetric } from '../../../../models/metrics';
import { RouteComponentProps, withRouter } from 'react-router';
import { fetchMetrics } from '../../../redux/Metrics/actions';


(window as any).CESIUM_BASE_URL = '../../js/vendor/Cesium';
require('../../../../server/public/js/vendor/Cesium/Cesium.js');
require('../../../../server/public/js/vendor/Cesium/Widgets/widgets.css');
const Cesium = (window as any).Cesium;

interface ICesiumViewerProps extends RouteComponentProps<any, any> {
    event: IEvent;
    metrics: Array<Array<IMetric>>;
    fetchMetrics: (slug: string) => void;
}

const genRoute = (metrics: Array<IMetric>, start, stop) => {
    return new Promise((resolve, reject) => {
        try {
            const positionProperty = new Cesium.SampledPositionProperty();
            const positionStatic: Array<number> = [];
            let avgAlt = 0;
            metrics
                .sort((a, b) => {
                    return new Date(a.datetime).valueOf() - new Date(b.datetime).valueOf();
                })
                .map((metric) => {
                    const [lng, lat, alt] = metric.location.coordinates;
                    const time = new Cesium.JulianDate.fromIso8601(metric.datetime);
                    const pos = new Cesium.Cartesian3.fromDegrees(lng, lat, alt);
                    positionProperty.addSample(time, pos);
                    positionStatic.push(lng);
                    positionStatic.push(lat);
                    avgAlt += alt;
                });

            const entity = new Cesium.Entity({
                // Set the entity availability to the same interval as the simulation time.
                availability: new Cesium.TimeIntervalCollection([new Cesium.TimeInterval({
                    start: start,
                    stop: stop
                })]),

                // Use our computed positions
                position: positionProperty,
                point: {
                    pixelSize: 10,
                    color: Cesium.Color.RED,
                },

                // Show the path as a Cesium corridor
                // corridor: {
                //     positions: Cesium.Cartesian3.fromDegreesArray(positionStatic),
                //     width: 20.0,
                //     material: Cesium.Color.RED.withAlpha(0.5),
                //     outline: false,
                //     height: avgAlt / metrics.length
                // }

                // or show the path as thin line
                path: {
                    resolution: 1,
                    material: Cesium.Color.BLACK.withAlpha(0.5),
                    width: 10
                }
            });

            // Set the interpolation function to smooth out the the data
            entity.position.setInterpolationOptions({
                interpolationDegree: 5,
                interpolationAlgorithm: Cesium.LagrangePolynomialApproximation
            });

            resolve(entity);
        }
        catch (err) {
            reject(err);
        }
    });
};

export class CesiumViewer extends React.Component<ICesiumViewerProps, any> {
    viewer;

    constructor(props) {
        super(props);
        this.viewer = undefined;
    }

    componentDidMount() {
        // Fetch event metrics from server
        // On first load, the list of metrics will be empty
        this.props.fetchMetrics(this.props.params.slug);
    }

    componentDidUpdate() {
        // Setup the viewer
        const {event, metrics} = this.props;
        if (event) {
            this.viewer = new Cesium.Viewer('cesiumViewer', {
                infoBox: false,
                baseLayerPicker: false,
                terrainProviderViewModels: [],
                imageryProvider: Cesium.createOpenStreetMapImageryProvider({
                    url: 'http://tile.stamen.com/toner'
                }),
            });

            // Load volumetric terrain
            this.viewer.terrainProvider = new Cesium.CesiumTerrainProvider({
                url: 'https://assets.agi.com/stk-terrain/world',
                requestWaterMask: false,
                requestVertexNormals: true
            });

            // Enable lighting and depth testing
            this.viewer.scene.globe.enableLighting = true;
            this.viewer.scene.globe.depthTestAgainstTerrain = true;

            // Convert the event start and end times to a format Cesium understands
            // This will give us the for the Cesium timeline
            const start = Cesium.JulianDate.fromIso8601(event.startDate);
            const stop = Cesium.JulianDate.fromIso8601(event.endDate);

            this.viewer.clock.startTime = start.clone();
            this.viewer.clock.stopTime = stop.clone();
            this.viewer.clock.currentTime = start.clone();
            this.viewer.clock.clockRange = Cesium.ClockRange.LOOP_STOP;
            this.viewer.clock.multiplier = 5;
            this.viewer.timeline.zoomTo(start, stop);

            // Place the event marker locations on the map
            event.markers.map((marker) => {
                const [lng, lat, alt] = marker.coordinates;
                const pos = new Cesium.Cartesian3.fromDegrees(lng, lat, alt);
                this.viewer.entities.add({
                    position: pos,
                    point: {
                        pixelSize: 8,
                        color: Cesium.Color.TRANSPARENT,
                        outlineColor: Cesium.Color.YELLOW,
                        outlineWidth: 3
                    }
                });
            });

            this.viewer.entities.suspendEvents();
            // Generate an a path for each contender
            // This returns an array of Promises which will resolve to entities
            /*const contenderRoutes = metrics.map((metric, i) => {
             return genRoute(metric, start, stop);
             });*/
            const contenderRoutes = [genRoute(metrics[0], start, stop)];

            // Wait for all the promises to resolve, then add the entities to the viewer
            Promise.all(contenderRoutes)
                .then((entities: Array<any>) => {
                    entities.map((entity) => {
                        this.viewer.entities.add(entity);
                    });
                    // Zoom to the area of the event in a top down viewer
                    this.viewer.entities.resumeEvents();
                    this.viewer.zoomTo(this.viewer.entities, new Cesium.HeadingPitchRange(0, Cesium.Math.toRadians(-90)));
                })
                .catch((err) => {
                    console.error(`Error parsing the data: ${err}`);
                });
        }
    }

    render() {
        return <div id='cesiumViewer'></div>;
    }
}

const mapStateToProps = (state) => {
    return {
        metrics: state.metrics.metrics
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchMetrics(slug: string) {
            dispatch(fetchMetrics(slug));
        }
    };
};

const CesiumViewerWithRouter = withRouter(CesiumViewer);
export const CesiumViewerCTN = (connect as any)(mapStateToProps, mapDispatchToProps)(CesiumViewerWithRouter);
