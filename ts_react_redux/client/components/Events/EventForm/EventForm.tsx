import * as React from 'react';
import { Field, reduxForm } from 'redux-form';
import { browserHistory } from 'react-router';
import { renderInput, ServerError } from '../../../../utils/utils';
import { required, isEmpty, validCategory } from '../../../../utils/validators';
import { CesiumRoutePicker } from '../CesiumRoutePicker/CesiumRouterPicker';
import { deleteEventsFunc } from '../Events';
import { IEvent } from '../../../../models/events';
import { EventContenders } from './EventContenders/EventContenders';

require('./EventForm.scss');

interface IEventFormProps {
    event: IEvent;
    mode: string;
    err?: Error;
    onDelete: deleteEventsFunc;
    onSubmit: (data) => void;
}
export const EventForm = (props: IEventFormProps) => {
    const handleCancel = () => {
        browserHistory.goBack();
    };

    const handleDelete = () => {
        props.onDelete(props.event.slug);
    };

    /* tslint:disable */
    const classes = 'redux-form-field pt-input pt-fill';

    const routePickerProps = {
        name: 'routePicker',
        component: CesiumRoutePicker,
        validate: [required, isEmpty],
    };
    const delBtn = props.mode === 'update'
        ? <button className='btn' type='button' onClick={handleDelete}>Delete</button>
        : undefined;

    return (
        <form className='content' onSubmit={(props as any).handleSubmit}>
            <div className='row'>
                <div className='col s3'>
                    <div><h3 className='center-align'>{`${props.mode.toUpperCase()} EVENT`}</h3></div>
                    <div>
                        <Field name='eventName' component={renderInput}
                               className={classes} type='text'
                               validate={[ required ]}
                               label='Event Name'/>
                        <Field name='category' component={renderInput} baseComponent='select'
                               validate={[ required, validCategory ]}
                               label='Category'/>
                        <Field name='desc' component={renderInput} baseComponent='textarea'
                               className='' validate={[ required ]}
                               label='Event Description '/>
                        <Field name='start' component={renderInput} baseComponent='datetime' validate={[ required ]}/>
                        <Field name='end' component={renderInput} baseComponent='datetime' validate={[ required ]}/>
                        <Field name='contenders' component={EventContenders} defaultValue={[]}/>
                        <div className='btn-group'>
                            <button className='btn' type='submit'>Submit</button>
                            <button className='btn' type='button' onClick={handleCancel}>Cancel</button>
                            {delBtn}
                        </div>
                    </div>
                </div>
                <div className='col s9'>
                    {
                        props.err
                            ? <ServerError err={'An error occurred while communcating with the server'}/>
                            : undefined
                    }
                    {
                        props.mode === 'create' || (props.mode === 'update' && props.event && props.event._id)
                            ? <Field {...routePickerProps}/>
                            : undefined
                    }
                </div>
            </div>
        </form>
        /* tslint:enable */
    );
};

export const EventFormCTN = reduxForm({
    form: 'event',
    enableReinitialize: true
})(EventForm as any);
