import * as React from 'react';
import { isArray } from 'util';

interface IEventContendersProps {
    input: any; // Passed in from Redux Form
}

export class EventContenders extends React.Component<IEventContendersProps, any> {
    constructor(props) {
        super(props);
        this.handleAdd = this.handleAdd.bind(this);
        this.handleDel = this.handleDel.bind(this);
    }

    render() {
        const contenders = EventContenders.sanitizeInput(this.props.input.value).map((contender, i) => {
            const username = contender._username;
            const txId = contender._txId;
            return (
                <tr key={`${username}-${txId}`}>
                    <td>
                        {username}
                    </td>
                    <td>
                        {txId}
                    </td>
                    <td>
                        <button className='fill btn' onClick={this.handleDel} type='button' value={i}>Remove</button>
                    </td>
                </tr>
            );
        });

        return (
            <div className='contenders'>
                <p className='center-align'>Contenders</p>
                <div className='contenders__input-container'>
                    <div className='contenders__input'>
                        <input type='text' id='username' name='username' placeholder='username'/>
                        <input type='text' id='txId' name='txId' placeholder='txId'/>
                    </div>
                    <button className='fill btn' onClick={this.handleAdd} type='button'>Add</button>
                </div>
                <div className='contenders__list'>
                    <table>
                        <thead>
                        <tr>
                            <td>USERNAME</td>
                            <td>TXID</td>
                        </tr>
                        </thead>
                        <tbody>
                        {contenders}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }

    handleAdd() {
        const username = (document.getElementById('username') as HTMLInputElement);
        const txId = (document.getElementById('txId') as HTMLInputElement);
        if (username.value !== '' && txId.value !== '') {
            const val = EventContenders.sanitizeInput(this.props.input.value);
            val.push({
                _username: username.value,
                _txId: txId.value,
                position: 0,
                maxSpeed: 0,
                maxAlt: 0,
                maxAccel: 0,
                finishTime: ''
            });
            username.value = '';
            txId.value = '';
            this.props.input.onChange(val);
            this.forceUpdate();
        }
    }

    handleDel(event) {
        const index = +event.target.value;
        const val = EventContenders.sanitizeInput(this.props.input.value);
        this.props.input.onChange(val.filter((contender, i) => {
            return i !== index;
        }));
        this.forceUpdate();
    }

    static sanitizeInput(input) {
        return input && isArray(input) ? input : [];
    }
}