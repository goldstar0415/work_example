import * as React from 'react';
import * as moment from 'moment';
import { IContenderData } from '../../../../../models/contenderData';

interface IContenderDataProps {
    contender: IContenderData;
}

export const ContenderData = (props: IContenderDataProps) => {
    const contender = props.contender;
    return (
        <tr>
            <td>{contender._username}</td>
            <td>{contender._txId}</td>
            <td>{contender.position}</td>
            <td>{moment(contender.finishTime).format('h:mma, MM/D/YYYY')}</td>
            <td>{contender.maxSpeed}</td>
        </tr>
    );
};