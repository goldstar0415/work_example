import * as React from 'react';
import * as moment from 'moment';
import { Link } from 'react-router';
import { EVENT_STATE, IEvent } from '../../../../models/events';
import { EVENT_TYPES } from '../../../../models/event_types';
import { ServerError, checkPermissionsClient } from '../../../../utils/utils';
import { IContenderData } from '../../../../models/contenderData';
import { ContenderData } from './ContenderData/ContenderData';
import { putEventsFunc } from '../Events';


interface IEventDetailProps {
    err?: Error;
    event: IEvent;
    putEvents: putEventsFunc;
}


export const EventDetail = (props: IEventDetailProps) => {
    const event = props.event;
    const token = sessionStorage.getItem('token');
    const handleActivate = () => {
        props.putEvents({}, `${props.event.slug}/activate`);
    };
    const handleDeactivate = () => {
        props.putEvents({}, `${props.event.slug}/end`);
    };

    let editBtn;
    if (token) {
        const isAllowed = checkPermissionsClient(token, {
            method: 'PUT_EVENT',
            resource: event && event.eventCreator ? event.eventCreator : ''
        });
        editBtn = isAllowed && props.event
            ?
            <div className='btn-group fill'>
                <Link to={`/events/${props.event.slug}/update`}>
                    <button className='waves-effect waves-light btn'>Edit</button>
                </Link>
                <button className='waves-effect waves-light btn' onClick={handleActivate}>Activate</button>
                <button className='waves-effect waves-light btn' onClick={handleDeactivate}>End</button>
            </div>
            : undefined;
    }

    const contenders = event && event.contenders
        ? event.contenders.sort((a, b) => {
            return a.position - b.position;
        })
            .map((contender: IContenderData) => {
                return <ContenderData key={`${contender._username}-${contender._txId}`} contender={contender}/>;
            })
        : undefined;
    return event ? (
            <div className='content'>
                <div className='row'>
                    <div className='col s3'>
                        <table>
                            <tbody>
                            <tr>
                                <td><strong>Title</strong></td>
                                <td>
                                    <p>{event.eventName}</p>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Created By</strong></td>
                                <td><p>{event.eventCreator}</p>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Description</strong></td>
                                <td><p>{event.desc}</p>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Location</strong></td>
                                <td><p>{event.locString}</p>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Start Date</strong></td>
                                <td>
                                    <div>{moment(event.startDate).format('LT')}</div>
                                    <div>{moment(event.startDate).format('ll')}</div>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>End Date</strong></td>
                                <td>
                                    <div>{moment(event.endDate).format('LT')}</div>
                                    <div>{moment(event.endDate).format('ll')}</div>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Category</strong></td>
                                <td><p>{EVENT_TYPES[event.category].replace(/_/, ' ')}</p>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Status</strong></td>
                                <td>
                                    <p>{EVENT_STATE[event.state]}</p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        {editBtn}
                        <div>
                            <Link to={`/events/${props.event.slug}/replay`}>
                                <button className='waves-effect waves-light btn'>Replay</button>
                            </Link>
                        </div>
                    </div>
                    <div className='col s9'>
                        {props.err ? <ServerError err={props.err.message}/> : undefined}
                        <table>
                            <thead>
                            <tr>
                                <td>username</td>
                                <td>txId</td>
                                <td>position</td>
                                <td>finish time</td>
                                <td>max speed</td>
                            </tr>
                            </thead>
                            <tbody>
                            {contenders}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        ) : <div></div>;
};