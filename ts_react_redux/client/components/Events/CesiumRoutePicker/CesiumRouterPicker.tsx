import * as React from 'react';

(window as any).CESIUM_BASE_URL = '../../js/vendor/Cesium';
require('../../../../server/public/js/vendor/Cesium/Cesium.js');
require('../../../../server/public/js/vendor/Cesium/Widgets/widgets.css');
const Cesium = (window as any).Cesium;

interface ICesiumRoutePickerProps {
    markers: Array<number>;
}

export class CesiumRoutePicker extends React.Component<ICesiumRoutePickerProps, any> {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        let {input: {value, onChange}} = this.props as any;
        if (value === '') {
            value = [];
        }
        // Create Cesium viewer. Remove most of the extra menus and enforce a 2D view
        const viewer = new Cesium.Viewer('cesiumEventCreate', {
            animation: false,
            baseLayerPicker: false,
            fullScreenButton: false,
            vrButton: false,
            homeButton: false,
            infoBox: false,
            sceneModePicker: false,
            timeline: false,
            sceneMode: Cesium.SceneMode.SCENE2D,
            imageryProvider: Cesium.createOpenStreetMapImageryProvider({
                url: 'http://tile.stamen.com/toner'
            })
        });

        // Add a informational label that follows the mouse and displays the lng/lat coordinates of the cursor
        const label = viewer.entities.add({
            label: {
                show: false,
                showBackground: true,
                font: '14px monospace',
                horizontalOrigin: Cesium.HorizontalOrigin.LEFT,
                verticalOrigin: Cesium.VerticalOrigin.TOP,
                pixelOffset: new Cesium.Cartesian2(15, 0)
            }
        });

        // Update the label above on mouse move
        // Store coordinates in lat and lng for later
        const scene = viewer.scene;
        const handler = new Cesium.ScreenSpaceEventHandler(scene.canvas);
        let lat = 0;
        let lng = 0;
        handler.setInputAction((movement) => {
            const cartesian = viewer.camera.pickEllipsoid(movement.endPosition, scene.globe.ellipsoid);
            if (cartesian) {
                const cartographic = (Cesium.Cartographic as any).fromCartesian(cartesian);
                const longitudeString = Cesium.Math.toDegrees(cartographic.longitude).toFixed(5);
                const latitudeString = Cesium.Math.toDegrees(cartographic.latitude).toFixed(5);

                label.position = cartesian;
                label.label.show = true;
                label.label.text =
                    `Lon: ${longitudeString}\u00B0\nLat: ${latitudeString}\u00B0`;
                lng = parseFloat(longitudeString);
                lat = parseFloat(latitudeString);
            } else {
                label.label.show = false;
            }
        }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);

        // A polyline will be used to draw the path as the user clicks on points
        const route = viewer.entities.add({
            name: 'route',
            polyline: {
                width: 5,
                material: Cesium.Color.RED,
                positions: Cesium.Cartesian3.fromDegreesArray(value)
            }
        });

        // markerEntitities will be used to remove the points
        // On right click, update the route position and add a marker to the display
        const markerEntities: Array<any> = [];
        handler.setInputAction(() => {
            const val = value;
            val.push(lng);
            val.push(lat);
            onChange(value = val);

            route.polyline.positions = Cesium.Cartesian3.fromDegreesArray(val);
            markerEntities.push(viewer.entities.add({
                position: Cesium.Cartesian3.fromDegrees(lng, lat),
                point: {
                    pixelSize: 5,
                    material: Cesium.Color.TRANSPARENT,
                    outlineColor: Cesium.Color.YELLOW,
                }
            }));
        }, Cesium.ScreenSpaceEventType.LEFT_CLICK);

        // On left click, remove most recent segment of route
        handler.setInputAction(() => {
            const val = value;
            val.pop();
            val.pop();
            onChange(value = val);

            route.polyline.positions = Cesium.Cartesian3.fromDegreesArray(val);
            const marker = markerEntities.pop();
            if (marker) {
                viewer.entities.remove(marker);
            }
        }, Cesium.ScreenSpaceEventType.RIGHT_CLICK);

        // Zoom the user's current location or the location of the loaded markers
        // TODO
        // What happens when the user denies access to geolocation API?
        if (value === undefined || value.length < 2) {
            navigator.geolocation.getCurrentPosition((position: any) => {
                viewer.camera.flyTo({destination: Cesium.Cartesian3.fromDegrees(position.coords.longitude, position.coords.latitude, 5000)});
            });
        }

        else {
            viewer.zoomTo(route);
        }
    }

    render() {
        const {meta: {touched, error, warning}} = this.props as any;
        const err = touched && error
            ? <div className='form-field-callout no-offset'>{error}</div>
            : touched && warning
                ? <div>{warning}</div>
                : undefined;
        return (
            <div>
                {err}
                <div id='cesiumEventCreate'></div>
            </div>
        );
    }
}