import * as React from 'react';
import * as jwtDecode from 'jwt-decode';
import { EVENT_STATE, IEventClient, IEvent } from '../../../../models/events';
import { IGeoPoint } from '../../../../models/geopoint';
import { EventFormCTN } from '../EventForm/EventForm';
import { postEventsFunc, putEventsFunc, deleteEventsFunc } from '../Events';

// TODO
// Add validation
/*
 Only add if all fields are filled out
 Don't add time before Date.now()
 Don't add time after x years in future
 Don't add if there are less than 2 markers (four entries)
 */

interface IEventUpdateProps {
    event: IEvent;
    mode: string;
    err?: Error;
    postEvents: postEventsFunc;
    putEvents: putEventsFunc;
    deleteEvents: deleteEventsFunc;
}

export const EventUpdate = (props: IEventUpdateProps) => {
    // Format the data in a way the server will be able to understand
    // Package the date objects into ISO strings
    // Parse the markers into GeoJSON objects
    // Final line sends to server
    const handleSubmit = (data) => {
        const token = jwtDecode(sessionStorage.getItem('token') || '');
        const creator = token ? token.payload.sub : '';

        // Create a GEOJSON object for each point pair
        const geoPoints = data.routePicker;
        let markers: Array<IGeoPoint> = [];
        for (let i = 0; i < geoPoints.length; i += 2) {
            markers.push({
                type: 'Point',
                coordinates: [geoPoints[i], geoPoints[i + 1]]
            });
        }

        const payload: IEventClient = {
            eventName: data.eventName.toLowerCase().trim(),
            eventCreator: creator,
            category: +data.category,
            desc: data.desc,
            startDate: data.start.toISOString(),
            endDate: data.end.toISOString(),
            markers: markers,
            state: EVENT_STATE.NOT_STARTED,
            contenders: data.contenders
        };

        if (props.mode === 'update') {
            props.putEvents(payload, props.event.slug);
        }

        else if (props.mode === 'create') {
            props.postEvents(payload);
        }
    };
    const eventFormProps = {
        event: props.event,
        onSubmit: handleSubmit,
        onDelete: props.deleteEvents,
        mode: props.mode,
        err: props.err
    };

    if (props.event) {
        const markers: Array<number> = [];
        props.event.markers.map((val) => {
            markers.push(val.coordinates[0]);
            markers.push(val.coordinates[1]);
        });
        (eventFormProps as any).initialValues = {
            eventName: props.event.eventName,
            category: props.event.category,
            desc: props.event.desc,
            start: new Date(props.event.startDate),
            end: new Date(props.event.endDate),
            contenders: props.event.contenders,
            routePicker: markers
        };
    }
    else {
        const today = new Date();
        const tomorrow = new Date(today);
        tomorrow.setDate(tomorrow.getDate() + 1);
        (eventFormProps as any).initialValues = {
            start: today,
            end: tomorrow,
        };
    }
    return (
        <EventFormCTN {...eventFormProps}/>
    );
};