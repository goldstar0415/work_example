import * as React from 'react';
import { connect } from 'react-redux';
import { IEventsState } from '../../redux/Events/reducer';
import { fetchEvents, deleteEvents, postEvents, putEvents } from '../../redux/Events/actions';
import { EventDetail } from './EventDetail/EventDetail';
import { EventUpdate } from './EventUpdate/EventUpdate';
import { IEventClient } from '../../../models/events';
import { EventList } from './EventList/EventList';
import { CesiumGlobe } from './CesiumGlobe/CesiumGlobe';
import { CesiumViewerCTN } from './CesiumViewer/CesiumViewer';

export type fetchEventsFunc = (resource: string) => void;
export type postEventsFunc = (payload: IEventClient) => void;
export type putEventsFunc = (payload: IEventClient | {}, resource: string) => void;
export type deleteEventsFunc = (resource: string) => void;

export interface IEventsProps extends IEventsState {
    fetchEvents: fetchEventsFunc;
    postEvents: postEventsFunc;
    putEvents: putEventsFunc;
    deleteEvents: deleteEventsFunc;
}

export class Events extends React.Component<IEventsProps, any> {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.fetchEvents) {
            this.props.fetchEvents('');
        }
    }

    render() {
        const events = this.props.events;
        const {route, routeParams} = (this.props as any);
        const eventProps: any = {
            err: this.props.err
        };
        switch (route.path) {
            case 'create':
                eventProps.mode = 'create';
                eventProps.postEvents = this.props.postEvents;
                eventProps.putEvents = this.props.putEvents;
                eventProps.deleteEvents = this.props.deleteEvents;
                return <EventUpdate {...eventProps}/>;
            case 'globe':
                eventProps.events = events;
                eventProps.fetchEvents = this.props.fetchEvents;
                return <CesiumGlobe {...eventProps}/>;
            case ':slug':
                events.map((event) => {
                    if (event.slug === routeParams.slug) {
                        eventProps.event = event;
                    }
                });
                eventProps.putEvents = this.props.putEvents;
                return <EventDetail {...eventProps} />;
            case ':slug/update':
                events.map((event) => {
                    if (event.slug === routeParams.slug) {
                        eventProps.event = event;
                    }
                });
                eventProps.mode = 'update';
                eventProps.postEvents = this.props.postEvents;
                eventProps.putEvents = this.props.putEvents;
                eventProps.deleteEvents = this.props.deleteEvents;
                return <EventUpdate {...eventProps}/>;
            case ':slug/replay':
                events.map((event) => {
                    if (event.slug === routeParams.slug) {
                        eventProps.event = event;
                    }
                });
                return <CesiumViewerCTN {...eventProps}/>;
            case ':slug/live':
                events.map((event) => {
                    if (event.slug === routeParams.slug) {
                        eventProps.event = event;
                    }
                });
                return <h1 className='content'>Live View. I suggest using a modified version of the CesiumViewerCTN component</h1>;
            default:
                eventProps.events = events;
                eventProps.fetchEvents = this.props.fetchEvents;
                return (
                    <EventList {...eventProps}/>
                );
        }
    }
}

// React-Redux binding
const mapStateToProps = (state) => {
    const _state: IEventsState = state.events;
    return {
        events: _state.events,
        err: _state.err
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchEvents(resource) {
            dispatch(fetchEvents(resource));
        },
        postEvents(payload) {
            dispatch(postEvents(payload));
        },
        putEvents(payload, resource) {
            dispatch(putEvents(payload, resource));
        },
        deleteEvents(resource) {
            dispatch(deleteEvents(resource));
        }
    };
};

export const EventsCTN = (connect as any)(mapStateToProps, mapDispatchToProps)(Events);