import * as React from 'react';
import { IEvent } from '../../../../models/events';
import { fetchEventsFunc } from '../Events';
import { IGeoPoint } from '../../../../models/geopoint';

(window as any).CESIUM_BASE_URL = '../../js/vendor/Cesium';
require('../../../../server/public/js/vendor/Cesium/Cesium.js');
require('../../../../server/public/js/vendor/Cesium/Widgets/widgets.css');
const Cesium = (window as any).Cesium;

interface ICesiumGlobeProps {
    events: Array<IEvent>;
    fetchEvents: fetchEventsFunc;
}

export class CesiumGlobe extends React.Component<ICesiumGlobeProps, any> {
    heightScale;
    entities;
    viewer;

    constructor(props) {
        super(props);
        this.heightScale = 10000000;
        this.viewer = undefined;
        this.load = this.load.bind(this);
    }

    componentDidMount() {
        if (this.props.fetchEvents) {
            this.props.fetchEvents('');
        }

        this.viewer = new Cesium.Viewer('cesiumGlobe', {
            animation: false,
            timeline: false,
            imageryProvider: Cesium.createOpenStreetMapImageryProvider({
                url: 'http://tile.stamen.com/toner'
            }),
            baseLayerPicker: false
        });

        this.viewer.clock.shouldAnimate = false;
        if (this.props.events && this.props.events.length > 0) {
            this.load(this.props.events.map((event) => {
                return event.loc;
            }));
        }
    }

    render() {
        return (
            <div id='cesiumGlobe'></div>
        );
    }

    load(geoPoints: Array<IGeoPoint>) {
        // Now loop over each coordinate and create
        // our entities from the data.
        geoPoints.map((geoPoint) => {
                const coordinates = geoPoint.coordinates;
                const lng = coordinates[0];
                const lat = coordinates[1];
                const alt = Math.random();
                const color = Cesium.Color.fromHsl((0.6 - (alt * 0.5)), 1.0, 0.5);
                const surfacePosition = Cesium.Cartesian3.fromDegrees(lng, lat, 0);
                const heightPosition = Cesium.Cartesian3.fromDegrees(lng, lat, alt * this.heightScale);

                // WebGL Globe only contains lines, so that's the only graphics we create.
                const polyline = new Cesium.PolylineGraphics();
                polyline.material = new Cesium.ColorMaterialProperty(color);
                polyline.width = new Cesium.ConstantProperty(2);
                polyline.followSurface = new Cesium.ConstantProperty(false);
                polyline.positions = new Cesium.ConstantProperty([surfacePosition, heightPosition]);

                // The polyline instance itself needs to be on an entity.
                const entity = new Cesium.Entity({
                    show: true,
                    polyline: polyline,
                });

                // Add the entity to the collection.
                this.viewer.entities.add(entity);
            }
        );
    }
}