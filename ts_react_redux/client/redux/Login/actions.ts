import { browserHistory } from 'react-router';
export const LOGIN_START = 'LOGIN_START';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';

export const SIGNUP_START = 'SIGNUP_START';
export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';
export const SIGNUP_ERROR = 'SIGNUP_ERROR';

export const loginStart = () => {
    return {
        type: LOGIN_START
    };
};

export const loginSuccess = () => {
    return {
        type: LOGIN_SUCCESS
    };
};

export const loginError = (err) => {
    return {
        type: LOGIN_ERROR,
        err
    };
};

export const signupStart = () => {
    return {
        type: SIGNUP_START
    };
};

export const signupSuccess = () => {
    return {
        type: SIGNUP_SUCCESS
    };
};

export const signupError = (err) => {
    return {
        type: SIGNUP_ERROR,
        err
    };
};

export const handleLogin = (data: any) => {
    return (dispatch) => {
        dispatch(loginStart());
        fetch('/login', {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((res) => {
                if (res.status !== 200) {
                    throw new Error('Incorrect login');
                }
                return res.text();
            })
            .then((token) => {
                if (token) {
                    sessionStorage.setItem('token', token);
                    dispatch(loginSuccess());
                    browserHistory.push('/events');
                }
            })
            .catch((err) => {
                dispatch(loginError(err.message));
            });
    };
};

export const handleSignup = (data: any) => {
    return (dispatch) => {
        dispatch(signupStart());
        fetch('/signup', {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((res) => {
                if (res.status !== 201) {
                    throw new Error('Could not create user');
                }
                return res.text();
            })
            .then((token) => {
                if (token) {
                    sessionStorage.setItem('token', token);
                    dispatch(loginSuccess());
                    browserHistory.push('/events');
                }
            })
            .catch(() => {
                dispatch(signupError('Could not create an account'));
            });
    };
};