import { LOGIN_ERROR, SIGNUP_ERROR } from './actions';

require('object-assign-shim');
declare const Object: any;


const initialState = {
    token: undefined,
    err: undefined
};

export const loginReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_ERROR:
            return Object.assign({}, state, {
                err: action.err,
            });
        case SIGNUP_ERROR:
            return Object.assign({}, state, {
                err: action.err,
            });
        default:
            return state;
    }
};