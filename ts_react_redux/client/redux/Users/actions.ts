import { IUser, IUserClient } from '../../../models/users';
import { FSA } from '../../index';
import { browserHistory } from 'react-router';

export const USERS_FETCH_START = 'USERS_FETCH_START';
export const USERS_FETCH_SUCCESS = 'USERS_FETCH_SUCCESS';
export const USERS_FETCH_ERROR = 'USERS_FETCH_ERROR';
export const USERS_POST_START = 'USERS_POST_START';
export const USERS_POST_SUCCESS = 'USERS_POST_SUCCESS';
export const USERS_POST_ERROR = 'USERS_POST_ERROR';
export const USERS_PUT_START = 'USERS_PUT_START';
export const USERS_PUT_SUCCESS = 'USERS_PUT_SUCCESS';
export const USERS_PUT_ERROR = 'USERS_PUT_ERROR';
export const USERS_DELETE_START = 'USERS_DELETE_START';
export const USERS_DELETE_SUCCESS = 'USERS_DELETE_SUCCESS';
export const USERS_DELETE_ERROR = 'USERS_DELETE_ERROR';

export const usersFetchStart = (): FSA<IUser> => {
    return {
        type: USERS_FETCH_START,
        err: undefined
    };
};

export const usersFetchSuccess = (payload): FSA<IUser> => {
    return {
        type: USERS_FETCH_SUCCESS,
        payload: payload,
        err: undefined
    };
};

export const usersFetchError = (err): FSA<IUser> => {
    return {
        type: USERS_FETCH_ERROR,
        err: new Error(err)
    };
};

export const usersPostStart = (): FSA<IUser> => {
    return {
        type: USERS_POST_START,
        err: undefined
    };
};

export const usersPostSuccess = (payload): FSA<IUser> => {
    return {
        type: USERS_POST_SUCCESS,
        payload: payload,
        err: undefined
    };
};

export const usersPostError = (err): FSA<IUser> => {
    return {
        type: USERS_POST_ERROR,
        err: new Error(err)
    };
};

export const usersPutStart = (): FSA<IUser> => {
    return {
        type: USERS_PUT_START,
        err: undefined
    };
};

export const usersPutSuccess = (payload): FSA<IUser> => {
    return {
        type: USERS_PUT_SUCCESS,
        payload: payload,
        err: undefined
    };
};

export const usersPutError = (err): FSA<IUser> => {
    return {
        type: USERS_PUT_ERROR,
        err: new Error(err)
    };
};

export const usersDeleteStart = (): FSA<IUser> => {
    return {
        type: USERS_DELETE_START,
        err: undefined
    };
};

export const usersDeleteSuccess = (): FSA<IUser> => {
    return {
        type: USERS_DELETE_SUCCESS,
        err: undefined
    };
};

export const usersDeleteError = (err): FSA<IUser> => {
    return {
        type: USERS_DELETE_ERROR,
        err: new Error(err)
    };
};

// Redux thunk actions
const endpoint = '/api/users/';
const message = 'An error occured. Please try again later';
export const fetchUsers = (resource: string) => {
    return (dispatch) => {
        dispatch(usersFetchStart());
        fetch(endpoint + resource, {
            method: 'GET',
            mode: 'cors',
            headers: {
                'Authorization': `BEARER ${sessionStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            }
        })
            .then((res) => {
                if (res.status === 200) {
                    return res.json();
                }
            })
            .then((data) => {
                dispatch(usersFetchSuccess(data));
            })
            .catch((err) => {
                console.error(err);
                dispatch(usersFetchError(message));
            });
    };
};

export const postUsers = (payload: IUserClient) => {
    return (dispatch) => {
        dispatch(usersPostStart());
        fetch(endpoint, {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify(payload),
            headers: {
                'Authorization': `BEARER ${sessionStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            }
        })
            .then((res) => {
                if (res.status !== 201) {
                    throw new Error('Error creating user');
                }
                return res.json();
            })
            .then((data: any) => {
                browserHistory.push(`/users/`);
                dispatch(usersPostSuccess(data));
            })
            .catch((err) => {
                console.error(err);
                dispatch(usersPostError(message));
            });
    };
};

export const putUsers = (payload: IUserClient, resource: string) => {
    return (dispatch) => {
        dispatch(usersPutStart());
        fetch(`${endpoint}${resource}`, {
            method: 'PUT',
            mode: 'cors',
            body: JSON.stringify(payload),
            headers: {
                'Authorization': `BEARER ${sessionStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            }
        })
            .then((res) => {
                if (res.status === 200) {
                    return res.json();
                }
            })
            .then((data) => {
                browserHistory.push(`/users/`);
                dispatch(usersPutSuccess(data));
            })
            .catch((err) => {
                console.error(err);
                dispatch(usersPutError(message));
            });
    };
};

export const deleteUsers = (resource: string) => {
    return (dispatch) => {
        dispatch(usersDeleteStart());
        fetch(`${endpoint}${resource}`, {
            method: 'DELETE',
            mode: 'cors',
            headers: {
                'Authorization': `BEARER ${sessionStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            }
        })
            .then((res) => {
                if (res.status === 200) {
                    sessionStorage.removeItem('token');
                    browserHistory.push('/');
                    dispatch(usersDeleteSuccess());
                }
                else {
                    throw new Error('Cannot delete that user');
                }
            })
            .catch((err) => {
                console.error(err);
                dispatch(usersDeleteError(message));
            });
    };
};