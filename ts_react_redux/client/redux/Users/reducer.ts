import * as ACTIONS from './actions';
import { IUser } from '../../../models/users';
import { FSA, IState } from '../../index';
import { isArray } from 'util';

require('object-assign-shim');
declare const Object: any;

export interface IUsersState extends IState {
    users: Array<IUser>;
}

const INITIAL_STATE: IUsersState = {
    users: [],
    err: undefined
};

export const userReducer = (state = INITIAL_STATE, action: FSA<IUser>): IUsersState => {
    let users: Array<IUser> = [];
    switch (action.type) {
        // GET
        case ACTIONS.USERS_FETCH_START:
            return Object.assign({}, state, {
                err: action.err,
            });
        case ACTIONS.USERS_FETCH_SUCCESS:
            return Object.assign({}, state, {
                users: action.payload,
                err: action.err,
            });
        case ACTIONS.USERS_FETCH_ERROR:
            return Object.assign({}, state, {
                err: action.err,
            });
        // POST
        case ACTIONS.USERS_POST_START:
            return Object.assign({}, state, {
                err: action.err,
            });
        case ACTIONS.USERS_POST_SUCCESS:
            if (action.payload && isArray(action.payload)) {
                const users = [...state.users];
                users.push(action.payload[0]);
                return Object.assign({}, state, {
                    users: users,
                    err: action.err
                });
            }
            return Object.assign({}, state, {
                err: action.err
            });
        case ACTIONS.USERS_POST_ERROR:
            return Object.assign({}, state, {
                err: action.err,
            });
        // PUT
        case ACTIONS.USERS_PUT_START:
            return Object.assign({}, state, {
                err: action.err,
            });
        case ACTIONS.USERS_PUT_SUCCESS:
            if (action.payload && isArray(action.payload)) {
                const update = action.payload[0];
                users = state.users.map((user) => {
                    return user.userName === update.userName
                        ? update
                        : user;
                });
                return Object.assign({}, state, {
                    users: users,
                    err: action.err
                });
            }
            return Object.assign({}, state, {
                err: action.err
            });
        case ACTIONS.USERS_PUT_ERROR:
            return Object.assign({}, state, {
                err: action.err,
            });
        // DELETE
        case ACTIONS.USERS_DELETE_START:
            return Object.assign({}, state, {
                err: action.err,
            });
        case ACTIONS.USERS_DELETE_SUCCESS:
            if (action.payload && isArray(action.payload)) {
                const del = action.payload[0];
                users = state.users.filter((user) => {
                    return user.userName !== del.userName;
                });
                return Object.assign({}, state, {
                    users: users,
                    err: action.err
                });
            }
            return Object.assign({}, state, {
                err: action.err
            });
        case ACTIONS.USERS_DELETE_ERROR:
            return Object.assign({}, state, {
                err: action.err,
            });
        default:
            return state;
    }
};
