import { IEvent, IEventClient } from '../../../models/events';
import { FSA } from '../../index';
import { browserHistory } from 'react-router';

export const EVENTS_FETCH_START = 'EVENTS_FETCH_START';
export const EVENTS_FETCH_SUCCESS = 'EVENTS_FETCH_SUCCESS';
export const EVENTS_FETCH_ERROR = 'EVENTS_FETCH_ERROR';
export const EVENTS_POST_START = 'EVENTS_POST_START';
export const EVENTS_POST_SUCCESS = 'EVENTS_POST_SUCCESS';
export const EVENTS_POST_ERROR = 'EVENTS_POST_ERROR';
export const EVENTS_PUT_START = 'EVENTS_PUT_START';
export const EVENTS_PUT_SUCCESS = 'EVENTS_PUT_SUCCESS';
export const EVENTS_PUT_ERROR = 'EVENTS_PUT_ERROR';
export const EVENTS_DELETE_START = 'EVENTS_DELETE_START';
export const EVENTS_DELETE_SUCCESS = 'EVENTS_DELETE_SUCCESS';
export const EVENTS_DELETE_ERROR = 'EVENTS_DELETE_ERROR';

export const eventsFetchStart = (): FSA<IEvent> => {
    return {
        type: EVENTS_FETCH_START,
        err: undefined
    };
};

export const eventsFetchSuccess = (payload: Array<IEvent>): FSA<IEvent> => {
    return {
        type: EVENTS_FETCH_SUCCESS,
        payload: payload,
        err: undefined
    };
};

export const eventsFetchError = (err): FSA<IEvent> => {
    return {
        type: EVENTS_FETCH_ERROR,
        err: new Error(err)
    };
};

export const eventsPostStart = (): FSA<IEvent> => {
    return {
        type: EVENTS_POST_START,
        err: undefined
    };
};

export const eventsPostSuccess = (payload: Array<IEvent>): FSA<IEvent> => {
    return {
        type: EVENTS_POST_SUCCESS,
        payload: payload,
        err: undefined
    };
};

export const eventsPostError = (err): FSA<IEvent> => {
    return {
        type: EVENTS_POST_ERROR,
        err: new Error(err)
    };
};

export const eventsPutStart = (): FSA<IEvent> => {
    return {
        type: EVENTS_PUT_START,
        err: undefined
    };
};

export const eventsPutSuccess = (payload: Array<IEvent>): FSA<IEvent> => {
    return {
        type: EVENTS_PUT_SUCCESS,
        payload: payload,
        err: undefined
    };
};

export const eventsPutError = (err): FSA<IEvent> => {
    return {
        type: EVENTS_PUT_ERROR,
        err: new Error(err)
    };
};

export const eventsDeleteStart = (): FSA<IEvent> => {
    return {
        type: EVENTS_DELETE_START,
        err: undefined
    };
};

export const eventsDeleteSuccess = (payload: Array<IEvent>): FSA<IEvent> => {
    return {
        payload: payload,
        type: EVENTS_DELETE_SUCCESS,
        err: undefined
    };
};

export const eventsDeleteError = (err): FSA<IEvent> => {
    return {
        type: EVENTS_DELETE_ERROR,
        err: new Error(err)
    };
};

// Redux thunk actions
const endpoint = '/api/events/';
const message = 'An error occured. Please try again later';
export const fetchEvents = (resource: string) => {
    return (dispatch) => {
        dispatch(eventsFetchStart());
        fetch(endpoint + resource, {
            method: 'GET',
            mode: 'cors',
            headers: {
                'Authorization': `BEARER ${sessionStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            }
        })
            .then((res) => {
                if (res.status !== 200) {
                    throw new Error('Error fetching events');
                }
                return res.json();
            })
            .then((data: any) => {
                dispatch(eventsFetchSuccess(data));
            })
            .catch((err) => {
                console.error(err);
                dispatch(eventsFetchError(message));
            });
    };
};

export const postEvents = (payload: IEventClient) => {
    return (dispatch) => {
        dispatch(eventsPostStart());
        fetch(endpoint, {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify(payload),
            headers: {
                'Authorization': `BEARER ${sessionStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            }
        })
            .then((res) => {
                if (res.status !== 201) {
                    throw new Error('Error creating event');
                }
                return res.json();
            })
            .then((data: any) => {
                browserHistory.push(`/events/${data[0].slug}`);
                dispatch(eventsPostSuccess(data));
            })
            .catch((err) => {
                console.error(err);
                dispatch(eventsPostError(message));
            });
    };
};

export const putEvents = (payload: IEventClient, resource: string) => {
    return (dispatch) => {
        dispatch(eventsPutStart());
        fetch(`${endpoint}${resource}`, {
            method: 'PUT',
            mode: 'cors',
            body: JSON.stringify(payload),
            headers: {
                'Authorization': `BEARER ${sessionStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            }
        })
            .then((res) => {
                if (res.status !== 200) {
                    throw new Error('Error updating event');
                }
                return res.json();
            })
            .then((data: any) => {
                browserHistory.push(`/events/${data[0].slug}`);
                dispatch(eventsPutSuccess(data as Array<IEvent>));
            })
            .catch((err) => {
                console.error(err);
                dispatch(eventsPutError(message));
            });
    };
};

export const deleteEvents = (resource: string) => {
    return (dispatch) => {
        dispatch(eventsDeleteStart());
        fetch(`${endpoint}${resource}`, {
            method: 'DELETE',
            mode: 'cors',
            headers: {
                'Authorization': `BEARER ${sessionStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            }
        })
            .then((res) => {
                if (res.status !== 200) {
                    throw new Error('Cannot delete that event');
                }
                return res.json();
            })
            .then((data: any) => {
                browserHistory.push('/events');
                dispatch(eventsDeleteSuccess(data));
            })
            .catch((err) => {
                console.error(err);
                dispatch(eventsDeleteError(message));
            });
    };
};