import * as ACTIONS from './actions';
import { IEvent } from '../../../models/events';
import { FSA, IState } from '../../index';
import { isArray } from 'util';

require('object-assign-shim');
declare const Object: any;

export interface IEventsState extends IState {
    events: Array<IEvent>;
}

const INITIAL_STATE: IEventsState = {
    events: [],
    err: undefined,
};

export const eventReducer = (state = INITIAL_STATE, action: FSA<IEvent>): IEventsState => {
    let events: Array<IEvent> = [];
    switch (action.type) {
        // GET
        case ACTIONS.EVENTS_FETCH_START:
            return Object.assign({}, state, {
                err: action.err
            });
        case ACTIONS.EVENTS_FETCH_SUCCESS:
            return Object.assign({}, state, {
                events: action.payload,
                err: action.err
            });
        case ACTIONS.EVENTS_FETCH_ERROR:
            return Object.assign({}, state, {
                err: action.err
            });
        // POST
        case ACTIONS.EVENTS_POST_START:
            return Object.assign({}, state, {
                err: action.err
            });
        case ACTIONS.EVENTS_POST_SUCCESS:
            if (action.payload && isArray(action.payload)) {
                events = [...state.events];
                events.push(action.payload[0]);
                return Object.assign({}, state, {
                    events: events,
                    err: action.err
                });
            }
            return Object.assign({}, state, {
                err: action.err
            });
        case ACTIONS.EVENTS_POST_ERROR:
            return Object.assign({}, state, {
                err: action.err
            });
        // PUT
        case ACTIONS.EVENTS_PUT_START:
            return Object.assign({}, state, {
                err: action.err
            });
        case ACTIONS.EVENTS_PUT_SUCCESS:
            if (action.payload && isArray(action.payload)) {
                const update = action.payload[0];
                events = state.events.map((event) => {
                    return event._id === update._id
                        ? update
                        : event;
                });
            }
            return Object.assign({}, state, {
                events: events,
                err: action.err
            });
        case ACTIONS.EVENTS_PUT_ERROR:
            return Object.assign({}, state, {
                err: action.err
            });
        // DELETE
        case ACTIONS.EVENTS_DELETE_START:
            return Object.assign({}, state, {
                err: action.err
            });
        case ACTIONS.EVENTS_DELETE_SUCCESS:
            if (action.payload && isArray(action.payload)) {
                const del = action.payload[0];
                events = state.events.filter((event) => {
                    return event._id !== del._id;
                });
                return Object.assign({}, state, {
                    events: events,
                    err: action.err
                });
            }
            return Object.assign({}, state, {
                err: action.err
            });
        case ACTIONS.EVENTS_DELETE_ERROR:
            return Object.assign({}, state, {
                err: action.err
            });
        default:
            return state;
    }
};