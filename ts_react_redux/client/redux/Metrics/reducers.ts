import * as ACTIONS from './actions';
import { IMetric } from '../../../models/metrics';
import { FSA } from '../../index';

require('object-assign-shim');
declare const Object: any;

export interface IMetricState {
    metrics: Array<Array<IMetric>>;
    err?: Error;
}

const INITIAL_STATE: IMetricState = {
    metrics: [],
    err: undefined,
};

export const metricReducer = (state = INITIAL_STATE, action: FSA<Array<IMetric>>): IMetricState => {
    switch (action.type) {
        // GET
        case ACTIONS.METRICS_FETCH_START:
            return Object.assign({}, state, {
                err: action.err,
            });
        case ACTIONS.METRICS_FETCH_SUCCESS:
            return Object.assign({}, state, {
                metrics: action.payload,
                err: action.err,
            });
        case ACTIONS.METRICS_FETCH_ERROR:
            return Object.assign({}, state, {
                metrics: action.payload,
                err: action.err,
            });
        // POST
        case ACTIONS.METRICS_POST_START:
            return Object.assign({}, state, {
                err: action.err,
            });
        case ACTIONS.METRICS_POST_SUCCESS:
            return Object.assign({}, state, {
                metrics: action.payload,
                err: action.err,
            });
        case ACTIONS.METRICS_POST_ERROR:
            return Object.assign({}, state, {
                metrics: action.payload,
                err: action.err,
            });
        // PUT
        case ACTIONS.METRICS_PUT_START:
            return Object.assign({}, state, {
                err: action.err,
            });
        case ACTIONS.METRICS_PUT_SUCCESS:
            return Object.assign({}, state, {
                metrics: action.payload,
                err: action.err,
            });
        case ACTIONS.METRICS_PUT_ERROR:
            return Object.assign({}, state, {
                metrics: action.payload,
                err: action.err,
            });
        // DELETE
        case ACTIONS.METRICS_DELETE_START:
            return Object.assign({}, state, {
                err: action.err,
            });
        case ACTIONS.METRICS_DELETE_SUCCESS:
            return Object.assign({}, state, {
                metrics: action.payload,
                err: action.err,
            });
        case ACTIONS.METRICS_DELETE_ERROR:
            return Object.assign({}, state, {
                metrics: action.payload,
                err: action.err,
            });
        default:
            return state;
    }
};