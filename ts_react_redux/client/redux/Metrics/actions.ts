import { IMetric } from '../../../models/metrics';
import { FSA } from '../../index';

export const METRICS_FETCH_START = 'METRICS_FETCH_START';
export const METRICS_FETCH_SUCCESS = 'METRICS_FETCH_SUCCESS';
export const METRICS_FETCH_ERROR = 'METRICS_FETCH_ERROR';
export const METRICS_POST_START = 'METRICS_POST_START';
export const METRICS_POST_SUCCESS = 'METRICS_POST_SUCCESS';
export const METRICS_POST_ERROR = 'METRICS_POST_ERROR';
export const METRICS_PUT_START = 'METRICS_PUT_START';
export const METRICS_PUT_SUCCESS = 'METRICS_PUT_SUCCESS';
export const METRICS_PUT_ERROR = 'METRICS_PUT_ERROR';
export const METRICS_DELETE_START = 'METRICS_DELETE_START';
export const METRICS_DELETE_SUCCESS = 'METRICS_DELETE_SUCCESS';
export const METRICS_DELETE_ERROR = 'METRICS_DELETE_ERROR';

export const metricFetchStart = (): FSA<IMetric> => {
    return {
        type: METRICS_FETCH_START,
        err: undefined
    };
};

export const metricFetchSuccess = (payload): FSA<IMetric> => {
    return {
        type: METRICS_FETCH_SUCCESS,
        payload: payload,
        err: undefined
    };
};

export const metricFetchError = (err): FSA<IMetric> => {
    return {
        type: METRICS_FETCH_ERROR,
        err: new Error(err),
    };
};

export const metricPostStart = (): FSA<IMetric> => {
    return {
        type: METRICS_POST_START,
        err: undefined
    };
};

export const metricPostSuccess = (payload): FSA<IMetric> => {
    return {
        type: METRICS_POST_SUCCESS,
        payload: payload,
        err: undefined
    };
};

export const metricPostError = (err): FSA<IMetric> => {
    return {
        type: METRICS_POST_ERROR,
        err: new Error(err)
    };
};

export const metricPutStart = (): FSA<IMetric> => {
    return {
        type: METRICS_PUT_START,
        err: undefined
    };
};

export const metricPutSuccess = (payload): FSA<IMetric> => {
    return {
        type: METRICS_PUT_SUCCESS,
        payload: payload,
        err: undefined
    };
};

export const metricPutError = (err): FSA<IMetric> => {
    return {
        type: METRICS_PUT_ERROR,
        err: new Error(err),
    };
};

export const metricDeleteStart = (): FSA<IMetric> => {
    return {
        type: METRICS_DELETE_START,
        err: undefined,
    };
};

export const metricDeleteSuccess = (payload): FSA<IMetric> => {
    return {
        type: METRICS_DELETE_SUCCESS,
        payload: payload,
        err: undefined
    };
};

export const metricDeleteError = (err): FSA<IMetric> => {
    return {
        type: METRICS_DELETE_ERROR,
        err: new Error(err)
    };
};

const endpoint = '/api/metrics/';
export const fetchMetrics = (resource: string) => {
    return (dispatch) => {
        dispatch(metricFetchStart());
        fetch(endpoint + resource, {
            method: 'GET',
            mode: 'cors',
            headers: {
                'Authorization': `BEARER ${sessionStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            }
        })
            .then((res) => {
                if (res.status !== 200) {
                    throw new Error('Error fetching metrics');
                }
                return res.json();
            })
            .then((data: any) => {
                dispatch(metricFetchSuccess(data));
            })
            .catch((err) => {
                console.error(err);
                dispatch(metricFetchError(err));
            });
    };
};