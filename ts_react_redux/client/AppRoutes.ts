import { App } from './components/Common/App/app';
declare const require: any;

const EventsSubRoutes = [
    {
        path: 'create',
        getComponent(nextState, cb) {
            require.ensure([], (require) => {
                cb(null, require('./components/Events/Events.tsx').EventsCTN);
            });
        }
    }, {
        path: 'globe',
        getComponent(nextState, cb) {
            require.ensure([], (require) => {
                cb(null, require('./components/Events/Events.tsx').EventsCTN);
            });
        }
    }, {
        path: ':slug',
        getComponent(nextState, cb) {
            require.ensure([], (require) => {
                cb(null, require('./components/Events/Events.tsx').EventsCTN);
            });
        }
    }, {
        path: ':slug/update',
        getComponent(nextState, cb) {
            require.ensure([], (require) => {
                cb(null, require('./components/Events/Events.tsx').EventsCTN);
            });
        }
    }, {
        path: ':slug/replay',
        getComponent(nextState, cb) {
            require.ensure([], (require) => {
                cb(null, require('./components/Events/Events.tsx').EventsCTN);
            });
        }
    }, {
        path: ':slug/live',
        getComponent(nextState, cb) {
            require.ensure([], (require) => {
                cb(null, require('./components/Events/Events.tsx').EventsCTN);
            });
        }
    }
];

const EventsRoute = {
    path: '/events',
    getIndexRoute(partialNextState, cb) {
        require.ensure([], function (require) {
            cb(null, {
                component: require('./components/Events/Events.tsx').EventsCTN
            });
        });
    },
    getChildRoutes(location, cb) {
        cb(null, EventsSubRoutes);
    }
};

const UsersSubRoutes = [
    {
        path: ':userName/update',
        getComponent(nextState, cb) {
            require.ensure([], (require) => {
                cb(null, require('./components/Users/Users.tsx').UsersCTN);
            });
        }
    }
];

const UsersRoute = {
    path: '/users',
    getIndexRoute(partialNextState, cb) {
        require.ensure([], function (require) {
            cb(null, {
                component: require('./components/Users/Users.tsx').UsersCTN
            });
        });
    },
    getChildRoutes(location, cb) {
        cb(null, UsersSubRoutes);
    }
};

const HelpRoute = {
    path: '/help',
    getIndexRoute(partialNextState, cb) {
        require.ensure([], function (require) {
            cb(null, {
                component: require('./components/Common/Help/Help.tsx').Help
            });
        });
    }
};

export const AppRoutes = {
    path: '/',
    component: App,
    getChildRoutes(location, cb) {
        cb(null, [EventsRoute, UsersRoute, HelpRoute]);
    }
};