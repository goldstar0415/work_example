declare const require: any;
const errorLoading = (error) => {
    throw new Error(`Dynamic page loading failed: ${error}`);
};

const SignupRoute = {
    path: 'signup',
    getComponent(nextState, cb) {
        require.ensure([], (require) => {
            cb(null, require('./components/Auth/Signup/signup.tsx').SignupCTN);
        });
    }
};

export const AuthRoutes = {
    path: '/',
    getIndexRoute(partialNextState, cb) {
        require.ensure([], function (require) {
            cb(null, {
                component: require('./components/Auth/Login/login.tsx').LoginCTN
            });
        });
    },
    getChildRoutes(location, cb) {
        cb(null, [SignupRoute]);
    }
};
