# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
document.addEventListener 'turbolinks:load', ->
  # generate a new deposit address
  $("#deposit_address_container").on 'click', "#new_deposit_address", ->
    href = '/transfers/deposit/new_bitcoin_address'
    $.get href, (data)->
      $('#deposit_address_container').html(data)

  # select all on deposit address field
  $('#deposit_address').on 'click', ->
    $(this).select()

  setTooltip = (btn, message) ->
    $(btn).attr('data-original-title', message).addClass('show-tooltip')

  hideTooltip = (btn) ->
    setTimeout (->
      $(btn).removeClass('show-tooltip')
    ), 1000

  clipboard = new Clipboard('.clipboard-btn')
  clipboard.on 'success', (e) ->
    setTooltip e.trigger, 'Copied!'
    hideTooltip e.trigger
  clipboard.on 'error', (e) ->
    setTooltip e.trigger, 'Failed!'
    hideTooltip e.trigger