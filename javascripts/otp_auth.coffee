# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
document.addEventListener 'turbolinks:load', ->

  $('#left_currency').change ->
    gon.order_data['left_currency'] = $(this).val()

  $('#order_type').change ->
    gon.order_data['order_type'] = $(this).val()


  $('.authenticator__input').keyup (e) ->
    if e.which == 8
      $(this).prev().focus()

    form = $(this).closest('form')
    otp_attempt_element = $(form).find('#otp_attempt')
    $(otp_attempt_element).val ''
    $('.authenticator__input').each ->
      $(otp_attempt_element).val($(otp_attempt_element).val() + $(this).val())
      if !$.trim(this.value).length
        $(this).removeClass('with-value')
      else
        $(this).addClass('with-value')

  $('.authenticator__input').keyup (e) ->
    if e.which != 8 && !isNaN(String.fromCharCode(event.which))
      $(this).next().focus()

  $('.authenticator__input').bind "paste", (e) ->
    e.preventDefault()
    clipboard = e.originalEvent.clipboardData.getData('text')
    $('.authenticator__input').each (i) ->
      $(this).val(clipboard.charAt(i))
      $(this).focus()

  $('.authenticator__input').focus (event) ->
    $(this).select()

  $('#EnableOtpModal .js-openModal').click ->
    setTimeout ->
      $('.authenticator__input[name="otp_1"]').focus()
    , 350

  $('#view-current-otp').click ->
    $('#current-otp').addClass('show')

  $('#disable-otp-form').on "ajax:success", (e,data,status,xhr)  ->
    if data['success']
      window.location = '/settings/security'
    else
      $('#disable-otp-form #otp_not_match_tip').text(data['message']).removeClass('hide');

  $('#enable-otp-form').on "ajax:success", (e,data,status,xhr)  ->
    if data['success']
      $('#TurnOnFactorAuthModal').removeClass('is-active')
      $('#CheckOtpEmailModal').addClass('is-active')
      $('#login-without-otp').removeClass('is-loading').removeAttr('disabled')
    else
      $('#enable-otp-form #otp_not_match_tip').text(data['message']).removeClass('hide');

  $('#new_user').on "ajax:success", (e,data,status,xhr)  ->
    if typeof(data) == 'object' && !data['success']
      $('#otp_not_match_tip').text(data['message']).show()

  $('#password_update_form').bind 'submit', (e) ->
    if gon.otp_enabled && $('#PasswordUpdateFactorAuthModal').css("visibility") == "hidden"
      $('#password_update_form #otp_attempt').prop('disabled', true);
    return

  $('#password_update_form').on "ajax:success", (e,data,status,xhr)  ->
    if data['current_step'] == '2factor'
      if data['success']
        window.location = '/settings/security'
      else
        $('#PasswordUpdateFactorAuthModal #otp_not_match_tip').text(data['message']).removeClass('hide');
    else
      if data['success']
        $('#PasswordUpdateFactorAuthModal').addClass('is-active')
        $('#password_update_form #otp_attempt').prop('disabled', false);
      else
        $('#password_update_error_tip').text(data['message']).removeClass('hide');

  $('#new_user input').keypress (e) ->
    if e.keyCode == 13 && $('#LoginFactorAuthModal').css("visibility") == "hidden"
      e.preventDefault();
      $('#login-without-otp').click()

  $('#login-without-otp').click (e) ->
    $('#login-without-otp').addClass('is-loading').attr('disabled', 'disabled')
    if $('#new_user').valid()
      $.ajax
        url: '/user/check_login'
        type: 'POST'
        dataType: 'json'
        data: {email: $('#user_email').val(), password: $('#user_password').val()}
        success: (json) ->
          if json.success
            ga('send', { 'hitType': 'event', 'eventCategory': 'User', 'eventAction': 'Sign in' })
            if json.otp_enabled
              $('#check_login_alert').addClass('hide')
              $('#LoginFactorAuthModal').addClass('is-active')
              $('.authenticator__input[name="otp_1"]').focus()
            else
              window.location = json.redirect_url
          else
            $('#login-without-otp').removeClass('is-loading').removeAttr('disabled')
            $('#check_login_alert').text(json['message']).removeClass('hide')
        error: (XMLHttpRequest, textStatus, errorThrown) ->
          $('#login-without-otp').removeClass('is-loading').removeAttr('disabled')
          if errorThrown == "Unauthorized"
            $('#check_login_alert').text('You have to confirm your email address before continuing.').removeClass('hide')
    else
      $('#login-without-otp').removeClass('is-loading').removeAttr('disabled')