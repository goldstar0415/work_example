document.addEventListener("turbolinks:load", function() {
  function format(num, fix) {
    var p = num.toFixed(fix).split(".");
    return p[0].split("").reduceRight(function(acc, num, i, orig) {
        if ("-" === num && 0 === i) {
            return num + acc;
        }
        var pos = orig.length - i - 1
        return  num + (pos && !(pos % 3) ? "," : "") + acc;
    }, "") + (p[1] ? "." + p[1] : "");
  }

  $('.show-history').on('click', function() {
    $('body,html').animate({scrollTop: $('.g-section.section-transfers').offset().top }, 350);
  })

  // Landing rotate currency
  $('.flip-container').on('click', function() {
    $(this).toggleClass('hover');
  })
  // Modal
  $('.js-openModal').on('click', function() {
    $('body').addClass('modal-open');

    var modal = $(this).attr('data-open-modal');

    $(modal).addClass('is-active');
  });
  // Close modal ESC and click on background

  function closeModal() {
    $('body').removeClass('modal-open');
    $('.g-modal.is-active').removeClass('is-active');
  }

  $(document).keyup(function(e) {
    if (e.keyCode == 27) {
      closeModal();
    }
  });
  $(document).mouseup(function (e) {
    if($('.g-modal.is-active').length > 0) {
      var container = $(".g-modal .g-section");

      if (!container.is(e.target) && container.has(e.target).length === 0) {
        closeModal();
      }
    }
  });


  $('[data-modal="close"]').on('click', function() {
    $('body').removeClass('modal-open');
    $(this).parents('.g-modal').removeClass('is-active');
  })

  // Mobile burger
  $('.burger').on('click', function() {
    $('body').toggleClass('burger-open')
    $(this).toggleClass('is-active');
  });

  // Show/hide user settings
  $('.a-header__user,.bank-item__settings').on('click', function() {
    $(this).toggleClass('is-active');
  });

  // Switch
  if($('#landingpage').length > 0 || $('.order').length > 0) {
    $(".g-switch label").on('click', function() {
      if($(this).prev('input').is(':checked')) {
        return false;
      } else {
        $(this).parents('.g-switch').toggleClass("switched");
      }
    });
  }

  $(document).mouseup(function (e) {
    var container = $(".a-header__user,.g-chart-select,.bank-item__settings");

    if (!container.is(e.target)
        && container.has(e.target).length === 0)
    {
        container.removeClass('is-active is-opened');
    }
  });
  // Aside check if overflow: ellipsis
  if($('.g-aside').length > 0) {
    function checkAssetsValue() {
      $('.g-aside__values-value').each(function(index, elem) {
         while(elem.offsetWidth !== elem.scrollWidth) {
            $(this).css('font-size', Number($(this).css('font-size').slice(0,-2)) - 1 +'px')
         }
       });
    }

    checkAssetsValue();
  }
  // Summary
  if($('.summary__wrapper .summary__charts').length > 0) {

    function getDataForCharts(period, currency) {
      switch(period) {
        case 'm1':
          count = 12
          break
        case 'w1':
          count = 12
          break
        case 'd1':
          count = 30;
      }

      $.getJSON( "/api/v1/markets/" + gon.markets[currency] + "/candles?period="+period+"", function(data) {
        (currency == 'btc_eur') ? drawBtcBottom(data, count) : drawEthBottom(data, count)
      });
    }

    $(".g-switch label").on('click', function() {
      if($(this).prev('input').is(':checked')) {
          return false;
      } else {
          // reflow charts on switch, check select range
          getDataForCharts($('.g-chart-select__list-item.is-active').attr('data-chart'), $('.g-switch input:not(:checked)').data('currency'));
          $('.highcharts-block').toggleClass('show');
          $(this).parents('.g-switch').toggleClass("switched");
      }
      $('.highcharts-block').each(function() {
        $(this).highcharts().reflow();
      })
    });

    $('.g-chart-select__list-item').on('click', function() {
      var name = $(this).text();
      var dataChart = $(this).attr('data-chart');
      $('.g-chart-select__list-item').removeClass('is-active');
      $(this).addClass('is-active');

      getDataForCharts(dataChart, $('.g-switch input:checked').data('currency'));

      $(this).parents('.g-chart-select').toggleClass('is-opened');
      $(this).parents('.g-chart-select').find('.g-chart-select__selected').text(name);
    });
  }

  // Buy / sell
  if($('.order').length > 0) {
    $('.order .g-switch label').on('click', function() {
      var wrapper = $(this).parents('.order__wrapper');
      var status = $(this).text();
      wrapper.find('.order__sell-select b').text(status);
    });
    $('.order__currencies-row li').on('click', function() {
      var currency = $(this).find('p').attr('data-currency');
      var wrapper = $(this).parents('.order__wrapper');

      wrapper.find('.order__sell-title').attr('data-currency', currency);
      wrapper.find('.order__sell-select span').text(currency);

    });
  }

  $('.order__sell-open').on('click', function() {
    var wrapper = $(this).parents('.order__wrapper');
    $('.order__backdrop').toggleClass('is-active');
    wrapper.toggleClass('is-active');
    wrapper.find('.order__sell-expand').stop().slideToggle(350);
  });

  $('.order__backdrop').on('click', function() {
    $('.order__backdrop').removeClass('is-active');
    setTimeout(function() {
      $('.order__wrapper.is-active').removeClass('is-active');
      $('.order__sell-expand').slideUp(350);
    }, 150)
  });


  // Scripts for all pages with .lp-header
  if($('.lp-header').length > 0 || $('.summary__charts').length > 0) {

    // BTC - EUR CHART DATA
    $.getJSON( "/api/v1/markets/" + gon.markets['btc_eur'] + "/candles?period=d1", function(data) {
      if($('.lp-header').length > 0){
        navGraph(data);
      }
      getBTCData(data);
      drawBtcBottom(data);
    });

    // BTC - EUR CHART DATA
    if($('#landingpage').length || $('.summary__charts').length > 0) {
      $.getJSON( "/api/v1/markets/" + gon.markets['eth_eur'] + "/candles?period=d1", function(data) {
        getETHData(data);
        drawEthBottom(data);
      });
    }

    // Navigation graph
    function navGraph(data) {
      var array = [];
      var count = 30;

      for (var i = 0; i < count; i++) {
        array.push([data[i][0], data[i][1]])
      }

      Highcharts.chart('menu-graph-placeholder', {
        title: {text: null},
        exporting: {enabled: false},
        legend: {enabled: false},
        credits: {enabled: false},
        tooltip: {enabled: false},
        xAxis: {
          labels: {enabled: false},
          tickWidth: 0,
          lineWidth: 0
        },
        yAxis: {
          labels: {enabled: false},
          title: {text: null},
          gridLineColor: 'transparent',
        },
        chart: {
          borderColor: '#D9E5E8',
          backgroundColor: 'transparent',
          height: 50,
          width: 100
        },
        series: [{
          name: 'Manufacturing',
          color: '#D9E5E8',
          data: array.sort(),
          marker: {
            lineWidth: 0,
            radius: 0,
            states: {
              hover: {enabled: false}
            }
          }
        }]

      });
    }



    function priceAjax(currency) {
      // Get the last BTC-EUR price
      $.getJSON( "/api/v1/markets/" + gon.markets[currency + '_eur'] + "/price", function(data) {
        getPrices(data, currency);
      });
    }
    // Refresh every 10 seconds
    if (gon.env != 'development') {
      setInterval(priceAjax('btc'), 10000);
      setInterval(priceAjax('eth'), 10000);
    }
  }

  if($('#landingpage').length) {
    // Hotjar
    if (gon.env != 'development')
    {
      (function (h, o, t, j, a, r) {
        h.hj = h.hj || function () {
          (h.hj.q = h.hj.q || []).push(arguments)
        };
        h._hjSettings = {hjid: 586479, hjsv: 5};
        a = o.getElementsByTagName('head')[0];
        r = o.createElement('script');
        r.async = 1;
        r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
        a.appendChild(r);
      })(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');
    }

    var rellax = new Rellax('.rellax', {
      center: true
    });

    $(".switch-toggle label, .switch-button-wrapper label").click(function () {
      // Add the class to the switched element
      $(".switch-toggle").toggleClass("switched");
    });

    $('.header__nav li a[href*=\\#]:not([href=\\#])').on('click', function() {
      $('.burger').removeClass('is-active');
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        self = this;
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top - 50
          }, 1000, function() {
            window.location.hash = self.hash.slice(1);
          });
          return false;
        }
      }
    });
  }



  // CHARTS FUNCTIONS
  var ethGraphPlaceholder;
  var btcGraphPlaceholder;
  var line_color;

  function getPrices(data, currency) {
    if(currency == 'btc') {

      // Set the variables from the results array
      var price = data.price;
      var priceRounded = Number(price);

      // Insert to HTML
      $('.intro-bitcoin .currency-fluctuation-bit').html(format(priceRounded,2) + "<span>€</span>");
      $('.value-upper').text((format(priceRounded,2) + " €"));
      BTCChange(priceRounded);
    }

    // Get the latest ETH-EUR price
    if(currency == 'eth') {

      // Set the variables from the results array
      var price = data.price;
      var priceRounded = Number(price);

      // Insert to HTML
      $('.intro-ethereum .currency-fluctuation-eth').html(format(priceRounded,2) + "<span>€</span>");
      if($('.summary__charts').length > 0) {
        ETHChange(priceRounded)
      }
    }
  }

  function BTCChange(price) {
    var coinmartketAPI_bit = "/api/v1/markets/" + gon.markets['btc_eur'] + "/changes";
    var line_color_btc;

    $.getJSON(coinmartketAPI_bit, function (json) {
      // Set the variables from the results array
      var percent_change_24h = json[0].percent_change_24h;
      var price_change_24h = (price * (json[0].percent_change_24h / 100)).toFixed(2);
      var value_percent = $('.intro-bitcoin .value-percent');
      if(percent_change_24h < 0) {
        value_percent.parents('.intro-currency-wrap').addClass('red');
        line_color_btc = '#F4406B';
      } else {
        value_percent.parents('.intro-currency-wrap').removeClass('red');
        line_color_btc = '#19DAA2';
      }

      // Insert to HTML
      $('.value-lower').text(price_change_24h + " € (" + percent_change_24h + "%)").attr('style', 'color:' + line_color_btc);
      value_percent.text(price_change_24h + " € (" + percent_change_24h + "%)").attr('style', 'color:' + line_color_btc);

      if($('#btc-graph-placeholder').length > 0) {
        var series = btcGraphPlaceholder.series[0];
        series.graph.attr({
          stroke: line_color_btc
        });
      }

    });
  }
  function ETHChange(price) {
    var coinmartketAPI_bit = "/api/v1/markets/" + gon.markets['eth_eur'] + "/changes";
    var line_color_eth;

    // Return the deffered object from the ajax function and use the always, done or fail method
    return $.getJSON(coinmartketAPI_bit, function (json) {
      // Set the variables from the results array
      var percent_change_24h = json[0].percent_change_24h;
      var price_change_24h = (price * (json[0].percent_change_24h / 100)).toFixed(2);
      var value_percent = $('.intro-ethereum .value-percent');
      if(percent_change_24h < 0) {
        value_percent.parents('.intro-currency-wrap').addClass('red');
        line_color_eth = '#F4406B';
      } else {
        value_percent.parents('.intro-currency-wrap').removeClass('red');
        line_color_eth = '#19DAA2';
      }

      // Insert to HTML
      value_percent.text(price_change_24h + " € (" + percent_change_24h + "%)").attr('style', 'color:' + line_color_eth);

      var series = ethGraphPlaceholder.series[0];
      series.graph.attr({
        stroke: line_color_eth
      });
    });
  }

  function getETHData(data) {
    var array = [];
    var count = 30;

    for(var i = 0; i < count; i++) {
      array.push([data[i][0], data[i][1]])
    }

    ethGraphPlaceholder = Highcharts.chart('eth-graph-placeholder', {
      title: { text: null },
      exporting: { enabled: false },
      legend: { enabled: false },
      credits: { enabled: false },
      tooltip: { enabled: false },
      xAxis: {
        labels: { enabled: false },
        tickWidth: 0,
        lineWidth: 0
      },
      yAxis: {
        labels: { enabled: false },
        title: { text: null },
        gridLineColor: 'transparent',
      },
      chart: {
        borderColor: '#D9E5E8',
        backgroundColor: 'transparent',
        height: 60,
        marginTop: -25
      },
      series: [{
        name: 'Manufacturing',
        data: array.sort(),
        marker: {
          lineWidth: 0,
          radius: 0,
          states: {
            hover: { enabled: false }
          }
        }
      }]

    });

    Highcharts.chart('bg-chart-eth', {
      title: { text: null },
      exporting: { enabled: false },
      legend: { enabled: false },
      credits: { enabled: false },
      tooltip: { enabled: false },
      xAxis: {
        labels: { enabled: false },
        tickWidth: 0,
        lineWidth: 0
      },
      yAxis: {
        labels: { enabled: false },
        title: { text: null },
        gridLineColor: 'transparent',
      },
      chart: {
        borderColor: '#D9E5E8',
        backgroundColor: 'transparent',
        margin: 0,
        marginTop: -20
      },
      series: [{
        name: 'Manufacturing',
        color: '#F7F9FA',
        data: array.sort(),
        lineWidth: 4,
        marker: {
          lineWidth: 0,
          radius: 0,
          states: {
            hover: { enabled: false }
          }
        }
      }]

    });
    priceAjax('eth')
  }

  function getBTCData(data) {
    var array = [];
    var count = 30;

    for (var i = 0; i < count; i++) {
      array.push([data[i][0], data[i][1]])
    }
    if($('#btc-graph-placeholder').length > 0) {
      btcGraphPlaceholder = Highcharts.chart('btc-graph-placeholder', {
        title: {text: null},
        exporting: {enabled: false},
        legend: {enabled: false},
        credits: {enabled: false},
        tooltip: {enabled: false},
        xAxis: {
          labels: {enabled: false},
          tickWidth: 0,
          lineWidth: 0
        },
        yAxis: {
          labels: {enabled: false},
          title: {text: null},
          gridLineColor: 'transparent',
        },
        chart: {
          borderColor: '#D9E5E8',
          backgroundColor: 'transparent',
          height: 60,
          marginTop: -5
        },
        series: [{
          name: 'Manufacturing',
          data: array.sort(),
          marker: {
            lineWidth: 0,
            radius: 0,
            states: {
              hover: {enabled: false}
            }
          }
        }]

      });
    }
    if($('#bg-chart-btc').length > 0) {
      Highcharts.chart('bg-chart-btc', {
        title: {text: null},
        exporting: {enabled: false},
        legend: {enabled: false},
        credits: {enabled: false},
        tooltip: {enabled: false},
        xAxis: {
          labels: {enabled: false},
          tickWidth: 0,
          lineWidth: 0
        },
        yAxis: {
          labels: {enabled: false},
          title: {text: null},
          gridLineColor: 'transparent',
        },
        chart: {
          borderColor: '#D9E5E8',
          backgroundColor: 'transparent',
          margin: 0,
          marginTop: -20
        },
        series: [{
          name: 'Manufacturing',
          color: '#F7F9FA',
          data: array.sort(),
          lineWidth: 4,
          marker: {
            lineWidth: 0,
            radius: 0,
            states: {
              hover: {enabled: false}
            }
          }
        }]

      });
    }
  }


  // ================================================================================== //


  // Get the last BTC-EUR price for footer chart
  function drawBtcBottom(data, period) {
    var array = [];
    if(period == null || period == 'undefined') {
      count = 30;
    } else {
      count = period;
    }

    for (var i = 0; i < count; i++) {
      array.push([data[i][0], data[i][1]])
    }
    if($('#highcharts-placeholder').length > 0) {
      drawFooterGraph(array.sort(), "highcharts-placeholder");
    }
  }

  function drawEthBottom(data, period) {
    var array = [];
    if(period == null || period == 'undefined') {
      if(data.length < period) {
        count = data.length;
      } else {
        count = 30;
      }
    } else {
      if(data.length < period) {
        count = data.length;
      } else {
        count = period;
      }
    }

    for (var i = 0; i < count; i++) {
      array.push([data[i][0], data[i][1]])
    }
    if($('#highcharts-placeholder1').length > 0) {
      drawFooterGraph(array.sort(), "highcharts-placeholder1");
    }
  }


  var ySettings = {
    labels: {enabled: false},
    title: {text: null},
    gridLineColor: '#D9E5E8',
    gridLineDashStyle: 'shortDash',
    tickAmount: 10
  }

  function drawFooterGraph(data, element, period) {
    if($('.summary__wrapper .summary__charts').length > 0) {
     ySettings = {
        labels: {
          enabled: true,
          maxPadding: 0.1,
          showLastLabel: true,
          formatter: function () {
            return this.value + ' €';
          },
          style: {
            "fontSize": '13px',
            "color": "#8FA9B0"
          }
        },
        title: {text: null},
        gridLineColor: '#EDF4F8 ',
        gridLineDashStyle: 'longDash',
        tickAmount: 4
      }
    }
    Highcharts.chart(element, {
      title: {text: null},
      exporting: {enabled: false},
      legend: {enabled: false},
      credits: {enabled: false},

      tooltip: {
        useHTML: true,
        shadow: false,
        formatter: function () {

          // Convert unix timestamp to 01 January 2017 format
          var month = new Array();
          month[0] = "January";
          month[1] = "February";
          month[2] = "March";
          month[3] = "April";
          month[4] = "May";
          month[5] = "June";
          month[6] = "July";
          month[7] = "August";
          month[8] = "September";
          month[9] = "October";
          month[10] = "November";
          month[11] = "December";
          var d = new Date(this.x * 1000);
          var n = month[d.getMonth()];
          // Final date format used for tooltip
          var tooltipdate = ('0' + d.getDate()).slice(-2) + " " + n + " " + d.getFullYear();
          var result = '<div class="chart-popup">' +
            '<div class="chart-time">' + tooltipdate + '<div>' +
            '<div class="chart-price">' + format(this.y,2) + ' €</div>' +
            '</div>';
          if (this.point.index > 0) {
            var prevPoint = this.y - this.series.data[this.point.index - 1].y;
            var percentageChange = (100 - (this.series.data[this.point.index - 1].y * 100) / this.y).toFixed(2);
            (prevPoint >= 0) ? status = 'green' : status = 'red';
            result += '<div class="chart-fluct '+status+'">' + prevPoint.toFixed(2) + ' € ('+percentageChange+'%)</div>'
          }
          return result;
        },
        borderRadius: 20,
        backgroundColor: "#ffffff",
        borderWidth: 1
      },
      xAxis: {
        labels: {enabled: false},
        tickWidth: 0,
        lineWidth: 0,
      },
      yAxis: ySettings,
      chart: {
        borderColor: '#FFFFFF',
        backgroundColor: 'transparent' // Chart background color
      },

      series: [{
        name: 'Manufacturing',
        color: '#D9E5E8',
        data: data,
        marker: {
          fillColor: "#ffffff",
          lineWidth: 2,
          radius: 4,
          lineColor: "#D9E5E8",
          enabled: true,
          states: {
            hover: {
              lineColor: "#19DAA2",
              lineWidth: 2,
              radius: 8
            }
          }
        }
      }]

    });
    priceAjax('btc');
  }


});