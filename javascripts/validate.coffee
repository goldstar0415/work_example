document.addEventListener 'turbolinks:load', ->
  $('[id^=edit_user_], [id^=edit_address_], #new_address').each ->
    $(this).validate()

  # Sign up, Login
  $('#new_user').validate()

  # Settings password update
  $('#password_update_form').validate()

  # Onboarding
  $('#edit_user_2').validate()

  # Add Bank Account
  $('#new_bank_account').validate()