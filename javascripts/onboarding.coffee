document.addEventListener 'turbolinks:load', ->
  console.log('onboarding residency')

  $('#identity_file_type').change ->
    console.log gon.id_verification
    gon.id_verification['file_type'] = $(this).val()

  $("#id_verification_form").dropzone
    clickable: '#upload'
    success: (file, response) ->
      console.log 'success'
      console.log file
      console.log response
      $('#x').prop("disabled", false)
    error: (file, response) ->
      console.log 'error happen'
      return
    addedfile: (file) ->
      console.log 'file added'
      console.log file
      $('.dz-message').addClass 'hidden'
      $('#preview').attr 'src', window.URL.createObjectURL(file)
      return

  $("#residency_form").dropzone
    clickable: [
      '.dz-message'
      '#preview'
      '#upload'
    ]
    success: (file, response) ->
      console.log 'success'
      console.log file
      console.log response
      $('#x').prop("disabled", false)
    error: (file, response) ->
      console.log 'error happen'
      return
    addedfile: (file) ->
      console.log 'file added'
      console.log file
      $('.dz-message').addClass 'hidden'
      $('#preview').attr 'src', window.URL.createObjectURL(file)
      return


  $('#user_profession').val($('#user_profession').attr('value'));