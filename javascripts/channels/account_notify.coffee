document.addEventListener 'turbolinks:load', ->
  if $('#new_order').length
    App.account_notify = App.cable.subscriptions.create {channel: "AccountNotifyChannel"},
      received: (data) ->
        $('#aside').html(data['aside_funds'])
        gon.order_data.balances = data['balances']
        $('.order__summary-info-under .eth').text(gon.order_data.balances['eth'])
        $('.order__summary-info-under .btc').text(gon.order_data.balances['btc'])
        $('.order__summary-info-under .eur').text(gon.order_data.balances['eur'])
        $('#OrderSuccessModal').addClass('is-active')
        $('#submit_order').removeClass('is-loading').removeAttr('disabled')
        if $('.g-aside').length > 0
          $('.g-aside__values-value').each (index, elem) ->
            while elem.offsetWidth != elem.scrollWidth
              $(this).css 'font-size', Number($(this).css('font-size').slice(0, -2)) - 1 + 'px'
