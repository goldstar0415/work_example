document.addEventListener 'turbolinks:load', ->
  $('.select2').select2()
  $('#show_withdraw_histories').click ->
    $('#WithdrawSuccessModal').removeClass('is-active')
    document.location.hash = "#histories_outer_container"
  $('#new_transfer').on "ajax:success", (e,data,status,xhr)  ->
    if data['current_step'] == '2factor'
      if data['success']
        console.log data
        $('#WithdrawFactorAuthModal').removeClass('is-active')
        $('#WithdrawSuccessModal').addClass('is-active')
        $('#histories_container').html(data['histories'])
        $('#histories_outer_container').removeClass('hidden')
        console.log(data['histories'])
      else
        $('#WithdrawFactorAuthModal #otp_not_match_tip').text(data['message']).show()
    else
      if data['success']
        $('#WithdrawFactorAuthModal').addClass('is-active')
        $('input[name="user[otp_attempt]"]').prop('disabled', false)
        console.log('histories is')
        console.log(data['histories'])
        $('#histories_container').html(data['histories'])
        $('#histories_outer_container').removeClass('hidden')
      else
        console.log('update text')
        console.log $('.danger_notice button')
        console.log(data['message'])
        $('.g-notice').addClass('danger_notice').removeClass('hide')
        $('.g-notice span').text(data['message'])

  $.validator.addMethod 'btc', ((value, element) ->
    @optional(element) or /^0*(1?[0-9]|20)?[0-9]{0,6}(\.\d{1,8})?$/.test(value)
  ), 'Please specify a valid amount'

  console.log(gon.currency)
  $('#withdraw_submit').on 'click',  ->
    if gon.currency == "btc"
      $('#new_transfer').validate rules: 'transfer[amount]': btc: true
    else
      $('#new_transfer').validate rules: 'transfer[amount]': currency: true

  $('#new_transfer').bind 'submit', (e) ->
    console.log('in new transfer submit')
    console.log gon
    if gon.otp_enabled && !$('#WithdrawFactorAuthModal').is(':visible')
      $('input[name="user[otp_attempt]"]').prop('disabled', true);
      console.log('disabled')

    return