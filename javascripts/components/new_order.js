document.addEventListener('turbolinks:load', function() {
  if ($('#new_order').length) {
    gon.order_data = {
      left_volume: null,
      right_volume: null,
      price: null,
      order_type: 'buy',
      price_inited: false,
      left_currency: gon.init_market.left_currency,
      right_currency: gon.init_market.right_currency,
      markets: [],
      left_currency_options: [],
      right_currency_options: [],
      current_market: gon.init_market,
      // might have `by_total`
      price_type: 'by_volume'
    }
    gon.order_data = _.extend(gon.order_data, {currency_svgs: gon.currency_svgs}, {balances: gon.balances})

    new_order = new Vue({
      el: '#new_order',
      filters: {
        capitalize: function(v) {
          if (!v) return ''
          v = v.toString()
          return v.toUpperCase()
        },
        fullname: function (v) {
          hash = {
            tc: 'bitcoin',
            eth: 'ethereum',
            eur: 'euro'
          }
          return hash[v]
        }
      },
      data: gon.order_data,
      mounted: function() {
        var that = this
        $.ajax({
          url: "/api/v1/markets/init",
          success: function(json) {
            that.markets = json['markets']
            that.current_market = that.markets[0]
            that.left_currency_options = json['left_currency_options']
            that.right_currency_options = json['right_currency_options']
            that.left_currency = that.left_currency_options[0]
            that.right_currency = that.right_currency_options[0]
            if (gon.env != 'development') {
              setInterval(that.fetch_latest_price, 5000);
            }
          }
        });
      },
      watch: {
        order_type: function(value, old_value) {
          this.fetch_latest_price();
        },
        left_currency_code: function(value, old_value) {
          this.right_currency_code = _.first(this.right_currency_options)
          this.left_volume = 0
          $('#left_currency_in_modal').val()
        },
      },
      computed: {
        right_text: function () {
          return this.order_type === 'buy'? 'Pay with': 'Receive'
        },
      },
      methods: {
        set_market: function (right_currency) {
          this.right_currency = right_currency
          this.current_market = _.find(this.markets, function(market){ return market.right_currency_code == right_currency.code; });
          this.fetch_latest_price()
        },
        update_market: function (left_currency) {
          var that = this
          this.left_currency = left_currency
          $.ajax({
            url: "/api/v1/markets",
            type: "get",
            data: {
              left_currency_id: left_currency.id,
            },
            success: function(json) {
              that.markets = json['markets']
              that.current_market = that.markets[0]
              that.right_currency_options = json['right_currency_options']
              that.right_currency = that.right_currency_options[0]
              that.fetch_latest_price()
            }
          });

        },
        use_all: function () {
          if (this.order_type === 'buy') {
            this.right_volume = this.balances[this.right_currency_code];
            this.update_right_volume()
          }
          else {
            this.left_volume = this.balances[this.left_currency_code];
            this.update_left_volume()
          }
        },
        update_left_volume: function () {
          this.price_type = 'by_volume'
          this.fetch_latest_price()
          // _.debounce(this.fetch_latest_price, 100, true)
        },
        update_right_volume: function () {
          this.price_type = 'by_total'
          this.fetch_latest_price()
          // _.debounce(this.fetch_latest_price, 100, true)
        },
        fetch_latest_price: function () {
          that = this
          current_volume = (this.price_type === 'by_volume'? this.left_volume: this.right_volume)
          if (current_volume <= 0.0 || current_volume === null) {
            return
          }
          $.ajax({
            url: "/markets/" + gon.order_data.current_market.id.toString() + "/latest_price",
            type: "get",
            data: {
              order_type:  gon.order_data.order_type,
              price_type: gon.order_data.price_type,
              left_currency_id: that.left_currency.id,
              right_currency_id: that.left_currency.id,
              volume: current_volume,
              right_volume: that.right_volume
            },
            beforeSend: function() {
              if (!that.price_inited) {
                $('.order__btn').addClass('is-loading').attr('disabled', true)
              }
            },
            success: function(json) {
              if (!that.price_inited) {
                $('.order__btn').removeClass('is-loading').removeAttr('disabled')
              }
              if (json['success']) {
                that.price = json['price']

                if (that.price === null) {
                  //disable button, show warning
                  if (that.price_type == 'by_volume') {
                    that.right_volume = 0.0
                  }else {
                    that.left_volume = 0.0
                  }
                }
                else {
                  if (that.price_type == 'by_volume') {
                    that.right_volume = (that.left_volume * that.price).toFixed(that.current_market.precision);
                  }else {
                    that.left_volume = (that.right_volume / that.price).toFixed(that.current_market.precision);
                  }

                }
                that.price_inited = true;
              } else {
                $('.order__btn').removeClass('is-loading').removeAttr('disabled')
                console.log('error happen')
                console.log(json)
              }

            }
          });

        }
      }
    });
  }
});