document.addEventListener('turbolinks:load', function() {
  if ($('#id_verification').length) {
    console.log('hello id verification')
    gon.id_verification = {file_type: 'national_id', all_files: []}
    new Vue({
      el: '#id_verification',
      data: gon.id_verification,
      mounted: function() {
        var that = this
        $.ajax({
          url: "/user/id_verification_files",
          success: function(json) {
            if (json['success']) {
              console.log(json)
              that.file_type = json['file_type']
              that.all_files = json['all_files']
            } else {
            }
          }
        });
      },
      watch: {

      },
      computed: {
        files: function () {
          var that = this
          return _.filter(this.all_files, function (record) {
            return record['file_type'] == that.file_type
          })
        }
      },
      methods: {
      }
    });
  }
});