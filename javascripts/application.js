// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//

// BEGIN VENDOR JS FOR PAGES (plus add in jquery.turbolinks)
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require underscore
//= require clipboard
//= require select2
//= require jquery.validate
//= require dropzone

// BEGIN SITE SCRIPTS

//= require_tree ./components
//= require_directory .
//= require_tree ./cable
//= require_tree ./channels
//= require_tree ./lib
//= require turbolinks
//= require rails.validations
//= require rails.validations.simple_form
