App.notifications = App.cable.subscriptions.create "NotificationChannel",
  received: (data) ->
    $('#notifications').prepend(data['notification'])

    do ->
      timeAll = 5000
      time = 5000
      delta = 110
      tid = undefined
      tid = setInterval((->
        changeWidth = ((100 * (timeAll - time)) / timeAll);
        if window.blurred
          $('[interval_id="'+interval_id+'"]').find('.g-single-notification__progress').each ->
            $(this).stop().animate({'width': changeWidth + '%'}, 100, 'linear')
          return
        time -= delta
        $('[interval_id="'+interval_id+'"]').find('.g-single-notification__progress').each ->
          $(this).stop().animate({'width': changeWidth + '%'}, 100, 'linear')
        if time <= 0
          clearInterval tid
          clearInterval interval_id
          $('.g-single-notification').last().addClass('hide')
          setTimeout (->
            $('.g-single-notification.hide').remove()
          ), 250
          document.title = $('title').attr('origin')
      ), delta)

    window.onblur = ->
      window.blurred = true

    window.onfocus = ->
      window.blurred = false

    interval_id = setInterval "flashTitle('CoinFalcon', '" + data['title'] + "')", 2000
    $('#notifications .g-single-notification:first').attr('interval_id', interval_id)
    console.log(data['histories']);
    $('#histories_container').html(data['histories'])
    $('#histories_outer_container').removeClass('hidden')
    $('#aside').html(data['aside_funds'])

    $('.g-single-notification .g-modal-close').click ->
      id = $(this).parents('.g-single-notification').attr('internal_id')
      clearInterval id
      $(this).parents('.g-single-notification').addClass('hide')
      setTimeout (->
        $('.g-single-notification.hide').remove()
      ), 250