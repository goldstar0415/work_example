document.addEventListener 'turbolinks:load', ->
  $.validator.addMethod 'currency', ((value, element) ->
    @optional(element) or /^((\d{1,3}(\.(\d){3})*)|\d*)(,\d{1,2})?\s*$/.test(value)
  ), 'Please specify a valid amount'

  $('#new_bank_account').on 'submit', ->
    $('#add_bank_account_button').addClass('is-loading').attr('disabled', 'disabled')

  $('#new_bank_account').on "ajax:success", (e,data,status,xhr)  ->
    $('#add_bank_account_button').removeClass('is-loading').removeAttr('disabled')